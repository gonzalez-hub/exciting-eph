#!/bin/bash
                                                                                     
#SBATCH --job-name=test
#SBATCH --partition=cpu16memory128
#SBATCH --ntasks=1                                    
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1  
#SBATCH --cpus-per-task=16
#SBATCH --ntasks-per-core=1
#SBATCH --time=200:00:00
                
. /app/intel-oneapi-2021/setvars.sh --force intel6
export KMP_AFFINITY=scatter
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
mpirun -np $SLURM_NTASKS -bootstrap slurm /users/sol/gonzalez/exciting_eph_2/bin/excitingser
#mpirun -bootstrap slurm /users/sol/gonzalez/exciting/bin/exciting_mpismp
#mpirun -np 9 -bootstrap slurm /users/sol/gonzalez/q-e-qe-6.8/D3Q/bin/pw.x -npool 9 < mono-scf.in > mono-scf.out 
#mpirun -bootstrap slurm /users/sol/gonzalez/q-e-qe-6.8/D3Q/bin/ph.x < mono-ph.in > mono-ph.out 
#mpirun -np 9 -bootstrap slurm /users/sol/gonzalez/q-e-qe-6.8/D3Q/bin/pw.x -npool 9 < mono-nscf.in > mono-nscf.out 
#mpirun -np 9 -bootstrap slurm /users/sol/gonzalez/q-e-qe-6.8/D3Q/bin/epw.x -npool 9 < mono-epw.in > mono-epw.out 
