module mod_rmat
  use modinput
  use mod_atoms
  use mod_APW_LO
  use mod_muffin_tin
  use mod_constants
  use mod_eigensystem, only : idxlo, nmatmax_ptr, oalo, ololo
  use mod_lattice, only : ainv
  use mod_Gvector, only : ngvec, gc, cfunir, ivg, ivgig, ngrtot, ngrid, intgv, igfft
  use mod_Gkvector, only : gkmax, ngkmax
  use mod_kpointset
  use m_plotmat

  implicit none
  private

  integer :: nlammax, nlmlammax, r_lmaxapw
  type( k_set) :: r_kset, r_kset0

  integer, allocatable :: nlam(:,:), nlmlam(:), lam2apwlo(:,:,:), idxlmlam(:,:,:), lm2l(:)
  complex(8), allocatable :: r_rintgnt(:,:,:,:), r_cfunr(:,:,:)

  public :: rmat_init, rmat_genrmat, rmat_destroy

  contains

    subroutine rmat_destroy
      if( allocated( nlam))      deallocate( nlam)
      if( allocated( nlmlam))    deallocate( nlmlam)
      if( allocated( lam2apwlo)) deallocate( lam2apwlo)
      if( allocated( idxlmlam))  deallocate( idxlmlam)
      if( allocated( lm2l))      deallocate( lm2l)
      if( allocated( r_rintgnt)) deallocate( r_rintgnt)
      if( allocated( r_cfunr))   deallocate( r_cfunr)
      call delete_k_vectors( r_kset)
      call delete_k_vectors( r_kset0)

      return
    end subroutine rmat_destroy

    subroutine rmat_init( lmaxapw_, kset, singlek)
      integer, intent( in)           :: lmaxapw_
      type( k_set), intent( in)      :: kset
      logical, optional, intent( in) :: singlek

      integer :: l1, l2, m1, m2, lm1, lm2, o1, o2, ilo1, ilo2, is, ia, ias, lam1, lam2, ir, nr, idx1, idx2
      integer :: i1, i2, i3
      integer :: d, ig, ik, g(3)
      real(8) :: t1, dotp, rl(3), rc(3), atl(3)
      logical :: single
      character( 64) :: fname

      complex(8), external :: gauntyry

      real(8), allocatable :: rint(:,:), fr(:), gf(:), cf(:,:)
      
      single = .false.
      if( present( singlek)) single = singlek

      r_lmaxapw = min( lmaxapw_, input%groundstate%lmaxapw)

      r_kset = kset
      call generate_k_vectors( r_kset0, r_kset%bvec, r_kset%ngridk, (/0.d0, 0.d0, 0.d0/), .false., .false.)

      write(*,*) 'muffin-tin'

      ! count combined (l,m,o) indices and build index maps
      allocate( nlam( 0:r_lmaxapw, nspecies))
      allocate( lam2apwlo( apwordmax+nlomax, 0:r_lmaxapw, nspecies))
      nlammax = 0
      do is = 1, nspecies
        do l1 = 0, r_lmaxapw
          nlam( l1, is) = 0
          lam2apwlo( :, l1, is) = 0
          do o1 = 1, apword( l1, is)
            nlam( l1, is) = nlam( l1, is)+1
            lam2apwlo( nlam( l1, is), l1, is) = o1
          end do
          do ilo1 = 1, nlorb( is)
            if( lorbl( ilo1, is) .eq. l1) then
              nlam( l1, is) = nlam( l1, is) + 1
              lam2apwlo( nlam( l1, is), l1, is) = -ilo1
            end if
          end do
          nlammax = max( nlammax, nlam( l1, is))
        end do
      end do

      allocate( lm2l( (r_lmaxapw + 1)**2))
      allocate( nlmlam( nspecies))
      allocate( idxlmlam( (r_lmaxapw + 1)**2, nlammax, nspecies)) 
      idxlmlam(:,:,:) = 0
      nlmlammax = 0
      nlmlam(:) = 0
      do l1 = 0, r_lmaxapw
        do m1 = -l1, l1
          lm1 = idxlm( l1, m1)
          lm2l( lm1) = l1
          do is = 1, nspecies
            do lam1 = 1, nlam( l1, is)
              nlmlam( is) = nlmlam( is) + 1
              idxlmlam( lm1, lam1, is) = nlmlam( is)
            end do
          end do
        end do
      end do
      nlmlammax = maxval( nlmlam, 1)

      ! build radial integrals times Gaunt coefficients
      if( allocated( r_rintgnt)) deallocate( r_rintgnt)
      allocate( r_rintgnt( nlmlammax, nlmlammax, 3, natmtot))
      allocate( rint( nlammax, nlammax))
      allocate( fr( nrmtmax), gf( nrmtmax), cf( 3, nrmtmax))

      r_rintgnt = zzero
      t1 = sqrt( fourpi/3.d0)

      do is = 1, nspecies
        nr = nrmt( is)
        do ia = 1, natoms( is)
          ias = idxas( ia, is)

          !call r3mv( ainv, atposc( :, ia, is), atl)
          atl = atposc( :, ia, is)
          write(*,'(2i,3f13.6)') ia, is, atl
          
          do l1 = 0, r_lmaxapw
            do l2 = 0, r_lmaxapw
              
              rint = 0.d0
              do lam1 = 1, nlam( l1, is)
                do lam2 = 1, nlam( l2, is)

                  if( lam2apwlo( lam1, l1, is) .gt. 0) then
                    o1 = lam2apwlo( lam1, l1, is)
                    ! APW-APW
                    if( lam2apwlo( lam2, l2, is) .gt. 0) then
                      o2 = lam2apwlo( lam2, l2, is)
                      do ir = 1, nr
                        fr( ir) = apwfr( ir, 1, o1, l1, ias)*apwfr( ir, 1, o2, l2, ias)*spr( ir, is)**3
                      end do
                      call fderiv( -1, nr, spr( :, is), fr, gf, cf)
                      rint( lam1, lam2) = gf( nr)
                    ! APW-LO
                    else if( lam2apwlo( lam2, l2, is) .lt. 0) then
                      ilo2 = -lam2apwlo( lam2, l2, is)
                      do ir = 1, nr
                        fr( ir) = apwfr( ir, 1, o1, l1, ias)*lofr( ir, 1, ilo2, ias)*spr( ir, is)**3
                      end do
                      call fderiv( -1, nr, spr( :, is), fr, gf, cf)
                      rint( lam1, lam2) = gf( nr)
                    end if
                  else if( lam2apwlo( lam1, l1, is) .lt. 0) then
                    ilo1 = -lam2apwlo( lam1, l1, is)
                    ! LO-APW
                    if( lam2apwlo( lam2, l2, is) .gt. 0) then
                      o2 = lam2apwlo( lam2, l2, is)
                      do ir = 1, nr
                        fr( ir) = lofr( ir, 1, ilo1, ias)*apwfr( ir, 1, o2, l2, ias)*spr( ir, is)**3
                      end do
                      call fderiv( -1, nr, spr( :, is), fr, gf, cf)
                      rint( lam1, lam2) = gf( nr)
                    ! LO-LO
                    else if( lam2apwlo( lam2, l2, is) .lt. 0) then
                      ilo2 = -lam2apwlo( lam2, l2, is)
                      do ir = 1, nr
                        fr( ir) = lofr( ir, 1, ilo1, ias)*lofr( ir, 1, ilo2, ias)*spr( ir, is)**3
                      end do
                      call fderiv( -1, nr, spr( :, is), fr, gf, cf)
                      rint( lam1, lam2) = gf( nr)
                    end if
                  end if

                end do
              end do

              do m1 = -l1, l1
                lm1 = idxlm( l1, m1)
                do m2 = -l2, l2
                  lm2 = idxlm( l2, m2)

                  do lam1 = 1, nlam( l1, is)
                    do lam2 = 1, nlam( l2, is)

                      idx1 = idxlmlam( lm1, lam1, is)
                      idx2 = idxlmlam( lm2, lam2, is)

                      r_rintgnt( idx1, idx2, 1, ias) = -cmplx( t1*rint( lam1, lam2), 0.d0, 8)*gauntyry( l1, 1, l2, m1,  1, m2) 
                      r_rintgnt( idx1, idx2, 2, ias) =  cmplx( t1*rint( lam1, lam2), 0.d0, 8)*gauntyry( l1, 1, l2, m1, -1, m2) 
                      r_rintgnt( idx1, idx2, 3, ias) = -cmplx( t1*rint( lam1, lam2), 0.d0, 8)*gauntyry( l1, 1, l2, m1,  0, m2) 
 
                      ! add atomic contribution
                      if( lm1 .eq. lm2) then
                        if( lam2apwlo( lam1, l1, is) .gt. 0) then
                          o1 = lam2apwlo( lam1, l1, is)
                          ! APW-APW
                          if( lam2apwlo( lam2, l2, is) .gt. 0) then
                            o2 = lam2apwlo( lam2, l2, is)
                            if( o1 .eq. o2) r_rintgnt( idx1, idx2, :, ias) = r_rintgnt( idx1, idx2, :, ias) + cmplx( atl, 0.d0, 8)
                          ! APW-LO
                          else if( lam2apwlo( lam2, l2, is) .lt. 0) then
                            ilo2 = -lam2apwlo( lam2, l2, is)
                            r_rintgnt( idx1, idx2, :, ias) = r_rintgnt( idx1, idx2, :, ias) + cmplx( atl*oalo( o1, ilo2, ias), 0.d0, 8)
                          end if
                        else if( lam2apwlo( lam1, l1, is) .lt. 0) then
                          ilo1 = -lam2apwlo( lam1, l1, is)
                          ! LO-APW
                          if( lam2apwlo( lam2, l2, is) .gt. 0) then
                            o2 = lam2apwlo( lam2, l2, is)
                            r_rintgnt( idx1, idx2, :, ias) = r_rintgnt( idx1, idx2, :, ias) + cmplx( atl*oalo( o2, ilo1, ias), 0.d0, 8)
                          ! LO-LO
                          else if( lam2apwlo( lam2, l2, is) .lt. 0) then
                            ilo2 = -lam2apwlo( lam2, l2, is)
                            r_rintgnt( idx1, idx2, :, ias) = r_rintgnt( idx1, idx2, :, ias) + cmplx( atl*ololo( ilo1, ilo2, ias), 0.d0, 8)
                          end if
                        end if
                      end if

                    end do
                  end do

                end do
              end do
   
            end do
          end do

          do d = 1, 3
            write( fname, '("RINTGNT_",i1.1,"_",i2.2)') d, ias
            call writematlab( r_rintgnt( :, :, d, ias), fname)
          end do

        end do
      end do

      deallocate( rint, fr, gf, cf)          

      write(*,*) 'interstitial'

      ! build the Fourier transform for the interstitial region
      if( allocated( r_cfunr)) deallocate( r_cfunr)
      if( single) then
        allocate( r_cfunr( ngrtot, 3, 1))
        r_cfunr = zzero
        do ig = 1, ngrtot
          rl = dble( ivg( :, ig))/dble( ngrid)
          call r3mv( input%structure%crystal%basevect, rl, rc)
          do d = 1, 3
            r_cfunr( igfft( ig), d, 1) = r_cfunr( igfft( ig), d, 1)*cmplx( rc( d), 0.d0, 8)
          end do
        end do
        do d = 1, 3
          call zfftifc( 3, ngrid, -1, r_cfunr( :, d, 1))
        end do
      else
        allocate( r_cfunr( ngrtot, 3, r_kset0%nkpt))
        r_cfunr = zzero
        do ik = 1, r_kset0%nkpt
          !do ig = 1, ngrtot
          !  rl = dble( ivg( :, ig))/dble( ngrid)
          !  call r3frac( input%structure%epslat, rl, g)
          !  call r3mv( input%structure%crystal%basevect, rl, rc)
          !  dotp = -dot_product( rc, r_kset0%vkc( :, ik))
          !  !if( ik .eq. 1) write(*,'(2i,3f13.6,f23.16)') ig, igfft( ig), r, dble( r_cfunr( igfft( ig), 1, ik))
          !  do d = 1, 3
          !    r_cfunr( igfft( ig), d, ik) = r_cfunr( igfft( ig), d, ik)*cmplx( rc( d), 0.d0, 8)*cmplx( cos( dotp), sin( dotp), 8)
          !  end do
          !end do
          ig = 0
          do i3 = 0, ngrid(3)-1
            do i2 = 0, ngrid(2)-1
              do i1 = 0, ngrid(1)-1
                ig = ig + 1
                rl = (dble( (/i1, i2, i3/)) + (/0.5d0, 0.5d0, 0.5d0/))/dble( ngrid)
                call r3mv( input%structure%crystal%basevect, rl, rc)
                dotp = -dot_product( rc, r_kset0%vkc( :, ik))
                do d = 1, 3
                  r_cfunr( ig, d, ik) = cmplx( cfunir( ig)*rc( d), 0.d0, 8)*cmplx( cos( dotp), sin( dotp), 8)
                end do
              end do
            end do
          end do
          do d = 1, 3
            call zfftifc( 3, ngrid, -1, r_cfunr( :, d, ik))
          end do
        end do
      end if

      ik = 1
      write( fname, '("CFUNR_",i3.3)') ik
      call writematlab( r_cfunr( :, :, ik), fname)

      !do l1 = 0, r_lmaxapw
      !  do m1 = -l1, l1
      !    do l2 = 0, r_lmaxapw
      !      do m2 = -l2, l2
      !        write(*,'(4i,3(2f13.6,"|"))') l1, m1, l2, m2, gauntyry( l1, 1, l2, m1, -1, m2), gauntyry( l1, 1, l2, m1, 0, m2), gauntyry( l1, 1, l2, m1, 1, m2)
      !      end do
      !    end do
      !  end do
      !end do

      return
    end subroutine rmat_init

    subroutine rmat_genrmat( ik1, ik2, fst1, lst1, fst2, lst2, evec1, evec2, rmat)
      integer, intent( in)     :: ik1, ik2, fst1, lst1, fst2, lst2
      complex(8), intent( inout)  :: evec1( nmatmax_ptr, fst1:lst1), evec2( nmatmax_ptr, fst2:lst2)
      complex(8), intent( out) :: rmat( fst1:lst1, fst2:lst2, 3)

      integer :: d, nst1, nst2, g(3), gt(3), ik, ig, ig1, ig2, igk1, igk2, ngk1, ngk2, is, ia, ias, lm, lam, o, ilo, l
      real(8) :: k(3)

      integer, allocatable :: igkig1(:), igkig2(:) 
      real(8), allocatable :: vecgkql(:,:), vecgkqc(:,:), gkqc(:), tpgkqc(:,:)
      complex(8), allocatable :: sfacgkq(:,:), apwalm(:,:,:,:)
      complex(8), allocatable :: auxmat(:,:), auxvec(:), evecshort1(:,:), evecshort2(:,:,:), cfunmat(:,:,:), mtolp(:,:)

      nst1 = lst1 - fst1 + 1
      nst2 = lst2 - fst2 + 1
      rmat = zzero

      k = r_kset%vkl( :, ik1) - r_kset%vkl( :, ik2)
      call r3frac( input%structure%epslat, k, g)
      call findkptinset( k, r_kset0, ig, ik)

      !--------------------------------------!
      !      muffin-tin matrix elements      !
      !--------------------------------------!

      allocate( igkig1( ngkmax), igkig2( ngkmax), vecgkql( 3, ngkmax), vecgkqc( 3, ngkmax), gkqc( ngkmax), tpgkqc( 2, ngkmax))
      allocate( sfacgkq( ngkmax, natmtot))
      allocate( apwalm( ngkmax, apwordmax, lmmaxapw, natmtot))
      allocate( evecshort2( fst2:lst2, nlmlammax, natmtot))
      allocate( auxvec( max( nst1, nst2)))

      ! generate the G+k2-vectors
      call gengpvec( r_kset%vkl( :, ik2), r_kset%vkc( :, ik2), ngk2, igkig2, vecgkql, vecgkqc, gkqc, tpgkqc)
      ! generate the structure factors
      call gensfacgp( ngk2, vecgkqc, ngkmax, sfacgkq)
      ! find matching coefficients for k-point k+q
      call match( ngk2, gkqc, tpgkqc, sfacgkq, apwalm)

      do is = 1, nspecies
        do ia = 1, natoms( is)
          ias = idxas( ia, is)
          evecshort2( :, :, ias) = zzero
          do lm = 1, (r_lmaxapw + 1)**2
            l = lm2l( lm)
            do lam = 1, nlam( l, is)
              ! lam belongs to apw
              if( lam2apwlo( lam, l, is) .gt. 0) then
                o = lam2apwlo( lam, l, is)
                call zgemv( 't', ngk2, nst2, zone, &
                       evec2, nmatmax_ptr, &
                       apwalm( :, o, lm, ias), 1, zzero, &
                       auxvec, 1)
                evecshort2( :, idxlmlam( lm, lam, is), ias) = auxvec( 1:nst2)
              end if
              ! lam belongs to lo
              if( lam2apwlo( lam, l, is) .lt. 0) then
                ilo = -lam2apwlo( lam, l, is)
                evecshort2( :, idxlmlam( lm, lam, is), ias) = evec2( ngk2+idxlo( lm, ilo, ias), :)
              end if
            end do
          end do
        end do
      end do

      ! generate the G+k1-vectors
      call gengpvec( r_kset%vkl( :, ik1), r_kset%vkc( :, ik1), ngk1, igkig1, vecgkql, vecgkqc, gkqc, tpgkqc)
      ! generate the structure factors
      call gensfacgp( ngk1, vecgkqc, ngkmax, sfacgkq)
      ! find matching coefficients for k-point k
      call match( ngk1, gkqc, tpgkqc, sfacgkq, apwalm)
      
      allocate( evecshort1( fst1:lst1, nlmlammax))
      allocate( auxmat( fst1:lst1, nlmlammax))
      
      do is = 1, nspecies
        do ia = 1, natoms( is)
          ias = idxas( ia, is)

          evecshort1(:,:) = zzero
          do lm = 1, (r_lmaxapw + 1)**2
            l = lm2l( lm)
            do lam = 1, nlam( l, is)
              ! lam belongs to apw
              if( lam2apwlo( lam, l, is) .gt. 0) then
                o = lam2apwlo( lam, l, is)
                call zgemv( 't', ngk1, nst1, zone, &
                       evec1, nmatmax_ptr, &
                       apwalm( :, o, lm, ias), 1, zzero, &
                       auxvec, 1)
                evecshort1( :, idxlmlam( lm, lam, is)) = conjg( auxvec( 1:nst1))
              end if
              ! lam belongs to lo
              if( lam2apwlo( lam, l, is) .lt. 0) then
                ilo = -lam2apwlo( lam, l, is)
                evecshort1( :, idxlmlam( lm, lam, is)) = conjg( evec1( ngk1+idxlo( lm, ilo, ias), :))
              end if
            end do
          end do

          do d = 1, 3
            call zgemm( 'n', 'n', nst1, nlmlam( is), nlmlam( is), zone, &
                   evecshort1, nst1, &
                   r_rintgnt( :, :, d, ias), nlmlammax, zzero, &
                   auxmat, nst1)
            call zgemm( 'n', 't', nst1, nst2, nlmlam( is), zone, &
                   auxmat, nst1, &
                   evecshort2( :, :, ias), nst2, zone, &
                   rmat( :, :, d), nst1)
          end do
        end do
      end do

      deallocate( vecgkql, vecgkqc, gkqc, tpgkqc, sfacgkq, apwalm) 
      deallocate( evecshort1, evecshort2, auxmat, auxvec)
          
      !--------------------------------------!
      !     interstitial matrix elements     !
      !--------------------------------------!

      allocate( cfunmat( ngk1, ngk2, 3))
      allocate( auxmat( fst1:lst1, ngk2))
      cfunmat = zzero
      do igk1 = 1, ngk1
        ig1 = igkig1( igk1)
        do igk2 = 1, ngk2
          ig2 = igkig2( igk2)
          gt = ivg( :, ig1) - ivg( :, ig2) + g
          if( (gt(1) .ge. intgv(1,1)) .and. (gt(1) .le. intgv(1,2)) .and. &
              (gt(2) .ge. intgv(2,1)) .and. (gt(2) .le. intgv(2,2)) .and. &
              (gt(3) .ge. intgv(3,1)) .and. (gt(3) .le. intgv(3,2))) then
            ig = ivgig( gt(1), gt(2), gt(3))
            cfunmat( igk1, igk2, :) = r_cfunr( igfft( ig), :, ik)
          end if
        end do
      end do

      do d = 1, 3
        call zgemm( 'c', 'n', nst1, ngk2, ngk1, zone, &
               evec1, nmatmax_ptr, &
               cfunmat( :, :, d), ngk1, zzero, &
               auxmat, nst1)
        call zgemm( 'n', 'n', nst1, nst2, ngk2, zone, &
               auxmat, nst1, &
               evec2, nmatmax_ptr, zone, &
               rmat( :, :, d), nst1)
      end do

      deallocate( auxmat, cfunmat, igkig1, igkig2)
 
      return
    end subroutine rmat_genrmat

end module mod_rmat
