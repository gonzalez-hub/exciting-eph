<xs:schema xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:ex="http://xml.exciting-code.org/inputschemaextentions.xsd"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xsi:schemaLocation="http://xml.exciting-code.org/inputschemaextentions.xsd    http://xml.exciting-code.org/inputschemaextentions.xsd">
    <xs:include id="common" schemaLocation="common.xsd"/>
    <xs:annotation>
        <xs:appinfo>
            <parent>/input</parent>
            <root>eph</root>
        </xs:appinfo>
    </xs:annotation>
    <xs:element ex:importance="expert" ex:unit="" name="eph">
        <xs:annotation>
            <xs:documentation>Electron-phonon calculation setup.</xs:documentation>
        </xs:annotation>
        <xs:complexType>
          <xs:all>

            <xs:element ex:importance="essential" ex:unit="" ref="freqgrid" maxOccurs="1" minOccurs="0"/>
 
            <xs:element ex:importance="expert" ex:unit="" name="selfenergyeph" maxOccurs="1" minOccurs="0">
              <xs:annotation>
                <xs:documentation>
                   Options related to the calculations of the correlation self-energy.
                </xs:documentation>
              </xs:annotation>
              <xs:complexType>

                <xs:all>
                  <xs:element ex:importance="expert" ex:unit="" name="SpectralFunctionPloteph" maxOccurs="1" minOccurs="0">
                    <xs:annotation>
                    <xs:documentation>
                    Parameters for the self-energy and spectral function visualization.
                    </xs:documentation>
                    </xs:annotation>
                    <xs:complexType>
                      <xs:attribute ex:importance="expert" type="xs:string" ex:unit="" name="axis" default="real"> 
                        <xs:annotation><xs:documentation>
                        Real ("real") or imaginary ("imag") frequency axis for visialization.
                        </xs:documentation></xs:annotation>
                      </xs:attribute>
                      <xs:attribute ex:importance="expert" type="xs:integer" ex:unit="" name="nwgrid" default="1001"> 
                        <xs:annotation><xs:documentation>
                        Number of grid points within the visualization interval.
                        </xs:documentation></xs:annotation>
                      </xs:attribute>
                      <xs:attribute ex:importance="expert" type="fortrandouble" ex:unit="Hartree" name="wmin" default="-10"> 
                        <xs:annotation><xs:documentation>
                        Lower bound for the visualization frequency interval.
                        </xs:documentation></xs:annotation>
                      </xs:attribute>
                      <xs:attribute ex:importance="expert" type="fortrandouble" ex:unit="Hartree" name="wmax" default="10"> 
                        <xs:annotation><xs:documentation>
                        Upper bound for the visualization frequency interval.
                        </xs:documentation></xs:annotation>
                      </xs:attribute>
                      <xs:attribute ex:importance="expert" type="fortrandouble" ex:unit="Hartree" name="eta" default="1.0d-4"> 
                        <xs:annotation><xs:documentation>
                        Smearing parameter (small number).
                        </xs:documentation></xs:annotation>
                      </xs:attribute>
                    </xs:complexType>
                  </xs:element>
                </xs:all>

              </xs:complexType>
            </xs:element>

            <xs:element ex:importance="essential" ex:unit="" name="target" maxOccurs="1" minOccurs="1">
              <xs:annotation>
                <xs:documentation>Target k-point set.</xs:documentation>
              </xs:annotation>
              <xs:complexType>

                <xs:attribute ex:importance="essential" ex:unit="" name="ngridp" type="integertriple" default="0 0 0">
                  <xs:annotation>
                    <xs:documentation>
                      p-point grid size to be used in e-ph calculations. If not specified, the electronic k-grid is used.
                      If a target path is specified this is ignored.
                    </xs:documentation>
                  </xs:annotation>
                </xs:attribute>

                <xs:attribute ex:importance="expert" ex:unit="" name="vploff" type="vect3d" default="0.0d0 0.0d0 0.0d0">
                  <xs:annotation>
                    <xs:documentation>
                      Offset of the p-point grid.
                    </xs:documentation>
                  </xs:annotation>
                </xs:attribute>

                <xs:attribute ex:importance="expert" ex:unit="" name="reducep" type="xs:boolean" default="true">
                  <xs:annotation>
                    <xs:documentation>
	               Flag to specify whether the p-point grid is reduced using symmetry operations.
	            </xs:documentation>
                  </xs:annotation>
                </xs:attribute>
          
                <xs:annotation>
                  <xs:documentation>
                    Vertices specifying a path in the BZ.
                  </xs:documentation>
                </xs:annotation>
                <xs:all>
                  <xs:element ex:importance="expert" ex:unit="" ref="plot1d"/>
                </xs:all>
              </xs:complexType>
            </xs:element>

            <xs:element ex:importance="essential" ex:unit="" name="eliashbergfun" maxOccurs="1" minOccurs="1">
              <xs:annotation>
                <xs:documentation>Parameters for the calculation of the Eliashberg function.</xs:documentation>
              </xs:annotation>
              <xs:complexType>
                <xs:attribute ex:importance="expert" ex:unit="" name="nefreq" type="xs:integer" default="500">
                  <xs:annotation>
                    <xs:documentation>Number of electron energies for which the Eliashberg function is evaluated.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
                <xs:attribute ex:importance="expert" ex:unit="" name="npfreq" type="xs:integer" default="500">
                  <xs:annotation>
                    <xs:documentation>Number of phonon frequencies for which the Eliashberg function is evaluated.</xs:documentation>
                  </xs:annotation>
                </xs:attribute>
              </xs:complexType>
            </xs:element>

          </xs:all>

          <xs:attribute ex:importance="expert" ex:unit="" name="tasknameeph" type="xs:string" use="optional" default="eph">
            <xs:annotation>
              <xs:documentation>
                  Type of calculations. Available tasks:
                  <list>
                    <li>eph    -  eph calculations</li>
                  </list>

  	          </xs:documentation>
            </xs:annotation>
          </xs:attribute>
         
          <xs:attribute ex:importance="expert" ex:unit="" name="nemptyeph" type="xs:integer" use="optional" default="0">
            <xs:annotation>
              <xs:documentation>Number of empty states (cutoff parameter) used in e-ph. If not specified, the same number as for the groundstate calculations is used.
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>

          <xs:attribute ex:importance="expert" ex:unit="" name="ibsumeph" type="xs:integer" use="optional" default="1">
            <xs:annotation>
              <xs:documentation>Lower band index for the summation in e-ph calculations.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          
          <xs:attribute ex:importance="expert" ex:unit="" name="nbsumeph" type="xs:integer" use="optional" default="0">
            <xs:annotation>
              <xs:documentation>Upper band index for the summation electron-phonon coupling output. 
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>

          
          <xs:attribute ex:importance="expert" ex:unit="" name="ibeph" type="xs:integer" use="optional" default="1">
            <xs:annotation>
              <xs:documentation>Lower band index for e-ph output.</xs:documentation>
            </xs:annotation>
          </xs:attribute>
          
          <xs:attribute ex:importance="expert" ex:unit="" name="nbeph" type="xs:integer" use="optional" default="0">
            <xs:annotation>
              <xs:documentation>Upper band index for electron-phonon coupling output. If not specified, the maximum number of the available states is used.
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>
                    
          <xs:attribute ex:importance="expert" ex:unit="" name="debugeph" type="xs:boolean" default="false">
            <xs:annotation>
              <xs:documentation>
	          Print debugging information.
	            </xs:documentation>
            </xs:annotation>
          </xs:attribute>

          <xs:attribute ex:importance="essential" ex:unit="" name="ngridq" type="integertriple" default="0 0 0">
            <xs:annotation>
              <xs:documentation>
                k/q-point grid size to be used in e-ph calculations. 
                If not specified,  (2,2,2) k-grid is used.
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>
          
          <xs:attribute ex:importance="expert" ex:unit="" name="vqloff" type="vect3d" default="0.0d0 0.0d0 0.0d0">
            <xs:annotation>
              <xs:documentation>
                The <inlinemath>{\mathbf k/q}</inlinemath>-point offset vector in lattice coordinates.
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>
          
          <xs:attribute ex:importance="essential" ex:unit="" name="nfreq" type="xs:integer" default="500">
            <xs:annotation>
              <xs:documentation>
                The number of frequencies for which frequency dependent quantities (e.g. self-energy) are computed.
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>
          
          <xs:attribute ex:importance="expert" ex:unit="Hartree" name="lwfreq" type="fortrandouble" default="0.02d0">
            <xs:annotation>
              <xs:documentation>
                Lorentzian width of frequency density around single-particle energies.
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>
          
          <xs:attribute ex:importance="expert" ex:unit="Hartree" name="addfreq" type="fortrandouble" default="0.1d0">
            <xs:annotation>
              <xs:documentation>
                Frequency window that is added to the minimum and maximum frequency.
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>
          
          <xs:attribute ex:importance="essential" ex:unit="Hartree" name="einstein" type="fortrandouble" default="0.d0">
            <xs:annotation>
              <xs:documentation>
                frequency of a Einstein phonon to couple to
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>
          
          <xs:attribute ex:importance="essential" ex:unit="Kelvin" name="temp" type="fortrandouble" default="0.d0">
            <xs:annotation>
              <xs:documentation>
                Temperature in Kelvin
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>

          <xs:attribute ex:importance="expert" ex:unit="Kelvin" name="tempset" type="vect3d" default="0.0d0 0.0d0 0.0d0">
            <xs:annotation>
              <xs:documentation>
                Specifies temperature set. First (second) value: temperature interval limits. Third value: temperature steps.
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>
          
          <xs:attribute ex:importance="essential" ex:unit="" name="coupling" type="fortrandouble" default="0.d0">
            <xs:annotation>
              <xs:documentation>
                Constant coupling strength in eV
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>
          
          <xs:attribute ex:importance="essential" ex:unit="eV" name="eta" type="fortrandouble" default="0.d0">
            <xs:annotation>
              <xs:documentation>
                Infinitesimal in Lehmann representation in eV
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>
          
          <xs:attribute ex:importance="expert" ex:unit="Hartree" name="scissor" type="fortrandouble" default="0.d0">
            <xs:annotation>
              <xs:documentation>
                Scissor operator to correct band-gap
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>
          
          <xs:attribute ex:importance="expert" ex:unit="Hartree" name="efermi" type="fortrandouble" default="0.d0">
            <xs:annotation>
              <xs:documentation>
                Fermi energy (if 0.d0 (default) it is calculated)
              </xs:documentation>
            </xs:annotation>
          </xs:attribute>
          
        </xs:complexType>
    </xs:element>
</xs:schema>
