#!/bin/bash
#
EXECUTABLE=~/exciting_eph_2/bin/excitingmpi

label=`ls -d *_??`
for dirn in $label ; do
    cd $dirn
    cp -f $dirn.xml input.xml
    echo
    echo 'SCF calculation of "'$dirn'" starts --------------------------------'
    time mpirun -np 4 $EXECUTABLE | tee output.screen
    cd ../
done
echo 
