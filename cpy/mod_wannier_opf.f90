module mod_wannier_opf
  use mod_wannier_variables
  use mod_wannier_maxloc, only: wannier_loc, wannier_phases
  use m_linalg

  implicit none

! module variables
    
contains
    subroutine wannier_gen_opf( maxit)
      use m_plotmat
      integer, optional, intent( in) :: maxit
      ! local variables
      integer :: iknr, is, n, nx, iproj, i, j, minit, idxn, ist, a
      real(8) :: lambda, sum_n_wgt, phi, phimean, uncertainty, theta, evalmin, omegamin, x1
      real(8) :: opf_min_q(3,3), opf_min_p(3,1), opf_min_sol(3), eval(3), revec(3,3), opf_min_big(6,6), opf_min_aux(3,3), r, tp(2)
      real(8) :: t0, t1
      complex(8) :: opf_min_z(3,1), eval_big(6), tmp, mx(2,2), gr(2,2)
      character( 256) :: fname

      ! allocatable arrays
      complex(8), allocatable :: opf_transform(:,:,:), opf_projm(:,:,:), lsvec(:,:), rsvec(:,:), opf_mixing(:,:), opf_x(:,:,:)
      complex(8), allocatable :: auxmat(:,:), projm(:,:), lsvec2(:,:), rsvec2(:,:)
      real(8), allocatable :: sval(:), opf_t(:), sval2(:), phihist(:)

      if( mpiglobal%rank .eq. 0) then
        write( wf_info, '(" calculate optimized projection functions (OPF)...")')
      end if
      call timesec( t0)

      !********************************************************************
      ! initialize M matrices and Wannier centers
      !********************************************************************
      call wannier_loc

      !********************************************************************
      ! build rectangular transformation matrices
      !********************************************************************
      allocate( opf_transform( wf_groups( wf_group)%nwf, wf_groups( wf_group)%nproj, wf_kset%nkpt))
      allocate( opf_projm( wf_groups( wf_group)%nwf, wf_groups( wf_group)%nproj, wf_kset%nkpt))
      allocate( lsvec( wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf), &
                rsvec( wf_groups( wf_group)%nproj, wf_groups( wf_group)%nproj), &
                sval( wf_groups( wf_group)%nwf))

      do iknr = 1, wf_kset%nkpt
        call zgemm( 'c', 'n', wf_groups( wf_group)%nwf, wf_groups( wf_group)%nproj, wf_groups( wf_group)%nst, zone, &
               wf_transform( wf_groups( wf_group)%fst, wf_groups( wf_group)%fwf, iknr), wf_nst, &
               wf_groups( wf_group)%projection(:,:,iknr), wf_groups( wf_group)%nst, zzero, &
               opf_projm( :, :, iknr), wf_groups( wf_group)%nwf)
        ! closest unitary matrix
        call zsvd( opf_projm( :, :, iknr), sval, lsvec, rsvec)
        ! for numerical stability
        !do ist = 1, wf_nst
        !  if( sval( ist) .lt. 1.d-12) lsvec( :, ist) = zzero
        !end do
        call zgemm( 'n', 'n', wf_groups( wf_group)%nwf, wf_groups( wf_group)%nproj, wf_groups( wf_group)%nwf, zone, &
               lsvec, wf_groups( wf_group)%nwf, &
               rsvec, wf_groups( wf_group)%nproj, zzero, &
               opf_transform( :, :, iknr), wf_groups( wf_group)%nwf)
      end do

      sum_n_wgt = 2.d0*sum( wf_n_wgt)
      lambda = sum_n_wgt*input%properties%wannier%grouparray( wf_group)%group%lambdaopf
      minit = 2

      !********************************************************************
      ! build enlarged overlap and constraint matrices
      !********************************************************************
      allocate( opf_x( wf_groups( wf_group)%nproj, wf_groups( wf_group)%nproj, wf_kset%nkpt*(1+wf_n_ntot)))
      allocate( opf_t( wf_kset%nkpt*(1+wf_n_ntot)))

      ! build enlarged overlap matrices
      allocate( auxmat( wf_groups( wf_group)%nwf, wf_groups( wf_group)%nproj))
      i = 0
      do iknr = 1, wf_kset%nkpt
        do idxn = 1, wf_n_ntot
          i = i + 1
          call zgemm( 'n', 'n', wf_groups( wf_group)%nwf, wf_groups( wf_group)%nproj, wf_groups( wf_group)%nwf, zone, &
                 wf_m( wf_groups( wf_group)%fwf, wf_groups( wf_group)%fwf, iknr, idxn), wf_nwf, &
                 opf_transform( :, :, wf_n_ik( idxn, iknr)), wf_groups( wf_group)%nwf, zzero, &
                 auxmat, wf_groups( wf_group)%nwf)
          call zgemm( 'c', 'n', wf_groups( wf_group)%nproj, wf_groups( wf_group)%nproj, wf_groups( wf_group)%nwf, zone, &
                 opf_transform( :, :, iknr), wf_groups( wf_group)%nwf, &
                 auxmat, wf_groups( wf_group)%nwf, zzero, &
                 opf_x( :, :, i), wf_groups( wf_group)%nproj)
          opf_t( i) = -2.d0*wf_n_wgt( idxn)/wf_kset%nkpt
        end do
      end do
      ! build constraint matrices
      do iknr = 1, wf_kset%nkpt
        i = i + 1
        call zgemm( 'c', 'n', wf_groups( wf_group)%nproj, wf_groups( wf_group)%nproj, wf_groups( wf_group)%nwf, zone, &
               opf_projm( :, :, iknr), wf_groups( wf_group)%nwf, &
               opf_projm( :, :, iknr), wf_groups( wf_group)%nwf, zzero, &
               opf_x( :, :, i), wf_groups( wf_group)%nproj)
        do is = 1, wf_groups( wf_group)%nproj
          opf_x( is, is, i) = opf_x( is, is, i) - zone
        end do
        opf_t( i) = lambda/wf_kset%nkpt
      end do
      nx = i
      deallocate( opf_transform)

      !********************************************************************
      ! minimize Lagrangian
      !********************************************************************
      allocate( opf_mixing( wf_groups( wf_group)%nproj, wf_groups( wf_group)%nproj))
      allocate( phihist( minit))
      deallocate( auxmat)
      allocate( auxmat( wf_groups( wf_group)%nproj, wf_groups( wf_group)%nproj))
      deallocate( lsvec, rsvec)
      allocate( lsvec(3,3), rsvec(3,3))
      omegamin = 1.d13
      opf_mixing = zzero
      if( .true.) then
      do i = 1, wf_groups( wf_group)%nproj
        opf_mixing( i, i) = zone
      end do
      else
      opf_mixing( 3, 1) = 1
      opf_mixing( 6, 2) = 1
      opf_mixing( 9, 3) = 1
      opf_mixing(15, 4) = 1
      opf_mixing(27, 5) = 1
      opf_mixing(30, 6) = 1

      opf_mixing( 1, 7) = 1
      opf_mixing( 2, 8) = 1
      opf_mixing( 4, 9) = 1
      opf_mixing( 5,10) = 1
      opf_mixing( 7,11) = 1
      opf_mixing( 8,12) = 1
      opf_mixing(10,13) = 1
      opf_mixing(11,14) = 1
      opf_mixing(13,15) = 1
      opf_mixing(14,16) = 1
      opf_mixing(12,17) = 1
      opf_mixing(16,18) = 1
      opf_mixing(17,19) = 1
      opf_mixing(18,20) = 1
      opf_mixing(19,21) = 1
      opf_mixing(20,22) = 1
      opf_mixing(22,23) = 1
      opf_mixing(23,24) = 1
      opf_mixing(24,25) = 1
      opf_mixing(25,26) = 1
      opf_mixing(26,27) = 1
      opf_mixing(21,28) = 1
      opf_mixing(28,29) = 1
      opf_mixing(29,30) = 1
      opf_mixing(31,31) = 1
      opf_mixing(32,32) = 1
      opf_mixing(33,33) = 1
      opf_mixing(34,34) = 1
      opf_mixing(35,35) = 1
      opf_mixing(36,36) = 1
      end if

      do is = 1, nx
        call zgemm( 'c', 'n', wf_groups( wf_group)%nproj, wf_groups( wf_group)%nproj, wf_groups( wf_group)%nproj, zone, &
               opf_mixing, wf_groups( wf_group)%nproj, &
               opf_x(:,:,is), wf_groups( wf_group)%nproj, zzero, &
               auxmat, wf_groups( wf_group)%nproj)
        call zgemm( 'n', 'n', wf_groups( wf_group)%nproj, wf_groups( wf_group)%nproj, wf_groups( wf_group)%nproj, zone, &
               auxmat, wf_groups( wf_group)%nproj, &
               opf_mixing, wf_groups( wf_group)%nproj, zzero, &
               opf_x(:,:,is), wf_groups( wf_group)%nproj)
      end do
      phi = 0.d0
      do is = 1, nx
        do ist = 1, wf_groups( wf_group)%nwf
          phi = phi + opf_t( is)*opf_x( ist, ist, is)*conjg( opf_x( ist, ist, is))
        end do
      end do
      phi = phi + wf_groups( wf_group)%nwf*sum_n_wgt

      n = 0
      phihist = 0.d0
      uncertainty = 1.d0
      ! start minimization
      do while( (n .lt. minit) .or. (uncertainty .gt. input%properties%wannier%grouparray( wf_group)%group%epsopf))
        n = n + 1
        if( present( maxit)) then
          if( n .gt. maxit) exit
        end if
        do i = 1, wf_groups( wf_group)%nwf
          do j = i+1, wf_groups( wf_group)%nproj
            opf_min_q = 0.d0
            opf_min_p = 0.d0
            do is = 1, nx
              opf_min_z(1,1) =    0.5d0*(opf_x( i, i, is) - opf_x( j, j, is))
              opf_min_z(2,1) =   -0.5d0*(opf_x( i, j, is) + opf_x( j, i, is))
              opf_min_z(3,1) = 0.5d0*zi*(opf_x( i, j, is) - opf_x( j, i, is))
              opf_min_q = opf_min_q + opf_t( is)*dble( matmul( opf_min_z, transpose( conjg( opf_min_z))))
              opf_min_p = opf_min_p + opf_t( is)*dble( conjg( opf_x( i, i, is) + opf_x( j, j, is))*opf_min_z)
            end do

            ! find solution of minimization
            call rsydiag( opf_min_q, eval, evec=revec)
            if( j .le. wf_groups( wf_group)%nwf) then
              opf_min_sol(:) = revec( :, 1)
              if( opf_min_sol(1) .lt. 0.d0) opf_min_sol = -opf_min_sol
            else
              opf_min_big = 0.d0
              opf_min_big( 1:3, 1:3) = 2*opf_min_q
              opf_min_big( 1:3, 4:6) = 0.25d0*matmul( opf_min_p, transpose( opf_min_p)) - matmul( opf_min_q, opf_min_q)
              opf_min_big( 4, 1) = 1.d0
              opf_min_big( 5, 2) = 1.d0
              opf_min_big( 6, 3) = 1.d0
              call zgediag( cmplx( opf_min_big, 0.d0, 8), eval_big)
              evalmin = maxval( abs( eval_big))
              do is = 1, 6
                if( abs( aimag( eval_big( is))) .lt. input%structure%epslat) evalmin = min( evalmin, dble( eval_big( is)))
              end do
              do is = 1, 3
                opf_min_q( is, is) = opf_min_q( is, is) - evalmin
              end do
              if( minval( eval(:) - evalmin) .lt. 1.d-6) then
                call rpinv( opf_min_q, opf_min_aux)
                call r3mv( opf_min_aux, -0.5d0*opf_min_p(:,1), opf_min_sol)
                call r3mv( opf_min_q, opf_min_sol, opf_min_aux(:,1))
                if( (norm2( opf_min_aux(:,1) + 0.5d0*opf_min_p(:,1)) .ge. 1.d-6) .or. (norm2( opf_min_sol) .gt. 1.d0)) then
                  opf_min_sol = 10.d0
                else
                  opf_min_sol = opf_min_sol + revec(:,1)/norm2( revec(:,1))*sqrt( 1.d0 - norm2( opf_min_sol))
                end if
              else
                call r3minv( opf_min_q, opf_min_aux)
                call r3mv( opf_min_aux, -0.5d0*opf_min_p(:,1), opf_min_sol)
              end if
            end if

            ! set up the Given's rotation
            if( abs( 1.d0 - norm2( opf_min_sol)) .le. input%structure%epslat) then
              call sphcrd( (/opf_min_sol(2), opf_min_sol(3), opf_min_sol(1)/), r, tp)
              x1 = tp(2)
              theta = tp(1)/2
              gr(1,1) = cmplx( cos( theta), 0, 8)
              gr(1,2) = exp( zi*x1)*sin( theta)
              gr(2,2) = gr(1,1)
              gr(2,1) = -conjg( gr(1,2))
              ! update mixing matrix
              do a = 1, wf_groups( wf_group)%nproj
                tmp = opf_mixing( a, i)
                opf_mixing( a, i) = tmp*gr(1,1) + opf_mixing( a, j)*gr(2,1)
                opf_mixing( a, j) = opf_mixing( a, j)*gr(1,1) + tmp*gr(1,2)
              end do
#ifdef USEOMP                
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE( is, tmp, a)
!$OMP DO  
#endif
              ! update X-matrices
              do is = 1, nx
                do a = 1, wf_groups( wf_group)%nproj
                  tmp = opf_x( a, i, is)
                  opf_x( a, i, is) = tmp*gr(1,1) + opf_x( a, j, is)*gr(2,1)
                  opf_x( a, j, is) = opf_x( a, j, is)*gr(1,1) + tmp*gr(1,2)
                end do
                do a = 1, wf_groups( wf_group)%nproj
                  tmp = opf_x( i, a, is)
                  opf_x( i, a, is) = tmp*gr(1,1) - opf_x( j, a, is)*gr(1,2)
                  opf_x( j, a, is) = opf_x( j, a, is)*gr(1,1) - tmp*gr(2,1)
                end do
              end do
#ifdef USEOMP
!$OMP END DO
!$OMP END PARALLEL
#endif
            else
              !write(*,*) "Doh!"
            end if
          end do
        end do

        phi = 0.d0
        do is = 1, nx
          do ist = 1, wf_groups( wf_group)%nwf
            phi = phi + opf_t( is)*opf_x( ist, ist, is)*conjg( opf_x( ist, ist, is))
          end do
        end do
        phi = phi + wf_groups( wf_group)%nwf*sum_n_wgt

        ! convergence analysis
        if( n .eq. 1) phihist(:) = phi
        phihist = cshift( phihist, -1)
        phihist(1) = phi
        phimean = sum( phihist(:))/minit
        if( n .gt. 1) then
          !uncertainty = sqrt( sum( (phihist(:)-phimean)**2)/(min( n, minit)-1))/abs( phi)
          uncertainty = abs( phihist(2)/phihist(1) - 1.d0)
        else
          uncertainty = 1.d0
        end if

      end do
      deallocate( opf_x, opf_t)

      if( allocated( wf_opf)) deallocate( wf_opf)
      allocate( wf_opf( wf_groups( wf_group)%nproj, wf_groups( wf_group)%nwf))
      wf_opf(:,:) = opf_mixing( :, 1:wf_groups( wf_group)%nwf)
      deallocate( opf_mixing)

      write( fname, '("OPFON_G",1i1)') wf_group
      call writematlab( wf_opf, fname)

      !********************************************************************
      ! find transformation matrices from OPFs
      !********************************************************************
      allocate( projm( wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf))
      allocate( sval2( wf_groups( wf_group)%nwf))
      allocate( lsvec2( wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf))
      allocate( rsvec2( wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf))
      deallocate( auxmat)
      allocate( auxmat( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, wf_groups( wf_group)%nwf))
!#ifdef USEOMP                
!!$OMP PARALLEL DEFAULT(SHARED) PRIVATE( iknr, projm, sval2, lsvec2, rsvec2, auxmat)
!!$OMP DO  
!#endif
      tmp = zzero
      do iknr = 1, wf_kset%nkpt
        call zgemm( 'n', 'n', wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf, wf_groups( wf_group)%nproj, zone, &
               opf_projm( :, :, iknr), wf_groups( wf_group)%nwf, &
               wf_opf, wf_groups( wf_group)%nproj, zzero, &
               projm, wf_groups( wf_group)%nwf)
        call zgemm( 'n', 'c', wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf, zone, &
               projm, wf_groups( wf_group)%nwf, &
               projm, wf_groups( wf_group)%nwf, zzero, &
               lsvec2, wf_groups( wf_group)%nwf)
        do a = 1, wf_groups( wf_group)%nwf
          tmp = tmp + lsvec2(a,a)
        end do
        !write(*,*) iknr
        !call plotmat( lsvec2)
        !write(*,*)
        ! closest unitary matrix
        call zsvd( projm, sval2, lsvec2, rsvec2)
        ! for numerical stability
        !do ist = 1, wf_nst
        !  if( sval2( ist) .lt. 1.d-12) lsvec2( :, ist) = zzero
        !end do
        !write(*,*) iknr
        call zgemm( 'n', 'n', wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf, zone, &
               lsvec2, wf_groups( wf_group)%nwf, &
               rsvec2, wf_groups( wf_group)%nwf, zzero, &
               projm, wf_groups( wf_group)%nwf)
        call zgemm( 'n', 'n', wf_groups( wf_group)%nst, wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf, zone, &
               wf_transform( wf_groups( wf_group)%fst, wf_groups( wf_group)%fwf, iknr), wf_nst, &
               projm, wf_groups( wf_group)%nwf, zzero, &
               auxmat, wf_groups( wf_group)%nst)
        wf_transform( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, iknr) = auxmat
      end do
      write(*,'(2f13.6)') tmp/wf_kset%nkpt
!#ifdef USEOMP
!!$OMP END DO
!!$OMP END PARALLEL
!#endif


      deallocate( auxmat)
      allocate( auxmat( wf_groups( wf_group)%nwf, wf_groups( wf_group)%nproj))
      auxmat = zzero
      do iknr = 1, wf_kset%nkpt
        call zgemm( 'c', 'n', wf_groups( wf_group)%nwf, wf_groups( wf_group)%nproj, wf_groups( wf_group)%nst, zone, &
               wf_transform( wf_groups( wf_group)%fst, wf_groups( wf_group)%fwf, iknr), wf_nst, &
               wf_groups( wf_group)%projection(:,:,iknr), wf_groups( wf_group)%nst, zone, &
               auxmat, wf_groups( wf_group)%nwf)
      end do
      auxmat = auxmat/wf_kset%nkpt
      call zgemm( 'n', 'c', wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf, wf_groups( wf_group)%nproj, zone, &
             auxmat, wf_groups( wf_group)%nwf, &
             auxmat, wf_groups( wf_group)%nwf, zzero, &
             projm, wf_groups( wf_group)%nwf)
      !write(*,*) '================================'
      !call plotmat( projm)
      call writematlab( projm, 'PP')
      !write(*,*) '================================'
      projm = zzero
      do iknr = 1, wf_kset%nkpt
        call zgemm( 'n', 'c', wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf, wf_groups( wf_group)%nproj, zone, &
               opf_projm(:,:,iknr), wf_groups( wf_group)%nwf, &
               opf_projm(:,:,iknr), wf_groups( wf_group)%nwf, zone, &
               projm, wf_groups( wf_group)%nwf)
      end do
      tmp = zzero
      do a = 1, wf_groups( wf_group)%nwf
        tmp = tmp + projm(a,a)
      end do
      write(*,'(2f13.6)') tmp/wf_kset%nkpt


      ! update M matrices
      call wannier_loc
      ! determine phases for logarithms
      call wannier_phases
      ! calculate spread
      call wannier_loc

      call timesec( t1)
      if( mpiglobal%rank .eq. 0) then
        write( wf_info, '(5x,"duration (seconds): ",T40,3x,F10.1)') t1-t0
        write( wf_info, '(5x,"minimum iterations: ",T40,7x,I6)') minit
        write( wf_info, '(5x,"iterations: ",T40,7x,I6)') n
        write( wf_info, '(5x,"cutoff uncertainty: ",T40,E13.6)') input%properties%wannier%grouparray( wf_group)%group%epsopf
        write( wf_info, '(5x,"achieved uncertainty: ",T40,E13.6)') uncertainty
        write( wf_info, '(5x,"Lagrangian: ",T40,F13.6)') phi
        write( wf_info, '(5x,"Omega: ",T40,F13.6)') sum( wf_omega ( wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf))
        write( wf_info, *)
        call flushifc( wf_info)
      end if

      deallocate( lsvec2, rsvec2, projm, auxmat, phihist, lsvec, rsvec, sval, sval2)
      return
      !EOC
    end subroutine wannier_gen_opf
    !EOP

end module mod_wannier_opf
