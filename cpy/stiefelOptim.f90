module mod_stiefel_optim
  use m_linalg
  use m_plotmat

  implicit none

  private
  
  complex(8), parameter :: zero = cmplx( 0.d0, 0.d0, 8)
  complex(8), parameter ::  one = cmplx( 1.d0, 0.d0, 8)
  complex(8), parameter ::   ii = cmplx( 0.d0, 1.d0, 8)

  integer :: N   ! dimension of full space (number of rows)
  integer :: P   ! dimension of subspace (number of columns)
  integer :: K   ! number of N x P matrices Y
  integer :: LDY ! leading dimension of Y

  real(8) :: eps0 = 1.d-14 ! numerical zero
  real(8) :: eps = 1.d-10  ! tolerance for gradient

  complex(8), allocatable :: G(:,:,:) ! the gradient
  complex(8), allocatable :: H(:,:,:) ! the search direction
  complex(8), allocatable :: Q(:,:,:) ! Q of QR decomposition
  complex(8), allocatable :: R(:,:,:) ! R of QR decomposition

  real(8), allocatable    :: expe(:,:)   ! eigenvvalues for exponential
  complex(8), allocatable :: expv(:,:,:) ! eigenvectors for exponential

  abstract interface
    subroutine fun_interface( n, p, k, ldy, y, f)
      integer, intent( in)    :: n, p, k, ldy
      complex(8), intent( in) :: y(:,:,:)
      real(8), intent( out)   :: f
    end subroutine
  end interface

  abstract interface
    subroutine grad_interface( n, p, k, ldy, y, g)
      integer, intent( in)     :: n, p, k, ldy
      complex(8), intent( in)  :: y(:,:,:)
      complex(8), intent( out) :: g(n,p,k)
    end subroutine
  end interface

  public :: stiefel_optim

  contains
    subroutine stiefel_optim( n_, p_, k_, ldy_, Y, fun, grad, &
                              tol, maxit)
      integer, intent( in)       :: n_, p_, k_, ldy_
      complex(8), intent( inout) :: Y(:,:,:)         ! on entry: initial guess for the matrices Y, on exit: the minimizer matrices Y
      procedure( fun_interface)  :: fun              ! subroutine fun(Y,f) that computes the objective function f
      procedure( grad_interface) :: grad             ! subroutine grad(Y,G) that computes the gradient G of the objective function
      real(8), optional, intent( inout) :: tol       ! tolerance for norm of gradient
      integer, optional, intent( in)    :: maxit     ! maximum number of iterations

      integer :: it, ik, dim, itmax
      real(8) :: ngrad, f, alpha
      complex(8) :: z1, z2
      complex(8), allocatable :: A(:,:), auxmat(:,:), r_(:,:), MN(:,:), G0(:,:,:)

      ! sanity checks
      if( n_ .le. 0) then
        write(*,*)
        write(*,'("Error (stiefel_optim): The dimension N must be positive")')
        stop
      end if
      if( p_ .le. 0) then
        write(*,*)
        write(*,'("Error (stiefel_optim): The dimension P must be positive")')
        stop
      end if
      if( n_ .lt. p_) then
        write(*,*)
        write(*,'("Error (stiefel_optim): The dimension N must not be smaller than P!")')
        stop
      end if
      if( k_ .le. 0) then
        write(*,*)
        write(*,'("Error (stiefel_optim): The number of matrices K must be positive")')
        stop
      end if
      if( ldy_ .lt. n_) then
        write(*,*)
        write(*,'("Error (stiefel_optim): The leading dimension LDY must not be smaller than N!")')
        stop
      end if

      if( present( tol)) eps = tol
      itmax = 1000
      if( present( maxit)) itmax = maxit

      N = n_
      P = p_
      K = k_
      LDY = ldy_
      dim = P*(N-P) + P*(P-1)/2
      call destroy

      !write(*,'("N = ",i2)') N
      !write(*,'("P = ",i2)') P
      !write(*,'("K = ",i2)') K
      allocate( G(N,P,K), H(N,P,K), Q(N,P,K), R(P,P,K))
      allocate( expe(2*P,K), expv(2*P,2*P,K))

      allocate( A(P,P), auxmat(N,P), r_(N,P), MN(2*P,2*P), G0(N,P,K))

      ! INITIALIZATION
      it = 0
      call gradient( Y, grad)              ! initialize gradient
      call innerp( G, G, Y, z1)  
      ngrad = dsqrt( dble( z1))/dble(K)    ! norm of gradient
      H = -G                               ! initialize search direction
      call fun( N, P, K, LDY, Y, f)
      write(*,'("ITERATION ",i4,2g26.16)') it, f, ngrad
      !do ik = 1, K
      !  write(*,'("Y",i2)') ik
      !  call plotmat( Y(:,:,ik))
      !  write(*,*)
      !end do

      ! LOOP
      do while( (it .lt. itmax) .and. (ngrad .gt. eps))
        it = it + 1
        ! get eigendecomposition for exponential
        do ik = 1, K
          !call writematlab( G(:,:,ik), 'G')
          !call writematlab( H(:,:,ik), 'H')
          call zgemm( 'c', 'n', P, P, N, one, Y(1,1,ik), LDY, H(1,1,ik), N, zero, A, P)
          A = cmplx( 0.5d0, 0.d0, 8)*(A - conjg( transpose( A)))
          !call writematlab( H(:,:,ik), 'A')
          auxmat = H(:,:,ik)
          call zgemm( 'n', 'n', N, P, P, -one, Y(1,1,ik), LDY, A, P, one, auxmat, N)
          call zqr( auxmat, q=Q(:,:,ik), r=r_)
          R(:,:,ik) = r_(1:P,:)
          !call writematlab( Q(:,:,ik), 'Q')
          !call writematlab( R(:,:,ik), 'R')
          MN = zero
          MN(  1:P,    1:P  ) = A
          MN(P+1:2*P,  1:P  ) = R(:,:,ik)
          MN(  1:P,  P+1:2*P) = -conjg( transpose( R(:,:,ik)))
          MN = cmplx( 0.d0, -1.d0, 8)*MN ! -iMN is Hermitian
          call zhediag( MN, expe(:,ik), evec=expv(:,:,ik))
          !expv(:,:,ik) = zero
          !expv(  1:P,    1:P  , ik) = A
          !expv(P+1:2*P,  1:P  , ik) = R(:,:,ik)
          !expv(  1:P,  P+1:2*P, ik) = -conjg( transpose( R(:,:,ik)))
        end do

        alpha = 0.1d0
        if( itmax .eq. 1) alpha = 2.d0
        call linesearch( Y, fun, grad, alpha)
        alpha = max( alpha, 1.d-1)
        call update( alpha, Y, dir=.true.) ! update Y 
        G0 = G
        call gradient( Y, grad)            ! update G
        call innerp( G-G0, G, Y, z2)
        !call innerp( G, G, Y, z2)
        H = -G + (z2/z1)*H                 ! update H
        call innerp( G, G, Y, z1)
        call fun( N, P, K, LDY, Y, f)
        ngrad = dsqrt( dble( z1))/dble(K)
        if( mod( it, dim) .eq. 0) then
          H = -G
        end if
        call makeUnitary( Y)
        !if( mod( it, 10) .eq. 0) H = -G
        !H = -G
        write(*,'("ITERATION ",i4,5g26.16)') it, f, ngrad, alpha, z2/z1
        !do ik = 1, K
        !  write(*,'("Y",i2)') ik
        !  call plotmat( Y(:,:,ik))
        !  write(*,*)
        !end do
      end do
      !write(*,'("ITERATION",i4,2g26.16)') it, f, ngrad
      if( present( tol)) tol = ngrad
      !do ik = 1, K
      !  write(*,'("Y",i2)') ik
      !  call plotmat( Y(:,:,ik))
      !  write(*,*)
      !end do
      deallocate( A, auxmat, r_, MN, G0)
      call destroy
    end subroutine stiefel_optim

    subroutine destroy
      if( allocated( G)) deallocate( G)
      if( allocated( H)) deallocate( H)
      if( allocated( Q)) deallocate( Q)
      if( allocated( R)) deallocate( R)

      if( allocated( expe)) deallocate( expe)
      if( allocated( expv)) deallocate( expv)
    end subroutine destroy

    subroutine linesearch( Y, fun, grad, alpha)
      complex(8), intent( inout) :: Y(:,:,:)
      procedure( fun_interface)  :: fun   
      procedure( grad_interface) :: grad  
      real(8), intent( inout)    :: alpha

      integer :: lsmaxit
      real(8) :: delta, sigma, epsilon, theta, gamma, eta

      integer :: ik, it
      real(8) :: p0, dp0, pk, a, b, pa, pb, dpa, dpb
      real(8) :: a_, b_
      complex(8) :: z
      complex(8), allocatable :: YQ(:,:), YQV(:,:,:), dY(:,:,:)

      lsmaxit = 100
      delta   = 0.1d0
      sigma   = 0.9d0
      epsilon = 1.d-6
      theta   = 0.5d0
      gamma   = 0.66d0

      allocate( YQ(N,2*P), YQV(N,2*P,K), dY(N,P,K))
      do ik = 1, K
        YQ(:,  1:P)   = Y(1:N,1:P,ik)
        YQ(:,P+1:2*P) = Q( :,  :, ik)
        call zgemm( 'n', 'n', N, 2*P, 2*P, one, YQ, N, expv(1,1,ik), 2*P, zero, YQV(1,1,ik), N)
        !YQV(:,:,ik) = YQ
      end do

      ! find initial interval
      a = 0.d0
      call step( a)
      call fun( N, P, K, LDY, Y, pa)
      call gradient( Y, grad); call innerp( G, dY, Y, z); dpa = dble( z)

      if( alpha .gt. 1.d0) then
        write(*,'(i,10g26.16)') 0, a, pa, z
        b = -1.d0
        do it = 1, 100
          b = b + 0.03d0
          call step( b)
          call fun( N, P, K, LDY, Y, pb)
          call gradient( Y, grad); call innerp( G, dY, Y, z); dpb = dble( z)
          write(*,'(i,10g26.16)') it, b, pb, z
        end do
      end if
        
      b = 0.01d0
      call step( b)
      call fun( N, P, K, LDY, Y, pb)
      call gradient( Y, grad); call innerp( G, dY, Y, z); dpb = dble( z)
      do while( (dpb .lt. 0.d0) .and. (b .lt. 1.d3))
        b = b*2.d0
        call step( b)
        call fun( N, P, K, LDY, Y, pb)
        call gradient( Y, grad); call innerp( G, dY, Y, z); dpb = dble( z)
      end do

      ! perform line search
      it = 0; p0 = pa; dp0 = dpa
      !write(*,'(i,6g16.6)') it, a, b, pa, pb, dpa, dpb
      do while( (it .lt. lsmaxit) .and. (b-a .gt. eps0))
        ! strong Wolfe condition
        if( (pa - p0 .lt. delta*b*dp0) .and. (dpb .ge. sigma*dp0)) exit
        ! approximate Wolfe condition
        if( ((2*delta - 1.d0)*dp0 .ge. dpb) .and. (dpb .ge. sigma*dp0) .and. (pb .le. p0 + epsilon*abs( pk))) exit
        a_ = a; b_ = b
        call secant2
        if( b - a .gt. gamma*(b_ - a_)) call interval( 0.5d0*(a + b))
        it = it + 1; pk = pb
        !write(*,'(i,6g16.6)') it, a, b, pa, pb, dpa, dpb
      end do

      alpha = 0.5d0*(a + b)
      call step( 0.d0) ! reset Y
      deallocate( YQ, YQV, dY)
      !write(*,'("STEP",f13.6)') alpha

      contains
        subroutine interval( c)
          real(8), intent( in) :: c
          real(8) :: d, aa, bb, pc, dpc
          if( (c .le. a) .or. (c .ge. b)) return
          call step( c)
          call fun( N, P, K, LDY, Y, pc)
          call gradient( Y, grad); call innerp( G, dY, Y, z); dpc = dble( z)
          !write(*,'(10(3g16.6,"/"))'), a, pa, dpa, b, pb, dpb, c, pc, dpc
          if( dpc .ge. -eps0) then
            b = c; pb = pc; dpb = dpc
          else
            if( pc .le. p0 + epsilon*abs( pk)) then
              a = c; pa = pc; dpa = dpc;
              return
            else
              aa = a; bb = c
              do while( (dpc .lt. -eps0) .and. (bb-aa .gt. eps0))
                !d = (1.d0 - theta)*aa + theta*bb
                d = bb*((1.d0 - theta)*(aa/bb) + theta)
                call step( d)
                call fun( N, P, K, LDY, Y, pc)
                call gradient( Y, grad); call innerp( G, dY, Y, z); dpc = dble( z)
                !write(*,'(10(3g16.6,"|"))'), aa, pa, dpa, bb, pb, dpb, c, pc, dpc
                if( dpc .ge. -eps0) exit
                if( pc .le. p0 + epsilon*abs( pk)) then
                  aa = d; pa = pc; dpa = dpc;
                else
                  bb = d; pb = pc; dpb = dpc
                end if
              end do
              a = aa
              b = d; pb = pc; dpb = dpc
              return
            end if
          end if
          return
        end subroutine interval
        
        subroutine step( a)
          real(8), intent( in) :: a
          integer :: ik, i
          complex(8) :: auxmat(N,2*P), expm(2*P,2*P)
          do ik = 1, K
            auxmat = YQV(:,:,ik)
            if( abs( a) .ge. 1.d-32) then
              do i = 1, 2*P
                auxmat(:,i) = exp( ii*a*expe(i,ik))*auxmat(:,i)
              end do
            end if
            call zgemm( 'n', 'c', N, P, 2*P, one, auxmat, N, expv(1,1,ik), 2*P, zero, Y(1,1,ik), LDY)
            do i = 1, 2*P
              auxmat(:,i) = ii*expe(i,ik)*auxmat(:,i)
            end do
            call zgemm( 'n', 'c', N, P, 2*P, one, auxmat, N, expv(1,1,ik), 2*P, zero, dY(1,1,ik), N)
          end do
          !do ik = 1, K
          !  call zexpm( a*expv(:,:,ik), expm)
          !  call zgemm( 'n', 'n', N, P, 2*P, one, YQV(1,1,ik), N, expm, 2*P, zero, Y(1,1,ik), LDY)
          !  call zgemm( 'n', 'n', N, 2*P, 2*P, one, YQV(1,1,ik), N, expv(1,1,ik), 2*P, zero, auxmat, N)
          !  call zgemm( 'n', 'n', N, P, 2*P, one, auxmat, N, expm, 2*P, zero, dY(1,1,ik), N)
          !end do
          return
        end subroutine step

        subroutine secant( c)
          real(8), intent( out) :: c
          c = (a*dpb - b*dpa)/(dpb - dpa)
          return
        end subroutine secant

        subroutine secant2
          real(8) :: c, aa, bb, paa, pbb, dpaa, dpbb
          aa = a; paa = pa; dpaa = dpa   !aa = a0
          bb = b; pbb = pb; dpbb = dpb   !bb = b0
          call secant( c)
          call interval( c) !a = A, b = B
          if( abs( c-b) .lt. eps0) then
            aa = a; paa = pa; dpaa = dpa !aa = A
            a = bb; pa = pbb; dpa = dpbb !a = b0
            call secant( c)
            a = aa; pa = paa; dpa = dpaa !a = A
            call interval( c)
          else if( abs( c-a) .lt. eps0) then
            bb = b; pbb = pb; dpbb = dpb !bb = B
            b = a; pb = pa; dpb = dpa    !b = A
            a = aa; pa = paa; dpa = dpaa !a = a0
            call secant( c)
            a = b; pa = pb; dpa = dpb    !a = A
            b = bb; pb = pbb; dpb = dpbb !b = B
            call interval( c)
          end if
          return
        end subroutine secant2
    end subroutine linesearch

    subroutine update( alpha, Y, dir)
      real(8), intent( in)       :: alpha
      complex(8), intent( inout) :: Y(:,:,:)
      logical, optional, intent( in) :: dir

      integer :: ik, i
      logical :: d
      complex(8) :: auxmat1(N,2*P), expm(2*P,2*P), auxmat2(N,2*P)

      d = .false.
      if( present( dir)) d = dir

      do ik = 1, K
        if( d) then
          auxmat1(:,1:P) = H(:,:,ik)
          call zgemm( 'n', 'c', N, P, P, -one, Y(1,1,ik), LDY, R(1,1,ik), P, zero, auxmat1(1,P+1), N)
          call zgemm( 'n', 'n', N, 2*P, 2*P, one, auxmat1, N, expv(1,1,ik), 2*P, zero, auxmat2, N)
          do i = 1, 2*P
            auxmat2(:,i) = exp( ii*alpha*expe(i,ik))*auxmat2(:,i)
          end do
          call zgemm( 'n', 'c', N, P, 2*P, one, auxmat2, N, expv(1,1,ik), 2*P, zero, H(1,1,ik), N)
        end if

        auxmat1(:,1:P) = Y(1:N,1:P,ik)
        auxmat1(:,P+1:2*P) = Q(:,:,ik)
        call zgemm( 'n', 'n', N, 2*P, 2*P, one, auxmat1, N, expv(1,1,ik), 2*P, zero, auxmat2, N)
        do i = 1, 2*P
          auxmat2(:,i) = exp( ii*alpha*expe(i,ik))*auxmat2(:,i)
        end do
        call zgemm( 'n', 'c', N, P, 2*P, one, auxmat2, N, expv(1,1,ik), 2*P, zero, Y(1,1,ik), LDY)
      end do
      !do ik = 1, K
      !  auxmat2(:,1:P) = Y(1:N,1:P,ik)
      !  auxmat2(:,P+1:2*P) = Q(:,:,ik)
      !  call zexpm( alpha*expv(:,:,ik), expm)
      !  call zgemm( 'n', 'n', N, P, 2*P, one, auxmat2, N, expm, 2*P, zero, Y(1,1,ik), LDY)
      !  if( d) then
      !    auxmat1(:,1:P) = H(:,:,ik)
      !    call zgemm( 'n', 'c', N, P, P, -one, Y(1,1,ik), LDY, R(1,1,ik), P, zero, auxmat1(1,P+1), N)
      !    call zgemm( 'n', 'n', N, P, 2*P, one, auxmat1, N, expm, 2*P, zero, H(1,1,ik), N)
      !  end if
      !end do
      return
    end subroutine update

    subroutine gradient( Y, grad)
      complex(8), intent( in)    :: Y(:,:,:)
      procedure( grad_interface) :: grad

      integer :: ik
      complex(8) :: auxmat(P,P)

      call grad( N, P, K, LDY, Y, G)
      do ik = 1, K
        call zgemm( 'c', 'n', P, P, N,  one, G(1,1,ik), N, Y(1,1,ik), LDY, zero, auxmat, P)
        call zgemm( 'n', 'n', N, P, P, -one, Y(1,1,ik), LDY, auxmat, P, one, G(1,1,ik), N)
      end do
      return
    end subroutine gradient

    subroutine innerp( a, b, Y, z)
      complex(8), intent( in)  :: a(N,P,K), b(N,P,K)
      complex(8), intent( in)  :: Y(:,:,:)
      complex(8), intent( out) :: z

      integer :: ik, i
      complex(8) :: zdotc, auxmat1(P,P), auxmat2(N,P)

      z = zero
      do ik = 1, K
        auxmat2 = b(:,:,ik)
        call zgemm( 'c', 'n', P, P, N, one, Y(1,1,ik), LDY, auxmat2, N, zero, auxmat1, P)
        call zgemm( 'n', 'n', N, P, P, cmplx( -0.5d0, 0.d0, 8), Y(1,1,ik), LDY, auxmat1, P, one, auxmat2, N)
        z = z + zdotc( N*P, a(1,1,ik), 1, auxmat2, 1)
      end do
      z = z/dble(K)
    end subroutine innerp

    subroutine makeUnitary( Y)
      complex(8), intent( inout) :: Y(:,:,:)

      integer :: ik
      real(8) :: sval(P)
      complex(8) :: lsvec(N,N), rsvec(P,P)
      
      do ik = 1, K
        call zsvd( Y(1:N,1:P,ik), sval, lsvec, rsvec)
        call zgemm( 'n', 'n', N, P, P, zone, lsvec, N, rsvec, P, zero, Y(1,1,ik), LDY)
      end do
    end subroutine makeUnitary
end module mod_stiefel_optim
