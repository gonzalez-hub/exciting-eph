module mod_eph
  use mod_eph_variables
  use mod_eph_helper
  use mod_eph_electrons
  use mod_eph_phonons
  use mod_eph_ephmat
  use mod_eph_sigma
  use mod_eph_sfun
  use mod_wannier
  use mod_wfint
  use mod_wfutil
  use mod_eph_scroot, only: eph_scroot

  implicit none

  logical :: reduce = .false.

! methods
  contains
    !
    subroutine eph_init
      use m_getunit
      integer :: iw, ip, i, j, k, ik, isym
      real(8) :: vr(3)
      character(256) :: fname
      logical :: exist

      eph_temp         = max( 1.d-16, abs( input%eph%temp))  ! temperature (in K) for occupation
      eph_phfreq_const = input%eph%einstein/h2ev            ! constant Einstein phonon frequency
      eph_ephmat_c     = input%eph%coupling/h2ev
      eph_eta          = input%eph%eta/h2ev                 ! complex infinitesimal in Lehmann representation
      eph_scissor      = input%eph%scissor
      eph_nqpmax       = 5

      write( fname, '("EPSINFZSTAR.OUT")')
      inquire( file=trim( fname), exist=exist)
      if( exist) then
        allocate( eph_zstar( 3, 3, natmtot))
        allocate( eph_epsinf(3,3))
        call eph_read_epsinf_zstar( fname, eph_epsinf, eph_zstar)
        eph_polar = .true.
      end if

      ! generate G-vectors
      eph_Gset = wf_Gset

      ! source k-grid
      eph_kset_el = wf_kset
      eph_Gkset_el = wf_Gkset

      ! bzint k-grid (reducible)
      call generate_k_vectors( eph_qset, eph_kset_el%bvec, input%eph%ngridq, input%eph%vqloff, reduce, .false.)

      ! target k-set (path or grid)
      call eph_gen_pset

      !write(*,*) wf_npp1d, tmp_kset%nkpt

      ! allocate energy/frequency grids
      eph_nfreq = input%eph%nfreq
      allocate( eph_freqs( eph_nfreq, eph_pset%nkpt))
      
      ! set band ranges
      ! Note: We use states expressed in the Wannier functions basis
      eph_nst = wf_nwf
      eph_fst = 1       ! in Wannier interpolated states
      eph_lst = eph_nst ! in Wannier interpolated states

      ! phonon k-grid
      if( eph_phfreq_const .le. 1.d-16) then
        if( eph_dfpt) then
          call generate_k_vectors( eph_kset_ph, eph_kset_el%bvec, input%phonons%ngridq, (/0.d0, 0.d0, 0.d0/), .false., .false.)
        else
          call generate_k_vectors( eph_kset_ph, eph_kset_el%bvec, input%phonons%ngridq, (/0.d0, 0.d0, 0.d0/), input%phonons%reduceq, .false.)
        end if
        !eph_kset_ph%nkpt = nphwrt
        !do ip = 1, nphwrt
        !  eph_kset_ph%vkl(:,ip) = vqlwrt(:,ip)
        !  call r3mv( eph_kset_ph%bvec, eph_kset_ph%vkl(:,ip), eph_kset_ph%vkc(:,ip))
        !  eph_kset_ph%ivk(:,ip) = nint( eph_kset_ph%vkl(:,ip)*dble( eph_kset_ph%ngridk))
        !end do
        !eph_kset_ph%wkpt = 0.d0
        !do i = 0, eph_kset_ph%ngridk(1)-1
        !  do j = 0, eph_kset_ph%ngridk(2)-1
        !    do k = 0, eph_kset_ph%ngridk(3)-1
        !      vr = dble( (/i, j, k/))/dble( eph_kset_ph%ngridk)
        !      call findkptinset( vr, eph_kset_ph, isym, ik)
        !      eph_kset_ph%ikmap( i, j, k) = ik
        !      eph_kset_ph%wkpt( ik) = eph_kset_ph%wkpt( ik) + 1.d0
        !      eph_kset_ph%ik2ikp( eph_kset_ph%ikmapnr( i, j, k)) = ik
        !    end do
        !  end do
        !end do
        !eph_kset_ph%wkpt = eph_kset_ph%wkpt/dble( product( eph_kset_ph%ngridk))
        !call getunit( i)
        !open( i, file='EPH_PHKSET.DAT', action='write', form='formatted')
        !call print_k_vectors( eph_kset_ph, i)
        !close( i)
        !call generate_Gk_vectors( eph_Gkset_ph, eph_kset_ph, eph_Gset, eph_Gkset_el%gkmax)
        !do ip = 1, eph_kset_ph%nkpt
        !  write(*,'(i,3f9.4)') ip, eph_kset_ph%vkl(:,ip)
        !end do
      end if
      
      return
    end subroutine eph_init

!============================================================================
!============================================================================

    subroutine eph_do
      use m_getunit

      ! tasks
      logical :: task_gensigma, task_genspecfun, task_writeephmat, task_phdisp, task_eldisp, task_findqp

      ! parameters
      integer :: nqpmax
      real(8) :: qpeps

      ! working variables
      integer :: ip, iq, is, ia, ias, imode, ist, iw, nqp
      real(8) :: qpdiag(2)
      logical :: success

      real(8), allocatable :: sigim(:,:,:)
      complex(8), allocatable :: dynk(:,:,:), dynq(:,:,:), ev(:,:), umnp(:,:,:), qp(:,:)

      ! auxilliary variables
      integer :: un
      real(8) :: r1
      character(256) :: fname

      task_gensigma    = .false.
      task_genspecfun  = .false.
      task_writeephmat = .false.
      task_phdisp      = .true.
      task_eldisp      = .false.
      task_findqp      = .false.
      qpeps = 0.05d0
      nqpmax = 5    

      ! find VBM and CBM
      !write(*,*) 'find gap'
      !call wfutil_find_bandgap
      !eph_efermi = wfint_evbm
      !eph_efermi = 0.4417235973279391
      !eph_efermi = 0.1429505394838780d0 !AlAs
      !eph_efermi = 0.194780 !b-Ga2O3
      !eph_efermi = 0.18124 !TiO2 anatase
            !eph_efermi = 0.d0
      eph_efermi = input%eph%efermi
      if( eph_efermi .eq. 0.d0) call eph_electrons_efermi( eph_efermi)
      write(*,'("EFERMI: ",f26.16)') eph_efermi

      !******************************************
      !* GET EIGENVALUES ON TARGET SET
      !******************************************
      if( allocated( eph_evalp)) deallocate( eph_evalp)
      allocate( eph_evalp( eph_fst:eph_lst, eph_pset%nkpt))
      allocate( umnp( eph_fst:eph_lst, eph_fst:eph_lst, eph_pset%nkpt))
      ! store Wannier transformation matrices on p-set for polar coupling
      call eph_electrons_interpolate( eph_pset, eph_evalp, evec=umnp)

      !******************************************
      !* COMPUTE SELFENERGY
      !******************************************
      ! do preparation if needed
      if( task_gensigma) then
        ! get phonon frequencies and eigenvectors on integration grid
        if( allocated( eph_phfreq)) deallocate( eph_phfreq)
        allocate( eph_phfreq( 3*natmtot, eph_qset%nkpt))
        if( allocated( eph_phevec)) deallocate( eph_phevec)
        allocate( eph_phevec( 3, natmtot, 3*natmtot, eph_qset%nkpt))
        if( eph_phfreq_const .gt. 1.d-16) then
          eph_phfreq = eph_phfreq_const
          eph_phevec = zzero
        else
          if( eph_polar) then
            call eph_phonons_interpolate( eph_kset_ph, eph_qset, eph_phfreq, eph_phevec, epsinf=eph_epsinf, zstar=eph_zstar)
          else
            call eph_phonons_interpolate( eph_kset_ph, eph_qset, eph_phfreq, eph_phevec)
          end if
        end if

        allocate( sigim( eph_nfreq, eph_fst:eph_lst, eph_pset%nkpt))

        ! calculate self-energy for each p-point
        do ip = firstofset( mpiglobal%rank, eph_pset%nkpt), lastofset( mpiglobal%rank, eph_pset%nkpt)
          write(*,'("P-POINT",i,3f16.6)') ip, eph_pset%vkl(:,ip)
          ! generate p+q set and tetrahedra
          call eph_init_ppoint( ip)

          ! get electron energies and Wannier transformation matrices on p+q set
          allocate( eph_evalqp( eph_fst:eph_lst, eph_qpset%nkpt))
          call eph_electrons_interpolate( eph_qpset, eph_evalqp, serial=.true.)

          ! set frequencies
          call eph_genfreqs( ip)

          ! generate eph matrix elements
          allocate( eph_ephmat( eph_fst:eph_lst, eph_fst:eph_lst, 3*natmtot, eph_qpset%nkpt))
          eph_ephmat = zzero
          if( eph_polar) call eph_ephmat_gen_lr( eph_qset, eph_qpset, eph_iqp2iq, eph_phfreq, eph_phevec, &
                                                 eph_epsinf, eph_zstar, wfint_transform, umnp(:,:,ip), zone, eph_ephmat)

          ! calculate self-energy
          call eph_sigma_genim( eph_nfreq, eph_freqs( :, ip), sigim( :, :, ip))
          deallocate( eph_evalqp, eph_ephmat)
        end do
        call mpi_allgatherv_ifc( set=eph_pset%nkpt, rlen=eph_nfreq*eph_nst, rbuf=sigim)
        call mpi_allgatherv_ifc( set=eph_pset%nkpt, rlen=eph_nfreq, rbuf=eph_freqs)
        do ip = 1, eph_pset%nkpt
          call eph_sigma_imtofile( ip, eph_nfreq, eph_freqs( :, ip), sigim( :, :, ip), success)
        end do
        call barrier
      end if

      !******************************************
      !* COMPUTE SPECTRAL FUNCTION
      !******************************************
      if( task_genspecfun) then
        ! allocate self energy
        if( allocated( eph_specfun_band)) deallocate( eph_specfun_band)
        allocate( eph_specfun_band( eph_nfreq, eph_fst:eph_lst, eph_pset%nkpt))
        if( allocated( eph_sigmafm)) deallocate( eph_sigmafm)
        allocate( eph_sigmafm( eph_nfreq, eph_fst:eph_lst, eph_pset%nkpt))
        eph_sigmafm = zzero
        eph_freqs = 0.d0

        allocate( qp( nqpmax, 2))
  
        ! loop over target points
        do ip = firstofset( mpiglobal%rank, eph_pset%nkpt), lastofset( mpiglobal%rank, eph_pset%nkpt)
          write(*,'("P-POINT",i,3f16.6)') ip, eph_pset%vkl(:,ip)
          call eph_sigma_get( ip, eph_sigmafm( :, :, ip))
  
#ifdef USEOMP
!$omp parallel default( shared) private( ist)
!$omp do
#endif
          do ist = eph_fst, eph_lst
            call eph_sfun_gen( eph_nfreq, eph_freqs( :, ip)-eph_evalp( ist, ip), eph_sigmafm( :, ist, ip), eph_specfun_band( :, ist, ip))
            !call eph_sfun_nthmoment( 0, eph_nfreq, eph_freqs( :, ip)-eph_evalp( ist, ip), eph_sigmafm( :, ist, ip), r1)
            !write(*,'(i,f26.16)') ist, r1
            !call eph_sfun_findqp( eph_nfreq, eph_freqs( :, ip)-eph_evalp( ist, ip), eph_sigmafm( :, ist, ip), nqpmax, qpeps, nqp, qp, qpdiag)
            !write(*,'(f4.2,f4.1)') qpdiag(1), qpdiag(2)*1.d2
          end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
        end do
        call eph_sigma_output( eph_nfreq, eph_fst, eph_lst, eph_pset%nkpt, eph_freqs, eph_sigmafm)
        call eph_sfun_output( eph_nfreq, eph_fst, eph_lst, eph_pset%nkpt, eph_freqs, eph_specfun_band)
      end if

      !******************************************
      !* FIND QUASI PARTICLE ENERGIES
      !******************************************
      if( task_findqp) then
        allocate( qp( 2, nqpmax))
        if( allocated( eph_evalp_qp)) deallocate( eph_evalp_qp)
        allocate( eph_evalp_qp( eph_nqpmax, eph_fst:eph_lst, eph_pset%nkpt))
        if( allocated( eph_sigmafm)) deallocate( eph_sigmafm)
        allocate( eph_sigmafm( eph_nfreq, eph_fst:eph_lst, eph_pset%nkpt))
        do ip = 29, 29!firstofset( mpiglobal%rank, eph_pset%nkpt), lastofset( mpiglobal%rank, eph_pset%nkpt)
          write(*,'("P-POINT",i,3f16.6)') ip, eph_pset%vkl(:,ip)
          call eph_sigma_get( ip, eph_sigmafm( :, :, ip))
          do ist = 19, 19!eph_fst, eph_lst
            write(*,*) ist, '-------------------'
            !write(*,'(2i,f26.16)') ip, ist, eph_evalp( ist, ip)
            call eph_sigma_findqp( eph_nfreq, eph_freqs( :, ip)-eph_evalp( ist, ip), eph_sigmafm( :, ist, ip), nqpmax, nqp, qp)
            eph_evalp_qp( :, ist, ip) = cmplx( eph_evalp( ist, ip), 0.d0, 8)+qp(1,:)
          end do
        end do
      end if

      !******************************************
      !* WRITE COUPLING MATRIX ELEMENTS
      !******************************************
      if( task_writeephmat) then
        call eph_ephmat_output( (/0.d0, 0.d0, 0.d0/), eph_fst, eph_lst)
      end if

      !******************************************
      !* WRITE PHONON DISPERSION
      !******************************************
      if( task_phdisp) then
        call eph_phonons_output
      end if

      !******************************************
      !* WRITE ELECTRON DISPERSION
      !******************************************
      if( task_eldisp) then
        call eph_electrons_output
      end if
      return
    end subroutine eph_do

!============================================================================
!============================================================================


!============================================================================
!============================================================================

    !subroutine eph_gen_eliashbergfun( evalp, ip)
    !  use m_getunit
    !  use m_lorentzfit
    !  real(8) :: evalp( eph_fst:eph_lst) ! eigenvalues at target point p (shifted by Fermi energy)
    !  integer :: ip                      ! index of target point p

    !  integer :: imode, ist, jst, ie, iw, iq, iqp, un, Nox, Noy, ns
    !  real(8) :: fac, t0, t1, ti, tp, te, ts, tio, e
    !  real(8) :: wgtp( 3*natmtot, eph_qset%nkpt, eph_a2f_pfset%nomeg), wq( 3*natmtot, eph_qset%nkpt)
    !  real(8) :: wgte( eph_nst, eph_qset%nkpt, eph_a2f_efset%nomeg), eqp( eph_nst, eph_qset%nkpt)
    !  real(8) :: auxmat( eph_a2f_efset%nomeg, 3*natmtot, eph_qset%nkpt)
    !  real(8), allocatable :: px(:,:), py(:,:), c(:,:), a2f(:,:), a2fs(:,:)
    !  character( 256) :: fname

    !  if( .not. allocated( eph_eliashbergfun)) then
    !    allocate( eph_eliashbergfun( eph_a2f_Nbe+2, eph_a2f_Nbp+2, eph_fst:eph_lst, eph_pset%nkpt))
    !    eph_eliashbergfun = 0.d0
    !  end if

    !  do iq = 1, eph_qset%nkpt
    !    wq( :, iq) = eph_phfreq( :, iq)
    !    iqp = eph_iq2iqp( iq)        ! from non-reduced q-set to non-reduced q+p-set
    !    !iqp = eph_qqpset%kqmtset%ik2ikp( iqp)    ! from non-reduced q+p-set to reduced q+p-set
    !    eqp( :, iq) = eph_evalqp( :, iqp)
    !  end do

    !  write(*,'("nkpt:",2i)') eph_qset%nkpt, eph_qset%nkptnr

    !  call timesec( t1)
    !  call opt_tetra_init( eph_tetra, eph_qset, tetra_type=2, reduce=.true.)
    !  call timesec( t0)
    !  ti = t0 - t1
    !  call opt_tetra_int( eph_tetra, 2, eph_qset%nkpt, 3*natmtot, wq, eph_a2f_pfset%freqs, eph_a2f_pfset%nomeg, wgtp)
    !  call timesec( t1)
    !  tp = t1 - t0
    !  call opt_tetra_int( eph_tetra, 2, eph_qset%nkpt, eph_nst, eqp, eph_a2f_efset%freqs, eph_a2f_efset%nomeg, wgte)
    !  !wgte = wgte*2.d0*eph_qset%nkpt
    !  do iq = 1, eph_qset%nkpt
    !    wgte( :, iq, :) = 2.d0*wgte( :, iq, :)/eph_qset%wkpt(iq) ! 2 for spin
    !  end do
    !  call timesec( t0)
    !  te = t0 - t1

    !  allocate( a2f( eph_a2f_efset%nomeg, eph_a2f_pfset%nomeg))
    !  allocate( a2fs( eph_a2f_efset%nomeg, eph_a2f_pfset%nomeg))
    !  write(*,*) 'start loop'
    !  do ist = eph_fst, eph_lst
    !    auxmat = 0.d0
    !    a2f = 0.d0
    !    do iq = 1, eph_qset%nkpt
    !      !iqp = eph_qqpset%ik2ikqmt_nr( iq)        ! from non-reduced q-set to non-reduced q+p-set
    !      !iqp = eph_qqpset%kqmtset%ik2ikp( iqp)    ! from non-reduced q+p-set to reduced q+p-set
    !      ! CHECK THIS IF NEEDED
    !      iqp = eph_iq2iqp( iq)
    !      do imode = 1, 3*natmtot
    !        do ie = 1, eph_a2f_efset%nomeg
    !          do jst = eph_fst, eph_lst
    !            auxmat( ie, imode, iq) = auxmat( ie, imode, iq) + abs( eph_ephmat( ist, jst, imode, iqp))**2*wgte( jst-eph_fst+1, iq, ie)
    !          end do
    !        end do
    !      end do
    !    end do
    !    
    !    call dgemm( 'n', 'n', eph_a2f_efset%nomeg, eph_a2f_pfset%nomeg, 3*natmtot*eph_qset%nkpt, 1.d0, &
    !           auxmat, eph_a2f_efset%nomeg, &
    !           wgtp, 3*natmtot*eph_qset%nkpt, 0.d0, &
    !           a2f, eph_a2f_efset%nomeg)

    !    write( fname, '("ELIASHBERG_P",i3.3,"_B",i3.3)') ip, ist
    !    call writematlab( cmplx( a2f, 0.d0, 8), fname)
    !    
    !    ! Lorentzify
    !    write(*,*) "Lorentzification", ist
    !    ns = -1
    !    e = 1.d0
    !    do while( (e .gt. 0.1) .and. (ns .lt. 10))
    !      call multipleLorentzFit2D( eph_a2f_efset%freqs, eph_a2f_pfset%freqs, a2f, &
    !                                 eph_a2f_efset%nomeg, eph_a2f_pfset%nomeg, px, py, c, Nox, Noy, e, &
    !                                 epsRel=1.d-4, Nbasex=eph_a2f_Nbe, Nbasey=eph_a2f_Nbp, Nred=50, epsLD=1.d-3)
    !      call savgol2( eph_a2f_efset%nomeg, eph_a2f_efset%freqs, &
    !                    eph_a2f_pfset%nomeg, eph_a2f_pfset%freqs, &
    !                    a2f, 1, 1, 1, 1, 0, 0, a2fs)
    !      a2f = a2fs
    !      ns = ns + 1
    !    end do
    !    eph_eliashbergfun( 1:eph_a2f_Nbe, 1:eph_a2f_Nbp, ist, ip) = c
    !    eph_eliashbergfun( 1:eph_a2f_Nbe, (eph_a2f_Nbp+1):(eph_a2f_Nbp+2), ist, ip) = transpose( px)
    !    eph_eliashbergfun( (eph_a2f_Nbe+1):(eph_a2f_Nbe+2), 1:eph_a2f_Nbp, ist, ip) = py
    !    write(*,*) e, ns, Nox, Noy
    !  end do
    !    
    !  call timesec( t1)
    !  ts = t1 - t0
    !  call eph_write_eliashberg( ip, eph_eliashbergfun(:,:,:,ip))
    !  call writematlab( cmplx( reshape( eph_a2f_efset%freqs, (/eph_a2f_efset%nomeg, 1/)), 0.d0, 8), 'efreq')
    !  call writematlab( cmplx( reshape( eph_a2f_pfset%freqs, (/eph_a2f_pfset%nomeg, 1/)), 0.d0, 8), 'pfreq')
    !  call timesec( t0)
    !  tio = t0 - t1

    !  write(*,'("initialization:   ",f13.6)') ti
    !  write(*,'("phonon weights:   ",f13.6)') tp
    !  write(*,'("electron weights: ",f13.6)') te
    !  write(*,'("summation:        ",f13.6)') ts
    !  write(*,'("I/O:              ",f13.6)') tio
    !end subroutine eph_gen_eliashbergfun

!============================================================================
!============================================================================

    !subroutine eph_write_eliashberg( ip, a2f)
    !  integer, intent( in) :: ip
    !  real(8), intent( in) :: a2f( eph_a2f_Nbe+2, eph_a2f_Nbp+2, eph_fst:eph_lst)

    !  integer :: un, recl, ist
    !  character(256) :: fname

    !  if( mpiglobal%rank .gt. 0) return

    !  inquire( iolength=recl) eph_pset%vkl( :, ip), eph_a2f_Nbe, eph_a2f_Nbp, eph_fst, eph_lst, a2f
    !  call getunit( un)
    !  open( un, file="ELIASHBERG"//trim( filext), action='write', form='unformatted', access='direct', recl=recl)
    !  write( un, rec=ip) eph_pset%vkl( :, ip), eph_a2f_Nbe, eph_a2f_Nbp, eph_fst, eph_lst, a2f
    !  close( un)
    !  do ist = eph_fst, eph_lst
    !    write( fname, '("ELIASHBERGL_P",i3.3,"_B",i3.3)') ip, ist
    !    call writematlab( cmplx( a2f( :, :, ist), 0.d0, 8), fname)
    !  end do
    !  return
    !end subroutine eph_write_eliashberg

    !subroutine eph_read_eliashberg( ip, a2f, success)
    !  integer, intent( in) :: ip
    !  real(8), intent( out) :: a2f( eph_a2f_Nbe+2, eph_a2f_Nbp+2, eph_fst:eph_lst)
    !  logical, intent( out) :: success

    !  integer :: un, recl, Nbe, Nbp, fst, lst
    !  real(8) :: vl(3)

    !  success = .false.
    !  if( mpiglobal%rank .gt. 0) return

    !  inquire( file="ELIASHBERG"//trim( filext), exist=success)
    !  if( .not. success) then
    !    write(*,*)
    !    write(*,'("Error (eph_read_eliashberg): File ELIASHBERG'//trim( filext)//' does not exist.")')
    !    return
    !  end if

    !  inquire( iolength=recl) vl, Nbe, Nbp, fst, lst, a2f
    !  call getunit( un)
    !  open( un, file="ELIASHBERG"//trim( filext), action='read', form='unformatted', access='direct', recl=recl)
    !  read( un, rec=1) vl, Nbe, Nbp, fst, lst
    !  
    !  if( Nbe .ne. eph_a2f_Nbe) then
    !    write(*,*)
    !    write(*,'("Error( eph_read_eliashberg): Different number of electron basis functions.")')
    !    write(*,'(" current       :",i)') eph_a2f_Nbe
    !    write(*,'(" ELIASHBERG'//trim( filext)//':",i)') Nbe
    !    stop
    !  end if
    !  if( Nbp .ne. eph_a2f_Nbp) then
    !    write(*,*)
    !    write(*,'("Error( eph_read_eliashberg): Different number of phonon basis functions.")')
    !    write(*,'(" current       :",i)') eph_a2f_Nbp
    !    write(*,'(" ELIASHBERG'//trim( filext)//':",i)') Nbp
    !    stop
    !  end if
    !  if( (fst .ne. eph_fst) .or. (lst .ne. eph_lst)) then
    !    write(*,*)
    !    write(*,'("Error( eph_read_eliashberg): Different band ranges.")')
    !    write(*,'(" current       :",2i)') eph_fst, eph_lst
    !    write(*,'(" ELIASHBERG'//trim( filext)//':",2i)') fst, lst
    !    stop
    !  end if

    !  read( un, rec=ip) vl, Nbe, Nbp, fst, lst, a2f
    !  if( abs( norm2( vl - eph_pset%vkl( :, ip))) .gt. input%structure%epslat) then
    !    write(*,*)
    !    write(*,'("Error( eph_read_eliashberg): Differing vectors for p-point ",i)') ip
    !    write(*,'(" current       :",3g18.10)') eph_pset%vkl( :, ip)
    !    write(*,'(" ELIASHBERG'//trim( filext)//':",3g18.10)') vl
    !    stop
    !  end if
    !  close( un)
    !  success = .true.
    !  return
    !end subroutine eph_read_eliashberg
end module mod_eph
