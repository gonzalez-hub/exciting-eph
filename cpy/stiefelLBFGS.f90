module mod_stiefel_lbfgs
  use m_linalg
  use m_plotmat

  implicit none

  private
  
  complex(8), parameter :: zero = cmplx( 0.d0, 0.d0, 8)
  complex(8), parameter ::  one = cmplx( 1.d0, 0.d0, 8)
  complex(8), parameter ::   ii = cmplx( 0.d0, 1.d0, 8)

  integer :: N   ! dimension of full space (number of rows)
  integer :: P   ! dimension of subspace (number of columns)
  integer :: K   ! number of N x P matrices Y
  integer :: LDY ! leading dimension of Y

  ! default settings
  real(8) :: eps0  = 1.d-14  ! numerical zero
  real(8) :: eps   = 1.d-10  ! tolerance for gradient
  integer :: itmax = 1000    ! maximum number of iterations 
  integer :: nhist = 5       ! history length

  complex(8), allocatable :: G(:,:,:) ! the gradients
  complex(8), allocatable :: H(:,:,:) ! the search direction
  complex(8), allocatable :: Q(:,:,:) ! Q of QR decomposition
  complex(8), allocatable :: R(:,:,:) ! R of QR decomposition

  complex(8), allocatable :: S(:,:,:,:) ! history of updates
  complex(8), allocatable :: T(:,:,:,:) ! history of gradients
  complex(8), allocatable :: rho(:)     ! history of products <T;S>

  real(8), allocatable    :: expe(:,:)   ! eigenvvalues for exponential
  complex(8), allocatable :: expv(:,:,:) ! eigenvectors for exponential

  abstract interface
    subroutine fun_interface( n, p, k, ldy, y, f)
      integer, intent( in)    :: n, p, k, ldy
      complex(8), intent( in) :: y(:,:,:)
      real(8), intent( out)   :: f
    end subroutine
  end interface

  abstract interface
    subroutine grad_interface( n, p, k, ldy, y, g)
      integer, intent( in)     :: n, p, k, ldy
      complex(8), intent( in)  :: y(:,:,:)
      complex(8), intent( out) :: g(n,p,k)
    end subroutine
  end interface

  public :: stiefel_lbfgs

  contains
    subroutine stiefel_lbfgs( n_, p_, k_, ldy_, Y, fun, grad, &
                              tol, maxit, hist)
      integer, intent( in)       :: n_, p_, k_, ldy_
      complex(8), intent( inout) :: Y(:,:,:)         ! on entry: initial guess for the matrices Y, on exit: the minimizer matrices Y
      procedure( fun_interface)  :: fun              ! subroutine fun(Y,f) that computes the objective function f
      procedure( grad_interface) :: grad             ! subroutine grad(Y,G) that computes the gradient G of the objective function
      real(8), optional, intent( inout) :: tol       ! tolerance for norm of gradient
      integer, optional, intent( in)    :: maxit     ! maximum number of iterations
      integer, optional, intent( in)    :: hist      ! history length

      integer :: it, ik, dim
      real(8) :: ngrad, f, alpha
      complex(8) :: z1, z2, A(p_,p_), auxmat(n_,p_), q_(n_,n_), r_(n_,p_), MN(2*p_,2*p_), G0(n_,p_,k_)

      ! sanity checks
      if( n_ .le. 0) then
        write(*,*)
        write(*,'("Error (stiefel_optim): The dimension N must be positive")')
        stop
      end if
      if( p_ .le. 0) then
        write(*,*)
        write(*,'("Error (stiefel_optim): The dimension P must be positive")')
        stop
      end if
      if( n_ .lt. p_) then
        write(*,*)
        write(*,'("Error (stiefel_optim): The dimension N must not be smaller than P!")')
        stop
      end if
      if( k_ .le. 0) then
        write(*,*)
        write(*,'("Error (stiefel_optim): The number of matrices K must be positive")')
        stop
      end if
      if( ldy_ .lt. n_) then
        write(*,*)
        write(*,'("Error (stiefel_optim): The leading dimension LDY must not be smaller than N!")')
        stop
      end if

      if( present( tol)) eps = tol
      if( present( maxit)) itmax = max( 1, maxit)
      if( present( hist)) nhist = max( 1, hist)

      N = n_
      P = p_
      K = k_
      LDY = ldy_
      dim = P*(N-P) + P*(P-1)/2
      call destroy

      !write(*,'("N = ",i2)') N
      !write(*,'("P = ",i2)') P
      !write(*,'("K = ",i2)') K
      allocate( G(N,P,K), H(N,P,K), Q(N,P,K), R(P,P,K))
      allocate( S(N,P,K,nhist), T(N,P,K,nhist), rho(nhist))
      allocate( expe(2*P,K), expv(2*P,2*P,K))

      ! INITIALIZATION
      it = 0
      call gradient( Y, grad)              ! initialize gradient
      call innerp( G, G, Y, z1)  
      ngrad = dsqrt( dble( z1))/dble(K)    ! norm of gradient
      H = -G                               ! initialize search direction
      call fun( N, P, K, LDY, Y, f)
      write(*,'("ITERATION ",i4,2g26.16)') it, f, ngrad
      !do ik = 1, K
      !  write(*,'("Y",i2)') ik
      !  call plotmat( Y(:,:,ik))
      !  write(*,*)
      !end do

      ! LOOP
      !do while( (it .lt. itmax) .and. (ngrad .gt. eps))
      !  it = it + 1
      !  ! get eigendecomposition for exponential
      !  do ik = 1, K
      !    !call writematlab( G(:,:,ik), 'G')
      !    !call writematlab( H(:,:,ik), 'H')
      !    call zgemm( 'c', 'n', P, P, N, one, Y(1,1,ik), LDY, H(1,1,ik), N, zero, A, P)
      !    A = cmplx( 0.5d0, 0.d0, 8)*(A - conjg( transpose( A)))
      !    !call writematlab( H(:,:,ik), 'A')
      !    auxmat = H(:,:,ik)
      !    call zgemm( 'n', 'n', N, P, P, -one, Y(1,1,ik), LDY, A, P, one, auxmat, N)
      !    call zqr( auxmat, q=q_, r=r_)
      !    Q(:,:,ik) = q_(:,1:P)
      !    R(:,:,ik) = r_(1:P,:)
      !    !call writematlab( Q(:,:,ik), 'Q')
      !    !call writematlab( R(:,:,ik), 'R')
      !    MN = zero
      !    MN(  1:P,    1:P)   = A
      !    MN(P+1:2*P,  1:P)   = R(:,:,ik)
      !    MN(  1:P,  P+1:2*P) = -conjg( transpose( R(:,:,ik)))
      !    MN = cmplx( 0.d0, -1.d0, 8)*MN ! -iMN is Hermitian
      !    call zhediag( MN, expe(:,ik), evec=expv(:,:,ik))
      !  end do

      !  alpha = 0.1d0
      !  if( itmax .eq. 1) alpha = 2.d0
      !  call linesearch( Y, fun, grad, alpha)
      !  alpha = max( alpha, 1.d-1)
      !  call update( alpha, Y, dir=.true.) ! update Y 
      !  G0 = G
      !  call gradient( Y, grad)            ! update G
      !  call innerp( G-G0, G, Y, z2)
      !  H = -G + (z2/z1)*H                 ! update H
      !  call innerp( G, G, Y, z1)
      !  call fun( N, P, K, LDY, Y, f)
      !  ngrad = dsqrt( dble( z1))/dble(K)
      !  if( mod( it, dim) .eq. 0) H = -G
      !  write(*,'("ITERATION ",i4,3g26.16)') it, f, ngrad, alpha
      !  !do ik = 1, K
      !  !  write(*,'("Y",i2)') ik
      !  !  call plotmat( Y(:,:,ik))
      !  !  write(*,*)
      !  !end do
      !end do
      !write(*,'("ITERATION",i4,2g26.16)') it, f, ngrad
      if( present( tol)) tol = ngrad
      !do ik = 1, K
      !  write(*,'("Y",i2)') ik
      !  call plotmat( Y(:,:,ik))
      !  write(*,*)
      !end do
      call destroy
    end subroutine stiefel_lbfgs

    subroutine destroy
      if( allocated( G)) deallocate( G)
      if( allocated( H)) deallocate( H)
      if( allocated( Q)) deallocate( Q)
      if( allocated( R)) deallocate( R)

      if( allocated( S)) deallocate( S)
      if( allocated( T)) deallocate( T)
      if( allocated( rho)) deallocate( rho)

      if( allocated( expe)) deallocate( expe)
      if( allocated( expv)) deallocate( expv)
    end subroutine destroy

    subroutine update( alpha, Y, dir)
      real(8), intent( in)       :: alpha
      complex(8), intent( inout) :: Y(:,:,:)
      logical, optional, intent( in) :: dir

      integer :: ik, i
      logical :: d
      complex(8) :: auxmat1(N,2*P), auxmat2(N,2*P)

      d = .false.
      if( present( dir)) d = dir

      do ik = 1, K
        if( d) then
          auxmat1(:,1:P) = H(:,:,ik)
          call zgemm( 'n', 'c', N, P, P, -one, Y(1,1,ik), LDY, R(1,1,ik), P, zero, auxmat1(1,P+1), N)
          call zgemm( 'n', 'n', N, 2*P, 2*P, one, auxmat1, N, expv(1,1,ik), 2*P, zero, auxmat2, N)
          do i = 1, 2*P
            auxmat2(:,i) = exp( ii*alpha*expe(i,ik))*auxmat2(:,i)
          end do
          call zgemm( 'n', 'c', N, P, 2*P, one, auxmat2, N, expv(1,1,ik), 2*P, zero, H(1,1,ik), N)
        end if

        auxmat1(:,1:P) = Y(1:N,1:P,ik)
        auxmat1(:,P+1:2*P) = Q(:,:,ik)
        call zgemm( 'n', 'n', N, 2*P, 2*P, one, auxmat1, N, expv(1,1,ik), 2*P, zero, auxmat2, N)
        do i = 1, 2*P
          auxmat2(:,i) = exp( ii*alpha*expe(i,ik))*auxmat2(:,i)
        end do
        call zgemm( 'n', 'c', N, P, 2*P, one, auxmat2, N, expv(1,1,ik), 2*P, zero, Y(1,1,ik), LDY)
      end do
      return
    end subroutine update

    subroutine gradient( Y, grad)
      complex(8), intent( in)    :: Y(:,:,:)
      procedure( grad_interface) :: grad

      integer :: ik
      complex(8) :: auxmat(P,P)

      call grad( N, P, K, LDY, Y, G)
      do ik = 1, K
        call zgemm( 'c', 'n', P, P, N,  one, G(1,1,ik), N, Y(1,1,ik), LDY, zero, auxmat, P)
        call zgemm( 'n', 'n', N, P, P, -one, Y(1,1,ik), LDY, auxmat, P, one, G(1,1,ik), N)
      end do
      return
    end subroutine gradient

    subroutine innerp( a, b, Y, z)
      complex(8), intent( in)  :: a(N,P,K), b(N,P,K)
      complex(8), intent( in)  :: Y(:,:,:)
      complex(8), intent( out) :: z

      integer :: ik, i
      complex(8) :: zdotc, auxmat1(P,P), auxmat2(N,P)

      z = zero
      do ik = 1, K
        auxmat2 = b(:,:,ik)
        call zgemm( 'c', 'n', P, P, N, one, Y(1,1,ik), LDY, auxmat2, N, zero, auxmat1, P)
        call zgemm( 'n', 'n', N, P, P, cmplx( -0.5d0, 0.d0, 8), Y(1,1,ik), LDY, auxmat1, P, one, auxmat2, N)
        z = z + zdotc( N*P, a(1,1,ik), 1, auxmat2, 1)
      end do
    end subroutine innerp
end module mod_stiefel_lbfgs
