MODULE mod_opt_tetra
  use mod_kpointset
  use modmpi
  implicit none
  private
  
  type tetra_set
      integer :: ntetra
      integer :: ttype
      logical :: isreduced
      real(8) :: wlsm( 4, 20)
      integer, allocatable :: tetra(:,:)
      real(8), allocatable :: tetwgt(:)
      logical :: initialized = .false.
  end type tetra_set

  real(8), save :: eps = 1.d-10

  public :: tetra_set, opt_tetra_init, opt_tetra_int, opt_tetra_int2, opt_tetra_int_ediff, opt_tetra_efermi, opt_tetra_get_wgt
CONTAINS
!
!-------------------------------------------------------------------------------------
subroutine opt_tetra_init( self, kset, tetra_type, reduce)
  !
  ! This routine set the corners and additional points for each tetrahedra
  !
  implicit none
  type( tetra_set), intent( out) :: self
  
  type( k_set), intent( in) :: kset
  integer, intent( in) :: tetra_type ! = 1 for Linear tetrahedron method
                                     ! = 2 for Optimized tetrahedron method
  logical, optional, intent( in) :: reduce

  ! local variables
  integer :: i1, i2, i3, itet, i, j, k, touch
  integer :: jk, nk1, nk2, nk3, ikv(3), nn, idx(4)
  integer :: ivvec( 3, 20, 6), divvec(4,4), ivvec0(4), tet(20)
  real(8) :: l(4), bvec2(3,3), bvec3(3,4)
  logical :: reduce_, exitloop

  integer, allocatable :: tetra(:,:), tetwgt(:), srt(:)

  reduce_ = .false.
  if( present( reduce)) reduce_ = reduce
  if( .not. kset%isreduced) reduce_ = .false.

  nk1 = kset%ngridk(1)
  nk2 = kset%ngridk(2)
  nk3 = kset%ngridk(3)
  
  ! Take the shortest diagonal line as the "shaft" of tetrahedral devision
  bvec2(:,1) = kset%bvec(:,1)/dble( nk1)
  bvec2(:,2) = kset%bvec(:,2)/dble( nk2)
  bvec2(:,3) = kset%bvec(:,3)/dble( nk3)
  
  bvec3(:,1) = -bvec2(:,1) + bvec2(:,2) + bvec2(:,3)
  bvec3(:,2) =  bvec2(:,1) - bvec2(:,2) + bvec2(:,3)
  bvec3(:,3) =  bvec2(:,1) + bvec2(:,2) - bvec2(:,3)
  bvec3(:,4) =  bvec2(:,1) + bvec2(:,2) + bvec2(:,3)
  
  do i = 1, 4
    l(i) = dot_product( bvec3( :, i), bvec3( :, i))
  end do
  
  i = minloc( l, 1)
  
  ivvec0 = (/ 0, 0, 0, 0 /)
  
  divvec(:,1) = (/ 1, 0, 0, 0 /)
  divvec(:,2) = (/ 0, 1, 0, 0 /)
  divvec(:,3) = (/ 0, 0, 1, 0 /)
  divvec(:,4) = (/ 0, 0, 0, 1 /)
  
  ivvec0(i) = 1
  divvec(i,i) = -1
  
  ! Devide a subcell into 6 tetrahedra
  itet = 0
  do i1 = 1, 3
    do i2 = 1, 3
      if(i2 == i1) cycle
      do i3 = 1, 3
        if(i3 == i1 .or. i3 == i2) cycle
        itet = itet + 1
        ivvec( :, 1, itet) = ivvec0( 1:3)
        ivvec( :, 2, itet) = ivvec( :, 1, itet) + divvec( 1:3, i1)
        ivvec( :, 3, itet) = ivvec( :, 2, itet) + divvec( 1:3, i2)
        ivvec( :, 4, itet) = ivvec( :, 3, itet) + divvec( 1:3, i3)
        !
      end do
    end do
  end do
  
  ! set weights for optimized tetrahedron method
  self%wlsm(1, 1: 4) = dble((/1440,    0,   30,    0/))
  self%wlsm(2, 1: 4) = dble((/   0, 1440,    0,   30/))
  self%wlsm(3, 1: 4) = dble((/  30,    0, 1440,    0/))
  self%wlsm(4, 1: 4) = dble((/   0,   30,    0, 1440/))
  !                                              
  self%wlsm(1, 5: 8) = dble((/ -38,    7,   17,  -28/))
  self%wlsm(2, 5: 8) = dble((/ -28,  -38,    7,   17/))
  self%wlsm(3, 5: 8) = dble((/  17,  -28,  -38,    7/))
  self%wlsm(4, 5: 8) = dble((/   7,   17,  -28,  -38/))
  !                                              
  self%wlsm(1, 9:12) = dble((/ -56,    9,  -46,    9/))
  self%wlsm(2, 9:12) = dble((/   9,  -56,    9,  -46/))
  self%wlsm(3, 9:12) = dble((/ -46,    9,  -56,    9/))
  self%wlsm(4, 9:12) = dble((/   9,  -46,    9,  -56/))
  !                                              
  self%wlsm(1,13:16) = dble((/ -38,  -28,   17,    7/))
  self%wlsm(2,13:16) = dble((/   7,  -38,  -28,   17/))
  self%wlsm(3,13:16) = dble((/  17,    7,  -38,  -28/))
  self%wlsm(4,13:16) = dble((/ -28,   17,    7,  -38/))
  !                                              
  self%wlsm(1,17:20) = dble((/ -18,  -18,   12,  -18/))
  self%wlsm(2,17:20) = dble((/ -18,  -18,  -18,   12/))
  self%wlsm(3,17:20) = dble((/  12,  -18,  -18,  -18/))
  self%wlsm(4,17:20) = dble((/ -18,   12,  -18,  -18/))
  !
  self%wlsm = self%wlsm/dble( 1260)

  if (tetra_type .eq. 1) then
    self%ttype = 1
    nn = 4
  else if(tetra_type .eq. 2) then
    self%ttype = 2
    nn = 20
  else
    self%ttype = 2
    write(*,*)
    write( *, '("Warning (opt_tetra_init): invalid tetra_type (",i3,")! I use type 2 (improved tetrahedron).")'), tetra_type
  end if
  
  ! construct tetrahedra
  self%ntetra = 6*nk1*nk2*nk3
  allocate( tetra( nn, self%ntetra), tetwgt( self%ntetra))

  self%ntetra = 0
  tetra = 0
  tetwgt = 1

  do i3 = 0, nk3-1
    do i2 = 0, nk2-1
      do i1 = 0, nk1-1

        do itet = 1, 6

          self%ntetra = self%ntetra + 1
          do i = 1, 4
            ikv = (/i1, i2, i3/) + ivvec( :, i, itet)
            ikv = modulo( ikv, kset%ngridk)
            tetra( i, self%ntetra) = kset%ikmap( ikv(1), ikv(2), ikv(3))
          end do
          if( self%ttype .eq. 2) then
            call opt_tetra_sort4( dble( tetra( 1:4, self%ntetra)), idx)
            tetra( 1:4, self%ntetra) = tetra( idx, self%ntetra)
            call opt_tetra_addCorners( ivvec( :, :, itet), order=idx)
            do i = 5, nn
              ikv = (/i1, i2, i3/) + ivvec( :, i, itet)
              ikv = modulo( ikv, kset%ngridk)
              tetra( i, self%ntetra) = kset%ikmap( ikv(1), ikv(2), ikv(3))
            end do
          end if

        end do

      end do
    end do
  end do

  if( reduce_) then
    allocate( srt( self%ntetra))
    call sortidxColumn( 4, self%ntetra, tetra( 1:4, :), srt)
    tetra = tetra( :, srt)
    k = self%ntetra
    self%ntetra = 1
    outer: do i = 2, k
      do j = 1, 4
        if( tetra( j, i) .ne. tetra( j, self%ntetra)) then
          self%ntetra = self%ntetra + 1
          tetra( :, self%ntetra) = tetra( :, i)
          cycle outer
        end if
      end do
      tetwgt( self%ntetra) = tetwgt( self%ntetra) + 1
    end do outer
    deallocate( srt)
  end if

  if (allocated( self%tetra)) deallocate( self%tetra)
  allocate( self%tetra( nn, self%ntetra))
  if (allocated( self%tetwgt)) deallocate( self%tetwgt)
  allocate( self%tetwgt( self%ntetra))

  self%tetra = tetra( :, 1:self%ntetra)
  self%tetwgt = dble( tetwgt( 1:self%ntetra))/dble( 6*nk1*nk2*nk3)
  self%initialized = .true.

  deallocate( tetra, tetwgt)
  return

  contains
    subroutine opt_tetra_addCorners( ivvec, order)
      integer, intent( inout) :: ivvec( 3, 20)
      integer, optional, intent( in) :: order(4)

      integer :: o(4)
      
      o = (/1, 2, 3, 4/)
      if( present( order)) o = order

      ivvec( :,  5) = 2*ivvec( :, o(1)) - ivvec( :, o(2))
      ivvec( :,  6) = 2*ivvec( :, o(2)) - ivvec( :, o(3))
      ivvec( :,  7) = 2*ivvec( :, o(3)) - ivvec( :, o(4))
      ivvec( :,  8) = 2*ivvec( :, o(4)) - ivvec( :, o(1))
      
      ivvec( :,  9) = 2*ivvec( :, o(1)) - ivvec( :, o(3))
      ivvec( :, 10) = 2*ivvec( :, o(2)) - ivvec( :, o(4))
      ivvec( :, 11) = 2*ivvec( :, o(3)) - ivvec( :, o(1))
      ivvec( :, 12) = 2*ivvec( :, o(4)) - ivvec( :, o(2))
      
      ivvec( :, 13) = 2*ivvec( :, o(1)) - ivvec( :, o(4))
      ivvec( :, 14) = 2*ivvec( :, o(2)) - ivvec( :, o(1))
      ivvec( :, 15) = 2*ivvec( :, o(3)) - ivvec( :, o(2))
      ivvec( :, 16) = 2*ivvec( :, o(4)) - ivvec( :, o(3))
      
      ivvec( :, 17) =   ivvec( :, o(4)) - ivvec( :, o(1)) + ivvec( :, o(2))
      ivvec( :, 18) =   ivvec( :, o(1)) - ivvec( :, o(2)) + ivvec( :, o(3))
      ivvec( :, 19) =   ivvec( :, o(2)) - ivvec( :, o(3)) + ivvec( :, o(4))
      ivvec( :, 20) =   ivvec( :, o(3)) - ivvec( :, o(4)) + ivvec( :, o(1))

      return
    end subroutine opt_tetra_addCorners
end subroutine opt_tetra_init

!----------------------------------------------------------------------------
subroutine opt_tetra_get_wgt( inttype, et, e, wgt, et2, e2)
  !--------------------------------------------------------------------------
  ! integration types
  !    1: theta( e-et)
  !    2: delta( e-et)
  !    3: delta( e1-et1)*delta( e2-et2)
  integer, intent( in)           :: inttype ! type on integral
  real(8), intent( in)           :: et(4)   ! energies at corners of tetrahedron
  real(8), intent( in)           :: e       ! energy/frequency to evaluate at
  real(8), intent( out)          :: wgt(4)  ! integration weigths
  real(8), optional, intent( in) :: et2(4)  ! optional second set of energies at corners
  real(8), optional, intent( in) :: e2      ! energy/frequency to evaluate at

  integer :: i, j, k, idx(4), idx2(4), n
  real(8) :: a(4,4), b(3), c(4), es(4), es2(4), de, p(3,4), s, w(3)
  real(8) :: de1(3), de2(3), m2(2,2), al(2), be(2), lim(2), ivl1(2), ivl2(2)

  real, external :: r3dot

  call opt_tetra_sort4( et, idx)
  es = et( idx)
  !es = et
  es2 = es
  if( present( et2)) es2 = et2( idx)
  !if( present( et2)) es2 = et2

  a = 0.d0
  wgt = 0.d0

  if( inttype .eq. 1) then

    if( e .le. es(1)) return
    if( es(4) .le. e) then
      wgt = 0.25d0
      return
    end if

    do i = 1, 4
      do j = i+1, 4
        de = es(j) - es(i)
        if( de .gt. eps) then
          a(i,j) = (es(j) - e)/de
          a(j,i) = 1.d0 - a(i,j)
        end if
      end do
    end do
    if( (es(1) .lt. e) .and. (e .lt. es(2))) then
      b(1) = a(2,1)*a(3,1)*a(4,1)*0.25d0
      wgt( idx(1)) = b(1)*(1.d0 + a(1,2) + a(1,3) + a(1,4))
      wgt( idx(2)) = b(1)*a(2,1)
      wgt( idx(3)) = b(1)*a(3,1)
      wgt( idx(4)) = b(1)*a(4,1)
    else if( (es(2) .le. e) .and. (e .lt. es(3))) then
      b(1) = a(4,1)*a(3,1)*a(2,4)*0.25d0
      b(2) = a(3,2)*a(4,2)*0.25d0
      b(3) = a(2,3)*a(3,1)*a(4,2)*0.25d0
      wgt( idx(1)) = b(1)*(1.d0 + a(1,3) + a(1,4)) + b(2)                          + b(3)*(1.d0 + a(1,3))
      wgt( idx(2)) = b(1)*a(2,4)                   + b(2)*(1.d0 + a(2,3) + a(2,4)) + b(3)*(a(2,3) + a(2,4))
      wgt( idx(3)) = b(1)*a(3,1)                   + b(2)*a(3,2)                   + b(3)*(a(3,1) + a(3,2))
      wgt( idx(4)) = b(1)*(a(4,1) + a(4,2))        + b(2)*a(4,2)                   + b(3)*a(4,2)
    else if( (es(3) .le. e) .and. (e .lt. es(4))) then
      b(1) = a(4,3)*0.25d0
      b(2) = a(3,4)*a(4,2)*0.25d0
      b(3) = a(3,4)*a(2,4)*a(4,1)*0.25d0
      wgt( idx(1)) = b(1)                 + b(2)                   + b(3)*(1.d0 + a(1,4))
      wgt( idx(2)) = b(1)                 + b(2)*(1.d0 + a(2,4))   + b(3)*a(2,4)
      wgt( idx(3)) = b(1)*(1.d0 + a(3,4)) + b(2)*a(3,4)            + b(3)*a(3,4)
      wgt( idx(4)) = b(1)*a(4,3)          + b(2)*(a(4,2) + a(4,3)) + b(3)*(a(4,1) + a(4,2) + a(4,3))
    end if

  else if( inttype .eq. 2) then

    if( (e .le. es(1)) .or. (e .ge. es(4))) return

    do i = 1, 4
      do j = i+1, 4
        de = es(j) - es(i)
        if( de .gt. eps) then
          a(i,j) = (es(j) - e)/de
          a(j,i) = 1.d0 - a(i,j)
        end if
      end do
    end do
    if( (es(1) .lt. e) .and. (e .lt. es(2))) then
      if( abs( es(4) - es(1)) .gt. eps) b(1) = a(2,1)*a(3,1)/(es(4) - es(1))
      wgt( idx(1)) = b(1)*(a(1,2) + a(1,3) + a(1,4))
      wgt( idx(2)) = b(1)*a(2,1)
      wgt( idx(3)) = b(1)*a(3,1)
      wgt( idx(4)) = b(1)*a(4,1)
    else if( (es(2) .le. e) .and. (e .lt. es(3))) then
      if( abs( es(4) - es(1)) .gt. eps) b(1) = a(3,1)*a(2,3)/(es(4) - es(1))
      if( abs( es(4) - es(2)) .gt. eps) b(2) = a(3,2)*a(1,4)/(es(4) - es(2))
      wgt( idx(1)) = b(1)*a(4,1)            + b(2)*(a(4,2) + a(4,1))
      wgt( idx(2)) = b(1)*(a(3,2) + a(3,1)) + b(2)*a(3,2)
      wgt( idx(3)) = b(1)*a(2,3)            + b(2)*(a(2,4) + a(2,3))
      wgt( idx(4)) = b(1)*(a(1,4) + a(1,3)) + b(2)*a(1,4)
    else if( (es(3) .le. e) .and. (e .lt. es(4))) then
      if( abs( es(4) - es(3)) .gt. eps) b(1) = a(1,4)*a(2,4)/(es(4) - es(3))
      wgt( idx(1)) = b(1)*a(1,4)
      wgt( idx(2)) = b(1)*a(2,4)
      wgt( idx(3)) = b(1)*a(3,4)
      wgt( idx(4)) = b(1)*(a(4,1) + a(4,2) + a(4,3))
    end if

  else if( (inttype .eq. 3)) then

    if( (e .le. es(1)) .or. (e .ge. es(4)) .or. (e2 .le. minval( es2)) .or. (e2 .ge. maxval( es2))) return
    de1 =  es(2:4) -  es(1)
    de2 = es2(2:4) - es2(1)
    m2(:,1) = (/ de2(3), -de2(2)/)
    m2(:,2) = (/-de1(3),  de1(2)/)
    m2 = m2/(de1(2)*de2(3) - de1(3)*de2(2))
    al = matmul( m2, (/e-es(1), e2-es2(1)/))
    be = matmul( m2, (/de1(1), de2(1)/))
    ivl1 = 0.d0
    if( be(1) .gt. 0.d0) then
      ivl1(2) = al(1)/be(1)
      if( be(1) .gt. 1.d0) ivl1(1) = max( 0.d0, (1.d0-al(1))/(1.d0-be(1)))
    else
      ivl1(1) = al(1)/be(1)
      ivl1(2) = min( 1.d0, (1.d0-al(1))/(1.d0-be(1)))
    end if
    ivl2 = 0.d0
    if( be(2) .gt. 0.d0) then
      ivl2(2) = al(2)/be(2)
      if( be(2) .gt. 1.d0-be(1)) ivl2(1) = max( 0.d0, (1.d0-al(1)-al(2))/(1.d0-be(1)-be(2)))
    else
      ivl2(1) = al(2)/be(2)
      ivl2(1) = min( 1.d0, (1.d0-al(1)-al(2))/(1.d0-be(1)-be(2)))
    end if
    lim(1) = max( ivl1(1), ivl2(1))
    lim(2) = min( ivl1(2), ivl2(2))
    if( lim(2) - lim(1) .lt. 1.d-16) return
    wgt( idx(1)) = (lim(2)-lim(1))*(1-al(1)-al(2) - 0.5d0*(lim(2)+lim(1))*(1.d0-be(1)-be(2)))
    wgt( idx(2)) = 0.5d0*(lim(2)**2 - lim(1)**2)
    wgt( idx(3)) = (lim(2)-lim(1))*(al(1) - 0.5d0*(lim(2)+lim(1))*be(1))
    wgt( idx(4)) = (lim(2)-lim(1))*(al(2) - 0.5d0*(lim(2)+lim(1))*be(2))
    wgt = wgt*6.d0

!    de1(1) = e
!    de1(2:4) = es(2:4)
!    de1 = de1-es(1)
!    de2(1) = e2
!    de2(2:4) = es2(2:4)
!    de2 = de2-es2(1)
!    !write(*,'("E",4f13.6)') de1
!    !write(*,'("W",4f13.6)') de2
!    a = 0.d0
!    do i = 1, 4
!      do j = i+1, 4
!        a(i,j) = de1(i)*de2(j) - de1(j)*de2(i)
!        a(j,i) = -a(i,j)
!      end do
!    end do
!    lo = 0.d0
!    up = 1.d0
!    i = 1; j = 1
!    s = a(1,3)/a(2,3)
!    if( a(2,3)/a(3,4) .gt. 0.d0) then
!      i = i + 1
!      lo(i) = s
!    else
!      j = j + 1
!      up(j) = s
!    end if
!    s = a(1,4)/a(2,4)
!    if( a(2,4)/a(4,3) .gt. 0.d0) then
!      i = i + 1
!      lo(i) = s
!    else
!      j = j + 1
!      up(j) = s
!    end if
!    s = (a(3,4) + a(4,1) + a(1,3))/(a(3,4) + a(4,2) + a(2,3))
!    if( (a(3,4) + a(4,2) + a(2,3))/a(3,4) .lt. 0.d0) then
!      i = i + 1
!      lo(i) = s
!    else
!      j = j + 1
!      up(j) = s
!    end if
!    s = (a(3,4) + a(4,1))/(a(3,4) + a(4,2))
!    if( (a(3,4) + a(4,2))/a(3,4) .lt. 0.d0) then
!      i = i + 1
!      lo(i) = s
!    else
!      j = j + 1
!      up(j) = s
!    end if
!    lim(1,1) = maxval( lo)
!    lim(2,1) = minval( up)
!    !write(*,'(5f13.6)') lim(:,1)
!
!    lo = 0.d0
!    up = 1.d0
!    i = 1; j = 1
!    j = j + 1
!    up(j) = (de1(4)-de1(1))/(de1(4)-de1(2))
!    i = i + 1
!    lo(i) = (de1(3)-de1(1))/(de1(3)-de1(2))
!    s = (a(3,4) + a(4,1) + a(1,3))/(a(3,4) + a(4,2) + a(2,3))
!    if( (a(3,4) + a(4,2) + a(2,3))/(de2(4)*(de1(4)-de1(3))) .gt. 0) then
!      i = i + 1
!      lo(i) = s
!    else
!      j = j + 1
!      up(j) = s
!    end if
!    lim(1,2) = maxval( lo)
!    lim(2,2) = minval( up)
!    !write(*,'(5f13.6)') lim(:,2)
!
!    lo = 0.d0
!    up = 1.d0
!    i = 1; j = 1
!    j = j + 1
!    up(j) = de1(1)/de1(2)
!    j = j + 1
!    up(j) = (de1(3) - de1(1))/(de1(3) - de1(2))
!    s = a(1,3)/a(2,3)
!    if( a(2,3)/(de1(3)*de2(4)) .lt. 0) then
!      i = i + 1
!      lo(i) = s
!    else
!      j = j + 1
!      up(j) = s
!    end if
!    lim(1,3) = maxval( lo)
!    lim(2,3) = minval( up)
!    !write(*,'(5f13.6)') lim(:,3)
!
!    d1 = 0.d0; d2 = 0.d0
!    do i = 1, 3
!      if( lim(2,i) .le. lim(1,i)) cycle
!      d1(i) = lim(2,i) - lim(1,i)
!      d2(i) = 0.5d0*(lim(2,i)**2 - lim(1,i)**2)
!      !write(*,'(i,2f13.6)') i, d1(i), d2(i)
!    end do
!
!    s = sign( 1.d0, a(3,4))
!    c(1)=  s*d1(1)/a(3,4) + d1(2)/((de1(4)-de1(3))*abs(de2(4))) - d1(3)/(de1(3)*abs(de2(4)))
!    c(2)=  s*d2(1)/a(3,4) + d2(2)/((de1(4)-de1(3))*abs(de2(4))) - d2(3)/(de1(3)*abs(de2(4)))
!    c(3)=  s*(a(1,4)*d1(1)-a(2,4)*d2(1))/a(3,4)**2 + &
!            ((de1(4)-de1(1))*d1(2)-(de1(4)-de1(2))*d2(2))/((de1(4)-de1(3))**2*abs(de2(4))) - &
!            (de1(1)*d1(3)-de1(2)*d2(3))/(de1(3)**2*abs(de2(4)))
!    c(4)= -s*(a(1,3)*d1(1)-a(2,3)*d2(1))/a(3,4)**2 + &
!            ((de1(3)-de1(1))*d1(2)-(de1(3)-de1(2))*d2(2))/((de1(4)-de1(3))**2*abs(de2(4)))
!    wgt( idx(1)) = c(1)-c(2)-c(3)-c(4)
!    wgt( idx(2)) = c(2)
!    wgt( idx(3)) = c(3)
!    wgt( idx(4)) = c(4)
!    !write(*,'(4f13.6)') wgt
  else
    write(*,*)
    write( *, '("Error (opt_tetra_get_wgt): Given integral type not yet supported.")')
    stop
  end if
  
  return
end subroutine opt_tetra_get_wgt

subroutine opt_tetra_sort4( a, i)
  real(8), intent( in)  :: a(4)
  integer, intent( out) :: i(4)

  integer :: t

  i = (/1, 2, 3, 4/)
  if( a(i(2)) .lt. a(i(1))) then
    i(1) = 2
    i(2) = 1
  end if
  if( a(i(4)) .lt. a(i(3))) then
    i(3) = 4
    i(4) = 3
  end if
  if( a(i(3)) .lt. a(i(1))) then
    t = i(1)
    i(1) = i(3)
    i(3) = t
  end if
  if( a(i(4)) .lt. a(i(2))) then
    t = i(2)
    i(2) = i(4)
    i(4) = t
  end if
  if( a(i(3)) .lt. a(i(2))) then
    t = i(2)
    i(2) = i(3)
    i(3) = t
  end if
end subroutine opt_tetra_sort4
!
!----------------------------------------------------------------------------
subroutine opt_tetra_efermi( self, nelec, nkp, nbnd, ebnd, ef, occ, ef0, df0)
  !----------------------------------------------------------------------------
  !
  ! calculate fermi energy by using bisection method
  !
  ! input
  type( tetra_set), intent( in) :: self
  real(8), intent(in)           :: nelec           ! number of electrons
  integer, intent(in)           :: nkp             ! # of k-points in irr-BZ
  integer, intent(in)           :: nbnd            ! # of bands
  real(8), intent(in)           :: ebnd( nbnd, nkp)  ! Kohn-Sham energies
  real(8), intent(in), optional :: ef0, df0
  ! output
  real(8), intent(out)          :: ef             ! the fermi energy
  real(8), intent(out)          :: occ( nbnd, nkp)  ! occupation numbers of each k

  ! local variables
  integer :: iter, maxiter = 500
  real(8) :: elw, eup, sumk, his(2,2)
  !
  ! find bounds for the fermi energy
  if( present( ef0) .and. present( df0)) then
    elw = ef0 - df0
    eup = ef0 + df0
  else
    elw = minval( ebnd)
    eup = maxval( ebnd)
  end if
  !
  ! bisection method
  do iter = 1, maxiter
    
    if( (iter .gt. 2) .and. ( abs( his(2,2) - his(2,1)) .gt. 1d-8)) then
      ! linear extrapolation instead for faster convergence (June 2018 SeTi)
      ef = (nelec - his(2,1))/(his(2,2) - his(2,1))*(his(1,2) - his(1,1)) + his(1,1)
      if( ef .lt. elw) elw = ef 
      if( ef .gt. eup) eup = ef
    else
      ef = (eup + elw)*0.5d0
    end if
    
    ! calc. # of electrons 
    call opt_tetra_int( self, 1, nkp, nbnd, ebnd, (/ef/), 1, occ)
    sumk = sum( occ)
    !write(*,'(i,5f23.16)') iter, ef, elw, eup, sumk, nelec
    his(1,1) = his(1,2)
    his(2,1) = his(2,2)
    his(1,2) = ef
    his(2,2) = sumk
    
    ! convergence check
    if( abs( sumk - nelec) .lt. 1.d-8) then
      exit
    else if( sumk < nelec) then
      elw = ef
    else
      eup = ef
    end if
    
  end do ! iter
  if (iter .ge. maxiter) write(6,*) "(opt_tetra_efermi) Not converged", iter
  !
end subroutine opt_tetra_efermi
!
!--------------------------------------------------------------------------------------
subroutine opt_tetra_int( self, inttype, nkp, nbnd, ebnd, omega, nomega, wgt)
  !------------------------------------------------------------------------------------
  !
  ! Calculatates integration weights for integrals of the type
  !     \[ \int_{BZ} d^3k A_{n,k} \delta( \omega - \epsilon_{n,k}) \]
  ! where $\delta$ is the Dirac delta for a set of energies (frequencies) $\omega$.
  ! The actual integral is given by the weighted sum
  !     \[ \sum_{k} A_{n,k} w_{n,k}(\omega) \]
  !
  ! integration types:
  !    1: theta
  !    2: delta
  
  type( tetra_set), intent( in) :: self
  integer, intent( in)          :: inttype, nkp, nbnd, nomega
  real(8), intent( in)          :: ebnd( nbnd, nkp), omega( nomega)
  real(8), intent( out)         :: wgt( nbnd, nkp, nomega)
  
  integer :: it, ib, i, ii, iw, nn, b1, b2
  real(8) :: e(4), w(4), emin, emax, add(20)

  nn = 20
  if( self%ttype .eq. 1) nn = 4
  emin = minval( omega)
  emax = maxval( omega)
  
  wgt = 0.d0
  
#ifdef USEOMP
!$omp parallel default( shared) private( ib, it, e, ii, i, w, iw, add)
!$omp do
#endif
  do ib = 1, nbnd
    do it = 1, self%ntetra
    
      e = 0.d0
      do i = 1, 4
        if( self%ttype .eq. 1) then
          e(i) = ebnd( ib, self%tetra( i, it))
        else
          do ii = 1, nn
            e(i) = e(i) + self%wlsm( i, ii)*ebnd( ib, self%tetra( ii, it))
          end do
        end if
      end do
      !write(*,'(2i,2(4f13.6,5x))') it, ib, e, ebnd( ib, self%tetra( 1:4, it))
      !write(*,'(2i,f13.6)') it, ib, norm2( e - ebnd( ib, self%tetra( 1:4, it)))
      
      if( ((inttype .ne. 1) .and. (minval( e) .le. emax) .and. (maxval( e) .ge. emin)) .or. &
          ((inttype .eq. 1) .and. (minval( e) .le. emax))) then 

        do iw = 1, nomega
          call opt_tetra_get_wgt( inttype, e, omega( iw), w)
          w = self%tetwgt( it)*w
          
          if( maxval( abs( w)) .gt. 1.d-20) then
            if( self%ttype .eq. 1) then
              add(1:4) = w
            else
              add = matmul( w, self%wlsm)
              !add(1:4) = w
            end if
            do ii = 1, nn
              wgt( ib, self%tetra( ii, it), iw) = wgt( ib, self%tetra( ii, it), iw) + add( ii)
            end do
          end if
        end do
   
      end if
      
    end do

  end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
  !call mpi_allgatherv_ifc( set=nbnd, rlen=nkp*nomega, rbuf=wtmp)
  !call barrier

  return
end subroutine opt_tetra_int
!
!--------------------------------------------------------------------------------------
subroutine opt_tetra_int2( self, inttype, nkp, nbnd, ebnd1, ebnd2, omega1, nomega1, omega2, nomega2, wgt)
  !------------------------------------------------------------------------------------
  !
  ! Calculatates integration weights for integrals of the type
  !     \[ \int_{BZ} d^3k A_{n,k} \delta( \omega - \epsilon_{n,k}) \]
  ! where $\delta$ is the Dirac delta for a set of energies (frequencies) $\omega$.
  ! The actual integral is given by the weighted sum
  !     \[ \sum_{k} A_{n,k} w_{n,k}(\omega) \]
  !
  ! integration types:
  !    3: delta*delta
  
  type( tetra_set), intent( in) :: self
  integer, intent( in)          :: inttype, nkp, nbnd, nomega1, nomega2
  real(8), intent( in)          :: ebnd1( nbnd, nkp), ebnd2( nbnd, nkp), omega1( nomega1), omega2( nomega2)
  real(8), intent( out)         :: wgt( nbnd, nkp, nomega1, nomega2)
  
  integer :: it, ib, i, ii, iw1, iw2, nn, b1, b2
  real(8) :: e1(4), e2(4), w(4), emin1, emax1, emin2, emax2, add(20)

  nn = 20
  if( self%ttype .eq. 1) nn = 4
  emin1 = minval( omega1)
  emax1 = maxval( omega1)
  emin2 = minval( omega2)
  emax2 = maxval( omega2)
  
  wgt = 0.d0
  
#ifdef USEOMP
!$omp parallel default( shared) private( ib, it, e1, e2, ii, i, w, iw1, iw2, add)
!$omp do
#endif
  do ib = 1, nbnd
    do it = 1, self%ntetra
    
      e1 = 0.d0
      e2 = 0.d0
      do i = 1, 4
        if( self%ttype .eq. 1) then
          e1(i) = ebnd1( ib, self%tetra( i, it))
          e2(i) = ebnd2( ib, self%tetra( i, it))
        else
          do ii = 1, nn
            e1(i) = e1(i) + self%wlsm( i, ii)*ebnd1( ib, self%tetra( ii, it))
            e2(i) = e2(i) + self%wlsm( i, ii)*ebnd2( ib, self%tetra( ii, it))
          end do
        end if
      end do
      !write(*,'(2i,2(4f13.6,5x))') it, ib, e, ebnd( ib, self%tetra( 1:4, it))
      !write(*,'(2i,f13.6)') it, ib, norm2( e - ebnd( ib, self%tetra( 1:4, it)))
      
      if( ((inttype .eq. 3) .and. (minval( e1) .le. emax1) .and. (maxval( e1) .ge. emin1) &
                            .and. (minval( e2) .le. emax2) .and. (maxval( e2) .ge. emin2))) then 

        do iw2 = 1, nomega2
          do iw1 = 1, nomega1
            call opt_tetra_get_wgt( inttype, e1, omega1( iw1), w, et2=e2, e2=omega2( iw2))
            w = self%tetwgt( it)*w
            
            if( maxval( abs( w)) .gt. 1.d-20) then
              if( self%ttype .eq. 1) then
                add(1:4) = w
              else
                add = matmul( w, self%wlsm)
                !add(1:4) = w
              end if
              do ii = 1, nn
                wgt( ib, self%tetra( ii, it), iw1, iw2) = wgt( ib, self%tetra( ii, it), iw1, iw2) + add( ii)
              end do
            end if
          end do
        end do
   
      end if
      
    end do

  end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
  !call mpi_allgatherv_ifc( set=nbnd, rlen=nkp*nomega, rbuf=wtmp)
  !call barrier

  return
end subroutine opt_tetra_int2
!
!--------------------------------------------------------------------------------------
subroutine opt_tetra_int_ediff( self, inttype, nkp, nbnd, ebnd, omega, nomega, wgt, brange)
  !------------------------------------------------------------------------------------
  type( tetra_set), intent( in)  :: self
  integer, intent( in)           :: inttype, nkp, nbnd, nomega
  real(8), intent( in)           :: ebnd( nbnd, nkp), omega( nomega)
  real(8), intent( out)          :: wgt( nbnd, nbnd, nkp, nomega)
  integer, optional, intent( in) :: brange(4)
  !
  integer :: it, ist, fst, ffst, i, ii, iw, nn, br(4)
  real(8) :: e(4), w(4), add(20), emin, emax

  br = (/1, nbnd, 0, nbnd/)
  if( present( brange)) br = brange

  nn = 20
  if( self%ttype .eq. 1) nn = 4
  emin = minval( omega)
  emax = maxval( omega)

  wgt = 0.d0
  
#ifdef USEOMP
!$omp parallel default( shared) private( it, ist, fst, ffst, e, iw, w, add, ii)
!$omp do collapse(2)
#endif
  do it = 1, self%ntetra

    do ist = br(1), br(2)
      ffst = br(3)
      if( ffst .eq. 0) ffst = ist + 1
      
      do fst = ffst, br(4)
        e = 0.d0
        do i = 1, 4
          if( self%ttype .eq. 1) then
            e(i) = ebnd( fst, self%tetra( i, it)) - ebnd( ist, self%tetra( i, it))
          else
            do ii = 1, nn
              e(i) = e(i) + self%wlsm( i, ii)*(ebnd( fst, self%tetra( ii, it)) - ebnd( ist, self%tetra( ii, it)))
            end do
          end if
        end do

        ! energies (partially) in range
        if( (minval( e) .le. emax) .and. (maxval( e) .ge. emin)) then 
          do iw = 1, nomega
            call opt_tetra_get_wgt( inttype, e, omega( iw), w)
            w = self%tetwgt( it)*w
            
            if( maxval( abs( w)) .gt. 1.d-20) then
              if( self%ttype .eq. 1) then
                add(1:4) = w
              else
                add = matmul( w, self%wlsm)
              end if
              do ii = 1, nn
#ifdef USEOMP
!$omp atomic update
#endif
                wgt( ist, fst, self%tetra( ii, it), iw) = wgt( ist, fst, self%tetra( ii, it), iw) + add( ii)
#ifdef USEOMP
!$omp end atomic
#endif
              end do
            end if
          
          end do !iw
        end if
      
      end do ! fst

    end do ! ist
    !
  end do ! it
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
  !
end subroutine opt_tetra_int_ediff
!
END MODULE

