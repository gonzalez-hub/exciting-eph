module mod_wannier_disentangle
  use mod_wannier_variables
  use mod_wannier_opf
  use mod_wannier_helper
  use mod_wannier_omega
  use mod_eigenvalue_occupancy, only: efermi
  use m_linalg

  implicit none

! module variables
    
contains

    subroutine wfdis_gen2
      use mod_stiefel_optim
      use m_getunit
      use m_plotmat

      integer :: convun, minit, maxit
      real(8) :: gradnorm

      integer :: ik, n, nik, nok, dyo(2)
      real(8) :: omegai0, omegai, t0, t1
      character(256) :: convfname

      integer, allocatable :: dy(:,:), map(:,:)
      real(8), allocatable :: sval(:)
      complex(8), allocatable :: projm(:,:), lsvec(:,:), rsvec(:,:), auxmat(:,:), auxmat2(:,:), Y(:,:,:)

      minit    = 0
      maxit    = 2000
      gradnorm = input%properties%wannier%grouparray( wf_group)%group%epsdis

      write( wf_info, '(" disentangle optimal subspace...")')
      call timesec( t0)

      !****************************
      !* PREPARATION
      !****************************
      ! reorder matrices such that frozen bands are at the end
      call wfomega_shuffle(1)
      ! get initial subspace
      allocate( Y( wf_groups( wf_group)%nst, wf_groups( wf_group)%nwf, wf_kset%nkpt))
      allocate( projm( wf_groups( wf_group)%nst, wf_groups( wf_group)%nproj))
      allocate( map( wf_groups( wf_group)%nwf, wf_kset%nkpt))
      allocate( lsvec( wf_groups( wf_group)%nst, wf_groups( wf_group)%nst), &
                rsvec( wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf), &
                sval( wf_groups( wf_group)%nst))
      allocate( auxmat( wf_groups( wf_group)%nst, wf_groups( wf_group)%nwf))
      allocate( auxmat2( wf_groups( wf_group)%nst, wf_groups( wf_group)%nst))
      allocate( dy(2,wf_kset%nkpt))
      wf_transform( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, &
                    wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, :) = zzero
      Y = zzero; map = 0
      do ik = 1, wf_kset%nkpt
        nok = wf_groups( wf_group)%win_no( ik)
        nik = wf_groups( wf_group)%win_ni( ik)
        dy(1,ik) = nok
        dy(2,ik) = wf_groups( wf_group)%nwf - nik
        do n = 1, nik
          map(n,ik) = dy(2,ik) + n + wf_groups( wf_group)%fwf - 1
        end do
        do n = 1, dy(2,ik)
          map(nik+n,ik) = n + wf_groups( wf_group)%fwf - 1
        end do

        projm = zzero
        do n = 0, nik-1
          wf_transform( wf_groups( wf_group)%fst+dy(1,ik)+n, wf_groups( wf_group)%fwf+dy(2,ik)+n, ik) = zone
          Y( dy(1,ik)+n+1, dy(2,ik)+n+1, ik) = zone
        end do
        
        if( nok .eq. 0) cycle

        do n = 1, wf_groups( wf_group)%win_no( ik)
          projm( n, :) = wf_groups( wf_group)%projection( wf_groups( wf_group)%win_io( n, ik), :, ik)
        end do
        do n = 1, wf_groups( wf_group)%win_ni( ik)
          projm( nok+n, :) = wf_groups( wf_group)%projection( wf_groups( wf_group)%win_ii( n, ik), :, ik)
        end do
        
        auxmat = zzero
        call zgemm( 'n', 'n', nok+nik, wf_groups( wf_group)%nwf, wf_groups( wf_group)%nproj, zone, &
               projm, wf_groups( wf_group)%nst, &
               wf_opf, wf_groups( wf_group)%nproj, zzero, & 
               auxmat, wf_groups( wf_group)%nst)
        if( nik .eq. 0) then
          call zsvd( auxmat( 1:nok, :), &
                 sval( 1:wf_groups( wf_group)%nwf), &
                 lsvec( 1:nok, 1:nok), &
                 rsvec)
          call zgemm( 'n', 'n', nok, wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf, zone, &
                 lsvec, wf_groups( wf_group)%nst, &
                 rsvec, wf_groups( wf_group)%nwf, zzero, &
                 Y(1,1,ik), wf_groups( wf_group)%nst)
        else
          call zgemm( 'n', 'c', nok+nik, nok+nik, wf_groups( wf_group)%nwf, zone, &
                 auxmat, wf_groups( wf_group)%nst, &
                 auxmat, wf_groups( wf_group)%nst, zzero, &
                 auxmat2, wf_groups( wf_group)%nst)

          lsvec = zzero
          call zhediag( auxmat2( 1:nok, 1:nok), sval( 1:nok), evec=lsvec( 1:nok, 1:nok))
          do n = 0, wf_groups( wf_group)%nwf - nik - 1
            Y(1:nok,n+1,ik) = lsvec( 1:nok, nok-n)
          end do
        end if
      end do
      deallocate( projm, lsvec, rsvec, sval, auxmat, auxmat2)
      dyo(1) = wf_groups( wf_group)%nst
      dyo(2) = wf_groups( wf_group)%nwf
      ! get initial spread
      call wfdis_omegai( Y, dyo, wf_kset%nkpt, dy, omegai0)

      !****************************
      !* MINIMIZATION
      !****************************
      convun = 0
      if( input%properties%wannier%grouparray( wf_group)%group%writeconv) then
        call getunit( convun)
        write( convfname, '("dis_conv_",i3.3,".dat")'), wf_group
        open( convun, file=trim( convfname), action='write', form='formatted')
      end if
      call stiefel_optim_lbfgs( Y, dyo, wf_kset%nkpt, dy, &
             wfdis_omegai, wfdis_gradient, &
             tolgrad=gradnorm, miniter=minit, maxiter=maxit, memory=12, outunit=convun)
      if( input%properties%wannier%grouparray( wf_group)%group%writeconv) close( convun)

      !****************************
      !* FINALIZATION
      !****************************
      ! get final spread
      call wfdis_omegai( Y, dyo, wf_kset%nkpt, dy, omegai)
      ! reorder matrices back to original order
      call wfomega_shuffle(-1)
      do ik = 1, wf_kset%nkpt
        wf_transform( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, ik) = &
          wf_transform( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, map(:,ik), ik)
      end do
      ! store subspace if needed
      if( trim( wf_groups( wf_group)%method) .eq. 'disSMV') then
        if( allocated( wf_subspace)) deallocate( wf_subspace)
        allocate( wf_subspace( wf_groups( wf_group)%nst, wf_groups( wf_group)%nwf, wf_kset%nkpt))
        wf_subspace = wf_transform( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, :)
      end if

      call timesec( t1)

      write( wf_info, '(5x,"duration (seconds): ",T40,3x,F10.1)') t1-t0
      write( wf_info, '(5x,"iterations: ",T40,7x,I6)') maxit
      write( wf_info, '(5x,"convergence cutoff: ",T40,E13.6)') input%properties%wannier%grouparray( wf_group)%group%epsdis
      write( wf_info, '(5x,"norm of gradient: ",T40,E13.6)') gradnorm
      write( wf_info, '(5x,"Omega_I before: ",T40,F13.6)') omegai0
      write( wf_info, '(5x,"Omega_I after: ",T40,F13.6)') omegai
      write( wf_info, '(5x,"reduction: ",T40,7x,I5,"%")') nint( 100.d0*(omegai0-omegai)/omegai0)
      write( wf_info, *)
      call flushifc( wf_info)
      deallocate( Y, dy, map)

    end subroutine wfdis_gen2

    subroutine wfdis_omegai( y, dyo, ky, dy, omega)
      integer, intent( in)    :: dyo(2), ky, dy(2,ky)
      complex(8), intent( in) :: y(dyo(1),dyo(2),*)
      real(8), intent( out)   :: omega

      integer :: ik

      ! Y to U
      do ik = 1, wf_kset%nkpt
        wf_transform( wf_groups( wf_group)%fst:(wf_groups( wf_group)%fst+dy(1,ik)-1), &
                      wf_groups( wf_group)%fwf:(wf_groups( wf_group)%fwf+dy(2,ik)-1), ik) = y(1:dy(1,ik),1:dy(2,ik),ik)
      end do
      ! compute spread
      call wfomega_gen
      omega = sum( wf_omega_i( wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf))
      return
    end subroutine wfdis_omegai

    subroutine wfdis_gradient( y, dyo, ky, dxy, gy, dgo)
      integer, intent( in)     :: dyo(2), ky, dxy(2,ky), dgo(2)
      complex(8), intent( in)  :: y(dyo(1),dyo(2),*)
      complex(8), intent( out) :: gy(dgo(1),dgo(2),*)

      integer :: ik
      complex(8), allocatable :: gu(:,:)

      ! Y to U
      do ik = 1, wf_kset%nkpt
        wf_transform( wf_groups( wf_group)%fst:(wf_groups( wf_group)%fst+dxy(1,ik)-1), &
                      wf_groups( wf_group)%fwf:(wf_groups( wf_group)%fwf+dxy(2,ik)-1), ik) = y(1:dxy(1,ik),1:dxy(2,ik),ik)
      end do
      ! update M matrices
      call wfomega_m
      ! compute gradient
#ifdef USEOMP
!$omp parallel default( shared) private( ik, gu)
#endif
      allocate( gu( wf_groups( wf_group)%nst, wf_groups( wf_group)%nwf))
#ifdef USEOMP
!$omp do
#endif
      do ik = 1, wf_kset%nkpt
        ! get the gradient w.r.t. U
        call wfomega_gradiu( ik, gu, wf_groups( wf_group)%nst)
        ! copy relevant part
        gy( 1:dxy(1,ik), 1:dxy(2,ik), ik) = gu( 1:dxy(1,ik), 1:dxy(2,ik))
      end do
#ifdef USEOMP
!$omp end do
#endif
      deallocate( gu)
#ifdef USEOMP
!$omp end parallel
#endif

      return
    end subroutine wfdis_gradient

    subroutine wfdis_gen
      use m_plotmat

      integer :: ik, ikb, i, nik, nikb, nok, nokb, n, ni, idxn, fst, lst, it, maxit, iproj
      real(8) :: mixing, maxdiff, omegai0, t0, t1, fac, win(2)
      real(8) :: omegai, omegaik, omegaiinner
      complex(8) :: zdotc

      real(8), allocatable :: sval(:), eval(:), evalmem(:,:), score(:,:), scoresum(:)
      complex(8), allocatable :: projm(:,:), z(:,:,:), z0(:,:,:), evec(:,:), lsvec(:,:), rsvec(:,:), auxmat(:,:), auxmat2(:,:), m(:,:,:,:), subspace(:,:,:), subspace_mem(:,:,:)

      maxit = 10000

      ! search for nwf OPFs
      call wfomega_gen
      omegai0 = sum( wf_omega_i( wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf))
      !call wfopf_gen

      allocate( projm( wf_groups( wf_group)%nst, wf_groups( wf_group)%nproj), &
                m( wf_groups( wf_group)%nst, wf_groups( wf_group)%nst, wf_kset%nkpt, 2*wf_n_ntot))
      allocate( lsvec( wf_groups( wf_group)%nst, wf_groups( wf_group)%nst), &
                rsvec( wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf), &
                sval( wf_groups( wf_group)%nst))
      allocate( subspace( wf_groups( wf_group)%nst, wf_groups( wf_group)%nwf, wf_kset%nkpt))
      allocate( subspace_mem( wf_groups( wf_group)%nst, wf_groups( wf_group)%nwf, wf_kset%nkpt))
      allocate( auxmat( wf_groups( wf_group)%nst, wf_groups( wf_group)%nwf))
      allocate( auxmat2( wf_groups( wf_group)%nst, wf_groups( wf_group)%nst))
      m = zzero
      
      ! get initial subspace
      do ik = 1, wf_kset%nkpt
        !write(*,*) ik
        nok = wf_groups( wf_group)%win_no( ik)
        nik = wf_groups( wf_group)%win_ni( ik)

        projm = zzero
        subspace( :, :, ik) = zzero
        
        if( nok .eq. 0) cycle

        do n = 1, wf_groups( wf_group)%win_ni( ik)
          projm( n, :) = wf_groups( wf_group)%projection( wf_groups( wf_group)%win_ii( n, ik), :, ik)
        end do
        do n = 1, wf_groups( wf_group)%win_no( ik)
          projm( nik+n, :) = wf_groups( wf_group)%projection( wf_groups( wf_group)%win_io( n, ik), :, ik)
        end do
        
        auxmat = zzero
        call zgemm( 'n', 'n', nok+nik, wf_groups( wf_group)%nwf, wf_groups( wf_group)%nproj, zone, &
               projm, wf_groups( wf_group)%nst, &
               wf_opf, wf_groups( wf_group)%nproj, zzero, & 
               auxmat, wf_groups( wf_group)%nst)
        if( nik .eq. 0) then
          call zsvd( auxmat( 1:(nok+nik), :), &
                 sval( 1:wf_groups( wf_group)%nwf), &
                 lsvec( 1:(nok+nik), 1:(nok+nik)), &
                 rsvec)
          call zgemm( 'n', 'n', nok+nik, wf_groups( wf_group)%nwf, wf_groups( wf_group)%nwf, zone, &
                 lsvec, wf_groups( wf_group)%nst, &
                 rsvec, wf_groups( wf_group)%nwf, zzero, &
                 subspace(1,1,ik), wf_groups( wf_group)%nst)
        else
          call zgemm( 'n', 'c', nok+nik, nok+nik, wf_groups( wf_group)%nwf, zone, &
                 auxmat, wf_groups( wf_group)%nst, &
                 auxmat, wf_groups( wf_group)%nst, zzero, &
                 auxmat2, wf_groups( wf_group)%nst)

          lsvec = zzero
          call zhediag( auxmat2( (nik+1):(nok+nik), (nik+1):(nok+nik)), sval( 1:nok), evec=lsvec( 1:nok, 1:nok))
          !write(*,*) nk, mk, wf_groups( wf_group)%nwf - mk
          !call plotmat( auxmat2( (mk+1):(nk+mk), (mk+1):(nk+mk)))
          !write(*,*)
          !write(*,'(1000f13.6)') sval( 1:nk)
          !call plotmat( lsvec(1:nk,1:nk))
          !write(*,*)
          !stop
          do n = 1, wf_groups( wf_group)%nwf - nik
            subspace( 1:nok, n, ik) = lsvec( 1:nok, nok-n+1)
          end do
          !call plotmat( subspace( :, :, ik))
          !write(*,*)
        end if

        !subspace(:,:,ik) = zzero
        !do n = 1, wf_groups( wf_group)%nwf
        !  i = wf_groups( wf_group)%fst + maxloc( score( :, ik), 1) - 1
        !  score( i, ik) = 0.d0
        !  if( n .gt. mk) then
        !    do ni = 1, nk
        !      if( wf_groups( wf_group)%win_io( ni, ik) .eq. i) exit
        !    end do
        !    subspace( ni, n-mk, ik) = zone
        !  end if
        !end do
      end do
      deallocate( auxmat)

      !subspace = zzero
      !do n = 1, wf_groups( wf_group)%nwf
      !  subspace( n, n, :) = zone
      !end do

      ! set up plane-wave matrix-elements
      m = zzero
      do ik = 1, wf_kset%nkpt
        nik = wf_groups( wf_group)%win_ni( ik)
        nok = wf_groups( wf_group)%win_no( ik)
        do idxn = 1, wf_n_ntot
          ! positive neighbors
          ikb = wf_n_ik( idxn, ik)
          nikb = wf_groups( wf_group)%win_ni( ikb)
          nokb = wf_groups( wf_group)%win_no( ikb)
          do i = 1, nik
            do n = 1, nikb
              m( i, n, ik, idxn) = wf_m0( wf_groups( wf_group)%win_ii( i, ik), wf_groups( wf_group)%win_ii( n, ikb), ik, idxn)
            end do
            do n = 1, nokb
              m( i, nikb+n, ik, idxn) = wf_m0( wf_groups( wf_group)%win_ii( i, ik), wf_groups( wf_group)%win_io( n, ikb), ik, idxn)
            end do
          end do
          do i = 1, nok
            do n = 1, nikb
              m( nik+i, n, ik, idxn) = wf_m0( wf_groups( wf_group)%win_io( i, ik), wf_groups( wf_group)%win_ii( n, ikb), ik, idxn)
            end do
            do n = 1, nokb
              m( nik+i, nikb+n, ik, idxn) = wf_m0( wf_groups( wf_group)%win_io( i, ik), wf_groups( wf_group)%win_io( n, ikb), ik, idxn)
            end do
          end do
          ! negative neighbors
          ikb = wf_n_ik2( idxn, ik)
          nikb = wf_groups( wf_group)%win_ni( ikb)
          nokb = wf_groups( wf_group)%win_no( ikb)
          do i = 1, nik
            do n = 1, nikb
              m( i, n, ik, wf_n_ntot+idxn) = conjg( wf_m0( wf_groups( wf_group)%win_ii( n, ikb), wf_groups( wf_group)%win_ii( i, ik), ikb, idxn))
            end do
            do n = 1, nokb
              m( i, nikb+n, ik, wf_n_ntot+idxn) = conjg( wf_m0( wf_groups( wf_group)%win_io( n, ikb), wf_groups( wf_group)%win_ii( i, ik), ikb, idxn))
            end do
          end do
          do i = 1, nok
            do n = 1, nikb
              m( nik+i, n, ik, wf_n_ntot+idxn) = conjg( wf_m0( wf_groups( wf_group)%win_ii( n, ikb), wf_groups( wf_group)%win_io( i, ik), ikb, idxn))
            end do
            do n = 1, nokb
              m( nik+i, nikb+n, ik, wf_n_ntot+idxn) = conjg( wf_m0( wf_groups( wf_group)%win_io( n, ikb), wf_groups( wf_group)%win_io( i, ik), ikb, idxn))
            end do
          end do
        end do
      end do

      ! calculate Z_0
      allocate( z0( wf_groups( wf_group)%nst, wf_groups( wf_group)%nst, wf_kset%nkpt))
      z0 = zzero
      do ik = 1, wf_kset%nkpt
        nik = wf_groups( wf_group)%win_ni( ik)
        nok = wf_groups( wf_group)%win_no( ik)
        if( nok .eq. 0) cycle
        do idxn = 1, wf_n_ntot
          ! positive neighbors
          ikb = wf_n_ik( idxn, ik)
          nikb = wf_groups( wf_group)%win_ni( ikb)
          if( nikb .gt. 0) then
            call zgemm( 'n', 'c', nok, nok, nikb, cmplx( wf_n_wgt( idxn), 0.d0, 8), &
                   m( nik+1, 1, ik, idxn), wf_groups( wf_group)%nst, &
                   m( nik+1, 1, ik, idxn), wf_groups( wf_group)%nst, zone, &
                   z0( :, :, ik), wf_groups( wf_group)%nst)
          end if
          ! negative neighbors
          ikb = wf_n_ik2( idxn, ik)
          nikb = wf_groups( wf_group)%win_ni( ikb)
          if( nikb .gt. 0) then
            call zgemm( 'n', 'c', nok, nok, nikb, cmplx( wf_n_wgt( idxn), 0.d0, 8), &
                   m( nik+1, 1, ik, wf_n_ntot+idxn), wf_groups( wf_group)%nst, &
                   m( nik+1, 1, ik, wf_n_ntot+idxn), wf_groups( wf_group)%nst, zone, &
                   z0( :, :, ik), wf_groups( wf_group)%nst)
          end if
        end do
      end do

      ! independent contribution to Omega_I from inner-window states
      omegaiinner = 0.d0
      do ik = 1, wf_kset%nkpt
        nik = wf_groups( wf_group)%win_ni( ik)
        do idxn = 1, wf_n_ntot
          ikb = wf_n_ik( idxn, ik)
          nikb = wf_groups( wf_group)%win_ni( ikb)
          do i = 1, nik
            omegaiinner = omegaiinner - wf_n_wgt( idxn)*dble( zdotc( nikb, m( i, 1:nikb, ik, idxn), 1, m( i, 1:nikb, ik, idxn), 1))
          end do
          ikb = wf_n_ik2( idxn, ik)
          nikb = wf_groups( wf_group)%win_ni( ikb)
          do i = 1, nik
            omegaiinner = omegaiinner - wf_n_wgt( idxn)*dble( zdotc( nikb, m( i, 1:nikb, ik, wf_n_ntot+idxn), 1, m( i, 1:nikb, ik, wf_n_ntot+idxn), 1))
          end do
        end do
      end do
 
      ! perfom disentanglement
      allocate( evalmem( wf_groups( wf_group)%nst, wf_kset%nkpt))
      allocate( z( wf_groups( wf_group)%nst, wf_groups( wf_group)%nst, wf_kset%nkpt))
      z = zzero
      subspace_mem = subspace
      maxdiff = 1.d0
      evalmem = 0.d0
      it = 0
      write( wf_info, '(" disentangle optimal subspace...")')
      call timesec( t0)
      do while( (maxdiff .gt. input%properties%wannier%grouparray( wf_group)%group%epsdis) .and. (it .lt. maxit))

        !if( mod( it, 100) .eq. 0) then
        !  !call plotmat( z( :, :, 12))
        !  wf_transform( :, wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, :) = zzero
        !  do ik = 1, wf_kset%nkpt
        !    do n = 1, wf_groups( wf_group)%win_ni( ik)
        !      wf_transform( wf_groups( wf_group)%win_ii( n, ik), wf_groups( wf_group)%fwf+n-1, ik) = zone
        !    end do
        !    do n = 1, wf_groups( wf_group)%win_no( ik)
        !      do i = 1, wf_groups( wf_group)%nwf - wf_groups( wf_group)%win_ni( ik)
        !        wf_transform( wf_groups( wf_group)%win_io( n, ik), wf_groups( wf_group)%fwf+wf_groups( wf_group)%win_ni( ik)+i-1, ik) = subspace( n, i, ik)
        !      end do
        !    end do
        !  end do
        !  call wannier_loc

        !  write(*,'(i,g13.6,2f13.6,g13.6)') it, maxdiff, sum( wf_omega_i( wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf))!, omegai, omegai/sum( wf_omega_i( wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf))-1.d0
        !end if
        
        it = it + 1
        maxdiff = 0.d0
        mixing = 0.5d0
        omegai = 0.d0
        if( it .eq. 1) mixing = 1.d0
#ifdef USEOMP
!$omp parallel default( shared) private( ik, ikb, i, n, nik, nok, nikb, nokb, idxn, auxmat, eval, evec, omegaik) reduction(+:omegai)
#endif
        allocate( evec( wf_groups( wf_group)%nst, wf_groups( wf_group)%nst), &
                  eval( wf_groups( wf_group)%nst), &
                  auxmat( wf_groups( wf_group)%nwf, wf_groups( wf_group)%nst))
#ifdef USEOMP
!$omp do
#endif
        do ik = 1, wf_kset%nkpt
          omegaik = 2.d0*sum( wf_n_wgt( 1:wf_n_ntot))*wf_groups( wf_group)%nwf
          nik = wf_groups( wf_group)%win_ni( ik)
          nok = wf_groups( wf_group)%win_no( ik)
          
          if( nok .ge. 1) then
            z( :, :, ik) = (1.d0 - mixing)*z( :, :, ik) + mixing*z0( :, :, ik)
            do idxn = 1, wf_n_ntot
              ikb = wf_n_ik( idxn, ik)
              nikb = wf_groups( wf_group)%win_ni( ikb)
              nokb = wf_groups( wf_group)%win_no( ikb)
              n = wf_groups( wf_group)%nwf-nikb
              if( (nokb .ge. 1) .and. (n .ge. 1)) then
                call zgemm( 'c', 'c', n, nok, nokb, zone, &
                       subspace( 1, 1, ikb), wf_groups( wf_group)%nst, &
                       m( nik+1, nikb+1, ik, idxn), wf_groups( wf_group)%nst, zzero, &
                       auxmat(1,1), wf_groups( wf_group)%nwf)
                !do i = 1, nik
                !  omegaik = omegaik - wf_n_wgt( idxn)*dble( zdotc( n, auxmat( 1, i), 1, auxmat( 1, i), 1))
                !end do
                call zgemm( 'c', 'n', nok, nok, n, cmplx( mixing*wf_n_wgt( idxn), 0.d0, 8), &
                       auxmat, wf_groups( wf_group)%nwf, &
                       auxmat, wf_groups( wf_group)%nwf, zone, &
                       z( 1, 1, ik), wf_groups( wf_group)%nst)
              end if

              ikb = wf_n_ik2( idxn, ik)
              nikb = wf_groups( wf_group)%win_ni( ikb)
              nokb = wf_groups( wf_group)%win_no( ikb)
              n = wf_groups( wf_group)%nwf-nikb
              if( (nokb .ge. 1) .and. (n .ge. 1)) then
                call zgemm( 'c', 'c', n, nok, nokb, zone, &
                       subspace( 1, 1, ikb), wf_groups( wf_group)%nst, &
                       m( nik+1, nikb+1, ik, wf_n_ntot+idxn), wf_groups( wf_group)%nst, zzero, &
                       auxmat(1,1), wf_groups( wf_group)%nwf)
                !do i = 1, nik
                !  omegaik = omegaik - wf_n_wgt( idxn)*dble( zdotc( n, auxmat( 1, i), 1, auxmat( 1, i), 1))
                !end do
                call zgemm( 'c', 'n', nok, nok, n, cmplx( mixing*wf_n_wgt( idxn), 0.d0, 8), &
                       auxmat, wf_groups( wf_group)%nwf, &
                       auxmat, wf_groups( wf_group)%nwf, zone, &
                       z( 1, 1, ik), wf_groups( wf_group)%nst)
              end if

            end do
            n = wf_groups( wf_group)%nwf - nik
            if( n .ge. 1) then
              call zhediag( z( 1:nok, 1:nok, ik), eval( 1:nok), evec=evec( 1:nok, 1:nok))
              !write(*,'(1000f13.6)') sum( wf_n_wgt( 1:wf_n_ntot)), eval( 1:nk)
              do i = 1, n
                subspace_mem( 1:nok, i, ik) = evec( 1:nok, nok-i+1)
                omegaik = omegaik - eval( nok-i+1)
              end do
#ifdef USEOMP
!$omp atomic update
#endif
              maxdiff = max( maxdiff, dsqrt( sum( (eval( (nok-n+1):nok) - evalmem( (nok-n+1):nok, ik))**2)/dble( n)))
#ifdef USEOMP
!$omp end atomic
#endif
              evalmem( 1:nok, ik) = eval( 1:nok)
            end if
          end if

          omegai = omegai + omegaik

        end do
#ifdef USEOMP
!$omp end do
#endif
        deallocate( evec, eval, auxmat)
#ifdef USEOMP
!$omp end parallel
#endif
        omegai = (omegai + omegaiinner)/wf_kset%nkpt
        !write(*,'(1a1,i8,f23.16,$)') char(13), it, omegai

        subspace = subspace_mem
        !write(*,'(i,f13.6)') it, maxdiff
        !do ik = 1, wf_kset%nkpt
        !  nok = wf_groups( wf_group)%win_no( ik)
        !  write(*,'(i,100f13.6)') ik, evalmem(1:nok,ik)
        !end do
        !stop
      end do
      !write(*,*)

      wf_transform( :, wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, :) = zzero
      do ik = 1, wf_kset%nkpt
        do n = 1, wf_groups( wf_group)%win_ni( ik)
          wf_transform( wf_groups( wf_group)%win_ii( n, ik), wf_groups( wf_group)%fwf+n-1, ik) = zone
        end do
        do n = 1, wf_groups( wf_group)%win_no( ik)
          do i = 1, wf_groups( wf_group)%nwf - wf_groups( wf_group)%win_ni( ik)
            wf_transform( wf_groups( wf_group)%win_io( n, ik), wf_groups( wf_group)%fwf+wf_groups( wf_group)%win_ni( ik)+i-1, ik) = subspace( n, i, ik)
          end do
        end do
      end do

      !ik = 12
      !write(*,*) wf_groups( wf_group)%win_ii( :, ik)
      !write(*,*) wf_groups( wf_group)%win_io( :, ik)
      !write(*,'(100f13.6)') score( :, ik)
      !call plotmat( wf_transform( :, :, ik), matlab=.true.)
      !write(*,*)

      deallocate( m, subspace, evalmem, lsvec, rsvec, sval, projm, z, z0, subspace_mem)
      call wfomega_gen
      call timesec( t1)

      write( wf_info, '(5x,"duration (seconds): ",T40,3x,F10.1)') t1-t0
      write( wf_info, '(5x,"iterations: ",T40,7x,I6)') it
      write( wf_info, '(5x,"convergence cutoff: ",T40,E13.6)') input%properties%wannier%grouparray( wf_group)%group%epsdis
      write( wf_info, '(5x,"Omega_I before: ",T40,F13.6)') omegai0
      write( wf_info, '(5x,"Omega_I after: ",T40,F13.6)') sum( wf_omega_i( wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf))
      write( wf_info, '(5x,"reduction: ",T40,7x,I5,"%")') nint( 100.d0*(omegai0-sum( wf_omega_i( wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf)))/omegai0)
      write( wf_info, *)
      call flushifc( wf_info)

    end subroutine wfdis_gen

end module mod_wannier_disentangle
