module mod_wannier_realspace
  use mod_mtbasis
  use mod_wannier
  use modxs, only: fftmap_type
  implicit none

! module variables
  complex(8), allocatable :: wfreal_mtbasis(:,:,:,:)
  complex(8), allocatable :: wfreal_irbasis(:,:,:)
  complex(8), allocatable :: wfreal_radint(:,:,:,:)
  complex(8), allocatable :: wfreal_irmat(:,:,:)

  type( fftmap_type) :: wfreal_fftmap

! methods
  contains

    subroutine wfreal_genmtbasis
      integer :: ik, ir
      complex(8) :: mttmp( nlmidxmax, nstfv, natmtot), evec( nmatmax_ptr, nstfv, nspinor), evecrot( nmatmax_ptr, wf_nwf)

      if( allocated( wfreal_mtbasis)) deallocate( wfreal_mtbasis)
      allocate( wfreal_mtbasis( nlmidxmax, wf_nwf, natmtot, wf_nrpt))
      wfreal_mtbasis = zzero

      call wfhelp_genradfun
      do ik = 1, wf_kset%nkpt
        call wfhelp_getevec( ik, evec)
        call zgemm( 'n', 'n', nmatmax_ptr, wf_nwf, wf_nst, zone, &
               evec( :, wf_fst:wf_lst, 1), nmatmax_ptr, &
               wf_transform( :, :, ik), wf_nst, zzero, &
               evecrot, nmatmax_ptr)
        evec( :, 1:wf_nwf, 1) = evecrot
        call genshortmtbasis( wf_kset%vkl( :, ik), evec(:,:,1), mttmp)
        do ir = 1, wf_nrpt
          wfreal_mtbasis( :, :, :, ir) = wfreal_mtbasis( :, :, :, ir) + wf_kset%wkpt( ik)*wf_pkr( ik, ir)*mttmp( :, 1:wf_nwf, :)
        end do
      end do
      
      return
    end subroutine wfreal_genmtbasis

    subroutine wfreal_genirbasis
      integer :: ik, ir, igk, ig, ist, ix, iy, iz, jx, jy, jz, vg(3)
      real(8) :: vl(3), dotp
      complex(8) :: evec( nmatmax_ptr, nstfv, nspinor), evecrot( nmatmax_ptr, wf_nwf)

      complex(8), allocatable :: zfft(:,:), tmpbasis(:,:,:)

      call genfftmap( wfreal_fftmap, wf_Gset%gmaxvr)

      allocate( zfft( wfreal_fftmap%ngrtot+1, wf_nwf))
      allocate( tmpbasis( wfreal_fftmap%ngrtot+1, wf_nwf, wf_nrpt))
      tmpbasis = zzero

      do ik = 1, wf_kset%nkpt
        call wfhelp_getevec( ik, evec)
        call zgemm( 'n', 'n', nmatmax_ptr, wf_nwf, wf_nst, zone, &
               evec( :, wf_fst:wf_lst, 1), nmatmax_ptr, &
               wf_transform( :, :, ik), wf_nst, zzero, &
               evecrot, nmatmax_ptr)
        zfft = zzero
        do igk = 1, wf_Gkset%ngk( 1, ik)
          ig = wf_Gkset%igkig( igk, 1, ik)
          zfft( wfreal_fftmap%igfft( ig), :) = evecrot( igk, :)
        end do
        do ist = 1, wf_nwf
          call zfftifc( 3, wfreal_fftmap%ngrid, 1, zfft( :, ist))
        end do
        do iz = wfreal_fftmap%intgv(3,1), wfreal_fftmap%intgv(3,2)
          jz = modulo( iz, wfreal_fftmap%ngrid(3))
          do iy = wfreal_fftmap%intgv(2,1), wfreal_fftmap%intgv(2,2)
            jy = modulo( iy, wfreal_fftmap%ngrid(2))
            do ix = wfreal_fftmap%intgv(1,1), wfreal_fftmap%intgv(1,2)
              jx = modulo( ix, wfreal_fftmap%ngrid(1))
              ig = jz*wfreal_fftmap%ngrid(2)*wfreal_fftmap%ngrid(1) + jy*wfreal_fftmap%ngrid(1) + jx + 1
              vl = dble( 2*(/ix, iy, iz/) - (/1, 1, 1/))/dble( 2*wfreal_fftmap%ngrid)
              dotp = twopi*dot_product( vl, wf_kset%vkl( :, ik))
              zfft( ig, :) = zfft( ig, :)*cmplx( cos( dotp), sin( dotp), 8)
            end do
          end do
        end do
        do ir = 1, wf_nrpt
          tmpbasis( :, :, ir) = tmpbasis( :, :, ir) + wf_kset%wkpt( ik)*wf_pkr( ik, ir)*zfft
        end do
      end do
      deallocate( zfft)

      if( allocated( wfreal_irbasis)) deallocate( wfreal_irbasis)
      allocate( wfreal_irbasis( wf_Gset%ngrtot, wf_nwf, wf_nrpt))
      wfreal_irbasis = zzero

      do ir = 1, wf_nrpt
        do ist = 1, wf_nwf
          call zfftifc( 3, wfreal_fftmap%ngrid, -1, tmpbasis( :, ist, ir))
        end do
        do ig = 1, wf_Gset%ngrtot
          wfreal_irbasis( ig, :, ir) = tmpbasis( wfreal_fftmap%igfft( ig), :, ir)
        end do
      end do
      deallocate( tmpbasis)

      return
    end subroutine wfreal_genirbasis

    subroutine wfreal_mtintegral( ir1, ir2, ld, olp)
      integer, intent( in)       :: ir1, ir2, ld
      complex(8), intent( inout) :: olp( wf_nwf, wf_nwf, ld)

      integer :: is, ia, ias, d
      complex(8) :: auxmat( wf_nwf, nlmidxmax, ld)

      do is = 1, nspecies
        do ia = 1, natoms( is)
          ias = idxas( ia, is)
          call zgemm( 'c', 'n', wf_nwf, nlmidxmax*ld, nlmidxmax, zone, &
                 wfreal_mtbasis( :, :, ias, ir1), nlmidxmax, &
                 wfreal_radint( :, :, :, ias), nlmidxmax, zzero, &
                 auxmat, wf_nwf)
          do d = 1, ld
            call zgemm( 'n', 'n', wf_nwf, wf_nwf, nlmidxmax, zone, &
                   auxmat( :, :, d), wf_nwf, &
                   wfreal_mtbasis( :, :, ias, ir2), nlmidxmax, zone, &
                   olp( :, :, d), wf_nwf)
          end do
        end do
      end do

      return
    end subroutine wfreal_mtintegral

    subroutine wfreal_irintegral( ir1, ir2, ld, olp)
      integer, intent( in)       :: ir1, ir2, ld
      complex(8), intent( inout) :: olp( wf_nwf, wf_nwf, ld)

      integer :: d
      complex(8) :: auxmat( wf_nwf, wf_Gset%ngrtot, ld)

      call zgemm( 'c', 'n', wf_nwf, wf_Gset%ngrtot*ld, wf_Gset%ngrtot, zone, &
             wfreal_irbasis( :, :, ir1), wf_Gset%ngrtot, &
             wfreal_irmat, wf_Gset%ngrtot, zzero, &
             auxmat, wf_nwf)
      do d = 1, ld
        call zgemm( 'n', 'n', wf_nwf, wf_nwf, wf_Gset%ngrtot, zone, &
               auxmat( :, :, d), wf_nwf, &
               wfreal_irbasis( :, :, ir2), wf_Gset%ngrtot, zone, &
               olp( :, :, d), wf_nwf)
      end do

      return
    end subroutine wfreal_irintegral

    subroutine wfreal_integral( ir1, ir2, ld, olp)
      integer, intent( in)       :: ir1, ir2, ld
      complex(8), intent( inout) :: olp( wf_nwf, wf_nwf, ld)

      call wfreal_mtintegral( ir1, ir2, ld, olp)
      call wfreal_irintegral( ir1, ir2, ld, olp)

      return
    end subroutine wfreal_integral

    subroutine wfreal_radint_overlap
      integer :: is, ia, ias, l, m, lm, lidx1, lidx2, lmidx1, lmidx2, o1, o2, ilo1, ilo2

      if( allocated( wfreal_radint)) deallocate( wfreal_radint)
      allocate( wfreal_radint( nlmidxmax, nlmidxmax, 1, natmtot))
      wfreal_radint = zzero

      do is = 1, nspecies
        do ia = 1, natoms( is)
          ias = idxas( ia, is)

          do l = 0, mtlmax
            do m = -l, l
              lm = idxlm( l, m)

              do lidx1 = 1, nlidx( l, is)
                lmidx1 = lidx2lmidx( lm, lidx1, is)
                do lidx2 = 1, nlidx( l, is)
                  lmidx2 = lidx2lmidx( lm, lidx2, is)

                  if( lidx2apwlo( lidx1, l, is) .gt. 0) then
                    o1 = lidx2apwlo( lidx1, l, is)
                    ! APW-APW
                    if( lidx2apwlo( lidx2, l, is) .gt. 0) then
                      o2 = lidx2apwlo( lidx2, l, is)
                      if( o1 .eq. o2) wfreal_radint( lmidx1, lmidx2, 1, ias) = zone
                    ! APW-LO
                    else if( lidx2apwlo( lidx2, l, is) .lt. 0) then
                      ilo2 = -lidx2apwlo( lidx2, l, is)
                      wfreal_radint( lmidx1, lmidx2, 1, ias) = cmplx( oalo( o1, ilo2, ias), 0.d0, 8)
                    end if
                  else if( lidx2apwlo( lidx1, l, is) .lt. 0) then
                    ilo1 = -lidx2apwlo( lidx1, l, is)
                    ! LO-APW
                    if( lidx2apwlo( lidx2, l, is) .gt. 0) then
                      o2 = lidx2apwlo( lidx2, l, is)
                      wfreal_radint( lmidx1, lmidx2, 1, ias) = cmplx( oalo( o2, ilo1, ias), 0.d0, 8)
                    ! LO-LO
                    else if( lidx2apwlo( lidx2, l, is) .lt. 0) then
                      ilo2 = -lidx2apwlo( lidx2, l, is)
                      wfreal_radint( lmidx1, lmidx2, 1, ias) = cmplx( ololo( ilo1, ilo2, ias), 0.d0, 8)
                    end if
                  end if

                end do
              end do

            end do
          end do

        end do
      end do
    end subroutine wfreal_radint_overlap

    subroutine wfreal_radint_rmat
      use mod_lattice, only: ainv

      integer :: is, ia, ias, l1, l2, m1, m2, lm1, lm2, lidx1, lidx2, lmidx1, lmidx2, o1, o2, ilo1, ilo2
      real(8) :: wgt, atl(3), atc(3)

      real(8), allocatable :: radint(:,:)

      complex(8), external :: gauntyry

      if( allocated( wfreal_radint)) deallocate( wfreal_radint)
      allocate( wfreal_radint( nlmidxmax, nlmidxmax, 3, natmtot))
      wfreal_radint = zzero

      allocate( radint( nlidxmax, nlidxmax))

      wgt = sqrt( fourpi/3.d0)
      do is = 1, nspecies
        do ia = 1, natoms( is)
          ias = idxas( ia, is)

          call r3mv( ainv, atposc( :, ia, is), atl)
          if( atl(1) .gt. 0.5d0) atl(1) = atl(1) - 1.d0
          if( atl(2) .gt. 0.5d0) atl(2) = atl(2) - 1.d0
          if( atl(3) .gt. 0.5d0) atl(3) = atl(3) - 1.d0
          call r3mv( input%structure%crystal%basevect, atl, atc)

          ! local contribution
          do l1 = 0, mtlmax
            do l2 = 0, mtlmax
              call wfreal_radint_rmat_genri( is, ia, l1, l2, radint)

              do m1 = -l1, l1
                lm1 = idxlm( l1, m1)
                do m2 = -l2, l2
                  lm2 = idxlm( l2, m2)

                  do lidx1 = 1, nlidx( l1, is)
                    lmidx1 = lidx2lmidx( lm1, lidx1, is)
                    do lidx2 = 1, nlidx( l2, is)
                      lmidx2 = lidx2lmidx( lm2, lidx2, is)

                      wfreal_radint( lmidx1, lmidx2, 1, ias) = -cmplx( wgt*radint( lidx1, lidx2), 0.d0, 8)*gauntyry( l1, 1, l2, m1,  1, m2)
                      wfreal_radint( lmidx1, lmidx2, 2, ias) = -cmplx( wgt*radint( lidx1, lidx2), 0.d0, 8)*gauntyry( l1, 1, l2, m1, -1, m2)
                      wfreal_radint( lmidx1, lmidx2, 3, ias) =  cmplx( wgt*radint( lidx1, lidx2), 0.d0, 8)*gauntyry( l1, 1, l2, m1,  0, m2)

                    end do
                  end do

                end do
              end do

            end do
          end do

          ! atomic contribution
          do l1 = 0, mtlmax
            do m1 = -l1, l1
              lm1 = idxlm( l1, m1)

              do lidx1 = 1, nlidx( l1, is)
                lmidx1 = lidx2lmidx( lm1, lidx1, is)
                do lidx2 = 1, nlidx( l1, is)
                  lmidx2 = lidx2lmidx( lm1, lidx2, is)

                  if( lidx2apwlo( lidx1, l1, is) .gt. 0) then
                    o1 = lidx2apwlo( lidx1, l1, is)
                    ! APW-APW
                    if( lidx2apwlo( lidx2, l1, is) .gt. 0) then
                      o2 = lidx2apwlo( lidx2, l1, is)
                      if( o1 .eq. o2) wfreal_radint( lmidx1, lmidx2, :, ias) = wfreal_radint( lmidx1, lmidx2, :, ias) + cmplx( atc, 0.d0, 8)
                    ! APW-LO
                    else if( lidx2apwlo( lidx2, l1, is) .lt. 0) then
                      ilo2 = -lidx2apwlo( lidx2, l1, is)
                      wfreal_radint( lmidx1, lmidx2, :, ias) = wfreal_radint( lmidx1, lmidx2, :, ias) + cmplx( atc*oalo( o1, ilo2, ias), 0.d0, 8)
                    end if
                  else if( lidx2apwlo( lidx1, l1, is) .lt. 0) then
                    ilo1 = -lidx2apwlo( lidx1, l1, is)
                    ! LO-APW
                    if( lidx2apwlo( lidx2, l1, is) .gt. 0) then
                      o2 = lidx2apwlo( lidx2, l1, is)
                      wfreal_radint( lmidx1, lmidx2, :, ias) = wfreal_radint( lmidx1, lmidx2, :, ias) + cmplx( atc*oalo( o2, ilo1, ias), 0.d0, 8)
                    ! LO-LO
                    else if( lidx2apwlo( lidx2, l1, is) .lt. 0) then
                      ilo2 = -lidx2apwlo( lidx2, l1, is)
                      wfreal_radint( lmidx1, lmidx2, :, ias) = wfreal_radint( lmidx1, lmidx2, :, ias) + cmplx( atc*ololo( ilo1, ilo2, ias), 0.d0, 8)
                    end if
                  end if

                end do
              end do

            end do
          end do

        end do
      end do

      deallocate( radint)
      return

      contains
        subroutine wfreal_radint_rmat_genri( is, ia, l1, l2, radint)
          integer, intent( in)  :: is, ia, l1, l2
          real(8), intent( out) :: radint( nlidxmax, nlidxmax)

          integer :: ias, ir, nr, o1, o2, ilo1, ilo2, lidx1, lidx2
          
          real(8), allocatable :: fr(:), gf(:), cf(:,:)

          radint = 0.d0
          ias = idxas( ia, is)
          nr = nrmt( is)

          allocate( fr( nr), gf( nr), cf( 3, nr))
          do lidx1 = 1, nlidx( l1, is)
            do lidx2 = 1, nlidx( l2, is)

              if( lidx2apwlo( lidx1, l1, is) .gt. 0) then
                o1 = lidx2apwlo( lidx1, l1, is)
                ! APW-APW
                if( lidx2apwlo( lidx2, l2, is) .gt. 0) then
                  o2 = lidx2apwlo( lidx2, l2, is)
                  do ir = 1, nr
                    fr( ir) = apwfr( ir, 1, o1, l1, ias)*apwfr( ir, 1, o2, l2, ias)*spr( ir, is)**3
                  end do
                  call fderiv( -1, nr, spr( :, is), fr, gf, cf)
                  radint( lidx1, lidx2) = gf( nr)
                ! APW-LO
                else if( lidx2apwlo( lidx2, l2, is) .lt. 0) then
                  ilo2 = -lidx2apwlo( lidx2, l2, is)
                  do ir = 1, nr
                    fr( ir) = apwfr( ir, 1, o1, l1, ias)*lofr( ir, 1, ilo2, ias)*spr( ir, is)**3
                  end do
                  call fderiv( -1, nr, spr( :, is), fr, gf, cf)
                  radint( lidx1, lidx2) = gf( nr)
                end if
              else if( lidx2apwlo( lidx1, l1, is) .lt. 0) then
                ilo1 = -lidx2apwlo( lidx1, l1, is)
                ! LO-APW
                if( lidx2apwlo( lidx2, l2, is) .gt. 0) then
                  o2 = lidx2apwlo( lidx2, l2, is)
                  do ir = 1, nr
                    fr( ir) = lofr( ir, 1, ilo1, ias)*apwfr( ir, 1, o2, l2, ias)*spr( ir, is)**3
                  end do
                  call fderiv( -1, nr, spr( :, is), fr, gf, cf)
                  radint( lidx1, lidx2) = gf( nr)
                ! LO-LO
                else if( lidx2apwlo( lidx2, l2, is) .lt. 0) then
                  ilo2 = -lidx2apwlo( lidx2, l2, is)
                  do ir = 1, nr
                    fr( ir) = lofr( ir, 1, ilo1, ias)*lofr( ir, 1, ilo2, ias)*spr( ir, is)**3
                  end do
                  call fderiv( -1, nr, spr( :, is), fr, gf, cf)
                  radint( lidx1, lidx2) = gf( nr)
                end if
              end if

            end do
          end do

          deallocate( fr, gf, cf)
          return
        end subroutine wfreal_radint_rmat_genri
    end subroutine wfreal_radint_rmat

    subroutine wfreal_irmat_overlap
      use mod_Gvector, only: cfunig, ivgig

      integer :: ig1, ig2, vg(3)

      if( allocated( wfreal_irmat)) deallocate( wfreal_irmat)
      allocate( wfreal_irmat( wf_Gset%ngrtot, wf_Gset%ngrtot, 1))
      wfreal_irmat = zzero

      do ig1 = 1, wf_Gset%ngrtot
        do ig2 = 1, wf_Gset%ngrtot
          vg = wf_Gset%ivg( :, ig1) - wf_Gset%ivg( :, ig2)
          if( (vg(1) .ge. wf_Gset%intgv(1,1)) .and. (vg(1) .lt. wf_Gset%intgv(1,2)) .and. &
              (vg(2) .ge. wf_Gset%intgv(2,1)) .and. (vg(2) .lt. wf_Gset%intgv(2,2)) .and. &
              (vg(3) .ge. wf_Gset%intgv(3,1)) .and. (vg(3) .lt. wf_Gset%intgv(3,2))) then
            wfreal_irmat( ig1, ig2, 1) = cfunig( ivgig( vg(1), vg(2), vg(3)))
          end if
        end do
      end do
    end subroutine wfreal_irmat_overlap

    subroutine wfreal_irmat_rmat
      use mod_Gvector, only: cfunig, ivgig

      integer :: ig1, ig2, ix, iy, iz, jx, jy, jz, vg(3)
      real(8) :: vl(3), vc(3)

      complex(8), allocatable :: zfft(:), tmpmat(:,:)

      allocate( zfft( wfreal_fftmap%ngrtot+1))
      allocate( tmpmat( wfreal_fftmap%ngrtot+1, 3))
      zfft = zzero

      do ig1 = 1, ngrtot
        zfft( wfreal_fftmap%igfft( ig1)) = cfunig( ig1)
      end do
      call zfftifc( 3, wfreal_fftmap%ngrid, 1, zfft)

      do iz = wfreal_fftmap%intgv(3,1), wfreal_fftmap%intgv(3,2)
        jz = modulo( iz, wfreal_fftmap%ngrid(3))
        do iy = wfreal_fftmap%intgv(2,1), wfreal_fftmap%intgv(2,2)
          jy = modulo( iy, wfreal_fftmap%ngrid(2))
          do ix = wfreal_fftmap%intgv(1,1), wfreal_fftmap%intgv(1,2)
            jx = modulo( ix, wfreal_fftmap%ngrid(1))
            ig1 = jz*wfreal_fftmap%ngrid(2)*wfreal_fftmap%ngrid(1) + jy*wfreal_fftmap%ngrid(1) + jx + 1
            vl = dble( 2*(/ix, iy, iz/) - (/1, 1, 1/))/dble( 2*wfreal_fftmap%ngrid)
            call r3mv( input%structure%crystal%basevect, vl, vc)
            tmpmat( ig1, :) = cmplx( dble( zfft( ig1))*vc, 0.d0, 8)
          end do
        end do
      end do
      deallocate( zfft)

      call zfftifc( 3, wfreal_fftmap%ngrid, -1, tmpmat( :, 1))
      call zfftifc( 3, wfreal_fftmap%ngrid, -1, tmpmat( :, 2))
      call zfftifc( 3, wfreal_fftmap%ngrid, -1, tmpmat( :, 3))

      if( allocated( wfreal_irmat)) deallocate( wfreal_irmat)
      allocate( wfreal_irmat( wf_Gset%ngrtot, wf_Gset%ngrtot, 3))
      wfreal_irmat = zzero

      do ig1 = 1, wf_Gset%ngrtot
        do ig2 = 1, wf_Gset%ngrtot
          vg = wf_Gset%ivg( :, ig1) - wf_Gset%ivg( :, ig2)
          if( (vg(1) .ge. wf_Gset%intgv(1,1)) .and. (vg(1) .lt. wf_Gset%intgv(1,2)) .and. &
              (vg(2) .ge. wf_Gset%intgv(2,1)) .and. (vg(2) .lt. wf_Gset%intgv(2,2)) .and. &
              (vg(3) .ge. wf_Gset%intgv(3,1)) .and. (vg(3) .lt. wf_Gset%intgv(3,2))) then
            wfreal_irmat( ig1, ig2, :) = tmpmat( wfreal_fftmap%igfft( wf_Gset%ivgig( vg(1), vg(2), vg(3))), :)
          end if
        end do
      end do
      deallocate( tmpmat)

      return
    end subroutine wfreal_irmat_rmat

    subroutine wfreal_do
      integer :: ir, ist
      real(8) :: vr(3)
      complex(8) :: rmat( wf_nwf, wf_nwf, 3), olp( wf_nwf, wf_nwf)
      complex(8) :: rmat2( wf_nwf, wf_nwf, 3), olp2( wf_nwf, wf_nwf)

      call genmtbasisindices
      write(*,*) 'MT basis'
      call wfreal_genmtbasis
      write(*,*) 'IR basis'
      call wfreal_genirbasis

      write(*,*) 'MT radial integrals'
      call wfreal_radint_rmat
      write(*,*) 'IR overlap matrix'
      call wfreal_irmat_rmat

      rmat = zzero
      do ir = 1, wf_nrpt
        rmat2 = zzero
        call wfreal_mtintegral( ir, ir, 3, rmat2)
        rmat = rmat + rmat2/dble( wf_rmul( ir))
      end do

      do ist = 1, wf_nwf
        write(*,'(i,3f13.6)') ist, wf_centers( :, ist)
        write(*,'(i,3f13.6)') ist, dble( rmat( ist, ist, :))
        write(*,*)
      end do

      write(*,*) 'MT radial integrals'
      call wfreal_radint_overlap
      write(*,*) 'IR overlap matrix'
      call wfreal_irmat_overlap

      olp = zzero
      do ir = 1, wf_nrpt
        olp2 = zzero
        call wfreal_mtintegral( ir, ir, 1, olp2)
        olp = olp + olp2/dble( wf_rmul( ir))
        call r3mv( input%structure%crystal%basevect, dble( wf_rvec( :, ir)), vr)
        rmat(:,:,1) = rmat(:,:,1) - vr(1)*olp2/dble( wf_rmul( ir))
        rmat(:,:,2) = rmat(:,:,2) - vr(2)*olp2/dble( wf_rmul( ir))
        rmat(:,:,3) = rmat(:,:,3) - vr(3)*olp2/dble( wf_rmul( ir))
      end do
      call plotmat( olp)
      write(*,*)

      do ist = 1, wf_nwf
        write(*,'(i,3f13.6)') ist, wf_centers( :, ist)
        write(*,'(i,3f13.6)') ist, dble( rmat( ist, ist, :))
        write(*,*)
      end do
      
      return
      
    end subroutine wfreal_do
end module mod_wannier_realspace
