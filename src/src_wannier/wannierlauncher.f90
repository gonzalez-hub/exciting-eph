subroutine wannierlauncher
    use modinput
    use mod_wannier
    use mod_wannier_util
    use mod_wannier_maxloc
    use mod_wannier_interpolate
    use mod_manopt

    implicit none

    integer :: dxo(2), kx, ik, ir
    integer, allocatable :: dx(:,:)
    real(8), allocatable :: rX(:,:,:)
    complex(8), allocatable :: cX(:,:,:)

    dxo = (/1, 1/)
    kx = 1

    if( .false.) then
      allocate( dx(2,kx), rX(dxo(1),dxo(2),kx), cX(dxo(1),dxo(2),kx))
      do ik = 1, kx
        do ir = 1, dxo(1)
          rX(ir,:,ik) = 1.d0*ir
        end do
        dx(:,ik) = (/1, 1/)
      end do

      ir = 30
      call manopt_euclid_lbfgs( rX, dxo, kx, dx, cost, grad, stdout=6, maxit=ir)
      write(*,*) rX
      !call manopt_euclid_cg( cX, dxo, kx, dx)
      !call manopt_euclid_lbfgs( rX, dxo, kx, dx)
      !call manopt_euclid_lbfgs( cX, dxo, kx, dx)
      stop
    end if

    if( associated( input%properties%wannier)) then
      ! generate Wannier functions
      call wannier_init
      if( input%properties%wannier%do .eq. "fromscratch") then
        !if( input%properties%wannier%printproj) call wffile_writeinfo_lo
        call wffile_writeinfo_overall
        if( mpiglobal%rank .eq. 0) call printbox( wf_info, '*', "Wannierization")
        do wf_group = 1, wf_ngroups
          call wffile_writeinfo_task
          if( wf_groups( wf_group)%method .eq. "pro") then
            if( mpiglobal%rank .eq. 0) call wannier_gen_pro
          else if( wf_groups( wf_group)%method .eq. "opf") then
            if( mpiglobal%rank .eq. 0) call wfopf_gen
          else if( wf_groups( wf_group)%method .eq. "promax") then
            if( mpiglobal%rank .eq. 0) call wannier_gen_pro
            if( mpiglobal%rank .eq. 0) call wfmax_gen
          else if( wf_groups( wf_group)%method .eq. "opfmax") then
            !if( mpiglobal%rank .eq. 0) call wfiopf_gen( subspace=.true.)
            if( mpiglobal%rank .eq. 0) call wfopf_gen!( subspace=.false.)
            if( mpiglobal%rank .eq. 0) call wfmax_gen
          else if( any( wf_groups( wf_group)%method .eq. (/"disSMV","disFull"/))) then
            if( mpiglobal%rank .eq. 0) call wfopf_gen
            if( mpiglobal%rank .eq. 0) call wfdis_gen2
            if( mpiglobal%rank .eq. 0) call wfopf_gen( subspace=.true.)
            if( mpiglobal%rank .eq. 0) call wfmax_gen
          else
            write(*,*) " Error (propertylauncher): invalid value for attribute method"
          end if
        end do
        call wffile_writetransform
      
      else if( input%properties%wannier%do .eq. "fromfile") then
        call wannier_gen_fromfile
        !if( input%properties%wannier%printproj) call wffile_writeinfo_lo

      else
        call wannier_gen_fromfile
        !if( input%properties%wannier%printproj) call wffile_writeinfo_lo
        call wffile_writeinfo_overall
        do wf_group = 1, wf_ngroups
          call wffile_writeinfo_task
          if( mpiglobal%rank .eq. 0) call wfmax_fromfile
        end do
        call wffile_writetransform
      end if

      if( input%properties%wannier%do .ne. "fromfile") call wffile_writeinfo_finish

      ! share results over all processes
#ifdef MPI
      call barrier
      call mpi_bcast( wf_transform, wf_nst*wf_nwf*wf_kset%nkpt, mpi_double_complex, 0, mpiglobal%comm, ierr)
#endif

      ! further tasks if requested
      ! bandstructure
      if( associated( input%properties%bandstructure)) then
        if( input%properties%bandstructure%wannier) call wfutil_bandstructure
        !if( input%properties%bandstructure%wannier) call wfint_pwmat
      end if
      ! density of states
      if( associated( input%properties%dos)) then
        if( input%properties%dos%wannier) call wfutil_dos
      end if
      ! band gap
      if( associated( input%properties%wanniergap)) then
        call wfutil_find_bandgap
      end if
      ! visualization
      if( associated( input%properties%wannierplot)) then
        if( mpiglobal%rank .eq. 0) call wfutil_plot( input%properties%wannierplot%fst, input%properties%wannierplot%lst, input%properties%wannierplot%cell)
      end if
      ! dielectric function
      if( associated( input%properties%dielmat)) then
        if( input%properties%dielmat%wannier) call wfutil_dielmat
      end if
    else
      write(*,*) " Error (wannierlauncher): Wannier element in input not found."
      stop
    end if

    contains
      subroutine cost( X, dxo, kx, dx, f)
        integer, intent( in)  :: dxo(2), kx, dx(2,kx)
        real(8), intent( in)  :: X(dxo(1),dxo(2),*)
        real(8), intent( out) :: f

        integer :: ik
        real(8), external :: ddot
        
        f = 0.d0
        !do ik = 1, kx
        !  f = f + ddot( dx(1,ik)*dx(2,ik), X( 1:dx(1,ik), 1:dx(2,ik), ik), 1, X( 1:dx(1,ik), 1:dx(2,ik), ik), 1)
        !end do
        f = (2.d0*x(1,1,1)**3 - x(1,1,1)**2 + 3.d0*x(1,1,1))**2 + exp(x(1,1,1))
        return
      end subroutine

      subroutine grad( X, dxo, kx, dx, G, dgo)
        integer, intent( in)  :: dxo(2), kx, dx(2,kx), dgo(2)
        real(8), intent( in)  :: X(dxo(1),dxo(2),*)
        real(8), intent( out) :: G(dgo(1),dgo(2),*)

        integer :: ik

        !do ik = 1, kx
        !  G(:,:,ik) = 0.d0
        !  G( 1:dx(1,ik), 1:dx(2,ik), ik) = 2.d0*X( 1:dx(1,ik), 1:dx(2,ik), ik)
        !end do
        g(1,1,1) = (6.d0*x(1,1,1)**2 - 2.d0*x(1,1,1) + 3.d0) * &
          2.d0*(2.d0*x(1,1,1)**3 - x(1,1,1)**2 + 3.d0*x(1,1,1)) + exp(x(1,1,1))
        return
      end subroutine

end subroutine wannierlauncher
