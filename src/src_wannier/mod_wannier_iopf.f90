module mod_wannier_iopf
  use mod_wannier_variables
  use mod_wannier_omega
  use m_linalg
  use m_plotmat

  implicit none

  private
  
! module variables
  integer :: N0, N, J, P
  integer :: dxo(2)
  logical :: sub
  real(8) :: epssvd = 1.d-4
  integer, allocatable :: dx(:,:)
  complex(8), allocatable :: X(:,:,:)
  complex(8), allocatable :: A(:,:,:)
  complex(8), allocatable :: U0(:,:,:)

! methods
  public :: wfiopf_gen
    
contains
    subroutine wfiopf_gen( subspace)
      use mod_stiefel_optim
      use m_getunit
      use mod_misc, only: filext

      logical, optional, intent( in) :: subspace

      integer :: convun, minit, maxit, projun
      real(8) :: gradnorm

      integer :: ik, i
      real(8) :: t0, t1, omegastart, omega
      character(256) :: convfname

      real(8), allocatable :: eval(:)
      complex(8), allocatable :: AA(:,:), evec(:,:)

      write( wf_info, '(" calculate improved optimized projection functions (iOPFs)...")')
      call timesec( t0)
      minit    = input%properties%wannier%grouparray( wf_group)%group%minitopf
      maxit    = input%properties%wannier%grouparray( wf_group)%group%maxitopf
      gradnorm = input%properties%wannier%grouparray( wf_group)%group%epsopf

      !****************************
      !* PREPARATION
      !****************************
      sub = .false.
      if( present( subspace)) sub = subspace

      J = wf_groups( wf_group)%nwf
      P = wf_groups( wf_group)%nproj
      N0 = wf_groups( wf_group)%nst
      N = N0
      if( sub) N = J
      allocate( U0(N0,J,wf_kset%nkpt))
      allocate( A(N,P,wf_kset%nkpt))
      allocate( X(P,J,1))

      U0 = wf_transform( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, &
                         wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, :)

      ! set size of OPF matrix
      dxo = (/P,J/)
      allocate( dx(2,1))
      dx(:,1) = dxo

      ! build projection matrices
      do ik = 1, wf_kset%nkpt
        if( sub) then
          call zgemm( 'c', 'n', J, P, N0, zone, &
                 U0(1,1,ik), N0, &
                 wf_groups( wf_group)%projection(:,:,ik), N0, zzero, &
                 A(1,1,ik), N)
        else
          A(:,:,ik) = wf_groups( wf_group)%projection(:,:,ik)
        end if
      end do

      ! initialize X
      allocate( AA(P,P), eval(P), evec(P,P))
      AA = zzero
      do ik = 1, wf_kset%nkpt
        call zgemm( 'c', 'n', P, P, N, zone, A(1,1,ik), N, A(1,1,ik), N, zone, AA, P)
      end do
      AA = AA/dble( wf_kset%nkpt)
      call zhediag( AA, eval, evec=evec)
      write(*,'(2f13.6)') sum( eval)/dble(J), sum( eval((P-J+1):P))/dble(J)
      do ik = 1, J
        X(:,ik,1) = evec(:,P-ik+1)
      end do
      deallocate( AA, eval, evec)

      call wfiopf_omega( X, dxo, 1, dx, omegastart)
      write(*,*) N, J, P
      write(*,'(f13.6)') omegastart

      !****************************
      !* MINIMIZATION
      !****************************
      convun = 0
      if( input%properties%wannier%grouparray( wf_group)%group%writeconv) then
        call getunit( convun)
        write( convfname, '("opfloc_conv_",i3.3,".dat")'), wf_group
        open( convun, file=trim( convfname), action='write', form='formatted')
      end if
      if( input%properties%wannier%grouparray( wf_group)%group%optim .eq. 'cg') then
        call stiefel_optim_cg( X, dxo, 1, dx, &
               wfiopf_omega, wfiopf_gradient, &
               tolgrad=gradnorm, miniter=minit, maxiter=maxit, outunit=convun, &
               minstep=input%properties%wannier%grouparray( wf_group)%group%minstep)
      else
        call stiefel_optim_lbfgs( X, dxo, 1, dx, &
               wfiopf_omega, wfiopf_gradient, &
               tolgrad=gradnorm, miniter=minit, maxiter=maxit, outunit=convun, &
               memory=input%properties%wannier%grouparray( wf_group)%group%memlen, &
               minstep=input%properties%wannier%grouparray( wf_group)%group%minstep)
      end if
      if( input%properties%wannier%grouparray( wf_group)%group%writeconv) close( convun)
      
      !****************************
      !* FINALIZATION
      !****************************
      ! determine phases for logarithms
      do i = 1, 0
        call wfomega_m
        call wfomega_diagphases( wf_transform( wf_groups( wf_group)%fst, wf_groups( wf_group)%fwf, 1), wf_nst, wf_nwf, wf_groups( wf_group)%nst)
      end do
      ! calculate spread
      call wfomega_gen
      omega = sum( wf_omega( wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf))
  
      if( allocated( wf_opf)) deallocate( wf_opf)
      allocate( wf_opf(P,J))
      wf_opf = X(:,:,1)

      call timesec( t1)
      write( wf_info, '(5x,"duration (seconds): ",T40,3x,F10.1)') t1-t0
      !write( wf_info, '(5x,"minimum/maximum iterations: ",T40,I6,"/",I6)') minit, maxit
      write( wf_info, '(5x,"iterations: ",T40,7x,I6)') maxit
      write( wf_info, '(5x,"gradient cutoff: ",T40,E13.6)') input%properties%wannier%grouparray( wf_group)%group%epsopf
      write( wf_info, '(5x,"norm of gradient: ",T40,E13.6)') gradnorm
      !if( input%properties%wannier%grouparray( wf_group)%group%uncertainty .gt. 0.d0) then
      !  write( wf_info, '(5x,"aimed uncertainty: ",T40,E13.6)') input%properties%wannier%grouparray( wf_group)%group%uncertainty
      !  write( wf_info, '(5x,"achieved uncertainty: ",T40,E13.6)') uncertainty
      !end if
      write( wf_info, '(5x,"Omega: ",T40,F13.6)') omega
      !write( wf_info, '(5x,"reduction: ",T40,7x,I5,"%")') nint( 100d0*(omegastart-omega)/omegastart)
      write( wf_info, *)
      call flushifc( wf_info)

      if( input%properties%wannier%printproj .and. sub) then
        call getunit( projun)
        open( projun, file=trim( wf_filename)//"_PROJECTION"//trim(filext), action='write', form='formatted', status='old', position='append')
        call wfiopf_write_opf( projun)
        close( projun)
      end if

      deallocate( U0, A, X, dx)
    end subroutine wfiopf_gen

    subroutine wfiopf_omega( X, dxo, kx, dx, omega)
      integer, intent( in)    :: dxo(2), kx, dx(2,kx)
      complex(8), intent( in) :: X(dxo(1),dxo(2),*)
      real(8), intent( out)   :: omega

      integer :: ik, i

      real(8), allocatable :: s(:)
      complex(8), allocatable :: U(:,:), V(:,:), W(:,:)

#ifdef USEOMP
!$omp parallel default( shared) private( ik, s, V, W, U)
#endif
      allocate( s(J), V(N,J), W(J,J), U(N,J))
#ifdef USEOMP
!$omp do
#endif

      do ik = 1, wf_kset%nkpt
        call wfiopf_X2U( X, dxo, dx(:,1), &
               A(1,1,ik), (/N,P/), &
               U, (/N,J/), &
               s, V, W)
        if( sub) then
          call zgemm( 'n', 'n', N0, J, J, zone, &
                 U0(1,1,ik), N0, &
                 U, N, zzero, &
                 wf_transform( wf_groups( wf_group)%fst, wf_groups( wf_group)%fwf, ik), wf_nst)
        else
          wf_transform( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, &
                        wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, ik) = U
        end if
      end do
#ifdef USEOMP
!$omp end do
#endif
      deallocate( s, U, V, W)
#ifdef USEOMP
!$omp end parallel
#endif
      call wfomega_gen( totonly=.true.)
      omega = sum( wf_omega( wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf))
      return
    end subroutine wfiopf_omega

    subroutine wfiopf_update( X, dxo, kx, dx, it)
      integer, intent( in)       :: dxo(2), kx, dx(2,kx), it
      complex(8), intent( inout) :: X(dxo(1),dxo(2),*)

      integer :: ik, i

      real(8), allocatable :: s(:)
      complex(8), allocatable :: U(:,:), V(:,:), W(:,:)

#ifdef USEOMP
!$omp parallel default( shared) private( ik, s, V, W, U)
#endif
      allocate( s(J), V(N,J), W(J,J), U(N,J))
#ifdef USEOMP
!$omp do
#endif

      do ik = 1, wf_kset%nkpt
        call wfiopf_X2U( X, dxo, dx(:,1), &
               A(1,1,ik), (/N,P/), &
               U, (/N,J/), &
               s, V, W)
        if( sub) then
          call zgemm( 'n', 'n', N0, J, J, zone, &
                 U0(1,1,ik), N0, &
                 U, N, zzero, &
                 wf_transform( wf_groups( wf_group)%fst, wf_groups( wf_group)%fwf, ik), wf_nst)
        else
          wf_transform( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, &
                        wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, ik) = U
        end if
      end do
#ifdef USEOMP
!$omp end do
#endif
      deallocate( s, U, V, W)
#ifdef USEOMP
!$omp end parallel
#endif
      call wfomega_m
      call wfomega_diagphases( wf_transform( wf_groups( wf_group)%fst, wf_groups( wf_group)%fwf, 1), &
                               wf_nst, wf_nwf, wf_groups( wf_group)%nst)
      return
    end subroutine wfiopf_update

    subroutine wfiopf_gradient( X, dxo, kx, dx, GX, dgo)
      integer, intent( in)     :: dxo(2), kx, dx(2,kx), dgo(2)
      complex(8), intent( in)  :: X(dxo(1),dxo(2),*)
      complex(8), intent( out) :: GX(dgo(1),dgo(2),*)

      integer :: ik, i
      real(8), allocatable :: s(:), F(:,:)
      complex(8), allocatable :: U(:,:), V(:,:), W(:,:), GU0(:,:), GU(:,:), AV(:,:), VGUW(:,:)
      complex(8), allocatable :: CPJ(:,:), CNN(:,:), CJJ1(:,:), CJJ2(:,:), CNJ1(:,:), CNJ2(:,:)

      complex(8), external :: zdotc

      GX(:,:,1) = zzero
      !write(*,*)
#ifdef USEOMP
!$omp parallel default( shared) private( ik, s, U, F, V, W, GU0, GU, AV, VGUW, CPJ, CNN, CJJ1, CJJ2, CNJ1, CNJ2)
#endif
      allocate( s(J), U(N,J), F(J,J), V(N,J), W(J,J), GU0(N0,J), GU(N,J), AV(P,J), VGUW(J,J))
      allocate( CPJ(P,J), CNN(N,N), CJJ1(J,J), CJJ2(J,J), CNJ1(N,J), CNJ2(N,J))
#ifdef USEOMP
!$omp do
#endif
      do ik = 1, wf_kset%nkpt
        !write(*,*) ik
        !call plotmat( A(:,:,ik))
        call wfiopf_X2U( X, dxo, dx(:,1), &
               A(1,1,ik), (/N,P/), &
               U, (/N,J/), &
               s, V, W)
        !write(*,*) "----"
        !call plotmat( U)
        !write(*,*) "----"
        if( sub) then
          call zgemm( 'n', 'n', N0, J, J, zone, &
                 U0(1,1,ik), N0, &
                 U, N, zzero, &
                 wf_transform( wf_groups( wf_group)%fst, wf_groups( wf_group)%fwf, ik), wf_nst)
        else
          wf_transform( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, &
                        wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, ik) = U
        end if
        call wfomega_gradu( ik, GU0, N0)
        if( sub) then
          call zgemm( 'c', 'n', N, J, N0, zone, U0(1,1,ik), N0, GU0, N0, zzero, GU, N)
        else
          GU = GU0
        end if
        !write(*,*) "GU"
        !call plotmat( GU)
        !write(*,'(100f13.6)') s
        call wfiopf_getF( s, J, F)
        !write(*,*) "F"
        !call plotmat( cmplx( F, 0.d0, 8))
        !write(*,'(i,g20.10)') ik, dble( zdotc( N*J, GU, 1, GU, 1))

        call zgemm( 'c', 'n', J, J, N, zone, V, N, GU, N, zzero, CJJ1, J)
        call zgemm( 'n', 'n', J, J, J, zone, CJJ1, J, W, J, zzero, VGUW, J)
        !write(*,*) "VGUW"
        !call plotmat( VGUW)
        call zgemm( 'c', 'n', P, J, N, zone, A(1,1,ik), N, V, N, zzero, AV, P)
        !write(*,*) "AV"
        !call plotmat( AV)
        CJJ1 = cmplx( F, 0.d0, 8)*VGUW
        CJJ1 = CJJ1 + conjg( transpose( CJJ1))
        !write(*,*) "CJJ1"
        !call plotmat( CJJ1)
        do i = 1, J
          if( s(i) .gt. epssvd) then
            CJJ2(:,i) = CJJ1(:,i)*cmplx( s(i), 0.d0, 8)
          else
            CJJ2(:,i) = zzero
          end if
        end do
        do i = 1, J
          if( s(i) .gt. epssvd) then
            CJJ2(i,:) = CJJ2(i,:) - CJJ1(i,:)*cmplx( s(i), 0.d0, 8)
          else
            CJJ2(i,:) = zzero
          end if
        end do
        !write(*,*) "CJJ2"
        !call plotmat( CJJ2)
        call zgemm( 'n', 'c', J, J, J, zone, CJJ2, J, W, J, zzero, CJJ1, J)
        !write(*,*) "CJJ1"
        !call plotmat( CJJ1)
        call zgemm( 'n', 'n', P, J, J, zone, AV, P, CJJ1, J, zone, GX, dgo(1))

        do i = 1, J
          if( s(i) .gt. epssvd) then
            CJJ1(:,i) = W(:,i)/cmplx( s(i), 0.d0, 8)
          else
            CJJ1(:,i) = zzero
          end if
        end do
        call zgemm( 'n', 'c', J, J, J, zone, CJJ1, J, W, J, zzero, CJJ2, J)
        call zgemm( 'n', 'n', N, J, J, zone, GU, N, CJJ2, J, zzero, CNJ1, N)
        call zgemm( 'n', 'c', N, N, J, -zone, V, N, V, N, zzero, CNN, N)
        do i = 1, N
          CNN(i,i) = CNN(i,i) + zone
        end do
        call zgemm( 'n', 'n', N, J, N, zone, CNN, N, CNJ1, N, zzero, CNJ2, N)
        call zgemm( 'c', 'n', P, J, N, zone, A(1,1,ik), N, CNJ2, N, zone, GX, dgo(1))

        !do i = 1, J
        !  if( s(i) .gt. epssvd) then
        !    CNJ1(:,i) = V(:,i)/cmplx( s(i), 0.d0, 8)
        !  else
        !    CNJ1(:,i) = zzero
        !  end if
        !end do
        !call zgemm( 'n', 'c', N, N, J, zone, CNJ1, N, V, N, zzero, CNN, N)
        !call zgemm( 'n', 'n', N, J, N, zone, CNN, N, GU, N, zzero, CNJ1, N)
        !call zgemm( 'n', 'c', J, J, J, -zone, V, N, V, N, zzero, CNN, N)
        !do i = 1, N
        !  CNN(i,i) = CNN(i,i) + zone
        !end do
        !call zgemm( 'n', 'n', N, J, N, zone, CNN, N, CNJ1, N, zzero, CNJ2, N)
        !call zgemm( 'c', 'n', P, J, N, zone, A(1,1,ik), N, CNJ2, N, zone, GX, dgo(1))

      end do
#ifdef USEOMP
!$omp end do
#endif
      deallocate( s, F, V, W, U, GU, AV, VGUW)
      deallocate( CPJ, CNN, CJJ1, CJJ2, CNJ1, CNJ2)
#ifdef USEOMP
!$omp end parallel
#endif
      return
    end subroutine wfiopf_gradient

    subroutine wfiopf_X2U( X, dxo, dx, A, da, U, du, s, V, W)
      integer, intent( in)     :: dxo(2), dx(2), da(2), du(2)
      complex(8), intent( in)  :: X(dxo(1),dxo(2))
      complex(8), intent( in)  :: A(da(1),da(2))
      complex(8), intent( out) :: U(du(1),du(2))
      real(8), intent( out)    :: s(du(2))
      complex(8), intent( out) :: V(du(1),du(2)), W(du(2),du(2))

      integer :: i
      complex(8), allocatable :: AX(:,:), tmp(:,:)

      allocate( AX( da(1), du(2)))
      allocate( tmp( da(1), da(1)))
      call zgemm( 'n', 'n', da(1), du(2), dx(1), zone, A, da(1), X, dxo(1), zzero, AX, da(1))
      call zsvd( AX, s, tmp, W)
      V = tmp( 1:N, 1:J)
      do i = 1, J
        if( s(i) .le. epssvd) V(:,i) = zzero
      end do
      W = conjg( transpose( W))
      call zgemm( 'n', 'c', du(1), du(2), du(2), zone, V, du(1), W, du(2), zzero, U, du(1))
      deallocate( AX, tmp)
      return
    end subroutine wfiopf_X2U

    subroutine wfiopf_getF( s, J, F)
      integer, intent( in)  :: J
      real(8), intent( in)  :: s(J)
      real(8), intent( out) :: F(J,J)

      integer :: k, l
       
      F = 0.d0
      do k = 1, J
        if( s(k) .le. epssvd) cycle
        do l = k+1, J
          if( s(l) .le. epssvd) cycle
          F(k,l) = 1.d0/(s(l)*s(l) - s(k)*s(k))
          F(l,k) = -F(k,l)
        end do
      end do
      return
    end subroutine wfiopf_getF

    subroutine wfiopf_write_opf( un)
      integer, intent( in) :: un

      integer :: i, j, n

      if( wf_group .eq. 1) then
        n = 0
        do i = 1, wf_ngroups
          n = max( n, wf_groups( i)%nproj)
        end do
        call printbox( un, '*', "Optimized projection functions")
        write( un, *)
        write( un, '(80("-"))')
        write( un, '(7x,"#",4x)', advance='no')
        do i = 1, n
          write( un, '(i3,2x)', advance='no') i
        end do
        write( un, *)
        write( un, '(80("-"))')
      end if
      do i = 1, wf_groups( wf_group)%nwf
        write( un, '(4x,i4,4x)', advance='no') i
        do j = 1, wf_groups( wf_group)%nproj
          write( un, '(i3,2x)', advance='no') nint( 1.d2*abs( wf_opf(j,i))**2)
        end do
        write( un, *)
      end do
      write( un, '(80("-"))')
      if( wf_group .eq. wf_ngroups) write( un, *)
      return
    end subroutine wfiopf_write_opf

end module mod_wannier_iopf
