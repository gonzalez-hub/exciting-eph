module mod_polylog
  use mod_constants
  implicit none

  real(8), parameter :: zeta3    = 1.2020569031595943d0
  real(8), parameter :: dzetam3  = 5.3785763577743011d-3
  real(8), parameter :: lng      = 2.4875447703378426d-1
  real(8), parameter :: eulmas   = 5.7721566490153286d-1
  real(8), parameter :: ln2pi    = 1.8378770664093455d0

  contains

    function polylog( n, z) result( f)
      integer, intent( in)    :: n
      complex(8), intent( in) :: z
      complex(8)              :: f
      
      if( n .eq. 1) then
        f = -log( zone - z)
      else if( n .eq. 2) then
        f = plog2( z)
      else if( n .eq. 3) then
        f = plog3( z)
      else if( n .eq. 4) then
        f = plog4( z)
      else
        write(*,*)
        write(*,'("Error( polylog): Polylogarithm not yet implemented for n = ",i)') n
        stop
      end if
    end function polylog

    !*******************************************
    ! DILOGARITHM Li(2,z)
    !*******************************************
    function plog2i( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f
      
      real(8), parameter :: b(11) = (/ 1.D0,                    0.8333333333333333D-1,  -0.1388888888888889D-2, &
                                       0.3306878306878307D-4,  -0.8267195767195767D-6,   0.208767569878681D-7, &
                                      -0.5284190138687493D-9,   0.1338253653068468D-10, -0.3389680296322583D-12, &
                                       0.8586062056277845D-14, -0.2174868698558062D-15/)
      integer, parameter :: nmax = 11

      complex(8) :: tz, tz2
      integer :: i
      
      tz = -log( zone - z)
      tz2 = tz*tz
      f = -tz2*0.25d0
      do i = 1, nmax
        f = f + tz**(2*i-1)*b( i)/(2.d0*i - 1.d0)
      end do

      return
    end function plog2i

    function plog2o( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f
      
      real(8), parameter :: b(29) = (/-0.01388888888888889d0, 0.00006944444444444444d0, -7.873519778281683d-7, &
                                       1.148221634332745d-8, -1.897886998897100d-10,     3.387301370953521d-12, &
                                      -6.372636443183180d-14, 1.246205991295067d-15,    -2.510544460899955d-17, &
                                       5.178258806090624d-19,-1.088735736830085d-20,     2.325744114302087d-22, &
                                      -5.035195213147390d-24, 1.102649929438122d-25,    -2.438658550900734d-27, &
                                       5.440142678856252d-29,-1.222834013121735d-30,     2.767263468967951d-32, &
                                      -6.300090591832014d-34, 1.442086838841848d-35,    -3.317093999159543d-37, &
                                       7.663913557920658d-39,-1.777871473383066d-40,     4.139605898234137d-42, &
                                      -9.671557036081102d-44, 2.266718701676613d-45,    -5.327956311328254d-47, &
                                       1.255724838956433d-48,-2.967000542247094d-50/)
      real(8), parameter :: b0 = 1.644934066848226d0
      integer, parameter :: nmax = 11

      complex(8) :: tz, tz2
      integer :: i
      
      tz = log( z)
      tz2 = tz*tz
      f = cmplx( b0, 0.d0, 8) + tz*(zone - log( -tz)) - tz2*0.25d0
      do i = 1, nmax
        f = f + tz**(2*i+1)*b( i)
      end do

      return
    end function plog2o

    function plog2( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f
      
      real(8) :: sig
      complex(8) :: tz, tz2
      integer :: i
      
      sig = 1.d0

      if( abs( z) .gt. 1.d0) then
        tz = log( z)
        f = pi*pi/3.d0 + zi*pi*tz - tz*tz*0.5d0
        if( (aimag( z) .lt. 0.d0) .or. ((aimag( z) .eq. 0.d0) .and. (dble( z) .ge. 1.d0))) then
          f = f - twopi*zi*tz
        end if
        tz = zone/z
        sig = -1.d0
      else
        tz = z
      end if

      if( abs( tz) .gt. 0.4d0) then
        f = f + sig*plog2o( tz)
      else
        f = f + sig*plog2i( tz)
      end if

      return
    end function plog2
      
    !*******************************************
    ! TRILOGARITHM Li(3,z)
    !*******************************************
    function plog3i( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f
      
      real(8), parameter :: b(21) = (/ 1d0,                      -0.7500000000000000d0,     0.2361111111111111d0, &
                                      -0.03472222222222222d0,     0.0006481481481481481d0,  0.0004861111111111111d0, &
                                      -0.00002393550012597632d0, -0.00001062925170068027d0, 7.794784580498866d-7, &
                                       2.526087595532040d-7,     -2.359163915200471d-8,    -6.168132746415575d-9, &
                                       6.824456748981078d-10,     1.524285616929085d-10,   -1.916909414174054d-11, &
                                      -3.791718683693992d-12,     5.277408409541286d-13,    9.471165533842511d-13, &
                                      -1.432311114490360d-14,    -2.372464515550457d-15,    3.846565792753191d-16/)
      integer, parameter :: nmax = 21

      complex(8) :: tz
      integer :: i
      
      tz = -log( zone - z)
      f = zzero
      do i = 1, nmax
        f = f + tz**i*b( i)/dble( i)
      end do

      return
    end function plog3i

    function plog3o( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f
      
      real(8), parameter :: b(29) = (/-0.003472222222222222d0,  0.00001157407407407407d0, -9.841899722852104d-8, &
                                       1.148221634332745d-9,   -1.581572499080917d-11,     2.419500979252515d-13, &
                                      -3.982897776989488d-15,   6.923366618305929d-17,    -1.255272230449977d-18, &
                                       2.353754002768465d-20,  -4.536398903458687d-22,     8.945169670392643d-24, &
                                      -1.798284004695496d-25,   3.675499764793738d-27,    -7.620807971564795d-29, &
                                       1.600041964369486d-30,  -3.396761147560376d-32,     7.282272286757765d-34, &
                                      -1.575022647958003d-35,   3.433540092480589d-37,    -7.538849998089870d-39, &
                                       1.666068164765360d-40,  -3.703898902881387d-42,     8.279211796468275d-44, &
                                      -1.859914814630981d-45,   4.197627225327060d-47,    -9.514207698800454d-49, &
                                       2.165042825786954d-50,  -4.945000903745158d-52/)
      real(8), parameter :: b0 =  1.202056903159594d0
      real(8), parameter :: b1 =  1.644934066848226d0
      real(8), parameter :: b2 = -0.08333333333333333d0
      integer, parameter :: nmax = 11

      complex(8) :: tz, tz2
      integer :: i
      
      tz = log( z)
      tz2 = tz*tz
      f = cmplx( b0, 0.d0, 8) + tz*b1 + tz*tz2*b2 + tz2*(1.5d0 -log( -tz))*0.5d0
      do i = 2, nmax+1
        f = f + tz2**i*b( i-1)
      end do

      return
    end function plog3o

    function plog3( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f
      
      real(8) :: sig
      complex(8) :: tz, tz2
      integer :: i
      
      sig = 1.d0

      if( abs( z) .gt. 1.d0) then
        tz = log( z)
        f = pi*pi/3.d0*tz + 0.5d0*zi*pi*tz*tz - tz*tz*tz/6.d0
        if( (aimag( z) .lt. 0.d0) .or. ((aimag( z) .eq. 0.d0) .and. (dble( z) .ge. 1.d0))) then
          f = f - twopi*zi*tz*tz*0.5d0
        end if
        tz = zone/z
        sig = 1.d0
      else
        tz = z
      end if

      if( abs( tz) .gt. 0.4d0) then
        f = f + sig*plog3o( tz)
      else
        f = f + sig*plog3i( tz)
      end if

      return
    end function plog3
      
    !*******************************************
    ! TETRALOGARITHM Li(4,z)
    !*******************************************
    function plog4i( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f
      
      real(8), parameter :: b(21) = (/ 1d0,                     -0.8750000000000000d0,       0.3495370370370370d0, &
                                      -0.07928240740740741d0,    0.009639660493827160d0,    -0.0001863425925925926d0, &
                                      -0.0001093680638040048d0,  0.000006788098837418565d0,  0.000002061865494287074d0, &
                                      -2.183261421852692d-7,    -4.271107367089217d-8,       6.535550523864399d-9, &
                                       9.049046773887543d-10,   -1.872603276102330d-10,     -1.917727902789986d-11, &
                                       5.216900572839828d-12,    4.020087098665104d-13,     -1.426164321965609d-13, &
                                      -8.256053984896996d-15,    3.847254012507184d-15,      1.640607009971150d-16/)
      integer, parameter :: nmax = 21

      complex(8) :: tz
      integer :: i
      
      tz = -log( zone - z)
      f = zzero
      do i = 1, nmax
        f = f + tz**i*b( i)/dble( i)
      end do

      return
    end function plog4i

    function plog4o( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f
      
      real(8), parameter :: b(28) = (/-0.0006944444444444444d0,  0.000001653439153439153d0, -1.093544413650234d-8, &
                                       1.043837849393405d-10,   -1.216594230062244d-12,      1.613000652835010d-14, &
                                      -2.342881045287934d-16,    3.643877167529436d-18,     -5.977486811666558d-20, &
                                       1.023371305551507d-21,   -1.814559561383475d-23,      3.313025803849127d-25, &
                                      -6.200979326536194d-27,    1.185645085417335d-28,     -2.309335748959029d-30, &
                                       4.571548469627103d-32,   -9.180435533946961d-34,      1.867249304296863d-35, &
                                      -3.841518653556106d-37,    7.984976959257184d-39,     -1.675299999575527d-40, &
                                       3.544825882479490d-42,   -7.558977352819157d-44,      1.623374862052603d-45, &
                                      -3.509273235152795d-47,    7.632049500594654d-49,     -1.669159245403588d-50, &
                                       3.669564111503313d-52/)
      real(8), parameter :: b0 =  1.082323233711138d0
      real(8), parameter :: b1 =  1.202056903159594d0
      real(8), parameter :: b2 =  0.8224670334241131d0
      real(8), parameter :: b3 = -0.02083333333333333d0
      integer, parameter :: nmax = 11

      complex(8) :: tz, tz2
      integer :: i
      
      tz = log( z)
      tz2 = tz*tz
      f = cmplx( b0, 0.d0, 8) + tz*b1 + tz2*b2 + tz2*tz*(1.5d0 + 1.d0/3.d0 -log( -tz))/6.0d0 + tz2*tz2*b3
      do i = 2, nmax+1
        f = f + tz*tz2**i*b( i-1)
      end do

      return
    end function plog4o

    function plog4( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f
      
      real(8) :: sig
      complex(8) :: tz, tz2
      integer :: i
      
      sig = 1.d0

      if( abs( z) .gt. 1.d0) then
        tz = log( z)
        f = pi**4/45.d0 + pi*pi/6.d0*tz*tz + zi*pi/6.d0*tz*tz*tz - tz**4/24.d0
        if( (aimag( z) .lt. 0.d0) .or. ((aimag( z) .eq. 0.d0) .and. (dble( z) .ge. 1.d0))) then
          f = f - twopi*zi*tz*tz*tz/6.0d0
        end if
        tz = zone/z
        sig = -1.d0
      else
        tz = z
      end if

      if( abs( tz) .gt. 0.4d0) then
        f = f + sig*plog4o( tz)
      else
        f = f + sig*plog4i( tz)
      end if

      return
    end function plog4
      

end module mod_polylog
