subroutine genolpmat( kset, Gset, Gkset, ik, olp)
  use modinput
  use mod_kpointset
  use mod_Gkvector,    only: ngkmax, ngkmax_ptr
  use mod_Gvector,     only: cfunig
  use mod_constants,   only: zzero, zone
  use mod_atoms,       only: nspecies, natoms, natmtot, idxas
  use mod_muffin_tin,  only: idxlm
  use mod_APW_LO
  use mod_eigensystem, only: idxlo, oalo, ololo
  implicit none

  type( k_set), intent( in)  :: kset
  type( G_set), intent( in)  :: Gset
  type( Gk_set), intent( in) :: Gkset
  integer, intent( in)       :: ik
  complex(8), intent( out)   :: olp( Gkset%ngkmax+nlotot, Gkset%ngkmax+nlotot)

  integer :: l, m, lm1, lm2, o, ia, is, ias, ilo1, ilo2, ig, jg, igk, jgk, vg(3)
  integer :: ngkmax_, lmax, lmmax, ngp
  
  complex(8), allocatable :: sfac(:,:), apwalm(:,:,:,:), apwi(:,:)

  lmax = input%groundstate%lmaxapw
  lmmax = (lmax + 1)**2
  ngkmax_ = ngkmax_ptr
  ngkmax = Gkset%ngkmax
  ngp = Gkset%ngk( 1, ik)

  allocate( sfac( ngkmax_ptr, natmtot))
  allocate( apwalm( ngkmax_ptr, apwordmax, lmmax, natmtot))
  
  call gensfacgp( ngp, Gkset%vgkc( :, :, 1, ik), Gkset%ngkmax, sfac)
  call match( ngp, Gkset%gkc( :, 1, ik), Gkset%tpgkc( :, :, 1, ik), sfac, apwalm)

  deallocate( sfac)
  allocate( apwi( apwordmax*lmmax, ngp))

  olp = zzero

  ! MUFFIN-TIN PART
  do is = 1, nspecies
    do ia = 1, natoms( is)
      ias = idxas( ia, is)
      ! APW-APW
      ig = 0
      apwi = zzero
      do l = 0, lmax
        do m = -l, l
          lm1 = idxlm( l, m)
          do o = 1, apword( l, is)
            ig = ig + 1
            apwi( ig, :) = apwalm( 1:ngp, o, lm1, ias)
          end do
        end do
      end do
      call zgemm( 'c', 'n', ngp, ngp, ig, zone, &
             apwi, apwordmax*lmmax, &
             apwi, apwordmax*lmmax, zone, &
             olp, Gkset%ngkmax+nlotot)
      do ilo1 = 1, nlorb( is)
        ! APW-LO
        l = lorbl( ilo1, is)
        lm1 = idxlm( l, -l)
        lm2 = idxlm( l,  l)
        ig = ngp + idxlo( lm1, ilo1, ias)
        jg = ngp + idxlo( lm2, ilo1, ias)
        do o = 1, apword( l, is)
          olp( 1:ngp, ig:jg) = olp( 1:ngp, ig:jg) + conjg( apwalm( 1:ngp, o, lm1:lm2, ias))*oalo( o, ilo1, ias)
        end do
        olp( ig:jg, 1:ngp) = conjg( transpose( olp( 1:ngp, ig:jg)))
        ! LO-LO
        do ilo2 = 1, nlorb( is)
          if( lorbl( ilo2, is) .eq. l) then
            ig = ngp + idxlo( lm1, ilo1, ias)
            jg = ngp + idxlo( lm1, ilo2, ias)
            do m = 0, 2*l
              olp( ig+m, jg+m) = olp( ig+m, jg+m) + cmplx( ololo( ilo1, ilo2, ias), 0.d0, 8)
            end do
          end if
        end do
      end do
    end do
  end do
  deallocate( apwi)

  ! INTERSTITIAL PART
  do igk = 1, ngp
    ig = Gkset%igkig( igk, 1, ik)
    do jgk = 1, igk
      jg = Gkset%igkig( jgk, 1, ik)
      vg = Gset%ivg( :, ig) - Gset%ivg( :, jg)
      if( vg(1) .ge. Gset%intgv( 1, 1) .and. vg(1) .le. Gset%intgv( 1, 2) .and. &
          vg(2) .ge. Gset%intgv( 2, 1) .and. vg(2) .le. Gset%intgv( 2, 2) .and. &
          vg(3) .ge. Gset%intgv( 3, 1) .and. vg(3) .le. Gset%intgv( 3, 2)) then
        olp( igk, jgk) = olp( igk, jgk) + cfunig( Gset%ivgig( vg(1), vg(2), vg(3)))
        olp( jgk, igk) = conjg( olp( igk, jgk))
      end if
    end do
  end do
  
  ngkmax = ngkmax_

  return
end subroutine genolpmat
