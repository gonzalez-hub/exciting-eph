\contentsline {section}{\numberline {1}Routine/Function Prologues}{2}
\contentsline {subsubsection}{\numberline {1.0.1}r3taxi (Source File: r3taxi.f90)}{2}
\contentsline {subsection}{\numberline {1.1}Fortran: Module Interface modmain (Source File: modspacegroup.f90)}{2}
\contentsline {section}{\numberline {2}Introduction}{2}
\contentsline {section}{\numberline {3}Usage}{2}
\contentsline {section}{\numberline {4}Table of space group symbols}{3}
\contentsline {subsubsection}{\numberline {4.0.1}r3dot (Source File: r3dot.f90)}{15}
\contentsline {subsection}{\numberline {4.1}Fortran: Module Interface modmain (Source File: modmain.f90)}{15}
\contentsline {subsubsection}{\numberline {4.1.1}r3cross (Source File: r3cross.f90)}{15}
\contentsline {subsubsection}{\numberline {4.1.2}r3frac (Source File: r3frac.f90)}{16}
\contentsline {subsubsection}{\numberline {4.1.3}r3minv (Source File: r3minv.f90)}{16}
\contentsline {subsubsection}{\numberline {4.1.4}r3mm (Source File: r3mm.f90)}{16}
\contentsline {subsubsection}{\numberline {4.1.5}r3mv (Source File: r3mv.f90)}{17}
\contentsline {subsubsection}{\numberline {4.1.6}findprim (Source File: findprim.f90)}{17}
\contentsline {subsubsection}{\numberline {4.1.7}sgsymb (Source File: sgsymb.f90)}{18}
\contentsline {subsubsection}{\numberline {4.1.8}r3ws (Source File: r3ws.f90)}{18}
