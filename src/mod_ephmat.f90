! Based on mod_pwmat by S.T.
module mod_ephmat
  use modinput
  use modmpi
  use mod_atoms
  use mod_APW_LO
  use mod_muffin_tin
  use mod_constants
  use mod_eigensystem,          only : idxlo, nmatmax_ptr
  use mod_lattice,              only : bvec
  use mod_Gvector,              only : ngvec, gc, cfunig, cfunir, ivg, ivgig, ngrtot, ngrid, intgv, igfft
  use mod_Gkvector,             only : gkmax, ngkmax_ptr
  use mod_eigenvalue_occupancy, only: nstfv
  use mod_misc,                 only: filext
  use mod_kpointset
  use modxs,                    only: fftmap_type
  use m_plotmat

  implicit none
  private

  integer            :: ephmat_fst_kq, ephmat_lst_kq, ephmat_fst_k, ephmat_lst_k, ephmat_nst_kq, ephmat_nst_k, n
  integer            :: ng, nlammax, nlmlammax, ephmat_lmaxapw, ephmat_lmaxexp
  real(8)            :: ephmat_gmax
  real(8)            :: vecql(3)
  character(256)     :: ephmat_fname
  logical            :: ephmat_usefft
  type( k_set)       :: ephmat_kset  
  type( fftmap_type) :: ephmat_fftmap 

  integer, allocatable    :: nlam(:,:), nlmlam(:), lam2apwlo(:,:,:), idxlmlam(:,:,:), lm2l(:), idxgnt(:,:,:)
  integer, allocatable    :: vecgl(:,:)
  real(8), allocatable    :: listgnt(:,:,:)
  complex(8), allocatable :: rignt(:,:,:,:), ephmat_cfunir(:)

  real(8) :: ts, te, t0, t1
  real(8) :: ttot, tinit, tri, trignt, tgk, tprep, tmt, tgg, tir, tio

  public :: ephmat_init, ephmat_prepare, ephmat_init_qg, ephmat_genephmat, ephmat_destroy

! variables

! methods
  contains
      subroutine ephmat_init(apwlmax, explmax, kset, fst_kq, lst_kq, fst_k, lst_k, fname, gmax, fft)
          integer, intent( in) :: apwlmax, explmax, fst_kq, lst_kq, fst_k, lst_k
          type( k_set), intent( in) :: kset
          character(*), optional, intent( in) :: fname
          real(8), optional, intent( in) :: gmax
          logical, optional, intent( in) :: fft

          real(8) :: gnt
          real(8), external :: gaunt
          integer :: l1, l2, l3, m1, m2, m3, i, ilo, o1, lm1, lm2, lm3, lam, is

          ttot = 0.d0
          tinit = 0.d0
          tri = 0.d0
          trignt = 0.d0
          tgk = 0.d0
          tprep = 0.d0
          tmt = 0.d0
          tgg = 0.d0
          tir = 0.d0
          tio = 0.d0
 
          call timesec( ts)  

          ephmat_fname = 'EVEC'
          if( present(fname)) ephmat_fname = trim(fname)

          ephmat_gmax = input%groundstate%gmaxvr
          if( present(gmax)) ephmat_gmax = gmax
 
          ephmat_usefft = .false.
          if( present(fft)) ephmat_usefft = fft

          ephmat_lmaxapw = min( apwlmax, input%groundstate%lmaxapw)
          ephmat_lmaxexp = min( explmax, ephmat_lmaxapw)
          ephmat_kset = kset
          ephmat_fst_kq = fst_kq
          ephmat_lst_kq = lst_kq
          ephmat_fst_k = fst_k
          ephmat_lst_k = lst_k
          ephmat_nst_kq = ephmat_lst_kq-ephmat_fst_kq+1
          ephmat_nst_k  = ephmat_lst_k-ephmat_fst_k+1

          ! Number of phonon modes
          n = 3*natmtot

          ! Storage initialization. Count combined (l,m,o) indices and build index maps
          allocate(nlam( 0:ephmat_lmaxapw, nspecies))
          allocate(lam2apwlo( apwordmax+nlomax, 0:ephmat_lmaxapw, nspecies))
          nlammax = 0
          do is = 1, nspecies
              do l1 = 0, ephmat_lmaxapw
                  nlam( l1, is) = 0
                  lam2apwlo(:, l1, is) = 0
                  do o1 = 1, apword(l1, is)
                      nlam( l1, is) = nlam( l1, is)+1
                      lam2apwlo(nlam( l1, is), l1, is) = o1
                  end do
                  do ilo = 1, nlorb( is)
                      if( lorbl( ilo, is) .eq. l1) then
                          nlam( l1, is) = nlam( l1, is) + 1
                          lam2apwlo( nlam(l1, is), l1, is) = -ilo
                      end if
                  end do   
                  nlammax = max( nlammax, nlam(l1, is))
              end do
          end do

          allocate(lm2l((ephmat_lmaxapw + 1)**2))
          allocate(nlmlam(nspecies))
          allocate(idxlmlam((ephmat_lmaxapw + 1)**2, nlammax, nspecies))

          ! Auxiliary arrays for matrix-matrix multiplication
          idxlmlam(:,:,:) = 0          
          nlmlammax = 0
          nlmlam(:) = 0
          do l1 = 0, ephmat_lmaxapw
              do m1 = -l1, l1
                  lm1 = idxlm( l1, m1)
                  lm2l( lm1) = l1
                  do is = 1, nspecies
                      do lam = 1, nlam( l1, is)
                          nlmlam( is) = nlmlam( is) + 1
                          idxlmlam( lm1, lam, is) = nlmlam( is)
                      end do
                  end do
              end do
          end do
          nlmlammax = maxval( nlmlam, 1)

         ! Calculate Gaunt coefficients (non-zero)
          allocate( idxgnt( ephmat_lmaxapw + 1, (ephmat_lmaxapw + 1)**2, (ephmat_lmaxapw + 1)**2)) 
          allocate( listgnt( ephmat_lmaxapw + 1, (ephmat_lmaxapw + 1)**2, (ephmat_lmaxapw + 1)**2))
          idxgnt(:,:,:) = 0
          listgnt(:,:,:) = 0.d0
          do l3 = 0, ephmat_lmaxapw
              do m3 = -l3, l3 
                  lm3 = idxlm(l3, m3)

                  do l1 = 0, ephmat_lmaxapw
                      do m1 = -l1, l1
                          lm1 = idxlm( l1, m1)
                          
                          i = 0
                          do l2 = 0, ephmat_lmaxexp
                              do m2 = -l2, l2
                                  lm2 = idxlm(l2, m2)
                                  gnt = gaunt(l1, l2, l3, m1, m2, m3)
                                  if( abs( gnt) .gt. 1.d-20) then
                                      i = i + 1
                                      listgnt(i, lm1, lm3) = gnt
                                      idxgnt(i, lm1, lm3) = lm2
                                  end if
                              end do
                          end do
                      end do
                  end do
              end do
          end do
          call timesec( t1)
          tinit = tinit + t1 - ts

          return
      end subroutine ephmat_init

      subroutine ephmat_prepare( ik, evec)
          integer, intent(in)    :: ik
          complex(8), intent(in) :: evec( nmatmax_ptr, nstfv)

          integer :: ngp, is, ia, ias, lm, l, lam, o, ilo, fst, lst, nst
          integer :: ig, igp

          integer, allocatable :: igpig(:)
          real(8), allocatable :: vgpl(:,:), vgpc(:,:), gpc(:), tpgpc(:,:)
          complex(8), allocatable :: evecmt(:,:,:), evecir(:,:)
          complex(8), allocatable :: sfacgp(:,:), apwalm(:,:,:,:), auxvec(:)
 
          fst = min( ephmat_fst_kq, ephmat_fst_k)
          lst = max( ephmat_lst_kq, ephmat_lst_k)
          nst = lst-fst+1 

          call timesec( t0)
          allocate( evecmt( nlmlammax, fst:lst, natmtot))
          allocate( igpig( ngkmax_ptr), vgpl( 3, ngkmax_ptr), vgpc( 3, ngkmax_ptr), gpc( ngkmax_ptr), tpgpc( 2, ngkmax_ptr))
          allocate( sfacgp( ngkmax_ptr, natmtot))
          allocate( apwalm( ngkmax_ptr, apwordmax, lmmaxapw, natmtot))
          allocate( auxvec( fst:lst))

          ! Generate the G+k vectors
          call gengpvec( ephmat_kset%vkl( :, ik), ephmat_kset%vkc( :, ik), ngp, igpig, vgpl, vgpc, gpc, tpgpc)
          ! Generate the structure factors
          call gensfacgp( ngp, vgpc, ngkmax_ptr, sfacgp)
          ! Find matching coeffiients for k-point k+q
          call match( ngp, gpc, tpgpc, sfacgp, apwalm)

          evecmt = zzero
          do is = 1, nspecies
           do ia = 1, natoms(is)
            ias = idxas( ia, is)
            do lm = 1, (ephmat_lmaxapw + 1)**2
             l = lm2l( lm)
             do lam = 1, nlam(l, is)
              ! lam belongs to apw
              if( lam2apwlo( lam, l, is) .gt. 0) then
               o = lam2apwlo( lam, l, is)
               call zgemv( 't', ngp, nst, zone, &
                           evec(:, fst:lst), nmatmax_ptr, &
                           apwalm( :, o, lm, ias), 1, zzero, &
                           auxvec, 1)
               evecmt( idxlmlam( lm, lam, is), :, ias) = auxvec
              end if
              ! lam belongs to lo
              if( lam2apwlo( lam, l, is) .lt. 0) then
               ilo = -lam2apwlo( lam, l, is)
               evecmt( idxlmlam( lm, lam, is), :, ias) = evec( ngp+idxlo( lm, ilo, ias), fst:lst)
              end if
             end do
            end do
           end do
          end do
          
          call timesec( t1)
          tprep = tprep + t1 - t0
          call ephmat_putevecmt( ephmat_kset%vkl( :, ik), fst, lst, evecmt)
          call timesec(t0)
          tio = tio + t0 - t1
          deallocate( evecmt, vgpl, vgpc, gpc, tpgpc, sfacgp, apwalm, auxvec)
 
          if( ephmat_usefft) then 
           if( .not. allocated( ephmat_cfunir)) then
            call genfftmap( ephmat_fftmap, ephmat_gmax)
            allocate( ephmat_cfunir( ephmat_fftmap%ngrtot))
            ephmat_cfunir = zzero
            do ig = 1, ngvec
             if( gc( ig) .lt. ephmat_gmax) ephmat_cfunir( ephmat_fftmap%igfft( ig)) = cfunig( ig)
            end do
            call zfftifc(3, ephmat_fftmap%ngrid, 1, ephmat_cfunir)
           end if
           
           allocate( evecir( ephmat_fftmap%ngrtot, fst:lst))
           evecir = zzero
           do is = fst, lst
            do igp = 1, ngp
             ig = igpig( igp)
             evecir( ephmat_fftmap%igfft( ig), is) = evec( igp, is)
            end do
            call zfftifc( 3, ephmat_fftmap%ngrid, 1, evecir( :, is))
           end do
 
           call timesec( t1)
           tprep = tprep + t1 - t0
           call ephmat_putevecir( ephmat_kset%vkl( :, ik), fst, lst, evecir)
           call timesec( t0)
           tio = tio + t0 - t1
           deallocate( evecir)
          end if
         
          deallocate( igpig)
          call timesec( t1)
       
          tprep = tprep + t1 - t0
          return
      end subroutine ephmat_prepare

      subroutine ephmat_init_qg( vecql_, vecgl_, ng_, ephdveffmt)
          real(8), intent( in) :: vecql_(3)
          integer, intent( in) :: vecgl_( 3, ng), ng_
          complex(8), intent( in) :: ephdveffmt(lmmaxvr, nrmtmax, natmtot)

          integer :: l1, l2, l3, m1, m2, m3, o1, o2, lm1, lm2, lm3, is, ia, ias, i, ig, vg(3) 
          integer :: lam1, lam2, ilo1, ilo2, idxlo1, idxlo2, idx1, idx2
          
          complex(8), allocatable ::  radint(:,:,:)

          ng = ng_
          vecql = vecql_
          call r3frac( input%structure%epslat, vecql, vg)
          if( allocated(  vecgl)) deallocate( vecgl)
          allocate( vecgl( 3, ng))
          do ig = 1, ng
           vecgl( :, ig) = vecgl_(:, ig) + vg
          end do
          
          call timesec( t0)
          if( allocated( rignt)) deallocate( rignt)
          allocate(rignt( nlmlammax, nlmlammax, natmtot, ng))
          rignt = zzero

          allocate( radint( nlammax, nlammax, lmmaxvr))
          call readfermi

          do ig = 1, ng       
           do is = 1, nspecies
            do ia = 1, natoms( is)
              ias = idxas( ia, is)
             ! Generate radial integral
              do l1 = 0, ephmat_lmaxapw
                do l2 = 0, ephmat_lmaxapw
                  call timesec( t1)
                  trignt = trignt + t1 - t0
                  call ephmat_genri( is, ia, l1, l2, ig, ephdveffmt, radint)
                  call timesec( t0)
                  tri = tri + t0 - t1
                  do m1 = -l1, l1
                    lm1 = idxlm( l1, m1)
                    do m2 = -l2, l2
                      lm2 = idxlm( l2, m2)
      
                      do lam1 = 1, nlam( l1, is)
                         idx1 = idxlmlam( lm1, lam1, is)
                         do lam2 = 1, nlam( l2, is)
                            idx2 = idxlmlam( lm2, lam2, is)

                            i = 1
                            do while( idxgnt( i, lm1, lm2) .ne. 0) 
                              lm3 = idxgnt( i, lm1, lm2)
                              rignt( idx1, idx2, ias, ig) = rignt( idx1, idx2, ias, ig) + &
                                             & listgnt( i, lm1, lm2)*radint( lam1, lam2, lm3)
                              i = i + 1
                            end do

                         end do
                      end do
      
                    end do
                  end do
                end do
              end do

            end do ! atoms
           end do  ! species

          end do
          deallocate( radint) 
    
          call timesec( t1) 
          trignt = trignt + t1 - t0
          return
      end subroutine ephmat_init_qg

      subroutine ephmat_genri( is, ia, l1, l2, ig, ephdveffmt, radint)
         integer, intent( in) :: is, ia, l1, l2, ig
         complex(8), intent( in) :: ephdveffmt( lmmaxvr, nrmtmax, natmtot) 
         complex(8), intent( out) :: radint( nlammax, nlammax, lmmaxvr) 

         integer :: ias, ir, nr, l3, m3, lm3, o1, o2, ilo1, ilo2, lam1, lam2
         real(8), allocatable :: fr1(:), gf1(:), cf1(:,:)
         real(8), allocatable :: fr2(:), gf2(:), cf2(:,:)

         radint(:,:,:) = zzero 
         ias = idxas( ia, is)
         nr = nrmt( is)

#ifdef USEOMP
!!$omp parallel default( shared) private( lam1, lam2, o1, o2, ilo1, ilo2, l3, ir, fr, gf, cf)
#endif
         allocate( fr1( nr), gf1( nr), cf1( 3, nr))
         allocate( fr2( nr), gf2( nr), cf2( 3, nr))
#ifdef USEOMP
!!$omp do collapse( 2)
#endif
         do lam1 = 1, nlam( l1, is)
          do lam2 = 1, nlam( l2, is)

           ! lam1 belongs to apw
           if( lam2apwlo( lam1, l1, is) .gt. 0) then
            o1 = lam2apwlo( lam1, l1, is)
            ! lam2 belongs to apw
            if( lam2apwlo( lam2, l2, is) .gt. 0) then
             o2 = lam2apwlo( lam2, l2, is)
             do l3 = 0, ephmat_lmaxexp
              do m3 = -l3, l3
                lm3 = idxlm( l3, m3)
                do ir = 1, nr
                  fr1( ir) = apwfr( ir, 1, o1, l1, ias)*apwfr( ir, 1, o2, l2, ias)*spr( ir, is)*spr( ir, is)*dble( ephdveffmt(lm3, ir, ias))
                  fr2( ir) = apwfr( ir, 1, o1, l1, ias)*apwfr( ir, 1, o2, l2, ias)*spr( ir, is)*spr( ir, is)*aimag( ephdveffmt(lm3, ir, ias))
                end do
                call fderiv( -1, nr, spr(:, is), fr1, gf1, cf1)
                call fderiv( -1, nr, spr(:, is), fr2, gf2, cf2)
                radint( lam1, lam2, lm3) = cmplx( gf1( nr), gf2( nr), 8)
              end do
             end do
            end if
            ! lam2 belongs to lo
            if( lam2apwlo( lam2, l2, is) .lt. 0) then
             ilo2 = -lam2apwlo( lam2, l2, is)
             do l3 = 0, ephmat_lmaxexp
              do m3 = -l3, l3
                lm3 = idxlm( l3, m3)
                do ir = 1, nr
                   fr1( ir) = apwfr( ir, 1, o1, l1, ias)*lofr( ir, 1, ilo2, ias)*spr(ir, is)*spr(ir, is)*dble( ephdveffmt(lm3, ir, ias))
                   fr2( ir) = apwfr( ir, 1, o1, l1, ias)*lofr( ir, 1, ilo2, ias)*spr(ir, is)*spr(ir, is)*aimag( ephdveffmt(lm3, ir, ias))
                end do
                call fderiv( -1, nr, spr(:, is), fr1, gf1, cf1)
                call fderiv( -1, nr, spr(:, is), fr2, gf2, cf2)
                radint( lam1, lam2, lm3) = cmplx( gf1( nr), gf2( nr), 8)
              end do
             end do
            end if
           end if
           ! lam1 belongs to lo
           if( lam2apwlo( lam1, l1, is) .lt. 0) then
            ilo1 = -lam2apwlo( lam1, l1, is)
            ! lam2 belongs to apw
            if( lam2apwlo( lam2, l2, is) .gt. 0) then
             o2 = lam2apwlo( lam2, l2, is) 
             do l3 = 0, ephmat_lmaxexp
              do m3 = -l3, l3
                lm3 = idxlm( l3, m3)
                do ir = 1, nr
                   fr1( ir) = lofr( ir, 1, ilo1, ias)*apwfr( ir, 1, o2, l2, ias)*spr( ir, is)*spr( ir, is)*dble( ephdveffmt(lm3, ir, ias))
                   fr2( ir) = lofr( ir, 1, ilo1, ias)*apwfr( ir, 1, o2, l2, ias)*spr( ir, is)*spr( ir, is)*aimag( ephdveffmt(lm3, ir, ias))
                end do
                call fderiv( -1, nr, spr(:, is), fr1, gf1, cf1)
                call fderiv( -1, nr, spr(:, is), fr2, gf2, cf2)
                radint( lam1, lam2, lm3) = cmplx( gf1( nr), gf2( nr), 8)  
              end do
             end do
            end if 
            ! lam2 belongs to lo
            if( lam2apwlo( lam2, l2, is) .lt. 0) then
             ilo2 = -lam2apwlo( lam2, l2, is)
             do l3 = 0, ephmat_lmaxexp
              do m3 = -l3, l3
                lm3 = idxlm( l3, m3)
                do ir = 1, nr, input%groundstate%lradstep
                   fr1( ir) = lofr( ir, 1, ilo1, ias)*lofr( ir, 1, ilo2, ias)*spr( ir, is)*spr(ir, is)*real( ephdveffmt(lm3, ir, ias))
                   fr2( ir) = lofr( ir, 1, ilo1, ias)*lofr( ir, 1, ilo2, ias)*spr( ir, is)*spr(ir, is)*aimag( ephdveffmt(lm3, ir, ias))
                end do
                call fderiv( -1, nr, spr(:, is), fr1, gf1, cf1)
                call fderiv( -1, nr, spr(:, is), fr2, gf2, cf2)
                radint( lam1, lam2, lm3) = cmplx( gf1( nr), gf2( nr), 8)
              end do
             end do
            end if
           end if

          end do
         end do
#ifdef USEOMP
!!$omp end do
#endif
         deallocate( fr1, gf1, cf1)
         deallocate( fr2, gf2, cf2)
#ifdef USEOMP
!!$omp end parallel
#endif
         
         return
      end subroutine ephmat_genri

      subroutine ephmat_genephmat( ik, evec_kq, evec_k, ephdveffir, ephmat)
         integer, intent( in) :: ik
         complex(8), intent( in) :: evec_kq( nmatmax_ptr, ephmat_fst_kq:ephmat_lst_kq) 
         complex(8), intent( in) :: evec_k( nmatmax_ptr, ephmat_fst_k:ephmat_lst_k)
         complex(8), intent( in) :: ephdveffir(ngrtot)
         complex(8), intent( out) :: ephmat( ephmat_fst_kq:ephmat_lst_kq, ephmat_fst_k:ephmat_lst_k, ng)

         integer :: ngp, ngpq, i, is, ia, ias, l, m, lm, o, ilo, lam, ir, i1, i2, i3
         real(8) :: veckl(3), veckc(3), veckql(3), veckqc(3)
         real (8) :: v1 (3), v2 (3), v3 (3)
         integer :: shift(3), g(3), gs(3), igk, igq, ig, ivg_(3, ngrtot)

         integer, allocatable :: igpig(:), igpqig(:)
         real(8), allocatable :: vgpql(:, :), vgpqc(:,:), gkqc(:), tpgkqc(:,:)
         complex(8), allocatable :: sfacgkq(:,:), apwalm(:,:,:,:)
         complex(8), allocatable :: auxmat(:,:), auxvec(:), evecmt_kq(:,:,:), evecmt_k(:,:,:), cfunmat(:,:)
         complex(8), allocatable :: ephdveffig(:)
         
         ephmat = zzero

         veckl = ephmat_kset%vkl( :, ik)
         veckc = ephmat_kset%vkc( :, ik)

         ! k+q vector in lattice coordinates
         veckql = veckl + vecql
         ! map vector components to [0,1) interval
         call r3frac( input%structure%epslat, veckql, shift)
         ! k+q vector in cartesian coordinates
         call r3mv( bvec, veckql, veckqc)
         !-------------------------------------!
         !      muffin-tin matrix elements     !
         !-------------------------------------!

         allocate( igpig( ngkmax_ptr), igpqig( ngkmax_ptr), vgpql( 3, ngkmax_ptr), vgpqc( 3, ngkmax_ptr), gkqc( ngkmax_ptr), tpgkqc( 2, ngkmax_ptr))

         call timesec( t0)
         ! generate the G+k+q-vectors
         call gengpvec( veckql, veckqc, ngpq, igpqig, vgpql, vgpqc, gkqc, tpgkqc)
         ! Generate the G+k-vectors
         call gengpvec( veckl, veckc, ngp, igpig, vgpql, vgpqc, gkqc, tpgkqc)
         call timesec( t1)
         tgk = tgk + t1 - t0
         allocate( evecmt_kq( nlmlammax, ephmat_fst_kq:ephmat_lst_kq, natmtot))
         allocate( evecmt_k( nlmlammax, ephmat_fst_k:ephmat_lst_k, natmtot)) 
         allocate( auxmat( ephmat_fst_kq:ephmat_lst_kq, nlmlammax))
         call timesec( t0)
         call ephmat_getevecmt( veckql, ephmat_fst_kq, ephmat_lst_kq, evecmt_kq)
         call ephmat_getevecmt( veckl, ephmat_fst_k, ephmat_lst_k, evecmt_k)
         call timesec( t1)
         tio = tio + t1 - t0

         do is = 1, nspecies
          do ia = 1, natoms( is) 
           ias = idxas( ia, is)
        
           do ig = 1, ng
             call zgemm( 'c', 'n', ephmat_nst_kq, nlmlam( is), nlmlam( is), zone, &
                  evecmt_kq( :, :, ias), nlmlammax, &
                  rignt( :, :, ias, ig), nlmlammax, zzero, &
                  auxmat, ephmat_nst_kq)
             call zgemm( 'n', 'n', ephmat_nst_kq, ephmat_nst_k, nlmlam( is), zone, &
                  auxmat, ephmat_nst_kq, &
                  evecmt_k(:, :, ias), nlmlammax, zone, &
                  ephmat( :, : , ig), ephmat_nst_kq)
           end do
          end do
         end do
         call timesec( t0)
         tmt = tmt + t0 - t1
         deallocate( evecmt_kq, evecmt_k, auxmat)
         !-------------------------------------!
         !    interstitial matrix elements     !
         !-------------------------------------!
         allocate( cfunmat( ngpq, ngp))
         allocate( auxmat( ephmat_fst_kq:ephmat_lst_kq, ngp))
         if (allocated( ephdveffig)) deallocate(ephdveffig)
         allocate(ephdveffig(ngrtot)) 
         ivg_ = ivg
   
         do ig = 1, ng
          gs = shift + vecgl(:, ig)
          g = 0
          cfunmat(:,:) = zzero
          call timesec( t0)
          call genephdveffig(ephdveffir, ephdveffig)

#ifdef USEOMP
!$omp parallel default( shared) private( igk, igq, g)
!$omp do collapse(2)
#endif
          do igq = 1, ngpq
           do igk = 1, ngp
             g = ivg_(:, igpqig( igq)) - ivg_( :, igpig( igk)) + gs
             if((g(1) .ge. intgv(1,1)) .and. (g(1) .le. intgv(1,2)) .and. &
                (g(2) .ge. intgv(2,1)) .and. (g(2) .le. intgv(2,2)) .and. &
                (g(3) .ge. intgv(3,1)) .and. (g(3) .le. intgv(3,2))) then
                   cfunmat( igq, igk) = ephdveffig( ivgig( g(1), g(2), g(3)))
             end if
           end do
          end do 
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
          call timesec( t1)
          tgg = tgg + t1 - t0
         
          call zgemm( 'c', 'n', ephmat_nst_kq, ngp, ngpq, zone, & 
               evec_kq, nmatmax_ptr, &
               cfunmat, ngpq, zzero, &
               auxmat, ephmat_nst_kq)
          call zgemm('n', 'n', ephmat_nst_kq, ephmat_nst_k, ngp, zone, &
               auxmat, ephmat_nst_kq, &
               evec_k, nmatmax_ptr, zone, &
               ephmat(:, :, ig), ephmat_nst_kq)
          call timesec(t0)
          tir = tir + t0 - t1
         end do

         deallocate( ephdveffig)
         deallocate( auxmat, cfunmat, igpig, igpqig)

         return
      end subroutine ephmat_genephmat

      subroutine ephmat_putevecmt(vpl, fst, lst, evecmt)
         use m_getunit
         real(8), intent( in)    :: vpl(3)
         integer, intent( in)    :: fst, lst
         complex(8), intent( in) :: evecmt( nlmlammax, fst:lst, natmtot)

         integer :: ik, un, recl
      
         call findkptinset( vpl, ephmat_kset, un, ik)
       
         inquire( iolength =recl) ephmat_kset%vkl( :, ik), nlmlammax, fst, lst, evecmt
         call getunit( un)
         open( un, file=trim( ephmat_fname)//'MT'//trim( filext), action='write', form='unformatted', access='direct', recl=recl)
         write( un, rec=ik) ephmat_kset%vkl( :, ik), nlmlammax, fst, lst, evecmt
         close( un)

         return
      end subroutine ephmat_putevecmt

      subroutine ephmat_putevecir( vpl, fst, lst, evecir)
         use m_getunit
         real(8), intent( in) :: vpl(3)
         integer, intent( in) :: fst, lst
         complex(8), intent(in) :: evecir( ephmat_fftmap%ngrtot, fst:lst)

         integer :: ik, un, recl

         call findkptinset( vpl, ephmat_kset, un, ik)

         inquire( iolength=recl) ephmat_kset%vkl( :, ik), ephmat_fftmap%ngrtot, fst, lst, evecir
         call getunit( un) 
         open( un, file=trim( ephmat_fname)//'IR'//trim( filext), action='write', form='unformatted', access='direct', recl=recl)
         write( un, rec=ik) ephmat_kset%vkl( :, ik), ephmat_fftmap%ngrtot, fst, lst, evecir
         close( un)

         return 
      end subroutine ephmat_putevecir

      subroutine ephmat_getevecmt( vpl, fst, lst, evecmt)
         use m_getunit
         real(8), intent( in)     :: vpl(3)
         integer, intent( in)     :: fst, lst
         complex(8), intent( out) :: evecmt( nlmlammax, fst:lst, natmtot)

         integer :: ik, un, recl, nlmlammax_, fst_, lst_
         real(8) :: vpl_(3)
         logical :: exist

         complex(8), allocatable :: tmp(:,:,:)
 
         call findkptinset( vpl, ephmat_kset, un, ik)
 
         inquire( file=trim( ephmat_fname)//'MT'//trim( filext), exist=exist)
         if( exist) then
          inquire( iolength=recl) vpl_, nlmlammax_, fst_, lst_
          call getunit(un)
          open( un, file=trim( ephmat_fname)//'MT'//trim( filext), action='read', form='unformatted', access='direct', recl=recl)
          read( un, rec=1) vpl_, nlmlammax_, fst_, lst_
          close( un)
          if( nlmlammax_ .ne. nlmlammax) then
           write(*,*)
           write(*, '("Error (ephmat_geteveshort): Different number of basis functions:")')
           write(*, '("Current:", i8)') nlmlammax
           write(*, '("File   :", i8)') nlmlammax_
           stop
          end if
          if( (fst .lt. fst_) .or. (lst .gt. lst_)) then
           write(*,*)
           write(*,'("Error (ephmat_geteveshort): Band indices out of range:")')
           write(*,'("Current:", 2i8)') fst, lst
           write(*,'("File   :", 2i8)') fst_, lst_
           stop
          end if
          allocate( tmp( nlmlammax, fst_:lst_, natmtot))
          inquire( iolength=recl) vpl_, nlmlammax_, fst_, lst_, tmp
          call getunit( un)
          open( un, file=trim( ephmat_fname)//'MT'//trim(filext), action='read', form='unformatted', access='direct', recl=recl)
          read( un, rec=ik) vpl_, nlmlammax_, fst_, lst_, tmp
          close( un)
          evecmt = tmp( :, fst:lst, :)
          deallocate( tmp)
         else
          write(*,*)
          write(*,'("Error (ephmat_getevecmt): File does not exist:", a)') trim( ephmat_fname)//'MT'//trim(filext)
          stop
         end if

         return
      end subroutine ephmat_getevecmt

      subroutine ephmat_getevecir( vpl, fst, lst, evecir)
         use m_getunit
         real(8), intent( in) :: vpl(3)
         integer, intent( in) :: fst, lst
         complex(8), intent( out) :: evecir( ephmat_fftmap%ngrtot, fst:lst)

         integer :: ik, un, recl, ngrtot_, fst_, lst_
         real(8) :: vpl_(3)
         logical :: exist
 
         complex(8), allocatable :: tmp(:,:)

         call findkptinset( vpl, ephmat_kset, un , ik)

         inquire( file=trim( ephmat_fname)//'IR'//trim( filext), exist=exist)
         if( exist) then
          inquire( iolength=recl) vpl_, ngrtot, fst_, lst_
          call getunit( un)
          open( un, file=trim( ephmat_fname)//'IR'//trim( filext), action='read', form='unformatted', access='direct', recl=recl)
          read( un, rec=1) vpl_, ngrtot_, fst_, lst_
          close( un)
          if( ngrtot_ .ne. ephmat_fftmap%ngrtot) then
           write(*,*)
           write(*,'("Error (ephmat_getevecir): Different number of plane waves:")')
           write(*,'("Current:",i8)') ephmat_fftmap%ngrtot
           write(*,'("File   :",i8)') ngrtot
           stop
          end if
          if( (fst .lt. fst_) .or. (lst .gt. lst_)) then
           write(*,*)
           write(*,'("Error (ephmat_getevecshort): Band indices out of range:")')
           write(*,'("Current:",2i8)') fst, lst
           write(*,'("File    :",2i8)') fst_, lst_
           stop
          end if
          allocate( tmp( ephmat_fftmap%ngrtot, fst_:lst_))
          inquire( iolength=recl) vpl_, ngrtot_, fst_, lst_, tmp
          call getunit( un)
          open( un, file=trim( ephmat_fname)//'IR'//trim( filext), action='read', form='unformatted', access='direct', recl=recl)
          read( un, rec=ik) vpl_, ngrtot_, fst_, lst_, evecir
          close( un)
         else
          write(*,*)
          write(*,'("Error (ephmat_getevecir): File does not exist:", a)') trim( ephmat_fname)//'IR'//trim(filext)
          stop
         end if

         return 
      end subroutine ephmat_getevecir

      subroutine ephmat_destroy
         call timesec( te)
         ttot = te - ts
         if( mpiglobal%rank .eq. 0 .and. .false.) then
            write(*,'("          EPHMAT TIMING          ")')
            write(*,'("================================")')
            write(*,'("initialization   : ",f13.6)') tinit
            write(*,'("preparation      : ",f13.6)') tprep
            write(*,'("radial integrals : ",f13.6)') tri
            write(*,'("gaunt integrals  : ",f13.6)') trignt
            write(*,'("G+k vectors      : ",f13.6)') tgk
            write(*,'("muffin-tin part  : ",f13.6)') tmt
            write(*,'("char. fun. GG    : ",f13.6)') tgg
            write(*,'("interstitial part: ",f13.6)') tir
            write(*,'("I/O              : ",f13.6)') tio
            write(*,'("--------------------------------")')
            write(*,'("sum              : ",f13.6)') tinit + tri + trignt + tmt + tgk + tprep + tgg + tir + tio
            write(*,'("rest             : ",f13.6)') ttot - (tinit + tri + trignt + tmt + tgk + tprep + tgg + tir + tio)
            write(*,'("--------------------------------")')
            write(*,'("total            : ",f13.6)') ttot
            write(*,'("================================")')
         end if

         if( allocated( nlam)) deallocate( nlam)
         if( allocated( nlmlam)) deallocate( nlmlam)
         if( allocated( lam2apwlo)) deallocate( lam2apwlo)
         if( allocated( idxlmlam)) deallocate( idxlmlam)
         if( allocated( lm2l)) deallocate( lm2l)
         if( allocated( rignt)) deallocate( rignt)
         if( allocated( idxgnt)) deallocate( idxgnt)
         if( allocated( listgnt)) deallocate( listgnt)
          
         return
      end subroutine ephmat_destroy

      subroutine genephdveffig(veffir, veffig)
         complex (8), intent (in) :: veffir (ngrtot)
         complex (8), intent (out) :: veffig (ngrtot)
         ! local variables
         Integer :: ig, ifg
         ! allocatable arrays
         complex (8), Allocatable :: zfft (:)
         allocate (zfft(ngrtot))
         ! multiply effective potential with characteristic function
         zfft = veffir*cfunir
         veffig = zzero
         ! Fourier transform to G-space
         Call zfftifc (3, ngrid,-1, zfft)
         do ig = 1, ngvec
           ifg = igfft(ig) 
           veffig (ig) = zfft (ifg)
         end do

         deallocate (zfft)
         return
      end subroutine genephdveffig
   
end module mod_ephmat
