module mod_mtbasis
  use modinput
  use mod_atoms, only: nspecies, natmtot, idxas, natoms
  use mod_APW_LO, only: nlorb, lorbl, apwordmax, nlotot, apword
  use mod_eigensystem, only: nmatmax_ptr, idxlo
  use mod_muffin_tin, only: idxlm, lmmaxapw
  use mod_Gkvector, only: ngkmax_ptr
  use mod_eigenvalue_occupancy, only: nstfv
  use mod_lattice, only: bvec
  use mod_constants, only: zzero, zone
  
  implicit none

! module variables
  integer :: nlidxmax, nlmidxmax, mtlmax
  integer, allocatable :: nlidx(:,:), nlmidx(:), lidx2apwlo(:,:,:), lidx2lmidx(:,:,:)
  

! methods
  contains
    subroutine genmtbasisindices( lmaxapw)
      integer, optional, intent( in) :: lmaxapw

      integer :: is, l, m, lm, o, ilo

      mtlmax = input%groundstate%lmaxapw
      if( present( lmaxapw)) mtlmax = lmaxapw

      allocate( nlidx( 0:mtlmax, nspecies))
      allocate( lidx2apwlo( apwordmax+nlotot, 0:mtlmax, nspecies))
      nlidxmax = 0
      do is = 1, nspecies
        do l = 0, mtlmax
          nlidx( l, is) = 0
          lidx2apwlo( :, l, is) = 0
          do o = 1, apword( l, is)
            nlidx( l, is) = nlidx( l, is) + 1
            lidx2apwlo( nlidx( l, is), l, is) = o
          end do
          do ilo = 1, nlorb( is)
            if( lorbl( ilo, is) .eq. l) then
              nlidx( l, is) = nlidx( l, is) + 1
              lidx2apwlo( nlidx( l, is), l, is) = -ilo
            end if
          end do
          nlidxmax = max( nlidxmax, nlidx( l, is))
        end do
      end do

      allocate( nlmidx( nspecies))
      allocate( lidx2lmidx( (mtlmax+1)**2, nlidxmax, nspecies))
      lidx2lmidx = 0
      nlmidx = 0
      do l = 0, mtlmax
        do m = -l, l
          lm = idxlm( l, m)
          do is = 1, nspecies
            do o = 1, nlidx( l, is)
              nlmidx( is) = nlmidx( is) + 1
              lidx2lmidx( lm, o, is) = nlmidx( is)
            end do
          end do
        end do
      end do
      nlmidxmax = maxval( nlmidx, 1)
      
      return
    end subroutine genmtbasisindices

    subroutine genshortmtbasis( vpl, evec, mtshort)
      real(8), intent( in) :: vpl(3)
      complex(8), intent( in) :: evec( nmatmax_ptr, nstfv)
      complex(8), intent( out) :: mtshort( nlmidxmax, nstfv, natmtot)

      integer :: ngp, is, ia, ias, l, m, lm, lidx, o, ilo
      real(8) :: vpc(3)

      integer, allocatable :: igpig(:) 
      real(8), allocatable :: vgpl(:,:), vgpc(:,:), gpc(:), tpgpc(:,:)
      complex(8), allocatable :: evecmt(:,:,:), evecir(:,:)
      complex(8), allocatable :: sfacgp(:,:), apwalm(:,:,:,:), auxvec(:)

      allocate( igpig( ngkmax_ptr), vgpl( 3, ngkmax_ptr), vgpc( 3, ngkmax_ptr), gpc( ngkmax_ptr), tpgpc( 2, ngkmax_ptr))
      allocate( sfacgp( ngkmax_ptr, natmtot))
      allocate( apwalm( ngkmax_ptr, apwordmax, lmmaxapw, natmtot))
      allocate( auxvec( nstfv))

      ! generate the G+k+q-vectors
      call r3mv( bvec, vpl, vpc)
      call gengpvec( vpl, vpc, ngp, igpig, vgpl, vgpc, gpc, tpgpc)
      ! generate the structure factors
      call gensfacgp( ngp, vgpc, ngkmax_ptr, sfacgp)
      ! find matching coefficients for k-point k+q
      call match( ngp, gpc, tpgpc, sfacgp, apwalm)

      mtshort = zzero
      do is = 1, nspecies
        do ia = 1, natoms( is)
          ias = idxas( ia, is)
          do l = 0, mtlmax
            do m = -l, l
              lm = idxlm( l, m)
              do lidx = 1, nlidx( l, is)
                ! lam belongs to apw
                if( lidx2apwlo( lidx, l, is) .gt. 0) then
                  o = lidx2apwlo( lidx, l, is)
                  call zgemv( 't', ngp, nstfv, zone, &
                         evec, nmatmax_ptr, &
                         apwalm( :, o, lm, ias), 1, zzero, &
                         auxvec, 1)
                  mtshort( lidx2lmidx( lm, lidx, is), :, ias) = auxvec
                end if
                ! lam belongs to lo
                if( lidx2apwlo( lidx, l, is) .lt. 0) then
                  ilo = -lidx2apwlo( lidx, l, is)
                  mtshort( lidx2lmidx( lm, lidx, is), :, ias) = evec( ngp+idxlo( lm, ilo, ias), :)
                end if
              end do
            end do
          end do
        end do
      end do
    
      deallocate( igpig, vgpl, vgpc, gpc, tpgpc, sfacgp, apwalm, auxvec)
      return
    end subroutine genshortmtbasis
end module mod_mtbasis
