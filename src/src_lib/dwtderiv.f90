subroutine fourierderiv( n, x, f, d, g)
  use mod_constants
  use m_plotmat
  implicit none

  integer, intent( in)     :: n      ! # of input points
  real(8), intent( in)     :: x( n)  ! the x-values at which f is given
  complex(8), intent( in)  :: f( n)  ! the input function
  integer, intent( in)     :: d      ! degree of derivative
  complex(8), intent( out) :: g( n)  ! the derivative

  integer :: i, m, w
  real(8) :: l
  complex(8) :: c, tmp( n)

  m = n
  l = (x(n)-x(1))/dble( n-1)*m
  !tmp( 1:n) = f
  !do i = 1, n-1
  !  tmp( n+i) = f( n-i)
  !end do
  c = (f(n) - f(1))/dble( n-1)
  do i = 1, n
    tmp( i) = f( i)! - (f(1) + c*dble( i-1))
  end do
  !do i = 1, n-1
  !  tmp( n+i) = -tmp( n-i)
  !end do

  do i = 1, m
    write(*,'(i,2g26.16)') i, dble( tmp( i))
  end do
  write(*,*)
  write(*,*)
  call writematlab( reshape( tmp, (/m, 1/)), 'data')

  !tmp( (n+1):(2*n)) = f
  call zfftifc( 1, (/m/), -1, tmp)
  do i = 1, m
    w = i
    if( i .gt. n) w = i-m
    !if( abs( w) .gt. n/1) tmp( i) = zzero
    !if( abs( tmp( i)) .lt. 1.d-3) tmp( i) = zzero
    write(*,'(i,2g26.16)') i, abs( tmp( i))
  end do
  write(*,*)
  write(*,*)

  do i = 0, m/2
    tmp( i+1) = tmp( i+1)*(zi*twopi/l*i)**d
  end do
  do i = m/2 + 1, m-1
    tmp( i+1) = tmp( i+1)*(zi*twopi/l*(i-m))**d
  end do
  if( (modulo( m, 2) .eq. 0) .and. (modulo( d, 2) .eq. 1)) tmp( m/2+1) = zzero
  call zfftifc( 1, (/m/), 1, tmp)
  g = tmp( 1:n)

  return
end subroutine fourierderiv
