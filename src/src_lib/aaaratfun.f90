subroutine aaaratfun( n, z, f, m, zr, fr, wr, fi, zer, pol, err)
  use m_linalg
  use m_plotmat
  implicit none

  integer, intent( in)     :: n              ! #(z,f) pairs
  complex(8), intent( in)  :: z(n)           ! abscissa values
  complex(8), intent( in)  :: f(n)           ! function values f=f(z)
  integer, intent( in)     :: m              ! maximum rational order
  complex(8), intent( out) :: zr(0:m)        ! selected support points
  complex(8), intent( out) :: fr(0:m)        ! function values fr=r(zr)=f(zr)
  complex(8), intent( out) :: wr(0:m)        ! weights
  complex(8), intent( out) :: fi(n)          ! rational function fi=r(z)
  complex(8), intent( out) :: zer(m), pol(m) ! zeros and poles of r
  real(8), intent( out)    :: err            ! error Sqrt[Sum(Abs(f-fi)^2)/Sum(Abs(f)^2)]

  integer :: i, j, k, nn, idx(n)

  complex(8), allocatable :: c(:,:), a(:,:), num(:), den(:)
  complex(8), allocatable :: lvec(:,:), rvec(:,:), eval1(:), eval2(:)
  real(8), allocatable :: sval(:)

  allocate( c( n, 0:m), a( n, 0:m))
  allocate( num( n), den( n))
  allocate( lvec( n, n), rvec( 0:m, 0:m), sval( 0:m))

  c = cmplx( 0.d0, 0.d0, 8)
  a = cmplx( 0.d0, 0.d0, 8)
  fi = sum( f)/dble( n)
  zr = cmplx( 0.d0, 0.d0, 8)
  fr = cmplx( 0.d0, 0.d0, 8)
  wr = cmplx( 0.d0, 0.d0, 8)
  do i = 1, n
    idx(i) = i
  end do

  do i = 0, m
    nn = n-i-1
    j = maxloc( abs( f-fi), 1)
    zr(i) = z(j)
    fr(i) = f(j)
    k = minloc( abs( idx - j), 1)
    idx( k:nn) = idx( (k+1):(nn+1))
    c(:,i) = cmplx( 1.d0, 0.d0, 8)/(z - zr(i))
    a(:,i) = (f - fr(i))*c(:,i)
    call zsvd( a( idx( 1:nn), 0:i), sval( 0:i), lvec( 1:nn, 1:nn), rvec( 0:i, 0:i))
    wr( 0:i) = conjg( rvec( i, 0:i))
    call zgemv( 'n', n, i+1, cmplx( 1.d0, 0.d0, 8), &
           c, n, &
           wr*fr, 1, cmplx( 0.d0, 0.d0, 8), &
           num, 1)
    call zgemv( 'n', n, i+1, cmplx( 1.d0, 0.d0, 8), &
           c, n, &
           wr, 1, cmplx( 0.d0, 0.d0, 8), &
           den, 1)
    fi = f
    fi( idx( 1:nn)) = num( idx( 1:nn))/den( idx( 1:nn))
    err = sqrt( sum( abs( fi - f)**2)/sum( abs( f)**2))
    if( err .lt. 1.d-10) exit
  end do
  deallocate( c, a, num, den, lvec, rvec, sval)

  i = min( i, m)
  nn = i + 2
  allocate( c( nn, nn), a( nn, nn), eval1( nn), eval2( nn), rvec( nn, nn))
  c = cmplx( 0.d0, 0.d0, 8)
  do j = 2, nn
    c(j,j) = cmplx( 1.d0, 0.d0, 8)
  end do
  a = cmplx( 0.d0, 0.d0, 8)
  a( 1, 2:nn) = wr( 0:i)
  a( 2:nn, 1) = cmplx( 1.d0, 0.d0, 8)
  do j = 2, nn
    a(j,j) = zr( j-2)
  end do
  call zgegdiag( a, c, eval1, eval2, revec=rvec)
  pol = cmplx( 1.d128, 1.d128, 8)
  k = 0
  do j = 1, nn
    if( abs( eval2(j)) .gt. 1.d-32) then
      k = k + 1
      pol( k) = eval1(j)/eval2(j)
      !write(*,'("POL",2f13.6)') pol(k)
    end if
  end do
  a( 1, 2:nn) = fr( 0:i)*wr( 0:i)
  call zgegdiag( a, c, eval1, eval2, revec=rvec)
  zer = cmplx( 1.d128, 1.d128, 8)
  k = 0
  do j = 1, nn
    if( abs( eval2(j)) .gt. 1.d-32) then
      k = k + 1
      zer( k) = eval1(j)/eval2(j)
      !write(*,'("ZER",2f13.6)') zer(k)
    end if
  end do

  deallocate( c, a, eval1, eval2, rvec)
  return
end subroutine aaaratfun
