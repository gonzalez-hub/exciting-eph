! This file is distributed under the terms of the GNU General Public License.
!
! Considerable parts of this module were strongly adopted from the manopt toolbox
! for matlab developed by Nicolas Boumal and Bamdev Mishra. 
! See http://www.manopt.org
! N. Boumal, B. Mishra, P.-A. Absil, and R. Sepulchre, Journal of Machine Learning Research 15 (2014), 1455-1459
!
! Created October 2019 (Sebastian Tillack)
!
! DESCRIPTION
! This module (together with mod_linalg.f90) provides functionalities for nonlinear optimization
! problems on Stiefel manifolds. I.e. it provides functions to minimize a real valued cost 
! function f({X}), where {X} is a set of (semi-)unitary matrices such that X^H*X=1.
! The individual matrices X do not necessarily have to have the same dimension.
! There are two routines available for minimization:
!   stiefel_optim_lbfgs  -  an L-BFGS version of the optimization (recommended)
!   stiefel_optim_cg     -  a conjugate gradient version
!
module mod_stiefel_optim
  use m_linalg
  use m_plotmat
  
  implicit none

  private

  ! numerical constants
  complex(8), parameter :: zero = cmplx( 0.d0, 0.d0, 8)
  complex(8), parameter ::  one = cmplx( 1.d0, 0.d0, 8)
  complex(8), parameter ::   ii = cmplx( 0.d0, 1.d0, 8)

  ! algorithmical parameters
  real(8) ::    eps0 = 1.d-14 ! numerical zero
  real(8) :: epsgrad = 1.d-6  ! tolerance for gradient
  integer ::   minit = 0      ! minimum number of iterations
  integer ::   maxit = 1000   ! maximum number of iterations
  integer ::  memlen = 6      ! number of previous steps that are stored
  real(8) ::    mins = 1.d-2  ! minimum step length
  integer ::  stdout = 0      ! ouptut unit (0 = no output)

  ! dimensions
  integer :: KX                          ! number of X matrices
  integer, allocatable :: DX(:,:)        ! number of rows/columns for each matrix X
  integer :: DXO(2)                      ! first and second dimension of the array X
                                         ! as allocated in the calling subroutine
  integer :: DXI(2)                      ! first and second dimension of the array X
                                         ! as used internally

  ! operational variables
  real(8)                 :: cCost        ! current cost function value
  real(8)                 :: nCost        ! next cost function value
  complex(8), allocatable :: cGX(:,:,:)   ! current X gradient
  complex(8), allocatable :: cSX(:,:,:)   ! current X step direction
  complex(8), allocatable :: nX(:,:,:)    ! next X

  complex(8), allocatable :: SHX(:,:,:,:) ! history of the steps X(i+1)-X(i)
  complex(8), allocatable :: GHX(:,:,:,:) ! history of the gradient differences dX(i+1)-dX(i)
  real(8), allocatable    :: RH(:)        ! history of coefficients rho

  ! statistics
  integer :: nCostEval = 0
  integer :: nGradEval = 0

  ! exponential conversion
  logical :: expc    = .false.
  real(8) :: epsexpc = 1d-2
  integer :: nexpc   = 10
  integer :: nextexpc

  integer :: nhist   = 10
  real(8), allocatable :: fhist(:)

  logical :: isgrad

  ! function interfaces
  abstract interface
    subroutine cost_interface( x, dxo, kx, dx, f)
      integer, intent( in)    :: dxo(2), kx, dx(2,kx)
      complex(8), intent( in) :: x(dxo(1),dxo(2),*)
      real(8), intent( out)   :: f
    end subroutine
  end interface

  abstract interface
    subroutine grad_interface( x, dxo, kx, dx, gx, dgo)
      integer, intent( in)     :: dxo(2), kx, dx(2,kx), dgo(2)
      complex(8), intent( in)  :: x(dxo(1),dxo(2),*)
      complex(8), intent( out) :: gx(dgo(1),dgo(2),*)
    end subroutine
  end interface

  abstract interface
    subroutine updt_interface( x, dxo, kx, dx, it)
      integer, intent( in)       :: dxo(2), kx, dx(2,kx), it
      complex(8), intent( inout) :: x(dxo(1),dxo(2),*)
    end subroutine
  end interface

  public :: stiefel_optim_cg, stiefel_optim_lbfgs, stiefel_optim_setexpconv

  contains
    !**********************************
    !* CG FUNCTIONS
    !**********************************
    subroutine stiefel_optim_cg( X, dxo_, kx_, dx_, cost, grad, &
                                 tolgrad, miniter, maxiter, minstep, outunit, update)
      integer, intent( in)       :: dxo_(2), kx_, dx_(2,kx_)
      complex(8), intent( inout) :: X(dxo_(1),dxo_(2),*)
      procedure( cost_interface) :: cost
      procedure( grad_interface) :: grad
      real(8), optional, intent( inout)    :: tolgrad
      integer, optional, intent( in)       :: miniter
      integer, optional, intent( inout)    :: maxiter
      real(8), optional, intent( in)       :: minstep
      integer, optional, intent( in)       :: outunit
      procedure( updt_interface), optional :: update

      integer :: it
      real(8) :: t0, t1, gradnorm, stepsize, df0, alpha, beta, orth, snorm, dCost
      logical :: finish

      ! default parameters
      epsgrad = 1.d-6
      minit = 0
      maxit = 200
      mins = 1.d-1
      stdout = 0

      ! input parameters
      if( present( tolgrad)) epsgrad = max( 0.d0, tolgrad)
      if( present( miniter)) minit = max( 0, miniter)
      if( present( maxiter)) maxit = max( 0, maxiter)
      if( present( minstep)) mins = max( 0.d0, minstep)
      if( present( outunit)) stdout = max( 0, outunit)
      memlen = 1

      ! sanity checks
      call sanityCheck( dxo_, kx_, dx_)
      ! global allocation
      KX = kx_
      DXO = dxo_
      allocate( DX(2,KX))
      DX = dx_
      DXI(1) = max( 0, maxval( DX(1,:))); DXI(2) = max( 0, maxval( DX(2,:)))
      allocate( cGX(DXI(1),DXI(2),KX), cSX(DXI(1),DXI(2),KX), nX(DXI(1),DXI(2),KX))
      allocate( SHX(DXI(1),DXI(2),KX,memlen), GHX(DXI(1),DXI(2),KX,memlen))
      allocate( RH(memlen))
      ! local allocation

      if( stdout .gt. 0) write( stdout, '("# Conjugate gradient minimization run statistics")')
      if( stdout .gt. 0) write( stdout, '("# maximum matrix dimensions: ",4x,2i5)') DXI
      if( stdout .gt. 0) write( stdout, '("# number of matrices:        ",4x,i5)') KX
      if( stdout .gt. 0) write( stdout, '("# dimension of optimization: ",i9)') dimX()
      if( stdout .gt. 0) write( stdout, '("#")')
      if( stdout .gt. 0) write( stdout, '("# iteration",13x,"time",17x,"cost",11x,"|gradient|",17x,"step",4x,"directional slope")')

      ! initialization
      stepsize = 1.d0; alpha = 1.d0
      it = 0
      finish = .false.
      call timesec( t0)

      ! get current cost and gradient
      if( present( update)) call update( X, DXO, KX, DX, it)
      call ucost( cost, X, DXO, cCost)
      call ugrad( grad, X, DXO, cGX, DXI)
      gradnorm = norm( cGX, DXI, DXI)
      cSX = -cGX
      call timesec( t1)
      if( stdout .gt. 0) write( stdout, '(2x,i9,4(x,g20.10))') it, t1-t0, cCost, gradnorm

      ! the minimization loop
      do while( .true.)
        ! check termination
        !if( abs( stepsize) .lt. minstep) finish = .true.
        if( it .ge. maxit)               finish = .true.
        if( gradnorm .lt. epsgrad)       finish = .true.
        if( it .lt. minit)               finish = .false.
        if( finish) exit

        ! get directional derivative
        df0 = inner( cGX, DXI, cSX, DXI, DXI)
        ! reset to steepest descend if direction is not decreasing
        if( df0 .ge. 0.d0) then
          cSX = -cGX
          df0 = inner( cGX, DXI, cSX, DXI, DXI)
        end if
        snorm = norm( cSX, DXI, DXI)

        ! perform line search
        stepsize = snorm
        if( it .gt. 0) stepsize = 2.d0*dCost/df0*snorm
        call linesearch( cost, grad, X, DXO, cSX, DXI, cCost, df0, stepsize, nX, DXI, nCost)
        if( present( update)) then
          call update( nX, DXI, KX, DX, it)
          call ucost( cost, nX, DXI, nCost)
        end if
        alpha = stepsize/snorm

        ! transport old gradient to new X
        call transp( X, DXO, nX, DXI, cGX, DXI, GHX, DXI)

        ! get gradient at new X
        call ugrad( grad, nX, DXI, cGX, DXI)
        gradnorm = inner( cGX, DXI, cGX, DXI, DXI)

        ! get CG parameter
        orth = inner( GHX, DXI, cGX, DXI, DXI)
        if( abs(orth/gradnorm) .gt. 1.d40) then
          beta = 0.d0
          cSX = -cGX
        else
          call transp( X, DXO, nX, DXI, cSX, DXI, SHX, DXI); cSX = SHX(:,:,:,1)
          ! Hestenes-Stiefel
          beta = gradnorm - orth
          beta = beta/inner( cGX-GHX(:,:,:,1), DXI, cSX, DXI, DXI)
          beta = max( 0.d0, beta)
        end if

        ! update X
        X(1:DXI(1),1:DXI(2),1:KX) = nX
        ! update search direction
        cSX = -cGX + beta*cSX
        ! update current values
        dCost = 2.d0*(nCost - cCost)
        cCost = nCost
        gradnorm = dsqrt( gradnorm)
        it = it + 1
        call timesec( t1)
        if( stdout .gt. 0) write( stdout, '(2x,i9,5(x,g20.10))') it, t1-t0, cCost, gradnorm, alpha, df0
      end do
      if( stdout .gt. 0) write( stdout, '("# evaluations of cost fun. : ",i9)') nCostEval
      if( stdout .gt. 0) write( stdout, '("# evaluations of gradient  : ",i9)') nGradEval

      if( present( tolgrad)) tolgrad = gradnorm
      if( present( maxiter)) maxiter = it
      call destroy
    end subroutine stiefel_optim_cg


    !**********************************
    !* RLBFGS FUNCTIONS
    !**********************************
    subroutine stiefel_optim_lbfgs( X, dxo_, kx_, dx_, cost, grad, &
                                 tolgrad, miniter, maxiter, minstep, outunit, memory, update)
      integer, intent( in)       :: dxo_(2), kx_, dx_(2,kx_)
      complex(8), intent( inout) :: X(dxo_(1),dxo_(2),*)
      procedure( cost_interface) :: cost
      procedure( grad_interface) :: grad
      real(8), optional, intent( inout)    :: tolgrad
      integer, optional, intent( in)       :: miniter
      integer, optional, intent( inout)    :: maxiter
      real(8), optional, intent( in)       :: minstep
      integer, optional, intent( in)       :: outunit
      integer, optional, intent( in)       :: memory
      procedure( updt_interface), optional :: update

      integer :: it, pos, im
      real(8) :: t0, t1, gradnorm, stepsize, df0, alpha, snorm, dCost, iss, isg, rho, scaleFactor, angle, mins0
      logical :: finish, accepted, ultimatum

      complex(8), allocatable :: sx(:,:,:), gx(:,:,:)

      ! default parameters
      epsgrad = 1.d-6
      minit = 0
      maxit = 200
      mins0 = 1.d-1
      memlen = 1
      stdout = 0

      ! input parameters
      if( present( tolgrad)) epsgrad = max( 0.d0, tolgrad)
      if( present( miniter)) minit = max( 0, miniter)
      if( present( maxiter)) maxit = max( 0, maxiter)
      if( present( minstep)) mins0 = max( 0.d0, minstep)
      if( present( memory)) memlen = max( 0, memory)
      if( present( outunit)) stdout = max( 0, outunit)

      ! sanity checks
      call sanityCheck( dxo_, kx_, dx_)
      ! global allocation
      KX = kx_
      DXO = dxo_
      allocate( DX(2,KX))
      DX = dx_
      DXI(1) = max( 0, maxval( DX(1,:))); DXI(2) = max( 0, maxval( DX(2,:)))
      allocate( cGX(DXI(1),DXI(2),KX), cSX(DXI(1),DXI(2),KX), nX(DXI(1),DXI(2),KX))
      allocate( SHX(DXI(1),DXI(2),KX,memlen), GHX(DXI(1),DXI(2),KX,memlen))
      allocate( RH(memlen))
      ! local allocation
      allocate( sx(DXI(1),DXI(2),KX), gx(DXI(1),DXI(2),KX))

      if( stdout .gt. 0) write( stdout, '("# L-BFGS minimization run statistics")')
      if( stdout .gt. 0) write( stdout, '("# maximum matrix dimensions: ",4x,2i5)') DXI
      if( stdout .gt. 0) write( stdout, '("# number of matrices:        ",4x,i5)') KX
      if( stdout .gt. 0) write( stdout, '("# dimension of optimization: ",i9)') dimX()
      if( stdout .gt. 0) write( stdout, '("# L-BFGS memory depth:       ",4x,i5)') memlen
      if( stdout .gt. 0) write( stdout, '("#")')
      if( stdout .gt. 0) write( stdout, '("# iteration",13x,"time",17x,"cost",11x,"|gradient|",17x,"step",4x,"directional slope")')

      ! initialization
      stepsize = 1.d0; alpha = 1.d0; scaleFactor = 1.d0
      accepted = .true.; ultimatum = .false.
      it = 0; pos = 0; mins = 0.d0
      finish = .false.
      call timesec( t0)

      ! get current cost and gradient
      if( present( update)) call update( X, DXO, KX, DX, it)
      call ucost( cost, X, DXO, cCost)
      call ugrad( grad, X, DXO, cGX, DXI)
      gradnorm = norm( cGX, DXI, DXI)
      cSX = -cGX
      call timesec( t1)
      if( expc) then
        fhist = cshift( fhist, 1)
        fhist( nhist) = cCost
      end if
      if( stdout .gt. 0) write( stdout, '(2x,i9,4(x,g20.10))') it, t1-t0, cCost, gradnorm

      ! the minimization loop
      do while( .true.)
        ! check termination
        if( alpha .lt. mins+eps0) then
          if( .not. ultimatum) then
            pos = 0
            mins = 0.d0
            scaleFactor = 1.d0
            ultimatum = .true.
          else
            finish = .true.
          end if
        else
          ultimatum = .false.
        end if
        if( it .ge. maxit)         finish = .true.
        if( gradnorm .lt. epsgrad) finish = .true.
        if( it .lt. minit)         finish = .false.
        if( finish) exit

        ! get direction
        call rlbfgs_getdir( X, DXO, scaleFactor, min( pos, memlen), cSX, DXI)
        snorm = norm( cSX, DXI, DXI)
        ! get directional derivative
        df0 = inner( cGX, DXI, cSX, DXI, DXI)
        !angle = acos( -df0/(snorm*gradnorm))
        ! perform line search
        !if( it .gt. 0) stepsize = 2.d0*dCost/df0
        stepsize = snorm
        call linesearch( cost, grad, X, DXO, cSX, DXI, cCost, df0, stepsize, nX, DXI, nCost)
        if( present( update)) then
          call update( nX, DXI, KX, DX, it)
          call ucost( cost, nX, DXI, nCost)
        end if
        ! get step multiplier
        alpha = stepsize/snorm
        if( (alpha .gt. mins0) .and. (mins .lt. eps0) .and. (pos .gt. 0)) mins = mins0
        if( alpha .gt. eps0) then
          cSX = alpha*cSX
          ! transport old gradient and direction to new X
          call transp( X, DXO, nX, DXI, cGX, DXI, gx, DXI)
          call transp( X, DXO, nX, DXI, cSX, DXI, sx, DXI)
          ! get gradient at new X
          call ugrad( grad, nX, DXI, cGX, DXI)
          ! get step s and gradient difference 
          gx = cGX - gx
          iss = norm( sx, DXI, DXI)
          sx = sx/iss; gx = gx/iss
          isg = inner( sx, DXI, gx, DXI, DXI)
          iss = norm( sx, DXI, DXI)**2
          ! update history
          if( (iss .gt. eps0) .and. (isg/iss .ge. 1.d-4*gradnorm)) then
            accepted = .true.
            rho = 1.d0/isg
            scaleFactor = isg/norm( gx, DXI, DXI)**2
            do im = max( 1, memlen-pos), memlen-1
              call transp( X, DXO, nX, DXI, SHX(1,1,1,im+1), DXI, SHX(1,1,1,im), DXI)
              call transp( X, DXO, nX, DXI, GHX(1,1,1,im+1), DXI, GHX(1,1,1,im), DXI)
              RH(im) = RH(im+1)
            end do
            if( memlen .gt. 0) then
              SHX(:,:,:,memlen) = sx; GHX(:,:,:,memlen) = gx
              RH(memlen) = rho
            end if
            if( pos .lt. memlen-1) pos = pos + 1
          else
            accepted = .false.
            do im = memlen-pos, memlen
              call transp( X, DXO, nX, DXI, SHX(1,1,1,im), DXI, sx, DXI); SHX(:,:,:,im) = sx
              call transp( X, DXO, nX, DXI, GHX(1,1,1,im), DXI, gx, DXI); GHX(:,:,:,im) = gx
            end do
            !scaleFactor = 1.d0
            !pos = 0
          end if
        end if
        ! update X
        X(1:DXI(1),1:DXI(2),1:KX) = nX
        ! update current values
        dCost = 2.d0*(nCost - cCost)
        gradnorm = norm( cGX, DXI, DXI)
        cCost = nCost
        
        it = it + 1

        call timesec( t1)
        if( stdout .gt. 0) write( stdout, '(2x,i9,5(x,g20.10),i)') it, t1-t0, cCost, gradnorm, alpha, df0, pos

        if( expc) then
          fhist = cshift( fhist, 1)
          fhist( nhist) = cCost
          if(it .eq. nextexpc) call expconv( fhist(0), nhist, finish)
        end if
      end do
      if( stdout .gt. 0) write( stdout, '("# evaluations of cost fun. : ",i9)') nCostEval
      if( stdout .gt. 0) write( stdout, '("# evaluations of gradient  : ",i9)') nGradEval
      deallocate( sx, gx)

      if( present( tolgrad)) tolgrad = gradnorm
      if( present( maxiter)) maxiter = it
      call destroy
    end subroutine stiefel_optim_lbfgs

    subroutine rlbfgs_getdir( x, dx, s, m, p, dp)
      integer, intent( in)     :: dx(2), m, dp(2)
      complex(8), intent( in)  :: x(dx(1),dx(2),*)
      real(8), intent( in)     :: s
      complex(8), intent( out) :: p(dp(1),dp(2),*)

      integer :: im
      real(8) :: isq(memlen)

      isgrad = .true.
      p(1:DXI(1),1:DXI(2),1:KX) = cGX
      do im = memlen, memlen-m+1, -1
        isgrad = .false.
        isq(im) = RH(im)*inner( SHX(1,1,1,im), DXI, p, dp, DXI)
        p(1:DXI(1),1:DXI(2),1:KX) = p(1:DXI(1),1:DXI(2),1:KX) - isq(im)*GHX(:,:,:,im)
      end do
      p(:,:,1:KX) = s*p(:,:,1:KX)
      do im = memlen-m+1, memlen
        isgrad = .false.
        isq(im) = isq(im) - RH(im)*inner( GHX(1,1,1,im), DXI, p, dp, DXI)
        p(1:DXI(1),1:DXI(2),1:KX) = p(1:DXI(1),1:DXI(2),1:KX) + isq(im)*SHX(:,:,:,im)
      end do
      p(:,:,1:KX) = -p(:,:,1:KX)
      
      return
    end subroutine rlbfgs_getdir

    !**********************************
    !* GENERAL FUNCTIONS
    !**********************************
    subroutine linesearch( cost, grad, x, dx, p, dp, f0, df0, s, nx, dn, nf)
      procedure( cost_interface) :: cost
      procedure( grad_interface) :: grad
      integer, intent( in)       :: dx(2), dp(2), dn(2)
      complex(8), intent( in)    :: x(dx(1),dx(2),*)
      complex(8), intent( in)    :: p(dp(1),dp(2),*)
      real(8), intent( in)       :: f0, df0
      real(8), intent( inout)    :: s
      complex(8), intent( out)   :: nx(dn(1),dn(2),*)
      real(8), intent( out)      :: nf

      real(8), parameter :: fcontr    = 0.5d0
      real(8), parameter :: suffdecr  = 1.d-4
      integer, parameter :: maxsteps  = 50
      logical, parameter :: backtrack = .true.
      logical, parameter :: forcedecr = .true.

      integer :: it
      real(8) :: alpha, df, pn

      pn = norm( p, dp, dp)
      alpha = 1.d0*s/pn; it = 1
      call retr( x, dx, p, dp, alpha, nx, dn)
      call ucost( cost, nx, dn, nf)

      do while( backtrack .and. (nf .gt. f0 + suffdecr*alpha*df0))
        alpha = max( mins, alpha*fcontr)
        call retr( x, dx, p, dp, alpha, nx, dn)
        call ucost( cost, nx, dn, nf)
        it = it + 1
        if( (it .gt. maxsteps) .or. (alpha .lt. mins+eps0)) exit
      end do

      if( (forcedecr .and. (nf .gt. f0)) .or. isnan( nf)) then
        alpha = 0.d0
        nx(:,:,1:KX) = x(1:dn(1),1:dn(2),1:KX)
        nf = f0
      end if

      s = alpha*pn
      return
    end subroutine linesearch

    subroutine linesearch_grad( cost, grad, x, dx, p, dp, f0, df0, s, nx, dn, nf)
      procedure( cost_interface) :: cost
      procedure( grad_interface) :: grad
      integer, intent( in)       :: dx(2), dp(2), dn(2)
      complex(8), intent( in)    :: x(dx(1),dx(2),*)
      complex(8), intent( in)    :: p(dp(1),dp(2),*)
      real(8), intent( in)       :: f0, df0
      real(8), intent( inout)    :: s
      complex(8), intent( out)   :: nx(dn(1),dn(2),*)
      real(8), intent( out)      :: nf

      real(8), parameter :: fcontr    = 0.61803398874989484820d0
      real(8), parameter :: suffdecr  = 1.d-4
      integer, parameter :: maxsteps  = 25
      logical, parameter :: backtrack = .true.
      logical, parameter :: forcedecr = .false.

      integer :: it
      real(8) :: alpha, pn, ia(2), id(2), r
      complex(8), allocatable :: g(:,:,:)

      pn = norm( p, dp, dp)

      ! test
      ia = (/0.d0, -1.d3/df0/)
      id = (/df0, 1.d120/)
      allocate( g(dx(1),dx(2),KX))
      write(*,*) '--- line search test ---'
      alpha = ia(2)*s/pn
      do it = 1, 30
        call retr( x, dx, p, dp, alpha, nx, dn)
        call ugrad( grad, nx, dn, g, dx)
        call ucost( cost, nx, dn, nf)
        ia(2) = alpha
        id(2) = inner( g, dx, p, dp, dp)
        if( abs( id(2)) .lt. abs( id(1))) then
          r = id(1); id(1) = id(2); id(2) = r
          r = ia(1); ia(1) = ia(2); ia(2) = r
        end if
        alpha = (ia(2)*id(1) - ia(1)*id(2))/(id(1) - id(2))
        if( alpha .lt. 0.d0) alpha = ia(2) + 1.d-2
        write(*,'(i,7g20.10)') it, nf, alpha, ia, id
        if( abs( ia(2) - ia(1)) .lt. 1.d-6) exit
      end do
      write(*,*) '------------------------'
      call retr( x, dx, p, dp, alpha, nx, dn)
      call ucost( cost, nx, dn, nf)
      deallocate( g)

      s = alpha*pn
      return
    end subroutine linesearch_grad
    ! fit an exponential to the last n function values and determine convergence
    subroutine expconv( f, n, conv)
      real(8), intent( in)  :: f(*)
      integer, intent( in)  :: n
      logical, intent( out) :: conv

      integer :: i
      real(8) :: x(n), y(n,1), fit(n,1), grad(n,3), p(3,1), dp(3,1), m(3,3), d

      conv = .false.

      ! normalize data
      do i = 1, n
        x(i) = dble( i-1)/dble( n-1)
        y(i,1) = (f(i) - f(n+1))/(f(1) - f(n))
      end do
      ! initialize parameters
      call init( x, y(:,1), p)
      write(*,'(100f13.6)') p(:,1)
      ! optimize fit
      do i = 0, 100-1
        call fg( x, p(:,1), fit(:,1), grad)
        dp = 2.d0*matmul( transpose( grad), y - fit)
        m = matmul( transpose( grad), grad)
        d = m(1,1)*m(2,2)*m(3,3) + m(1,2)*m(2,3)*m(3,1) + m(1,3)*m(2,1)*m(3,2) -&
            m(3,1)*m(2,2)*m(1,3) - m(3,2)*m(2,3)*m(1,1) - m(3,3)*m(2,1)*m(1,2)
        if( (norm2( dp) .lt. 1.d-6) .or. (norm2( dp) .gt. 1.d6) .or. (d .lt. 1.d-6)) exit
        call rlsp( grad, y - fit, dp)
        p = p + dp
      end do
      p(1,1) = p(1,1)*(f(1) - f(n))
      p(2,1) = p(2,1)*(f(1) - f(n)) + f(n+1)
      p(3,1) = p(3,1)/dble( n-1)
      ! check convergence
      write(*,'(i,2f13.6)') nextexpc, p(2,1), sum( f(1:n))/(n*p(2,1)) - 1.d0
      if( (abs( p(1,1)) .lt. 1.d-6) .or. (i .eq. 100) .or. (d .lt. 1.d-6) .or. (p(3,1) .gt. 0.d0)) then
        nextexpc = nextexpc + 1
      else
        if( abs( sum( f(1:n))/(n*p(2,1)) - 1.d0) .lt. epsexpc) conv = .true.
        nextexpc = nextexpc + n
      end if

      return

      contains
        subroutine init( x, y, p)
          real(8), intent( in)  :: x(n), y(n)
          real(8), intent( out) :: p(3,1)

          integer :: i
          real(8) :: R2, coeff(n,2), rhs(n,1), lp(2,1), v(n)

          coeff(:,1) = 1.d0
          coeff(:,2) = x
          p(2,1) = minval(y) - 1.d0
          write(*,'(100f13.6)') x
          write(*,'(100f13.6)') y
          do i = 1, 10
            rhs(:,1) = log( abs( y - p(2,1)))
            write(*,*) i
            write(*,'(100f13.6)') rhs(:,1)
            call rlsp( coeff, rhs, lp)
            v = rhs(:,1) - sum( rhs)/dble( n)
            rhs = matmul( coeff, lp) - rhs
            R2 = 1.d0 - dot_product( rhs(:,1), rhs(:,1))/dot_product( v, v)
            write(*,'(100f13.6)') R2, lp(:,1), p(2,1)
            if( R2 .gt. 0.95d0) exit
            p(2,1) = y(1) - exp( lp(1,1))
          end do
          p(3,1) = lp(2,1)
          v = exp( lp(2,1)*x)
          coeff(:,1) = v
          coeff(:,2) = 1.d0 - v
          call rlsp( coeff, reshape( y, (/n,1/)), lp)
          p(1,1) = lp(1,1) - lp(2,1)
          p(2,1) = lp(2,1)
          return
        end subroutine init

        subroutine fg( x, p, f, g)
          real(8), intent( in)  :: x(n), p(3)
          real(8), intent( out) :: f(n), g(n,3)

          integer :: i

          do i = 1, n
            g(i,1) = exp( p(3)*x(i))
            g(i,2) = 1.d0
            g(i,3) = p(1)*x(i)*g(i,1)
            f(i) = p(1)*g(i,1) + p(2)
          end do
          
          return
        end subroutine fg

    end subroutine expconv
    
    subroutine stiefel_optim_setexpconv( n, m, eps)
      integer, intent( in) :: n, m
      real(8), intent( in) :: eps

      expc = .true.
      nexpc = n
      epsexpc = eps
      nextexpc = n
      nhist = m
      if( allocated( fhist)) deallocate( fhist)
      allocate( fhist(0:nhist))
      fhist = 0.d0
    end subroutine stiefel_optim_setexpconv

    !**********************************
    !* HELPER FUNCTIONS
    !**********************************
    subroutine sanityCheck( dxo_, kx_, dx_)
      integer, intent( in) :: dxo_(2), kx_, dx_(2,kx_)

      if( kx_ .lt. 0) then
        write(*,*)
        write(*,'("Error (stiefel_optim_rlbfgs): The number of matrices K must not be negative.")')
        stop
      end if
      if( minval( dx_(1,:)) .lt. 0) then
        write(*,*)
        write(*,'("Error (stiefel_optim_rlbfgs): The number of rows in X must not be negative.")')
        stop
      end if
      if( minval( dx_(2,:)) .lt. 0) then
        write(*,*)
        write(*,'("Error (stiefel_optim_rlbfgs): The number of columns in X must not be negative.")')
        stop
      end if
      if( any( dx_(1,:) - dx_(2,:) .lt. 0)) then
        write(*,*)
        write(*,'("Error (stiefel_optim_rlbfgs): The number of rows in X must not be smaller than the number columns in X.")')
        stop
      end if
      if( maxval( dx_(1,:)) .gt. dxo_(1)) then
        write(*,*)
        write(*,'("Error (stiefel_optim_rlbfgs): The number of rows in X must not be greater than the allocated dimension.")')
        stop
      end if
      if( maxval( dx_(2,:)) .gt. dxo_(2)) then
        write(*,*)
        write(*,'("Error (stiefel_optim_rlbfgs): The number of columns in X must not be greater than the allocated dimension.")')
        stop
      end if
    end subroutine sanityCheck
    
    subroutine destroy
      nCostEval = 0
      nGradEval = 0

      if( allocated( DX))  deallocate( DX)

      if( allocated( cGX)) deallocate( cGX)
      if( allocated( cSX)) deallocate( cSX)
      if( allocated( nX))  deallocate( nX)

      if( allocated( SHX)) deallocate( SHX)
      if( allocated( GHX)) deallocate( GHX)
      if( allocated(  RH)) deallocate(  RH)

      if( allocated( fhist)) deallocate( fhist)
      expc = .false.
    end subroutine destroy

    !**********************************
    !* UTILITY FUNCTIONS
    !**********************************
    subroutine ucost( cost, x, dx_, f)
      procedure( cost_interface) :: cost
      integer, intent( in)       :: dx_(2)
      complex(8), intent( in)    :: x(dx_(1),dx_(2),*)
      real(8), intent( out)      :: f
      call cost( x, dx_, KX, DX, f)
      nCostEval = nCostEval + 1
      return
    end subroutine ucost

    subroutine ugrad( grad, x, dx_, gx, dg_)
      procedure( grad_interface) :: grad
      integer, intent( in)       :: dx_(2), dg_(2)
      complex(8), intent( in)    :: x(dx_(1),dx_(2),*)
      complex(8), intent( out)   :: gx(dg_(1),dg_(2),*)
      complex(8), allocatable :: tmp(:,:,:)
      allocate( tmp(dg_(1),dg_(2),KX))
      call grad( x, dx_, KX, DX, tmp, dg_)
      call egrad2rgrad( x, dx_, tmp, dg_, gx, dg_)
      nGradEval = nGradEval + 1
      deallocate( tmp)
      return
    end subroutine ugrad

    ! dimension
    function dimkX( ik) result( d)
      integer, intent( in) :: ik
      integer :: d
      d = 2*DX(1,ik)*DX(2,ik) - DX(2,ik)*DX(2,ik)
    end function dimkX

    function dimX result( d)
      integer :: d
      integer :: ik
      d = 0
      do ik = 1, KX
        d = d + dimkX( ik)
      end do
    end function dimX

    ! inner product
    function inner( m1, d1, m2, d2, d3) result( r)
      integer, intent( in)    :: d1(2), d2(2), d3(2)
      complex(8), intent( in) :: m1(d1(1),d1(2),*), m2(d2(1),d2(2),*)
      real(8)                 :: r
      complex(8), external :: zdotc
      r = 0.d0
      if( d3(1)*d3(2)*KX .le. 0) return
      r = dble( zdotc( d3(1)*d3(2)*KX, m1, 1, m2, 1))
    end function inner

    ! norm
    function norm( m, d1, d2) result( r)
      integer, intent( in)    :: d1(2), d2(2)
      complex(8), intent( in) :: m(d1(1),d1(2),*)
      real(8)                 :: r
      r = dsqrt( inner( m, d1, m, d1, d2))
    end function norm

    ! projection
    subroutine proj( m, dm, u, du, p, dp)
      integer, intent( in)     :: dm(2), du(2), dp(2)
      complex(8), intent( in)  :: m(dm(1),dm(2),*)
      complex(8), intent( in)  :: u(du(1),du(2),*)
      complex(8), intent( out) :: p(dp(1),dp(2),*)

      integer :: ik
      complex(8), allocatable :: auxm(:,:)

#ifdef USEOMP
!$omp parallel default( shared) private( ik, auxm)
#endif
      allocate( auxm(dm(2),dm(2)))
#ifdef USEOMP
!$omp do
#endif
      do ik = 1, KX
        p(:,:,ik) = zero
        if( (DX(1,ik) .le. 0) .or. (DX(2,ik) .le. 0)) cycle
        auxm = zero
        p(1:DX(1,ik),1:DX(2,ik),ik) = u(1:DX(1,ik),1:DX(2,ik),ik)
        call zgemm( 'c', 'n', DX(2,ik), DX(2,ik), DX(1,ik), one, m(1,1,ik), dm(1), u(1,1,ik), du(1), zero, auxm(1,1), dm(2))
        auxm = 0.5d0*( auxm + conjg( transpose( auxm)))
        call zgemm( 'n', 'n', DX(1,ik), DX(2,ik), DX(2,ik), -one, m(1,1,ik), dm(1), auxm(1,1), dm(2), one, p(1,1,ik), dp(1))
      end do
#ifdef USEOMP
!$omp end do
#endif
      deallocate( auxm)
#ifdef USEOMP
!$omp end parallel
#endif
      return
    end subroutine proj

    ! retraction
    subroutine retr( m, dm, u, du, t, re, dr)
      integer, intent( in)     :: dm(2), du(2), dr(2)
      real(8), intent( in)     :: t
      complex(8), intent( in)  :: m(dm(1),dm(2),*)
      complex(8), intent( in)  :: u(du(1),du(2),*)
      complex(8), intent( out) :: re(dr(1),dr(2),*)

      integer :: ik, i
      complex(8), allocatable :: r(:,:)
      
#ifdef USEOMP
!$omp parallel default( shared) private( ik, r)
#endif
      allocate( r(dr(1),dr(2)))
#ifdef USEOMP
!$omp do
#endif
      do ik = 1, KX
        re(:,:,ik) = zero
        if( (DX(1,ik) .le. 0) .or. (DX(2,ik) .le. 0)) cycle
        re(1:DX(1,ik),1:DX(2,ik),ik) = m(1:DX(1,ik),1:DX(2,ik),ik)
        if( t .ne. 0.d0) then
          r(1:DX(1,ik),1:DX(2,ik)) = re(1:DX(1,ik),1:DX(2,ik),ik) + cmplx( t, 0.d0, 8)*u(1:DX(1,ik),1:DX(2,ik),ik)
          call zqr( r(1:DX(1,ik),1:DX(2,ik)), q=re(1:DX(1,ik),1:DX(2,ik),ik))
        end if
      end do
#ifdef USEOMP
!$omp end do
#endif
      deallocate( r)
#ifdef USEOMP
!$omp end parallel
#endif
      return
    end subroutine retr

    !exponential
    subroutine mexp( m, dm, u, du, t, r, dr)
      integer, intent( in)     :: dm(2), du(2), dr(2)
      real(8), intent( in)     :: t
      complex(8), intent( in)  :: m(dm(1),dm(2),*)
      complex(8), intent( in)  :: u(du(1),du(2),*)
      complex(8), intent( out) :: r(dr(1),dr(2),*)

      integer :: ik, i
      complex(8), allocatable :: auxm1(:,:), auxm2(:,:), expm1(:,:), expm2(:,:)

#ifdef USEOMP
!$omp parallel default( shared) private( ik, i, auxm1, expm1, auxm2, expm2)
#endif
      allocate( auxm1(dm(2),du(2)), expm1(dm(2),du(2)))
      allocate( auxm2(2*dm(2),2*du(2)), expm2(2*dm(2),2*du(2)))
#ifdef USEOMP
!$omp do
#endif
      do ik = 1, KX
        r(:,:,ik) = zero
        call zgemm( 'c', 'n', DX(2,ik), DX(2,ik), DX(1,ik), cmplx( t, 0.d0, 8), m(1,1,ik), dm(1), u(1,1,ik), du(1), zero, auxm1(1,1), dm(2))
        auxm2 = zero
        auxm2( 1:DX(2,ik),              1:DX(2,ik))              = auxm1( 1:DX(2,ik), 1:DX(2,ik))
        auxm2( (DX(2,ik)+1):2*DX(2,ik), (DX(2,ik)+1):2*DX(2,ik)) = auxm1( 1:DX(2,ik), 1:DX(2,ik))
        call zgemm( 'c', 'n', DX(2,ik), DX(2,ik), DX(1,ik), -cmplx( t*t, 0.d0, 8), u(1,1,ik), du(1), u(1,1,ik), du(1), zero, auxm2(1,DX(2,ik)+1), 2*dm(2))
        do i = 1, DX(2,ik)
          auxm2( DX(2,ik)+i, i) = one
        end do
        call zexpm( auxm2( 1:2*DX(2,ik), 1:2*DX(2,ik)), expm2( 1:2*DX(2,ik), 1:2*DX(2,ik)))
        expm1 = zero
        auxm2 = zero
        call zexpm( -auxm1( 1:DX(2,ik), 1:DX(2,ik)), expm1( 1:DX(2,ik), 1:DX(2,ik)))
        call zgemm( 'n', 'n', 2*DX(2,ik), DX(2,ik), DX(2,ik), one, expm2(1,1), 2*dm(2), expm1(1,1), dm(2), zero, auxm2, 2*dm(2))
        call zgemm( 'n', 'n', DX(1,ik), DX(2,ik), DX(2,ik), one, m(1,1,ik), dm(1), auxm2(1,1), 2*dm(2), zero, r(1,1,ik), dr(1))
        call zgemm( 'n', 'n', DX(1,ik), DX(2,ik), DX(2,ik), cmplx( t, 0.d0, 8), u(1,1,ik), du(1), auxm2(DX(2,ik)+1,1), 2*dm(2), one, r(1,1,ik), dr(1))
      end do
#ifdef USEOMP
!$omp end do
#endif
      deallocate( auxm1, auxm2, expm1, expm2)
#ifdef USEOMP
!$omp end parallel
#endif
      return
    end subroutine mexp

    ! transport
    subroutine transp( x, dx, y, dy, s, ds, u, du)
      integer, intent( in)     :: dx(2), dy(2), ds(2), du(2)
      complex(8), intent( in)  :: x(dx(1),dx(2),*)
      complex(8), intent( in)  :: y(dy(1),dy(2),*)
      complex(8), intent( in)  :: s(ds(1),ds(2),*)
      complex(8), intent( out) :: u(du(1),du(2),*)
      call proj( y, dy, s, ds, u, du)
      return
    end subroutine transp

    ! tangent space
    subroutine tangent( x, dx, s, ds, u, du)
      integer, intent( in)     :: dx(2), ds(2), du(2)
      complex(8), intent( in)  :: x(dx(1),dx(2),*)
      complex(8), intent( in)  :: s(ds(1),ds(2),*)
      complex(8), intent( out) :: u(du(1),du(2),*)
      call proj( x, dx, s, ds, u, du)
      return
    end subroutine tangent

    ! Euclidian to Riemannian gradient
    subroutine egrad2rgrad( x, dx, s, ds, u, du)
      integer, intent( in)     :: dx(2), ds(2), du(2)
      complex(8), intent( in)  :: x(dx(1),dx(2),*)
      complex(8), intent( in)  :: s(ds(1),ds(2),*)
      complex(8), intent( out) :: u(du(1),du(2),*)
      call proj( x, dx, s, ds, u, du)
      return
    end subroutine egrad2rgrad
end module mod_stiefel_optim
