!
!
!
! Copyright (C) 2002-2005 J. K. Dewhurst, S. Sharma and C. Ambrosch-Draxl.
! This file is distributed under the terms of the GNU Lesser General Public
! License. See the file COPYING for license details.
!
!BOP
! !ROUTINE: hermitize
! !INTERFACE:
subroutine hermitize( m, t)
! !INPUT/OUTPUT PARAMETERS:
!   m : Complex matrix to hermitize (inout,complex)
!   t : type (in,*,optional)
! !DESCRIPTION:
!   Forces Hermiticity on the matrix $$M$$.
!   Possible types: \tt{upper}, \tt{lower} and \tt{avg}
!
! !REVISION HISTORY:
!   Created September 2019 (SeTi)
!EOP
!BOC
  implicit none
! arguments
  complex(8), intent( inout)          :: m(:,:)
  character(*), optional, intent( in) :: t
! local variables
  integer :: nx, ny, x, y
  character(256) :: t_

  write( t_, '("upper")')

  if( present( t)) t_ = trim( t)
  nx = size( m, 1)
  ny = size( m, 2)

  if( nx .ne. ny) then
    write(*,*)
    write(*,'("Error (hermitize): Given matrix is not square.")')
    stop
  end if

  do x = 1, nx
    m(x,x) = cmplx( dble( m(x,x)), 0.d0, 8)
  end do

  select case( trim( t_))
    case( 'upper')
      do x = 1, nx
        do y = 1, nx-1
          m(x,y) = conjg( m(y,x))
        end do
      end do
    case( 'lower')
      do x = 1, nx
        do y = 1, nx-1
          m(y,x) = conjg( m(x,y))
        end do
      end do
    case( 'avg')
      do x = 1, nx
        do y = 1, nx-1
          m(x,y) = cmplx( 0.5d0, 0.d0, 8)*(m(y,x) + conjg( m(x,y)))
        end do
      end do
    case default
      write(*,*)
      write(*,'("Error (hermitize): Invalid type.")')
      stop
  end select
  return
end subroutine hermitize
!EOC
