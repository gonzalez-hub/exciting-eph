subroutine convolute( n, x, f, k, width, m, g)
  use mod_constants, only: pi
  implicit none

  integer, intent( in)  :: n            ! # of input points
  real(8), intent( in)  :: x( n)        ! abscissa values
  real(8), intent( in)  :: f( n)        ! the function to convolute
  integer, intent( in)  :: k            ! the convolution kernel (Lorentzian only up to now)
  real(8), intent( in)  :: width        ! width of the kernel
  integer, intent( in)  :: m            ! mode (1-linear, 2-cubic)
  real(8), intent( out) :: g( n)        ! the convoluted function

  integer :: i, j

  real(8), allocatable :: xk(:), kernel(:), fk(:), gk(:), cf(:,:)

  g = 0.d0

  allocate( cf( 0:3, n))
  cf(0,:) = f

  if( m .eq. 2) then
    call spline( n, x, 1, f, cf(1:3,:))
  else
    do i = 1, n-1
      cf(1,i) = (f(i+1) - f(i))/(x(i+1) - x(i))
    end do
  end if

  do i = 1, n
    g(i) = 0.5d0*(f(1) + f(n)) + pintLorentz0( cf(0:0,1), width, x(i), x(1), x(1)) - pintLorentz0( cf(0:0,n), width, x(i), x(n), x(n))
    do j = 1, n-1
      if( m .eq. 2) then
        g(i) = g(i) + (pintLorentz3( cf(0:3,j), width, x(i), x(j), x(j+1)) - pintLorentz3( cf(0:3,j), width, x(i), x(j), x(j)))
      else
        g(i) = g(i) + (pintLorentz1( cf(0:1,j), width, x(i), x(j), x(j+1)) - pintLorentz1( cf(0:1,j), width, x(i), x(j), x(j)))
      end if
    end do
  end do

  deallocate( cf)

  return

  contains
    ! These are Integrals of the type
    ! I_n(x,t) = \int dt f_n(t) K(x-t)
    ! where f_n(x) = \Sum_{i=0}^n c_i (x-x0)^i is a polynomial of degree n
    ! and K(x) is the normalized convolution kernel with width w (e.g. Lorentzian).
    ! The width is given as the full width at half maximum (FWHM), i.e. K(w/2) = 1/2 K(0).
    ! Lorentzian: K(x) = 1/pi (w/2)/(x^2 + (w/2)^2)
    ! Gaussian:   K(x) = 2 sqrt(ln(2)/pi) exp(-4 ln(2) (x/w)^2)
    ! Feel free to add integrals for other kernels (e.g. Gaussian)
    function pintLorentz0( c, w, x, x0, t) result( p)
      real(8), intent( in) :: c(0:0), w, x, x0, t
      real(8) :: p

      real(8) :: r1

      r1 = 1.d0/pi

      p = r1*c(0)*atan( 2.d0*(t-x)/w)
      return
    end function pintLorentz0

    function pintLorentz1( c, w, x, x0, t) result( p)
      real(8), intent( in) :: c(0:1), w, x, x0, t
      real(8) :: p

      real(8) :: r1

      r1 = 1.d0/(4.d0*pi)

      p = r1*(4.d0*(c(0)+c(1)*(x-x0))*atan( 2.d0*(t-x)/w) + &
              c(1)*w*log( w**2 + 4.d0*(t-x)**2))
      return
    end function pintLorentz1

    function pintLorentz3( c, w, x, x0, t) result( p)
      real(8), intent( in) :: c(0:3), w, x, x0, t
      real(8) :: p

      real(8) :: r1

      r1 = 1.d0/(16.d0*pi)

      p = r1*(8.d0*w*(t-x0)*(c(2) + 2.d0*c(3)*(x-x0)) + &
              4.d0*w*c(3)*(t-x0)**2 + &
              4.d0*(4.d0*c(0) - w**2*c(2) + (x-x0)*(4.d0*c(1) - 3.d0*w**2*c(3) + 4.d0*(x-x0)*(c(2) + c(3)*(x-x0))))*atan( 2.d0*(t-x)/w) + &
              w*(4.d0*c(1) + 8.d0*c(2)*(x-x0) - c(3)*(w**2 - 12.d0*(x-x0)**2))*log( w**2 + 4.d0*(t-x)**2))
      return
    end function pintLorentz3

end subroutine convolute
