function occ( x, flag) result( o)
  implicit none
  ! Fermi-Dirac occupation for flag >= 0
  ! Bose-Einstein occupation otherwise
  real(8), intent( in) :: x
  integer, intent( in) :: flag
  real(8)              :: o

  if( x .gt. 700.d0) then
    o = 0.d0
  else if( x .lt. -700.d0) then
    o = 1.d0
  else
    o = 1.d0/(exp( x) + 1.d0)
  end if
  if( flag .ge. 0) return

  if( o .ge. 0.5d0) then
    o = 0.d0
    return
  end if

  if( log10( 0.5d0 - o) .lt. -300.d0) then
    o = 1.d300
    return
  end if

  o = o/(1.d0 - 2.d0*o)
  return
end function occ
