subroutine dwtderiv( n, x, f, d, g)
  use mod_constants
  use m_plotmat
  implicit none

  integer, intent( in)  :: n      ! # of input points
  real(8), intent( in)  :: x( n)  ! the x-values at which f is given
  real(8), intent( in)  :: f( n)  ! the input function
  integer, intent( in)  :: d      ! degree of derivative
  real(8), intent( out) :: g( n)  ! the derivative

  integer :: i, j, n0, n1, nn, m0, m1, mode
  real(8) :: dx, a, b
  integer, allocatable :: shift(:)
  real(8), allocatable :: h0(:), h1(:), xx(:), ff(:), hh0(:), hh1(:), y0(:), y1(:), yt(:)

  mode = 1

  if( mode .eq. 1) then
    allocate( h0(4), h1(2))
    h0 = (/0.125d0, 0.375d0, 0.375d0, 0.125d0/)*sqrt( 2.d0)
    h1 = (/-2.d0, 2.d0/)*sqrt( 2.d0)
  else
    allocate( h0(2), h1(2))
    h0 = (/ 0.5d0, 0.5d0/)*sqrt( 2.d0)
    h1 = (/-0.5d0, 0.5d0/)*sqrt( 2.d0)
  end if

  n0 = 5000
  n1 = n0/2
  nn = 2*n0 - 1
  dx = (x(n) - x(1))/dble( n0-1)

  allocate( xx( n0), ff( nn), shift( nn))
  xx = x(1)
  do i = 2, n0
    xx( i) = xx( i-1) + dx
  end do
  call interp1d( n, x, f, n0, xx, ff( 1:n0), 'spline')
  a = (ff( n0) - ff(1))/(xx( n0) - xx( 1))
  b = ff(1) - a*xx(1)
  do i = 1, n0
    ff( i)      = ff( i) - a*xx( i) - b
    ff( 2*n0-i) = -ff( i)
  end do
  !do i = 1, nn
  !  write(*,'(i,g26.16)') i, ff( i)
  !end do
  !write(*,*)
  !write(*,*)
  do i = 1, nn
    shift( 1 + modulo( i-1+n1, nn)) = i
  end do
  ff = ff( shift)
  !do i = 1, nn
  !  write(*,'(i,g26.16)') i, ff( i)
  !end do
  !write(*,*)
  !write(*,*)

  m0 = (size( h0)-1)*2**(d-1) + 1
  m1 = (size( h1)-1)*2**(d-1) + 1
  allocate( hh0( m0), hh1( m1), y0( nn))
  y0 = ff
  do i = 1, d
    m0 = (size( h0)-1)*2**(i-1) + 1
    m1 = (size( h1)-1)*2**(i-1) + 1
    hh0 = 0.d0
    hh1 = 0.d0
    do j = 1, size( h0)
      hh0( 1+(j-1)*2**(i-1)) = h0( j)
    end do
    do j = 1, size( h1)
      hh1( 1+(j-1)*2**(i-1)) = h1( j)
    end do
    if( allocated( yt)) deallocate( yt)
    allocate( yt( size( y0)))
    yt = y0
    call conv( yt, hh1( 1:m1), y1)
    call conv( yt, hh0( 1:m1), y0)
  end do

  j = nint( 0.5d0*(size( y1) - nn))
  if( mode .eq. 1) then
    b = dx*2.d0**(1.d0 + 0.5d0*d)
  else
    b = dx*2.d0**(1.5d0*d)*0.25d0
  end if
  do i = 1, nn
    ff( i) = -y1( i+j)/b + a
  end do
  call interp1d( n0, xx, ff( (n1+1):(n1+n0)), n, x, g, 'spline')

  return

  contains
    subroutine conv( a, b, c)
      real(8), intent( in) :: a(:), b(:)
      real(8), allocatable, intent( out) :: c(:)

      integer :: i, j, n, m
      
      m = size( a)
      n = size( b)
      if( allocated( c)) deallocate( c)
      allocate( c( m+n-1))
      c = 0.d0

      do i = 1, m+n-1
        do j = max( 1, i+1-n), min( i, m)
          c( i) = c( i) + a( j)*b( i - j + 1)
        end do
      end do

      return
    end subroutine
end subroutine dwtderiv
