module mod_polygamma
  use mod_constants
  implicit none

  real(8), parameter :: zeta3    = 1.2020569031595943d0
  real(8), parameter :: dzetam3  = 5.3785763577743011d-3
  real(8), parameter :: lng      = 2.4875447703378426d-1
  real(8), parameter :: eulmas   = 5.7721566490153286d-1
  real(8), parameter :: ln2pi    = 1.8378770664093455d0

  contains

    function polygamma( n, z) result( f)
      integer, intent( in)    :: n
      complex(8), intent( in) :: z
      complex(8)              :: f
      
      if( n .eq. 1) then
        f = pgamma1( z)
      else if( n .eq. 0) then
        f = digamma( z)
      else if( n .eq. -1) then
        f = ngamma1( z)
      else if( n .eq. -2) then
        f = ngamma2( z)
      else if( n .eq. -3) then
        f = ngamma3( z)
      else if( n .eq. -4) then
        f = ngamma4( z)
      else
        write(*,*)
        write(*,'("Error( polygamma): Polygamma function not yet implemented for n = ",i)') n
        stop
      end if
    end function polygamma

    function digamma( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f

      real(8) :: re, im
      complex(8) :: tz, tzi, tzi2
      integer :: n, k
      real(8) :: rlim

      rlim = 15.d0

      if( dble( z) .ge. 0.d0) then
        re = dble( z)
        im = aimag( z)
      else
        re = -dble( z)
        im = -aimag( z)
      end if
      tz = cmplx( re, im, 8)

      n = ceiling( rlim - re)
      f = zzero
      if( n .gt. 0) then
        do k = 0, n-1
          f = f - zone/tz
          tz = tz + zone
        end do
      end if

      tzi = zone/tz
      tzi2 = tzi*tzi
      f = f + log( tz) &
          + tzi*(-0.5d0 &
                 + tzi*(-zone/12.d0 &
                       + tzi2*(zone/120.d0 &
                               + tzi2*(-zone/252.d0 &
                                       + tzi2/240.d0))))

      if( dble( z) .lt. 0.d0) f = f - pi/tan( pi*z) - zone/z
    end function digamma

    function pgamma1( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f

      real(8) :: re, im
      complex(8) :: tz, tz2
      integer :: n, k
      real(8) :: rlim

      rlim = 15.d0

      if( dble( z) .ge. 0.d0) then
        re = dble( z)
        im = aimag( z)
      else
        re = -dble( z)
        im = -aimag( z)
      end if
      tz = cmplx( re, im, 8)

      n = ceiling( rlim - re)
      f = zzero
      if( n .gt. 0) then
        do k = 0, n-1
          f = f + zone/(tz*tz)
          tz = tz + zone
        end do
      end if

      tz2 = tz*tz
      f = f + zone/tz &
            + zone/(2.d0*tz2) &
            + zone/(6.d0*tz*tz2) &
            - zone/(30.d0*tz*tz2*tz2) &
            + zone/(42.d0*tz*tz2*tz2*tz2)

      if( dble( z) .lt. 0.d0) f = -f + pi*pi/(sin( pi*z)*sin( pi*z)) + zone/(z*z)
    end function pgamma1

    function ngamma1( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f

      real(8) :: re, im
      complex(8) :: tz, tzi, tzi2, tzli
      integer :: n, k
      real(8) :: rlim

      rlim = 15.d0

      re = dble( z)
      im = aimag( z)
      tz = cmplx( re, im, 8)

      n = ceiling( rlim - re)
      f = zzero
      if( n .gt. 0) then
        do k = 0, n-1
          f = f - log( tz)
          tz = tz + zone
        end do
      end if

      tzi = zone/tz
      tzi2 = tzi*tzi
      tzli = log( tzi)
      f = f + tz*(-zone - tzli) &
            + 0.5d0*( ln2pi + tzli) &
            + tzi*(zone/12.d0 &
                   +tzi2*(-zone/360.d0 &
                          +tzi2*(zone/1260.d0 &
                                 +tzi2*(-zone/1680.d0 &
                                        +tzi2/1188.d0))))
    end function ngamma1

    function ngamma2( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f

      real(8) :: re, im
      complex(8) :: tz, tzi, tzi2, tzli
      integer :: n, k
      real(8) :: rlim

      rlim = 15.d0

      re = dble( z)
      im = aimag( z)
      tz = cmplx( re, im, 8)

      n = ceiling( rlim - re)
      f = zzero
      if( n .gt. 0) then
        do k = 0, n-1
          f = f - tz*(log( tz) - zone) - 0.5d0*ln2pi
          tz = tz + zone
        end do
      end if

      tzi = zone/tz
      tzi2 = tzi*tzi
      tzli = log( tzi)
      f = f + 0.5d0*tz*(zone + ln2pi + tzli &
                        +tz*(-1.5d0 -tzli)) &
            + lng - tzli/12.d0 &
            + tzi2*(zone/720.d0 &
                    +tzi2*(-zone/5040.d0 &
                           +tzi2*(zone/10080.d0 &
                                  +tzi2*(-zone/9504.d0 &
                                         +tzi2*(691.d0/3603600.d0)))))
    end function ngamma2

    function ngamma3( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f

      real(8) :: re, im
      complex(8) :: tz, tz2, tzi
      integer :: n, k
      real(8) :: rlim

      rlim = 15.d0

      re = dble( z)
      im = aimag( z)
      tz = cmplx( re, im, 8)

      n = ceiling( rlim - re)
      f = zzero
      if( n .gt. 0) then
        do k = 0, n-1
          f = f - 0.25d0*tz*(2.d0*tz*log( tz) - 3.d0*tz + 2.d0*ln2pi) - lng - 0.25d0*ln2pi
          tz = tz + zone
        end do
      end if

      tz2 = tz*tz
      tzi = log( zone/tz)
      f = f - (11.d0/36.d0 + tzi/6.d0)*tz*tz2 &
            + (0.375d0 + 0.25d0*ln2pi)*tz2 &
            + 0.25d0*tz2*tzi &
            + (lng - 1.d0/12.d0 - tzi/12.d0)*tz &
            + zeta3/(8.d0*pi*pi) &
            - zone/(720.d0*tz) &
            + zone/(15120.d0*tz*tz2) &
            - zone/(50400.d0*tz*tz2*tz2) &
            + zone/(66528.d0*tz*tz2*tz2*tz2)
    end function ngamma3

    function ngamma4( z) result( f)
      complex(8), intent( in) :: z
      complex(8)              :: f

      real(8) :: re, im
      complex(8) :: tz, tz2, tzi
      integer :: n, k
      real(8) :: rlim

      rlim = 15.d0

      re = dble( z)
      im = aimag( z)
      tz = cmplx( re, im, 8)

      n = ceiling( rlim - re)
      f = zzero
      if( n .gt. 0) then
        do k = 0, n-1
          f = f - 0.5d0*(1.d0/6.d0*ln2pi + lng + zeta3/(4.d0*pi*pi)) &
                - tz*(lng + 0.25d0*ln2pi &
                      + tz*(0.25d0*ln2pi &
                            - tz*(11.d0/6.d0 + log( zone/tz))/6.d0))
          tz = tz + zone
        end do
      end if

      tz2 = tz*tz
      tzi = log( zone/tz)
      f = f - (25.d0/12.d0 + tzi)*tz2*tz2/24.d0 &
            + (11.d0 + 6.d0*ln2pi + 6.d0*tzi)/72.d0*tz*tz2 &
            - (3.d0 - 24*lng + 2.d0*tzi)/48.d0*tz2 &
            + zeta3/(8.d0*pi*pi)*tz &
            - (11.d0 + 720.d0*dzetam3 - 6.d0*tzi)/4320.d0 &
            - zone/(30240.d0*tz2) &
            + zone/(201600.d0*tz2*tz2) &
            - zone/(399168.d0*tz2*tz2*tz2) &
            + 691.d0/(259459200.d0*tz2*tz2*tz2*tz)
    end function ngamma4
end module mod_polygamma
