!
!
!
! Copyright (C) 2002-2005 J. K. Dewhurst, S. Sharma and C. Ambrosch-Draxl.
! This file is distributed under the terms of the GNU General Public License.
! See the file COPYING for license details.
!
!
Subroutine readdyn_espresso (tsym,dynq)
      Use modmain
      use mod_kpointset
      use m_plotmat
      !Use modinput
      Implicit None
! arguments
      logical, intent(in) :: tsym
      Complex (8), Intent (Out) :: dynq (3*natmtot, 3*natmtot, nqpt)
! local variables
      Logical :: exist
      Integer :: iq, is, js, ia, ja, m(3), n(3)
      Integer :: ip, jp, i, j, k, nk, isym, ik
      Real (8) :: a, b
      complex(8) :: dyns( 3*natmtot, 3*natmtot)
      Character (256) :: fext
      Character (256) :: chdummy
      type( k_set) :: qset
      integer, allocatable :: map(:)
! external functions
      Integer, external :: gcd
      !External gcd

      call generate_k_vectors( qset, bvec, (/1, 1, nphwrt/), (/0.d0, 0.d0, 0.d0/), .false., .false.)
      do i = 1, nphwrt
        qset%vkl(:,i) = vqlwrt(:,i)
        call r3mv( qset%bvec, qset%vkl(:,i), qset%vkc(:,i))
      end do
!      nqpt = nphwrt
      Do iq = 1, nqpt
!         vql(:,iq) = qset%vkl(:,iq)
!         ivq(:,iq) = nint( vql(:,iq)*ngridq)
!         call vecfbz( input%structure%epslat, qset%bvec, vql(:,iq), m)
!         call r3mv( qset%bvec, vql(:,iq), vqc(:,iq))

         call findkptinset( vql(:,iq), qset, isym, ik)
         !write(*,'(2(i,3f19.4),i)') iq, vql(:,iq), ik, qset%vkl(:,ik), isym
         do i = 1, 3
           j = nint( qset%vkl(i,ik)*ngridq(i))
           if( j .ne. 0) then
             is = gcd( j, ngridq(i))
             m(i) = j/is
             n(i) = ngridq(i)/is
           else
             m(i) = 0
             n(i) = 0
           end if
         end do
         i = 0
         Do is = 1, nspecies
            Do ia = 1, natoms (is)
               Do ip = 1, 3
                  Write (fext, '("_Q", 2I2.2, "_", 2I2.2, "_", 2I2.2, "_S", I2.2, "&
                    &_A", I3.3, "_P", I1, ".OUT")') m (1), n (1), m (2), n (2), m (3), &
                    & n (3), is, ia, ip
                  i = i + 1
                  Inquire (File='DYN'//trim(fext), Exist=Exist)
                  If ( .Not. exist) Then
                     Write (*,*)
                     Write (*, '("Error(readdyn): file not found :")')
                     Write (*, '(A)') ' DYN' // trim (fext)
                     Write (*,*)
                     Stop
                  End If
                  Open (50, File='DYN'//trim(fext), Action='READ', &
                 & Status='OLD', Form='FORMATTED')
                  j = 0
                  Do js = 1, nspecies
                     Do ja = 1, natoms (js)
                        Do jp = 1, 3
                           j = j + 1
                           Read (50,*) a, b
                           dynq (i, j, iq) = cmplx (a, b, 8)
                        End Do
                     End Do
                  End Do
                  Close (50)
               End Do
! end loops over atoms and species
            End Do
         End Do
         !dyns = zzero
         !call dynsymapp( isym, qset%vkl(:,ik), dynq(:,:,iq), dyns)
         !dynq(:,:,iq) = dyns
! symmetrise the dynamical matrix
         !call plotmat( dynq(:,:,iq))
         !if (tsym) Call dynsym (vql(:, iq), dynq(:, :, iq))
         if (tsym) Call dynsym (qset%vkl(:, ik), dynq(:, :, iq))
! end loop over q-vectors
      End Do
      !wqpt = 0.d0
      !do i = 0, ngridq(1)-1
      !  do j = 0, ngridq(2)-1
      !    do k = 0, ngridq(3)-1
      !      call findequivkpt( dble( (/i, j, k/))/dble( ngridq), qset, nk, isym, ik)
      !      if( nk .eq. 0) then
      !        write(*,*)
      !        write(*,'("Error (readdyn_espresso): (",3f13.6,") cannot be mapped to reduced grid.")') dble( (/i, j, k/))/dble( ngridq)
      !        stop
      !      end if
      !      iqmap( i, j, k) = ik(1)
      !      wqpt(ik(1)) = wqpt(ik(1)) + 1.d0/dble( product( ngridq))
      !    end do
      !  end do
      !end do
      !call readdyn( tsym, dynq)
      call delete_k_vectors( qset)
      Return
End Subroutine
