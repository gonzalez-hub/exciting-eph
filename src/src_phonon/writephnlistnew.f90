Subroutine writephnlistnew(nmodes, nppt,vpl, w, ev)
      Use modmain
      Implicit None
! arguments
      integer, intent(in) :: nmodes
      integer, intent(in) :: nppt
      real(8), intent(in) :: vpl(3, nppt)
      Real (8), intent(in) :: w(nmodes, nppt)
      Complex (8), intent(in) :: ev (1:3, natmtot, nmodes, nppt)
      !logical, intent(in) :: twrev
      !character(*), intent(in) :: fname
! local variables
      Integer :: n, iq, i, j, is, ia, ip, ias
      Real (8) :: er, ei, eeps
! allocatable arrays
      !Complex (8), Allocatable :: dynq (:, :, :)
      !Complex (8), Allocatable :: dynp (:, :)
      !Complex (8), Allocatable :: dynr (:, :, :)
      !n = 3 * natmtot
      !Allocate (w(n))
      !Allocate (ev(n, n))
      !Allocate (dynq(n, n, nqpt))
      !Allocate (dynp(n, n))
      !Allocate (dynr(n, n, ngridq(1)*ngridq(2)*ngridq(3)))
! set threshold for the eigenvector components
      eeps = 1E-15
! read in the dynamical matrices
      !Call readdyn (.true.,dynq)
! apply the acoustic sum rule
      !Call sumrule (dynq)
! Fourier transform the dynamical matrices to real-space
      !Call dynqtor (dynq, dynr)
      Open (50, File='PHONON_NEW.OUT', Action='WRITE', Form='FORMATTED')
      Do iq = 1, nppt
         !Call dynrtoq (vpl(:, iq), dynr, dynp)
         !Call dyndiag (dynp, w, ev)
         Write (50,*)
         Write (50, '(I6, 3G18.10, " : q-point, vpl")') iq, vpl(:, iq)
         Do j = 1, nmodes
            !if (twrev) Write (50,*)
            Write (50, '(I6, G18.10, " : mode, frequency")') j, w (j, iq)
            !Write (50, '(3I4, 2G18.10, " : species, atom, polarisation, eigenvector")')&
            !if (twrev) then
                !i = 0
                Do is = 1, nspecies
                   Do ia = 1, natoms (is)
                      ias = idxas(ia, is)
                      Do ip = 1, 3
                         Write (50, '(3I4, 2G18.10)') is, ia, ip, ev(ip, ias, j, iq)
                         !i = i + 1
                         !er = dreal(ev(i, j))
                         !if (dabs(er) .lt. eeps) then 
                         !    er = 0.E0
                         !end if
                         !ei = dimag(ev(i, j))
                         !if (dabs(ei) .lt. eeps) then 
                         !    ei = 0.E0
                         !end if
                         !ev(i, j) = cmplx(er, ei)
                         !If (i .Eq. 1) Then
                         !  & is, ia, ip, ev (i, j)
                         !Else
                         !End If
                      End Do
                   End Do
                End Do
            !end if
         End Do
         Write (50,*)
      End Do
      Close (50)
      Write (*,*)
      Write (*, '("Info(writephnnew): phonon frequencies (and eigenvectors) &
     &written to PHONON_NEW")')
      !Write (*, '(" for all q-vectors in the phwrite list")')
      Write (*,*)
      !Deallocate (w, ev, dynq, dynp, dynr)
      Return
End Subroutine writephnlistnew
