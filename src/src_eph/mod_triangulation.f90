module mod_triangulation
  implicit none

  type triangulation
    integer :: nv   ! number of vertices
    integer :: ne   ! number of edges
    integer :: nf   ! number of faces
    complex(8), allocatable :: verts(:)     ! vertices in the complex plane
    integer, allocatable    :: edges(:,:)   ! edges given by two vertices
    integer, allocatable    :: faces(:,:)   ! faces given by three vertices (in counter clockwise order)
    integer, allocatable    :: e2f(:,:)     ! two faces an edge belongs to
    integer, allocatable    :: f2e(:,:)     ! three edges forming a face
  end type triangulation

  contains

    subroutine triangulation_gen( self, verts, eps, shake)
      use m_getunit
      type( triangulation), intent( out) :: self
      complex(8), intent( in)            :: verts(:)
      real(8), optional, intent( in)     :: eps, shake

      integer :: i, nv, nf, ne, un
      real(8) :: e, s, r(2)
      complex(8) :: v1, v2, v3
      complex(8), allocatable :: cpy(:)
      integer, allocatable :: faces(:,:), tmp(:,:)

      e = 1.d-16
      if( present( eps)) e = eps
      ! when shake is provided the vertices are shakes randomly with this amplitude.
      ! This may cure troubles in the generation of the Delaunay triangulation.
      s = 0.d0
      if( present( shake)) s = shake

      nv = size( verts, 1)
      ! remove identical vertices
      allocate( cpy( nv))
      cpy = verts
      i = 1
      do while( i .lt. nv)
        if( minval( abs( cpy( 1:i) - cpy( i+1))) .lt. e) then
          cpy( i+1) = cpy( nv)
          nv = nv - 1
        else
          i = i + 1
        end if
      end do
      if( nv .lt. 3) then
        write(*,*)
        write(*,'("Error (gen_triangulation): Less than three different vertices given. No triangulation can be performed")')
        stop
      end if

      ! shake
      if( s .gt. e) then
        do i = 1, nv
          call random_number( r)
          r = s*(2*r - 1.d0)
          cpy( i) = cpy( i) + cmplx( r(1), r(2), 8)
        end do
      end if

      self%nv = nv
      if( allocated( self%verts)) deallocate( self%verts)
      allocate( self%verts( self%nv))
      self%verts = cpy( 1:self%nv)
      deallocate( cpy)

      ! generate triangulation
      allocate( faces( 3, 2*self%nv), tmp( 3, 2*self%nv))
      call dtris2( self%nv, self%verts, nf, faces, tmp)

      !  call getunit( un)
      !  open( un, file='CHECK.VRT', action='write', status='unknown', form='formatted')
      !  do i = 1, self%nv
      !    write( un, '(2g20.10e3)') self%verts( i)
      !  end do
      !  close( un)
      !  open( un, file='CHECK.EDG', action='write', status='unknown', form='formatted')
      !  do i = 1, nf
      !    write( un, '(2i)') faces( 1, i), faces( 2, i)
      !    write( un, '(2i)') faces( 2, i), faces( 3, i)
      !    write( un, '(2i)') faces( 3, i), faces( 1, i)
      !  end do
      !  close( un)

      self%nf = nf
      if( allocated( self%faces)) deallocate( self%faces)
      allocate( self%faces( 3, self%nf))
      self%faces = faces( :, 1:self%nf)
      deallocate( faces, tmp)

      ! find edges and build maps
      call triangulation_edges( self%verts, self%nv, self%faces, self%nf, self%edges, self%ne, self%e2f, self%f2e, eps=e)
            
      return
    end subroutine triangulation_gen

    subroutine triangulation_edges( verts, nv, faces, nf, edges, ne, e2f, f2e, eps)
      integer, intent( in)               :: nv, nf
      complex(8), intent( in)            :: verts( nv)
      integer, intent( in)               :: faces( 3, nf)
      real(8), optional, intent( in)     :: eps
      integer, intent( out)              :: ne
      integer, allocatable, intent( out) :: edges(:,:), e2f(:,:), f2e(:,:)
      
      integer :: i, j, k
      real(8) :: e
      complex(8) :: v1, v2, v3
      integer, allocatable :: etmp(:,:), e2ftmp(:,:)
      complex(8), allocatable :: cpy(:)

      e = 1.d-16
      if( present( eps)) e = eps

      allocate( etmp( 2, 3*nf))
      allocate( e2ftmp( 2, 3*nf))
      if( allocated( f2e)) deallocate( f2e)
      allocate( f2e( 3, nf))
      e2ftmp = 0
      f2e = 0
      allocate( cpy( 3*nf))
      cpy = cmplx( 1.d128, 1.d128, 8)
      ne = 0
      do i = 1, nf
        v1 = verts( faces( 1, i))
        v2 = verts( faces( 2, i))
        v3 = verts( faces( 3, i))

        if( i .eq. 1) then
          ne = ne + 1
          etmp( 1, ne) = faces( 2, i)
          etmp( 2, ne) = faces( 3, i)
          cpy( ne) = 0.5d0*(v2 + v3)
          e2ftmp( 1, ne) = i
          f2e( 1, i) = ne
          ne = ne + 1
          etmp( 1, ne) = faces( 3, i)
          etmp( 2, ne) = faces( 1, i)
          cpy( ne) = 0.5d0*(v3 + v1)
          e2ftmp( 1, ne) = i
          f2e( 2, i) = ne
          ne = ne + 1
          etmp( 1, ne) = faces( 1, i)
          etmp( 2, ne) = faces( 2, i)
          cpy( ne) = 0.5d0*(v1 + v2)
          e2ftmp( 1, ne) = i
          f2e( 3, i) = ne
          cycle
        end if

        j = minloc( abs( cpy( 1:ne) - 0.5d0*(v2 + v3)), 1)
        if( abs( cpy( j) - 0.5d0*(v2 + v3)) .gt. 0.5d0*e) then
          ne = ne + 1
          etmp( 1, ne) = faces( 2, i)
          etmp( 2, ne) = faces( 3, i)
          cpy( ne) = 0.5d0*(v2 + v3)
          k = max( e2ftmp( 1, ne), e2ftmp( 2, ne))
          e2ftmp( 1, ne) = max( i, k)
          e2ftmp( 2, ne) = min( i, k)
          f2e( 1, i) = ne
        else
          k = max( e2ftmp( 1, j), e2ftmp( 2, j))
          e2ftmp( 1, j) = max( i, k)
          e2ftmp( 2, j) = min( i, k)
          f2e( 1, i) = j
        end if

        j = minloc( abs( cpy( 1:ne) - 0.5d0*(v3 + v1)), 1)
        if( abs( cpy( j) - 0.5d0*(v3 + v1)) .gt. 0.5d0*e) then
          ne = ne + 1
          etmp( 1, ne) = faces( 3, i)
          etmp( 2, ne) = faces( 1, i)
          cpy( ne) = 0.5d0*(v3 + v1)
          k = max( e2ftmp( 1, ne), e2ftmp( 2, ne))
          e2ftmp( 1, ne) = max( i, k)
          e2ftmp( 2, ne) = min( i, k)
          f2e( 2, i) = ne
        else
          k = max( e2ftmp( 1, j), e2ftmp( 2, j))
          e2ftmp( 1, j) = max( i, k)
          e2ftmp( 2, j) = min( i, k)
          f2e( 2, i) = j
        end if

        j = minloc( abs( cpy( 1:ne) - 0.5d0*(v1 + v2)), 1)
        if( abs( cpy( j) - 0.5d0*(v1 + v2)) .gt. 0.5d0*e) then
          ne = ne + 1
          etmp( 1, ne) = faces( 1, i)
          etmp( 2, ne) = faces( 2, i)
          cpy( ne) = 0.5d0*(v1 + v2)
          k = max( e2ftmp( 1, ne), e2ftmp( 2, ne))
          e2ftmp( 1, ne) = max( i, k)
          e2ftmp( 2, ne) = min( i, k)
          f2e( 3, i) = ne
        else
          k = max( e2ftmp( 1, j), e2ftmp( 2, j))
          e2ftmp( 1, j) = max( i, k)
          e2ftmp( 2, j) = min( i, k)
          f2e( 3, i) = j
        end if
      end do
      deallocate( cpy)
      if( ne .ne. nv + nf - 1) then
        write(*,*)
        write(*,'("Warning (triangulation_edges): Triangles are not Delaunay!")')
        write(*,*) ne, nv + nf - 1
      end if
      if( allocated( edges)) deallocate( edges)
      allocate( edges( 2, ne))
      edges = etmp( :, 1:ne)
      if( allocated( e2f)) deallocate( e2f)
      allocate( e2f( 2, ne))
      e2f = e2ftmp( :, 1:ne)
      deallocate( etmp, e2ftmp)
      return
    end subroutine triangulation_edges

    subroutine triangulation_tofile( self, fname)
      use m_getunit
      type( triangulation) :: self
      character(*), intent( in) :: fname

      integer :: un, i

      call getunit( un)
      open( un, file=trim( fname)//'.VRT', action='write', status='unknown', form='formatted')
      do i = 1, self%nv
        write( un, '(2g20.10e3)') self%verts( i)
      end do
      close( un)
      open( un, file=trim( fname)//'.EDG', action='write', status='unknown', form='formatted')
      do i = 1, self%ne
        write( un, '(2i)') self%edges( :, i)
      end do
      close( un)
      
      return
    end subroutine triangulation_tofile

    subroutine triangulation_destroy( self)
      type( triangulation) :: self

      if( allocated( self%verts)) deallocate( self%verts)
      if( allocated( self%edges)) deallocate( self%edges)
      if( allocated( self%faces)) deallocate( self%faces)
      if( allocated( self%e2f))   deallocate( self%e2f)
      if( allocated( self%f2e))   deallocate( self%f2e)
    end subroutine triangulation_destroy
end module
