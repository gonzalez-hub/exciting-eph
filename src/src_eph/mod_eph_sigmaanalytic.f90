module mod_eph_sigmaanalytic
  use mod_eph_variables
  use mod_polygamma
  use mod_polylog

  implicit none

  integer, parameter :: factorial(0:10) = (/1, 1, 2, 6, 24, 125, 720, 5040, 40320, 362880, 3628800/)
  real(8), parameter :: zeta(10) = (/1.d200,                 1.644934066848226436d0, 1.202056903159594285d0, &
                                     1.082323233711138192d0, 1.036927755143369926d0, 1.017343061984449140d0, &
                                     1.008349277381922827d0, 1.004077356197944339d0, 1.002008392826082214d0, &
                                     1.000994575127818085d0/)

  contains

  subroutine sigmaein( z, etherm, freqs, nfreq, res)
    complex(8), intent( in)  :: z              ! complex frequency to evaluate at
    real(8), intent( in)     :: etherm         ! thermal energy
    integer, intent( in)     :: nfreq          ! # of frequencies
    real(8), intent( in)     :: freqs( nfreq)  ! real frequencies to evaluate at
    complex(8), intent( out) :: res( nfreq)    ! the integral

    integer :: iw
    real(8) :: nnuq

    real(8), external :: occ

    do iw = 1, nfreq
      nnuq = occ( freqs( iw)/etherm, -1)
      res( iw) = -zi*pi*(nnuq + 0.5d0) + 0.5d0*(polygamma( 0, 0.5d0 + zi*(freqs( iw) - z)/(twopi*etherm)) - &
                                                polygamma( 0, 0.5d0 - zi*(freqs( iw) + z)/(twopi*etherm)))
    end do
    return
  end subroutine sigmaein

  ! computes the n-th integral of analytically continued self-energy
  ! of a einstein mode w.r.t. to a real frequency
  ! \Sigma^{Ei}_{n+1}(z,w,T) = \int_0^w \Sigma^{Ei}_{n}(z,w',T) dw'
  subroutine sigmaeinint( n, z, etherm, freqs, nfreq, res)
    integer, intent( in)     :: n              ! order of the integral
    complex(8), intent( in)  :: z              ! complex frequency to evaluate at
    real(8), intent( in)     :: etherm         ! thermal energy
    integer, intent( in)     :: nfreq          ! # of frequencies
    real(8), intent( in)     :: freqs( nfreq)  ! real frequencies to evaluate at
    complex(8), intent( out) :: res( nfreq)    ! the integral

    integer :: iw, i
    complex(8) :: zindep( nfreq), r1, pit

    if( (n .lt. 1) .or. (n .gt. min( size( factorial)-1, size( zeta)))) then
      write(*,*)
      write(*,'("Error( sigmaeinint): Integral of Einstein self-energy not yet implemented for n = ",i)') n
      stop
    end if

    ! the z-independent part of the integral
    zindep = zzero
    do iw = 1, nfreq
      r1 = freqs( iw)**n/(2.d0*dble( factorial( n)))
      do i = 2, n
        r1 = r1 - zeta( i)/dble( factorial( n-i))*etherm**i*freqs( iw)**(n-i)
      end do
      r1 = r1 + etherm**n*polylog( n, cmplx( exp( freqs( iw)/etherm), 0.d0, 8))
      zindep( iw) = pi*r1
    end do
    res = zi*zindep

    ! the z-dependent part of the integral
    pit = pi*etherm
    do iw = 1, nfreq
      res( iw) = res( iw) + 0.5d0*(2.d0*zi*pit)**n* &
          ((-1.d0)**n*polygamma( -n, 0.5d0 + zi*(freqs( iw) - z)/(2.d0*pit)) - &
                      polygamma( -n, 0.5d0 - zi*(freqs( iw) + z)/(2.d0*pit)))
      do i = 0, (n-1)/2
        res( iw) = res( iw) + 2.d0*zi*sigmaeinintcoeff( n, i)*pit**(2*i+1)*freqs( iw)**(n-(2*i+1))* &
            polygamma( -(2*i+1), 0.5d0 - zi*z/(2.d0*pit))
      end do
      !write(*,'(i,f13.6,2g26.16)') iw, freqs( iw), res( iw)
    end do

    return
  end subroutine sigmaeinint

  ! computes the integral of the Eliashberg function times the einstein mode self-energy
  ! \Sigma(z,T) = \int_0^\infty \alpha^2 F(w) \Simga^{Ei}(z,w,T) dw
  ! for numerical reasons, what is realy calculated is
  ! \Sigma(z,T) = \int_0^\infty \frac{\partial^2\alpha^2 F(w)}{\partial w^2} \Simga^{Ei}_{2}(z,w,T) dw
  subroutine sigmaeineliashbergint( z, etherm, freqs, nfreq, a2f, res)
    use m_plotmat
    complex(8), intent( in)  :: z              ! complex frequency to evaluate at
    real(8), intent( in)     :: etherm         ! thermal energy
    integer, intent( in)     :: nfreq          ! # of frequencies
    real(8), intent( inout)  :: freqs( nfreq)  ! real frequencies to integrate over
    real(8), intent( inout)  :: a2f( nfreq)    ! Eliashberg function at frequencies
    complex(8), intent( out) :: res            ! the integral

    integer :: iw
    real(8) :: d2a2f( nfreq), f( nfreq), g( nfreq, 5), cf( 3, nfreq), fac, r1
    complex(8) :: c, ein( nfreq), einint1( nfreq), einint2( nfreq), einint3( nfreq), einint4( nfreq)

    real(8), external :: occ

    fac = 1.d0!1.d3*h2ev
    freqs = freqs*fac
    a2f = a2f/fac

    do iw = 1, nfreq
      r1 = occ( freqs( iw)/etherm, -1)
      g( iw, 1) = a2f( iw)*(r1 + 0.5d0)
    end do
    call fderiv( -1, nfreq, freqs, g(:,1), f, cf)
    c = -zi*pi*f( nfreq)
    !call savitzkygolay( nfreq, freqs, a2f, 1, 6, 0, g(:,5))
    call writematlab( reshape( cmplx( a2f, 0.d0, 8), (/nfreq, 1/)), 'eliash')
    !call dwtderiv( nfreq, freqs*r1, a2f/r1, 1, g(:,1))
    !call dwtderiv( nfreq, freqs*r1, a2f/r1, 2, g(:,2))
    call dwtderiv( nfreq, freqs, a2f, 3, g(:,1))
    call fderiv( -1, nfreq, freqs, g(:,1), g(:,2), cf)
    g(:,2) = g(:,2) - g( nfreq, 2)
    call dwtderiv( nfreq, freqs, g(:,1), 3, g(:,3))
    call fderiv( -1, nfreq, freqs, g(:,3), g(:,4), cf)
    g(:,4) = g(:,4) - g( nfreq, 4)
    call fderiv( -1, nfreq, freqs, g(:,4), g(:,5), cf)
    g(:,5) = g(:,5) - g( nfreq, 5)
    !call dwtderiv( nfreq, freqs*r1, a2f/r1, 4, g(:,4))
    !call dwtderiv( nfreq, freqs*r1, a2f/r1, 5, g(:,5))
    call sigmaein(       z*fac, etherm*fac, freqs, nfreq, ein)
    call sigmaeinint( 1, z*fac, etherm*fac, freqs, nfreq, einint1)
    call sigmaeinint( 2, z*fac, etherm*fac, freqs, nfreq, einint2)
    !do iw = 1, nfreq
    !  write(*,'(i,f13.6,100g26.16)') iw, freqs( iw), a2f( iw), einint2( iw), g( iw, 3)*einint1( iw)!, d2a2f( iw)*einint1( iw), einint2( iw)
    !end do

    call fderiv( -1, nfreq, freqs, a2f*dble( ein), f, cf)
    r1 = f( nfreq)
    call fderiv( -1, nfreq, freqs, a2f*aimag( ein), f, cf)
    res = cmplx( r1, f( nfreq), 8) - c
    return
    !write(*,'(2g26.16)') res
    !stop

    d2a2f = g(:,1)
    call fderiv( -1, nfreq, freqs, d2a2f*dble( einint1), f, cf)
    r1 = f( nfreq)
    call fderiv( -1, nfreq, freqs, d2a2f*aimag( einint1), f, cf)
    res = cmplx( r1, f( nfreq), 8)
    !write(*,'(2g26.16)') -res
    !stop

    d2a2f = g(:,3)
    call fderiv( -1, nfreq, freqs, d2a2f*dble( einint2), f, cf)
    r1 = f( nfreq)
    call fderiv( -1, nfreq, freqs, d2a2f*aimag( einint2), f, cf)
    res = cmplx( r1, f( nfreq), 8)
    !write(*,'(2g26.16)') res
    !stop
    !return

    ! get the second derivative of the Eliashberg function
    !call fderiv( 1, nfreq, freqs, a2f, d2a2f, cf)

    ! get the 3rd and 4th Einstein self-energy integral
    !call sigmaeinint( 2, z*fac, etherm*fac, freqs, nfreq, einint3)
    !call sigmaeinint( 3, z*fac, etherm*fac, freqs, nfreq, einint4)

    !call sigmaeinint( 2, z, etherm, freqs, nfreq, einint2)
    !call fderiv( -1, nfreq, freqs, d2a2f*dble( einint2), f, cf)
    !r1 = f( nfreq)
    !call fderiv( -1, nfreq, freqs, d2a2f*aimag( einint2), f, cf)
    !res = cmplx( r1, f( nfreq), 8)
    !return

    ! do the integral
    !res = zzero
    !do iw = 1, nfreq-1
    !  !write(*,'(i,f13.6,g26.16)') iw, freqs( iw), d2a2f( iw)
    !  r1 = (d2a2f( iw+1) - d2a2f( iw))/(freqs( iw+1) - freqs( iw))
    !  res = res + d2a2f( iw+1)*einint3( iw+1) - d2a2f( iw)*einint3( iw) - r1*(einint4( iw+1) - einint4( iw))
    !end do
    !write(*,'(2g26.16)') res
    !write(*,'(i,f13.6,g26.16)') iw, freqs( iw), d2a2f( iw)

    return
  end subroutine sigmaeineliashbergint

  recursive function sigmaeinintcoeff( n, k) result( c)
    integer, intent( in) :: n, k
    real(8)              :: c

    if( (n .lt. 1) .or. (k .lt. 0) .or. (2*k .ge. n)) then
      c = 0.d0
      return
    end if

    if( (n .eq. 1) .and. (k .eq. 0)) then
      c = 1.d0
      return
    end if

    if( (n-1 .eq. 2*k)) then
      c = -4.d0*sigmaeinintcoeff( n-1, k-1)
    else
      c = sigmaeinintcoeff( n-1, k)/dble( n-1-2*k)
    end if
    return
  end function sigmaeinintcoeff

  subroutine anado
    integer :: nfreq, iw
    real(8) :: etherm, dw
    complex(8) :: z, res

    integer :: i
    real(8) :: r
    real(8), allocatable :: freqs(:), a2f(:), rf(:,:)
    complex(8), allocatable :: einint(:), cf(:,:)

    do i = 3, 5
    nfreq = 10**i
    etherm = 1.d0
    z = cmplx( 1.d0, 0.d0, 8)

    if( allocated( freqs)) deallocate( freqs)
    if( allocated( einint)) deallocate( einint)
    if( allocated( a2f)) deallocate( a2f)
    allocate( freqs( nfreq), einint( nfreq), a2f( nfreq))
    allocate( cf( nfreq, 4), rf( nfreq, 0:4))
    freqs = 1.d-15

    dw = 20.d0/(nfreq - 1)
    a2f( 1) = exp( -0.1d0*(freqs( 1)-2.d0)**2) + (0.05*freqs( 1))**2 + 0.01*(2.d0*r - 1.d0)
    rf( 1, 0) = -0.2d0*exp( -0.1d0*(freqs( 1)-2.d0)**2)*(freqs( 1) - 2.d0) + 0.005d0*freqs( 1)
    do iw = 2, nfreq
      freqs( iw) = freqs( iw-1) + dw
      call random_number( r)
      a2f( iw) = exp( -0.1d0*(freqs( iw)-2.d0)**2) + (0.05*freqs( iw))**2 + 0.01*(2.d0*r - 1.d0)
      rf( iw, 0) = -0.2d0*exp( -0.1d0*(freqs( iw)-2.d0)**2)*(freqs( iw) - 2.d0) + 0.005d0*freqs( iw)
      !write(*,'(i,f13.6,g26.16)') iw, freqs( iw), a2f( iw)
    end do
    cf( :, 1) = cmplx( a2f, 0.d0, 8)
    call savitzkygolay( nfreq, freqs, a2f, 1, nfreq/100, 0, rf(:,0))
    call dwtderiv( nfreq, freqs, a2f, 6, rf(:,1))
    call dwtderiv( nfreq, freqs, rf(:,0), 6, rf(:,2))
    !call fourierderiv( nfreq, freqs, cf(:,1), 1, cf(:,2))
    !call fourierderiv( nfreq, freqs, cf(:,1), 2, cf(:,3))
    !call fourierderiv( nfreq, freqs, cf(:,1), 3, cf(:,4))
    do iw = 1, nfreq
      write(*,'(i,f13.6,100g26.16)') iw, freqs( iw), a2f( iw), rf(iw, :)!dble( cf( iw, 1)), cf( iw, 2:4)
    end do
    stop

    !call sigmaeinint( 1, z, etherm, freqs, nfreq, einint)
    call sigmaeineliashbergint( z, etherm, freqs, nfreq, a2f, res)
    write(*,'(i,2g26.16)') nfreq, res
    end do

  end subroutine anado

end module mod_eph_sigmaanalytic
