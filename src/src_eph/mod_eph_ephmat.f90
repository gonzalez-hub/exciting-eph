module mod_eph_ephmat
  use mod_eph_variables
  use mod_atoms

  implicit none

! methods
  contains

    subroutine eph_ephmat_gen_lr( qset, qpset, iqp2iq, phfreq, phevec, epsinf, zstar, umnqp, umnp, pref, ephmat, firstcall)
      use mod_lattice, only: omega
      type( k_set), intent( in)      :: qset, qpset
      integer, intent( in)           :: iqp2iq( qpset%nkpt)
      real(8), intent( in)           :: phfreq( eph_nmode, qset%nkpt)
      complex(8), intent( in)        :: phevec( 3, natmtot, eph_nmode, qset%nkpt)
      real(8), intent( in)           :: epsinf(3,3)
      real(8), intent( in)           :: zstar( 3, 3, natmtot)
      complex(8), intent( in)        :: umnqp( eph_fst:eph_lst, eph_fst:eph_lst, qpset%nkpt)
      complex(8), intent( in)        :: umnp( eph_fst:eph_lst, eph_fst:eph_lst)
      complex(8), intent( in)        :: pref
      complex(8), intent( inout)     :: ephmat( eph_fst:eph_lst, eph_fst:eph_lst, eph_nmode, qpset%nkpt)
      logical, optional, intent( in) :: firstcall
      
      real(8), parameter :: gmax = 37.d0 ! exp(-37) ~ 1e-16
      real(8), parameter :: alpha = 2.d0

      complex(8), allocatable, save :: prefac(:,:)
      logical, save                 :: first = .true.

      integer :: ia, is, ias, iq, iqp, nu, g1, g2, g3, ng(3), ig(3), numg
      real(8) :: t1, t2, m(3,3), vql(3), vgql(3), vgqc(3), nvgqc(3), lgqc, gqegq
      complex(8) :: z1, ze( 3, natmtot), auxmat( eph_fst:eph_lst, eph_fst:eph_lst)

      if( present( firstcall)) first = firstcall

      ! initialize the prefactors (p-independent)
      if( first) then

        m = matmul( transpose( qset%bvec), matmul( epsinf, qset%bvec))
        ng(1) = ceiling( sqrt( gmax*4.d0*alpha**2/m(1,1)))
        ng(2) = ceiling( sqrt( gmax*4.d0*alpha**2/m(2,2)))
        ng(3) = ceiling( sqrt( gmax*4.d0*alpha**2/m(3,3)))
        write(*,*) ng

        if( allocated( prefac)) deallocate( prefac)
        allocate( prefac( eph_nmode, qset%nkpt))
        prefac = zzero
!#ifdef USEOMP
!!$omp parallel default( shared) private( iq, vql, ig, g1, vgqc, lqp, idx, nu, ze, is, t1, ia, ias, lgqc, nvgqc, gqegq, z1)
!!$omp do
!#endif
        do iq = 1, qset%nkpt
          vql = qset%vkl(:,iq)
          call r3ws( input%structure%epslat, qset%bvec, vql, ig)
          do nu = 1, eph_nmode
            ! effective charge times eigenvector times prefactor
            ze = zzero
            if( abs( phfreq( nu, iq)) .gt. 1.d-5) then
              do is = 1, nspecies
                t1 = 1.d0/sqrt( 2.d0*spmass( is)*abs( phfreq( nu, iq)))
                do ia = 1, natoms( is)
                  ias = idxas( ia, is)
                  ze( :, ias) = t1*(zstar( :, 1, ias)*conjg(phevec( 1, ias, nu, iq)) + &
                                    zstar( :, 2, ias)*conjg(phevec( 2, ias, nu, iq)) + &
                                    zstar( :, 3, ias)*conjg(phevec( 3, ias, nu, iq)))
                  !write(*,'(3i,g16.6,2("(",3(2g16.6,3x),")",3x))') nu, iq, ias, eph_phfreq( nu, iq), eph_phevec( :, ias, nu, iq)/sqrt( spmass( is)), ze( :, ias)
                end do
              end do
            end if
            if( nu .eq. eph_nmode) then
              !write(*,'(i,2(6f13.6,3x),f13.6)') iq, phevec(:,1,nu,iq), phevec(:,2,nu,iq), &
              !  fourpi**3*(1.d0/omega*zstar(1,1,1)/epsinf(1,1))**2/(2.d0*abs(phfreq(nu,iq)))* &
              !  !abs( phevec(2,1,nu,iq)/sqrt(spmass(1)) - phevec(2,2,nu,iq)/sqrt(spmass(2)))**2
              !  abs( 0.d0/sqrt(spmass(1)) - 1.d0/sqrt(spmass(2)))**2
            end if

            do g1 = -ng(1), ng(1)
              do g2 = -ng(2), ng(2)
                do g3 = -ng(3), ng(3)
                  call r3mv( qset%bvec, vql+dble((/g1,g2,g3/)), vgqc)
                  lgqc = norm2( vgqc)
                  if( lgqc .lt. 1.d-16) cycle
                  nvgqc = vgqc/lgqc
                  gqegq = nvgqc(1)*(epsinf(1,1)*nvgqc(1) + epsinf(1,2)*nvgqc(2) + epsinf(1,3)*nvgqc(3)) + &
                          nvgqc(2)*(epsinf(2,1)*nvgqc(1) + epsinf(2,2)*nvgqc(2) + epsinf(2,3)*nvgqc(3)) + &
                          nvgqc(3)*(epsinf(3,1)*nvgqc(1) + epsinf(3,2)*nvgqc(2) + epsinf(3,3)*nvgqc(3))
                  if( lgqc**2*abs(gqegq) < 1.d-16 .or. lgqc**2*abs(gqegq)/(4.d0*alpha**2) > gmax) cycle
                  t1 = exp( -lgqc**2*abs(gqegq)/(4.d0*alpha**2))/(lgqc*gqegq)
                  !t1 = 1.d0/(lgqc*gqegq)
                  do is = 1, nspecies
                    do ia = 1, natoms( is)
                      ias = idxas( ia, is)
                      z1 = nvgqc(1)*ze( 1, ias) + nvgqc(2)*ze( 2, ias) + nvgqc(3)*ze( 3, ias)
                      t2 = -dot_product( vgqc, atposc( :, ia, is))
                      z1 = z1*cmplx( cos( t2), sin( t2), 8)
                      prefac( nu, iq) = prefac( nu, iq) + z1*t1
                    end do
                  end do
                end do
              end do
            end do

          end do
        end do
!#ifdef USEOMP
!!$omp end do
!!$omp end parallel
!#endif
        prefac = prefac*zi*fourpi/omega
        first = .false.
        do nu = 1, eph_nmode
          !write(*,'(i,2f26.16)') nu, maxval( dble( prefac( nu, :)), 1), maxval( aimag( prefac( nu, :)), 1)
          !do ias = 1, natmtot
          !  write(*,'(i,3(2f13.6))') ias, phevec( :, ias, nu, 1)
          !end do
        end do
      end if

      ! generate long-range coupling matrices
#ifdef USEOMP
!$omp parallel default( shared) private( iqp, iq, auxmat, nu)
!$omp do
#endif
      do iqp = 1, qpset%nkpt
        iq = iqp2iq( iqp)
        call zgemm( 'c', 'n', eph_nst, eph_nst, eph_nst, zone, &
               umnqp( :, :, iqp), eph_nst, &
               umnp, eph_nst, zzero, &
               auxmat, eph_nst)
        do nu = 1, eph_nmode
          ! with plane-wave matrix-elements
          !ephmat( :, :, nu, iqp) = ephmat( :, :, nu, iqp) + pref*prefac( nu, iq)*auxmat
          ! without plane-wave matrix-elements
          do is = eph_fst, eph_lst
            ephmat( is, is, nu, iqp) = ephmat( is, is, nu, iqp) + pref*prefac( nu, iq)
          end do
        end do
      end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
      return
    end subroutine eph_ephmat_gen_lr

    subroutine eph_ephmat_output( vpl, fst)
      use mod_wannier_interpolate, only: wfint_transform, wfint_eval, wfint_init
      use mod_eph_phonons
      use m_getunit
      use modmpi
      real(8), intent( in) :: vpl(3)
      integer, intent( in) :: fst

      real(8), parameter :: epsd = 1.d-6

      integer :: iq, ist, jst, ndeg, imode, un
      real(8) :: vpc(3), s( eph_fst:eph_lst), evalp( eph_fst:eph_lst), g
      complex(8) :: umnp( eph_fst:eph_lst, eph_fst:eph_lst)
      type( k_set) :: qpset

      integer, allocatable :: iqp2iq(:)
      real(8), allocatable :: phfreq(:,:)
      complex(8), allocatable :: phevec(:,:,:,:), ephmat(:,:,:,:)

      if( mpiglobal%rank .ne. 0) return

      if( eph_polar) then
        call r3mv( eph_pset%bvec, vpl, vpc)
        call generate_k_vectors( qpset, eph_pset%bvec, (/1, 1, 1/), dble((/0, 0, 0/)), .true., .false.)
        qpset%vkl(:,1) = vpl
        qpset%vkc(:,1) = vpc
        call wfint_init( qpset)
        umnp = wfint_transform(:,:,1)
        evalp = wfint_eval(:,1)-eph_efermi
        call delete_k_vectors( qpset)
      end if

      qpset = eph_pset
      allocate( iqp2iq( qpset%nkpt))
      do iq = 1, qpset%nkpt
        qpset%vkl(:,iq) = qpset%vkl(:,iq) + vpl
        qpset%vkc(:,iq) = qpset%vkc(:,iq) + vpc
        iqp2iq(iq) = iq
      end do
      allocate( ephmat( eph_fst:eph_lst, eph_fst:eph_lst, eph_nmode, qpset%nkpt))
      ephmat = zzero

      allocate( phfreq( eph_nmode, eph_pset%nkpt))
      allocate( phevec( 3, natmtot, eph_nmode, eph_pset%nkpt))
      call eph_phonons_interpolate( eph_kset_ph, eph_pset, phfreq, phevec, epsinf=eph_epsinf, zstar=eph_zstar)
      if( eph_polar) then
        call wfint_init( qpset)
        call eph_ephmat_gen_lr( eph_pset, qpset, iqp2iq, phfreq, phevec, &
                                eph_epsinf, eph_zstar, wfint_transform, umnp, zone, ephmat, &
                                firstcall=.true.)
      end if
      deallocate( iqp2iq, phevec)

      write(*,'(3f13.6,i,g20.10)') vpl, fst, evalp( fst)
      call getunit( un)
      open( un, file='EPH_EPHMAT.DAT', action='write', form='formatted')
      do imode = 1, eph_nmode
        do iq = 1, eph_pset%nkpt
          if( allocated( eph_pset_dpp1d)) then
            write( un, '(2g20.10e3)', advance='no') eph_pset_dpp1d( iq), phfreq( imode, iq)
          else
            write( un, '("#",3f26.16)') eph_pset%vkl( :, iq)
          end if
          ! degenerate states at k
          ndeg = 0; s = 0.d0
          do ist = eph_fst, eph_lst
            if( abs( evalp( ist) - evalp( fst)) .gt. epsd) cycle
            ndeg = ndeg + 1
            s = s + abs( ephmat( :, ist+ndeg, imode, iq))**2
          end do
          s = s/ndeg
          ! degenerate states at k+q
          ist = eph_fst
          do while( ist .lt. eph_lst)
            ndeg = 0; g = 0.d0
            do while( ist + ndeg .le. eph_lst)
              if( abs( wfint_eval( ist+ndeg, iq) - wfint_eval( ist, iq)) .gt. epsd) exit
              ndeg = ndeg + 1
              g = g + s( ist+ndeg)
            end do
            g = g/ndeg
            do jst = 1, ndeg
              !write( un, '(g20.10e3)', advance='no') abs( ephmat( ist, ist, imode, iq))
              !write( un, '(g20.10e3)', advance='no') sqrt( g)
              write( un, '(g20.10e3)', advance='no') fourpi*norm2( eph_pset%vkc(:,iq))**2*g
            end do
            ist = ist + ndeg
          end do
          write( un, *)
        end do
        write( un, *)
        write( un, *)
      end do
      close( un)

      deallocate( ephmat, phfreq)
      return        
    end subroutine eph_ephmat_output

    subroutine eph_ephmat_ephmatrix(elset, phset, ephmat_hkq)
      use modinput
      use mod_ephmat
      use mod_kpointset
      use mod_muffin_tin, only: lmmaxvr, nrmtmax, nrmt, rcmt
      use mod_eigensystem, only: nmatmax_ptr
      use mod_eigenvalue_occupancy, only: nstfv
      use mod_spin, only: nspnfv
      use mod_Gvector, only: ngrtot
      use mod_eph_phonons, only: eph_phonons_interpolate
      use mod_wannier_helper, only: wfhelp_getevec, wfhelp_geteval
      use mod_wannier_variables, only: wf_fst, wf_lst, wf_nst
      use mod_potential_and_density, only: veffmt
      ! Based on subroutines genephmat.f90 and mod_pwmat.f90
      ! Computes the electron-phonon matrix elements (from the potential response either with DFPT or SC)
      ! in the coarse grids (original input grids)
      type( k_set), intent( in)    :: elset, phset
      complex(8), intent( out)     :: ephmat_hkq( wf_fst:wf_lst, wf_fst:wf_lst, eph_nmode, elset%nkpt, phset%nkptnr)

      !  Auxiliary variables
      integer    :: ik, ikq, im, jn, iq, iq_irr, fst, lst, is, ia, ias, ip, ir, irc, js, ja, jas
      integer    :: isym, isym_q, phmode, un, i 
      real(8)    :: vpkql(3), t1
      complex(8) :: zt1
 
      ! Electron variables in coarse k-grid 
      real(8), allocatable    :: evalfv_kset(:,:)
      complex(8), allocatable :: evecfv_ini(:,:,:), evecfv_k(:,:,:), evecfv_kq(:,:,:)

      ! Potential response variables
      complex(8), allocatable :: zfmt(:,:,:)
      complex(8), allocatable :: zfmt_rot(:,:,:)
      complex(8), allocatable :: zfmt_mode(:,:,:,:)
      complex(8), allocatable :: zfmt_aux(:,:,:,:)
      complex(8), allocatable :: gzfmt (:,:,:,:)

      complex(8), allocatable :: zfir(:)
      complex(8), allocatable :: zfir_rot(:)
      complex(8), allocatable :: zfir_aux(:,:)
      complex(8), allocatable :: zfir_mode(:,:)

      ! E-ph matrix elements in coarse grids (k,q)  
      complex(8), allocatable :: ephmat_aux(:,:,:)
      !complex(8), allocatable :: ephmat_atom(:,:,:,:,:)

      ! Auxiliary variables for output
      logical                 :: exist
      real(8), allocatable    :: phfreq_interp(:,:)
      complex(8), allocatable :: phevec_interp(:,:,:,:)
      complex(8), allocatable :: G(:,:)
 
      ! Allocate phonon variables in full BZ
      if( allocated(phevec_interp)) deallocate(phevec_interp)
      allocate(phevec_interp( 3, natmtot, eph_nmode, phset%nkptnr))
      if( allocated(phfreq_interp)) deallocate(phfreq_interp)
      allocate(phfreq_interp(eph_nmode, phset%nkptnr))

      ! Interpolation of phonon frequencies and phonon eigenvectors in full BZ
      if (eph_polar) then
        Call eph_phonons_interpolate( phset, eph_kset_ph_aux, phfreq_interp, phevec_interp, epsinf=eph_epsinf, zstar=eph_zstar)
      else
        Call eph_phonons_interpolate( phset, eph_kset_ph_aux, phfreq_interp, phevec_interp)
      end if

      ! Get the electron eigenvalues
      Call wfhelp_geteval(evalfv_kset, fst, lst)
      
      ! Read the matrix elements from file if it exists
      inquire( file="EPH_MATRIX.OUT", exist=exist)
      if( exist) then
        Call eph_ephmat_getephmat(el_set=elset, ph_set=eph_kset_ph_aux, &
                                fst_n=wf_fst, lst_n=wf_lst, &
                                fst_m=wf_fst, lst_m=wf_lst, &
                                ephmat=ephmat_hkq)
        ! Write the averaged matrix elements g(k,q) (coarse grid) to file
        Call eph_ephmat_print2file(el_set=elset, ph_set=eph_kset_ph_aux, kqset=elset, &
                                fst_n=wf_fst, lst_n=wf_lst, evalk=evalfv_kset(wf_fst:wf_lst,:), &
                                fst_m=wf_fst, lst_m=wf_lst, evalkq=evalfv_kset(wf_fst:wf_lst,:), &
                                phfreq=phfreq_interp, ephmat=ephmat_hkq, file_name='EPH_MATRIX_KQ.OUT', type_print=.True.) 
        return  
      end if

      ! Initialization of mod_ephmat
      Call ephmat_init( apwlmax=input%groundstate%lmaxapw, & 
                        explmax=input%groundstate%lmaxvr,  &
                        kset=elset, fst_kq=wf_fst, lst_kq=wf_lst, fst_k=wf_fst, lst_k=wf_lst)

      ! Write muffin-tin eigenvectors to file for quick access later
      if( allocated(evecfv_ini)) deallocate(evecfv_ini)
      allocate(evecfv_ini(nmatmax_ptr, nstfv, nspnfv))
      do ik = 1, elset%nkpt
        ! Get the eigenvectors for all k-points with Wannier phase
        Call wfhelp_getevec(ik, evecfv_ini)
        Call ephmat_prepare(ik, evecfv_ini)
      end do

      ! Allocate electron variables
      if( allocated(evecfv_k)) deallocate(evecfv_k)
      allocate(evecfv_k(nmatmax_ptr, nstfv, nspnfv)) 

      if( allocated(evecfv_kq)) deallocate(evecfv_kq)
      allocate(evecfv_kq(nmatmax_ptr, nstfv, nspnfv)) 

      ! Allocate potential variables
      ! MT variables 
      allocate(gzfmt(lmmaxvr, nrmtmax, 3, natmtot))
      if( allocated(zfmt)) deallocate(zfmt)
      allocate(zfmt(lmmaxvr, nrmtmax, natmtot))
      if( allocated(zfmt_rot)) deallocate(zfmt_rot)
      allocate(zfmt_rot(lmmaxvr, nrmtmax, natmtot))
      if( allocated(zfmt_mode)) deallocate(zfmt_mode)
      allocate(zfmt_mode(lmmaxvr, nrmtmax, natmtot, 3*natmtot))
      if( allocated(zfmt_aux)) deallocate(zfmt_aux)
      allocate(zfmt_aux(lmmaxvr, nrmtmax, natmtot, 3*natmtot))

      ! Int variables
      if( allocated(zfir)) deallocate(zfir)
      allocate(zfir(ngrtot))
      if( allocated(zfir_rot)) deallocate(zfir_rot)
      allocate(zfir_rot(ngrtot))
      if( allocated(zfir_mode)) deallocate(zfir_mode)
      allocate(zfir_mode(ngrtot, 3*natmtot))
      if( allocated(zfir_aux)) deallocate(zfir_aux)
      allocate(zfir_aux(ngrtot, 3*natmtot))

      ! Read the density and potential from file
      Call readstate

      ! Compute the gradient of the non-displaced KS potential 
      do is = 1, nspecies
        do ia = 1, natoms(is)
          ias = idxas(ia, is)
          irc = 0
          do ir = 1, nrmt(is), input%groundstate%lradstep
            irc = irc + 1
            Call rtozflm( input%groundstate%lmaxvr, & 
                        veffmt(:, ir, ias), zfmt(:, irc, ias))
          end do 
          Call gradzfmt( input%groundstate%lmaxvr, &
                        nrmt(is), rcmt(:,is), lmmaxvr, nrmtmax, &
                        zfmt(:,:,ias), gzfmt(:,:,:,ias))
        end do
      end do

      ! Auxilary matrix for e-ph elements
      if( allocated(ephmat_aux)) deallocate(ephmat_aux)
      allocate(ephmat_aux( wf_fst:wf_lst, wf_fst:wf_lst, 1)) 

      ephmat_hkq = zzero

      ! Loop over q-points
      do iq = 1, phset%nkptnr
        zfir_aux = zzero
        zfir_mode = zzero
        zfmt_mode = zzero
        zfmt_aux = zzero
        ! Get equivalent irreducible q-point from original coarse grid 
        ! and the symmetry that relates Sq with q
        Call findkptinset(phset%vklnr(:, iq), phset, isym_q, iq_irr)
        ! Generate the Gamma rotation matrix
        Call eph_ephmat_gamma(isym= isym_q, vpl=phset%vkl(:, iq_irr), G=G)
        do is = 1, nspecies
          do ia = 1, natoms(is)
            ias = idxas(ia, is)
            do ip = 1, 3
              i = 3*(ias - 1) + ip
              zfmt = zzero 
              zfir = zzero
              ! Read potential response for atoms and directions (x,y,z)
              Call readdveff(iq_irr, is, ia, ip, zfmt, zfir)
              zfir_aux(:, i) = zfir
              ! Substract the gradient 
              zfmt(:, :, ias) =  zfmt(:, :, ias) - gzfmt(:, :, ip, ias)
              zfmt_aux(:,:,:, i) = zfmt
            end do
          end do
        end do
        ! Potential times the Gamma matrix
        Call zgemm( 'n', 'c' , ngrtot, 3*natmtot, 3*natmtot, zone, &
                    zfir_aux, ngrtot, &
                    G, 3*natmtot, zzero, &
                    zfir_mode, ngrtot)
        Call zgemm( 'n', 'c' , lmmaxvr*nrmtmax*natmtot, 3*natmtot, 3*natmtot, zone, &
                    zfmt_aux, lmmaxvr*nrmtmax*natmtot, &
                    G, 3*natmtot, zzero, &
                    zfmt_mode, lmmaxvr*nrmtmax*natmtot)
        write(*,'(3i, 3f13.6, 3f13.6)') iq, iq_irr, isym_q, phset%vklnr(:, iq), phset%vkl(:, iq_irr)
        do phmode = 1, eph_nmode
          zfmt = zzero
          zfmt_rot = zzero
          zfir = zzero
          zfir_rot = zzero
          ephmat_aux = zzero
          do is = 1, nspecies
            ! Prefactor: phonon frequencies times atomic mass
            if ( phfreq_interp(phmode, iq) < 1.d-4) then
              t1 = 0.d0
            else
              t1 = 1.d0/dsqrt(2.d0*phfreq_interp(phmode, iq)*spmass(is))
            end if
            do ia = 1, natoms(is)
              ias = idxas(ia, is)
              do ip = 1, 3
                i = 3*(ias - 1) + ip
                ! Prefactor times phonon eigenvector
                zt1 = t1 * conjg(phevec_interp( ip, ias, phmode, iq))
                ! Prefactor times potential response
                do js = 1, nspecies
                  do ja = 1, natoms(js)
                    jas = idxas(ja, js)
                    zfmt(:,:, jas) = zfmt(:,:, jas) + zt1*zfmt_mode(:,:, jas, i)
                  end do
                end do
                zfir = zfir + zt1*zfir_mode(:, i)
              end do ! ip
            end do ! ia
          end do ! is
          ! Rotate the potential response
          Call eph_ephmat_rotate_potcoeff(isym=isym_q, vecq_irr=phset%vkl(:, iq_irr), &
          mt_pot_coeff=zfmt, int_pot_coeff=zfir, mt_pot_coeff_rot=zfmt_rot, int_pot_coeff_rot=zfir_rot)
          !----------------------------------------------------------!
          ! Calculation of  < \Psi_m,k+q |\Delta V_{n,q}| \Psi_n,k > !
          !----------------------------------------------------------!
          Call ephmat_init_qg( vecql_=phset%vklnr(:, iq), &
                               vecgl_=(/0, 0, 0/), ng_=1, & 
                               ephdveffmt=zfmt_rot)
          ! Loop over k-points
          do ik = 1, elset%nkpt
            ! Get the eigenvectors for k-point k
             Call wfhelp_getevec(ik, evecfv_k) 

            ! Get the eigenvectors for k-point k+q
            vpkql(:) = phset%vklnr(:, iq) + elset%vkl(:, ik)

            Call findkptinset( vpkql, elset, isym, ikq) 
            Call wfhelp_getevec(ikq, evecfv_kq)

            ! Compute the matrix elements
            Call ephmat_genephmat( ik=ik, & 
              evec_kq=evecfv_kq(:,wf_fst:wf_lst,1), & 
              evec_k=evecfv_k(:,wf_fst:wf_lst,1), &
              ephdveffir=zfir_rot, &
              ephmat=ephmat_aux)
            ! Extract g
            ephmat_hkq(:,:, phmode, ik, iq) = ephmat_aux(:,:,1)
          end do ! ik
        end do ! phmode loop 
      end do ! iq loop

      ! Write to binary file complex matrix elements g(k,q) (coarse grid) 
      Call eph_ephmat_putephmat(el_set=elset, ph_set=eph_kset_ph_aux, & 
                              fst_n=wf_fst, lst_n=wf_lst, &
                              fst_m=wf_fst, lst_m=wf_lst, &
                              ephmat=ephmat_hkq)

      ! Write the averaged matrix elements g(k,q) (coarse grid) to file
      Call eph_ephmat_print2file(el_set=elset, ph_set=eph_kset_ph_aux, kqset=elset, &
                              fst_n=wf_fst, lst_n=wf_lst, evalk=evalfv_kset(wf_fst:wf_lst,:), &
                              fst_m=wf_fst, lst_m=wf_lst, evalkq=evalfv_kset(wf_fst:wf_lst,:), &
                              phfreq=phfreq_interp, ephmat=ephmat_hkq, file_name='EPH_MATRIX_KQ.OUT', type_print=.True.)

      ! Deallocate variables
      deallocate(evecfv_ini, evecfv_k, evecfv_kq, evalfv_kset)
      deallocate(phfreq_interp, phevec_interp)
      deallocate(ephmat_aux, G)
      deallocate(zfmt, zfir, gzfmt)
      deallocate(zfmt_rot, zfir_rot)
      deallocate(zfmt_mode, zfir_mode)
      deallocate(zfmt_aux, zfir_aux)
      return
    end subroutine eph_ephmat_ephmatrix

    subroutine eph_ephmat_transformation(el_set, ph_freq, ph_evec, ephmat_cartesian, ephmat_phonon)
      ! Transforms the matrix elements from cartesian coordinates to phonon coordinates
      use mod_wannier_variables, only: wf_fst, wf_lst, wf_nst

      type( k_set), intent( in)    :: el_set
      real(8), intent( in)         :: ph_freq( eph_nmode)
      complex(8), intent( in)      :: ph_evec( 3, natmtot, eph_nmode)
      complex(8), intent( in)      :: ephmat_cartesian( wf_fst:wf_lst, wf_fst:wf_lst, 3, natmtot, el_set%nkpt)
      complex(8), intent( out)     :: ephmat_phonon( wf_fst:wf_lst, wf_fst:wf_lst, eph_nmode, el_set%nkpt)

      integer    :: ip, ias, is, ia, phmode, ik
      real(8)    :: t1
      complex(8) :: zt1

      do ik = 1, el_set%nkpt
        do phmode = 1, eph_nmode
          ephmat_phonon(:,:, phmode, ik) = zzero
          do is = 1, nspecies
            ! Prefactor: phonon frequencies times atomic mass
            if ( ph_freq(phmode) < 1.d-4) then
              t1 = 0.d0
            else
              t1 = 1.d0/dsqrt(2.d0*ph_freq(phmode)*spmass(is))
            end if
            do ia = 1, natoms(is)
              ias = idxas(ia, is)
              do ip = 1, 3
                ! Prefactor times phonon eigenvector
                zt1 = t1 * ph_evec( ip, ias, phmode)
                ! Prefactor times matrix elemetns
                ephmat_phonon(:,:, phmode, ik) = ephmat_phonon(:,:, phmode, ik) + zt1*ephmat_cartesian(:,:, ip, ias, ik)
              end do ! ip
            end do ! ia
          end do ! is
        end do ! phmode
      end do ! ik
        
      return
    end subroutine eph_ephmat_transformation
 
    subroutine eph_ephmat_coarse2real(elset, phset, ephmat_hkq, ephmat_wrr)
      use modinput
      use mod_kpointset
      use m_getunit
      use mod_eph_phonons, only: eph_phonons_interpolate
      use mod_wannier, only: wannier_rvectors
      use mod_wannier_variables, only: wf_nrpt, wf_nwf, wf_rvec, wf_fst, wf_lst, wf_nst
      use mod_wannier_interpolate, only: wfint_ftk2r, wfint_h2wk, wfint_init, wfint_hwr

      type( k_set), intent( in)              :: elset, phset
      complex(8), intent( in)                :: ephmat_hkq( wf_fst:wf_lst, wf_fst:wf_lst, eph_nmode, elset%nkpt, phset%nkptnr)
      complex(8), allocatable, intent( out)  :: ephmat_wrr( :, :, :, :, :) !ephmat_wrr( wf_nwf, wf_nwf, eph_nmode, wf_nrpt, ph_wf_nrpt)

      ! Auxiliary variables
      integer  :: iq, ik, ip, ir, ikq, im, jn, phmode, isym, un
      real(8)  :: wf_vkc(3), ph_wf_vkc(3), vpkql(3), vkql(3)
      logical  :: file_type
      integer  :: irec, ire, irp, recl
  
      ! Interpolation variables for the e-ph matrix elements
      complex(8), allocatable :: ephmat_wkq(:,:,:,:,:)
      complex(8), allocatable :: ephmat_wrq(:,:,:,:,:)
      complex(8), allocatable :: ephmat_wrq_aux(:,:,:,:,:)
      
      ! Phonon variables in original set
      real(8), allocatable    :: phfreq_q(:,:)
      complex(8), allocatable :: phevec_q(:,:,:,:)

      ! Auxiliary k-set
      Type( k_set) :: eph_kqset

      ! Long-range variables
      complex(8), allocatable :: umnk(:,:)
      complex(8), allocatable :: umnkp(:,:,:)
      integer, allocatable    :: ikq2iq(:)
      complex(8), allocatable :: ephmat_long_wkq(:,:,:,:,:)

      ! For output
      logical                 :: exist

      !-----------------------------------------------------------!
      ! Wannier-Fourier interpolation of the e-ph matrix elements !
      !-----------------------------------------------------------!

      ! Allocate phonon variables in full BZ
      if( allocated(phevec_q)) deallocate(phevec_q)
      allocate(phevec_q( 3, natmtot, eph_nmode, phset%nkptnr))
      if( allocated(phfreq_q)) deallocate(phfreq_q)
      allocate(phfreq_q(eph_nmode, phset%nkptnr))

      if (eph_polar) then
        Call eph_phonons_interpolate( phset, eph_kset_ph_aux, phfreq_q, phevec_q, epsinf=eph_epsinf, zstar=eph_zstar)
      else
        Call eph_phonons_interpolate( phset, eph_kset_ph_aux, phfreq_q, phevec_q)
      end if

      ! Generate Rp vectors     
      Call wannier_rvectors(input%structure%crystal%basevect, eph_kset_ph_aux, &
           ph_wf_nrpt, ph_wf_rvec, ph_wf_rmul, ph_wf_pkr)

      ! Allocate output electron-phonon matrix elements in real space
      if( allocated(ephmat_wrr)) deallocate(ephmat_wrr)
      allocate(ephmat_wrr( wf_nwf, wf_nwf, eph_nmode, & 
                wf_nrpt, & 
                ph_wf_nrpt))

      inquire( file="EPH_MATRIX_REAL.OUT", exist=exist)
      if( exist) then
        Call eph_ephmat_getephmat_real(fst_m=1, lst_m=wf_nwf, fst_n=1, lst_n=wf_nwf, &
                                      re_nrpt=wf_nrpt, rp_nrpt=ph_wf_nrpt, re_vec=wf_rvec, &
                                      rp_vec=ph_wf_rvec, ephmat_real=ephmat_wrr)
        return  
      end if

      ! Allocate the e-ph matrix elements in term of the Wannier functions
      if( allocated(ephmat_wkq)) deallocate(ephmat_wkq)
      allocate(ephmat_wkq( wf_nwf, wf_nwf, eph_nmode, & 
                    elset%nkpt, & 
                    phset%nkptnr))

      ! Allocate the e-ph matrix elements as g(Re,q)
      if( allocated(ephmat_wrq)) deallocate(ephmat_wrq)
      allocate(ephmat_wrq( wf_nwf, wf_nwf, eph_nmode, & 
                    wf_nrpt, & 
                    phset%nkptnr))

      ! Allocate the long-range e-ph matrix elements for the coarse grid g(k,q)
      if( allocated(ephmat_long_wkq)) deallocate(ephmat_long_wkq)
      allocate(ephmat_long_wkq(wf_nwf, wf_nwf, eph_nmode, elset%nkpt, phset%nkptnr))

      ephmat_long_wkq = zzero
      if (eph_polar) then
        !Auxiliary kqset should have qset dimension
        eph_kqset = eph_kset_ph_aux
        !Loop over k-points
        do ik = 1, elset%nkpt
          ! Generate the Wannier function for k
          !Call wfint_init(elset)
          ! Wannier function for k set 
          if( allocated(umnk)) deallocate(umnk)
          allocate(umnk(wf_nwf, wf_nwf))
          umnk(1:wf_nwf, 1:wf_nwf) = zzero !wfint_transform(1:wf_nwf, 1:wf_nwf, ik)
          do iq = 1, phset%nkptnr
            eph_kqset%vkl(:,iq) = phset%vklnr(:,iq) + elset%vkl(:, ik)
            eph_kqset%vkc(:,iq) = phset%vklnr(:,iq) + elset%vkl(:, ik) 
          end do
          ! Map back from target set to path set
          allocate( ikq2iq( eph_kqset%nkpt))
          ! Generate the Wannier function for k+q
          !Call wfint_init(eph_kqset)
          ! Generate the wannier functions for target set (qp-set)
          if( allocated(umnkp)) deallocate(umnkp)
          allocate(umnkp(wf_nwf, wf_nwf, eph_kqset%nkpt))
          do iq = 1, eph_kqset%nkpt
            umnkp(1:wf_nwf, 1:wf_nwf, iq) = zzero !wfint_transform(1:wf_nwf, 1:wf_nwf, iq)
            ikq2iq(iq) = iq
          end do
          ! Generate the long range matrix elements
          Call eph_ephmat_gen_lr( qset=eph_kset_ph_aux, qpset=eph_kqset, iqp2iq=ikq2iq, phfreq=phfreq_q, phevec=phevec_q, &
                              epsinf=eph_epsinf, zstar=eph_zstar, umnqp=umnkp, umnp=umnk, pref=zone, ephmat=ephmat_long_wkq(:,:,:,ik,:), &
                              firstcall=.true.)
          deallocate(umnk, umnkp, ikq2iq)
        end do 
      end if

      ! Transform to Re; k,q ---> Re,q
      !Call wfint_init(elset)
      do iq = 1, phset%nkptnr
        do ik = 1, elset%nkpt
          ! Get the index for k-point k+q
          vpkql(:) =  elset%vkl(:,ik) + phset%vklnr(:,iq)
          Call findkptinset( vpkql, elset, isym, ikq)
          ! Move to Wannier gauge, the Wannier functions are now the main dimensions 
          Call wfint_h2wk(ephmat_hkq(:,:,:, ik, iq), &
                          ephmat_wkq(:,:,:, ik, iq), &
                          ikq, eph_nmode, ik)
          ! Remove the long range
          ephmat_wkq(:,:,:,ik, iq) = ephmat_wkq(:,:,:,ik, iq) - ephmat_long_wkq(:,:,:,ik, iq)
        end do
        ! Fourier transform to Re vectors (real space) ---> g(Re, q)
        Call wfint_ftk2r(ephmat_wkq(:,:,:,:,iq), ephmat_wrq(:,:,:,:,iq), eph_nmode)
      end do

      ! Transform to Rp; Re, q ----> Re, Rp 
      ! Auxiliary matrix for multiplication with the phonon eigenvector
      if( allocated(ephmat_wrq_aux)) deallocate(ephmat_wrq_aux)
      allocate(ephmat_wrq_aux( wf_nwf, wf_nwf, eph_nmode, & 
                   wf_nrpt, & 
                   phset%nkptnr))

      do ir = 1, wf_nrpt
        do iq = 1, phset%nkptnr
          ! E-ph matrix elements times the phonon eigenvector in the coarse q-grid 
          Call zgemm( 'n', 'c' , wf_nwf*wf_nwf, eph_nmode, eph_nmode, zone, &
                  ephmat_wrq(:,:,:, ir, iq), wf_nwf*wf_nwf, &
                  conjg(phevec_q(:,:,:, iq)), eph_nmode, zzero, &
                  ephmat_wrq_aux(:,:,:, ir, iq), wf_nwf*wf_nwf)
        end do
        ! Times the phase factor coming from the Rp vectors ----> g(Re, Rp) in
        ! Wannier gauge
        Call zgemm( 'n', 'n', wf_nwf*wf_nwf*eph_nmode, ph_wf_nrpt, phset%nkptnr, cmplx( 1.d0/phset%nkptnr, 0.d0, 8), &
                  ephmat_wrq_aux(:,:,:, ir, :), wf_nwf*wf_nwf*eph_nmode, &
                  ph_wf_pkr, phset%nkptnr, zzero, &
                  ephmat_wrr(:,:,:, ir, :), wf_nwf*wf_nwf*eph_nmode)
      end do

      ! Write the electron-phonon matrix elements (Re, Rp)
      call eph_ephmat_putephmat_real(fst_m=1, lst_m=wf_nwf, fst_n=1, lst_n=wf_nwf, re_nrpt=wf_nrpt, rp_nrpt=ph_wf_nrpt, & 
                                    re_vec=wf_rvec, rp_vec=ph_wf_rvec, ephmat_real=ephmat_wrr)
      !Call getunit( un)
      !Open(un, File="EPH_SPATIAL_DECAY.OUT", Action='WRITE', Form='FORMATTED')
      !Write(un,'("# !-----------------------------------!")')
      !Write(un,'("# !     SPATIAL DECAY (Re, Rp)        !")')
      !Write(un,'("# !-----------------------------------!")')
      !Write(un,'("# Number of Wannier functions =", T40, I6)') wf_nwf
      !Write(un,'("# Number of Re vectors =", T40, I6)') wf_nrpt
      !Write(un,'("# Number of Rp vectors =", T40, I6)') ph_wf_nrpt
      !Write(un,'("# Number of species =", T40, I6)') nspecies
      !Write(un,'("# Number of atoms =", T40, I6)') natmtot
      !Write(un,'("# Number of branches =", T40, I6)') eph_nmode  
      !ip = 1 ! Set Rp=0
      !do ir = 1, wf_nrpt
      !  ! Transform wf_rvec to cartesian coordinates
      !  Call r3mv(input%structure%crystal%basevect, dble(wf_rvec(:, ir)), wf_vkc)
      !  ! Write the length and max values of g(Re, q=0)
      !  Write(un, '(3F20.8)') norm2(wf_vkc), & 
      !     maxval(abs(ephmat_wrr(:,:,:, ir,ip))), &
      !     sqrt( sum( abs( ephmat_wrr(:,:,:,ir,ip))**2))
      !end do
      !Write(un, *)
      !Write(un,'("-------------------------------------------------------------------------------------")')
      !Write(un, *)
      !ir = 1 ! Set Re=0
      !do ip = 1, ph_wf_nrpt
      !  ! Transform wf_rvec to cartesian coordinates
      !  Call r3mv(input%structure%crystal%basevect, dble(ph_wf_rvec(:, ip)), ph_wf_vkc)
      !  ! Write the length and max values of g(Re, q=0)
      !  Write(un, '(3F20.8)') norm2(ph_wf_vkc), & 
      !    maxval(abs(ephmat_wrr(:,:,:, ir, ip))), &
      !    sqrt( sum( abs( ephmat_wrr(:,:,:, ir, ip))**2))
      !end do
      !Close(un)

      !  Free memory from unnecessary variable
      Call delete_k_vectors(eph_kqset)

      ! Deallocate variables
      deallocate(ephmat_wkq, ephmat_wrq, ephmat_wrq_aux)
      deallocate(phfreq_q, phevec_q)
      deallocate(ephmat_long_wkq)

      return

    end subroutine eph_ephmat_coarse2real

    subroutine eph_ephmat_real2fine(phset, pset, qset, eph_pqset, ephmat_wrr, ephmat_hkp)
      use modinput
      use mod_kpointset
      use m_getunit
      use mod_eph_electrons, only: eph_electrons_interpolate
      use mod_eph_phonons, only: eph_phonons_interpolate
      use mod_wannier_variables, only: wf_nrpt, wf_nwf, wf_rvec, wf_fst, wf_lst, wf_nst
      use mod_wannier_interpolate, only: wfint_ftr2q, wfint_fourierphases, wfint_init, wfint_pqr

      type( k_set), intent( in)    :: phset, pset, qset, eph_pqset
      complex(8), intent( in)      :: ephmat_wrr( wf_nwf, wf_nwf, eph_nmode, wf_nrpt, ph_wf_nrpt)
      complex(8), intent( out)     :: ephmat_hkp( wf_nwf, wf_nwf, eph_nmode, eph_qset%nkptnr)

      ! Auxiliary variables
      integer  :: iq, ik, ip, ir, ikq, im, jn, phmode, isym, un
      real(8)  :: wf_vkc(3), ph_wf_vkc(3), vpkql(3), vkql(3)
      logical  :: file_type
  
      ! Interpolation variables for the e-ph matrix elements
      complex(8), allocatable :: ephmat_wkr(:,:,:,:)
      complex(8), allocatable :: ephmat_wkp(:,:,:,:)
      complex(8), allocatable :: ephmat_wkp_aux(:,:,:,:)
      complex(8), allocatable :: ephmat_hkp_aux(:,:,:,:)
      
      ! Phonon variables in target set
      real(8), allocatable    :: phfreq_q(:,:)
      complex(8), allocatable :: phevec_q(:,:,:,:)

      ! Interpolation (fine grid) variables
      real(8), allocatable    :: eph_int_evalk(:,:)
      real(8), allocatable    :: eph_int_evalkp(:,:)
      complex(8), allocatable :: eph_int_umnk(:,:,:)
      complex(8), allocatable :: eph_int_umnkp(:,:,:)

      ! Auxiliary k-set
      !Type( k_set) :: eph_pqset

      ! Long-range variables
      complex(8), allocatable :: umnk(:,:)
      complex(8), allocatable :: umnkp(:,:,:)
      integer, allocatable :: ikq2iq(:)
      complex(8), allocatable :: ephmat_long_wkp(:,:,:,:)

      ! Transform from g(Re, Rp) to g(p, q') (fine grids)
      ! Generate a q'+p set: fix p-point + target_set. Necessary to obtain the
      ! Wannier matrices at p 
      !eph_pqset = qset
      !do ip = 1, pset%nkpt
      !  do iq = 1, qset%nkpt
      !    eph_pqset%vkl(:,iq) = pset%vkl(:,ip) + qset%vkl(:, iq)
      !    eph_pqset%vkc(:,iq) = pset%vkc(:,ip) + qset%vkc(:, iq)
      !  end do
      !end do

      ! Allocate electronic variables for p and q'+p grids
      if( allocated( eph_int_evalkp)) deallocate( eph_int_evalkp)
      allocate(eph_int_evalkp( wf_nwf, eph_pqset%nkpt)) 

      if( allocated( eph_int_umnkp)) deallocate( eph_int_umnkp)
      allocate(eph_int_umnkp( wf_nwf, wf_nwf, eph_pqset%nkpt)) 

      if( allocated( eph_int_evalk)) deallocate( eph_int_evalk)
      allocate(eph_int_evalk( wf_nwf, pset%nkpt)) 

      if( allocated( eph_int_umnk)) deallocate( eph_int_umnk)
      allocate(eph_int_umnk( wf_nwf, wf_nwf, pset%nkpt)) 

      ! This generates the Wannier matrices in the k'+p fine grid (eph_kpset)
      Call eph_electrons_interpolate(eph_pqset, eph_int_evalkp, evec=eph_int_umnkp) 

      ! Generates the Wannier matrix for k' 
      Call eph_electrons_interpolate(pset, eph_int_evalk, evec=eph_int_umnk) 
      
      ! E-ph matrix elements in terms of g(k', Rp) 
      if( allocated(ephmat_wkr)) deallocate(ephmat_wkr)
      allocate(ephmat_wkr( wf_nwf, wf_nwf, eph_nmode, & 
                         & ph_wf_nrpt))

      ! Interpolation to p (it should be just one point)
      do ip = 1, pset%nkpt 
         do iq = 1, ph_wf_nrpt
            ! E-ph matrix elements now written as g(k',Rp) in Wannier gauge
            Call wfint_ftr2q(ephmat_wrr(:,:,:,:, iq), &
                            ephmat_wkr(:,:,:, iq), &
                            ip, eph_nmode) 
         end do
      end do

      ! Generate the phases for q-set 
      Call wfint_fourierphases(qset, ph_wf_nrpt, ph_wf_rvec, ph_wf_rmul, ph_wf_pqr)

      ! Allocate phonon variables for p-set
      allocate(phevec_q( 3, natmtot, eph_nmode, qset%nkpt))
      allocate(phfreq_q(eph_nmode, qset%nkpt))

      ! Interpolation of phonon frequencies and phonon eigenvectors in the q-set (integration grid)
      if (eph_polar) then
        Call eph_phonons_interpolate( phset, qset, phfreq_q, phevec_q, epsinf=eph_epsinf, zstar=eph_zstar)
      else
        Call eph_phonons_interpolate( phset, qset, phfreq_q, phevec_q)
      end if

      ! E-ph matrix elements in terms of g(p,q') 
      if( allocated(ephmat_wkp)) deallocate(ephmat_wkp)
      allocate(ephmat_wkp( wf_nwf, wf_nwf, eph_nmode, & 
                           qset%nkpt))

      ! E-ph matrix elements in terms of g(k',p) auxilary variable to multiply
      ! times the Wannier matrix at k'+p 
      if( allocated(ephmat_wkp_aux)) deallocate(ephmat_wkp_aux)
      allocate(ephmat_wkp_aux( wf_nwf, wf_nwf, eph_nmode, & 
                               qset%nkpt))

      ! E-ph matrix elements in terms of g(k',p) in H gauge 
      if( allocated(ephmat_hkp_aux)) deallocate(ephmat_hkp_aux)
      allocate(ephmat_hkp_aux( wf_nwf, wf_nwf, eph_nmode, & 
                               qset%nkpt))

      if( allocated(ephmat_long_wkp)) deallocate(ephmat_long_wkp)
      allocate(ephmat_long_wkp(wf_nwf, wf_nwf, eph_nmode, eph_pqset%nkpt))
      ephmat_long_wkp = zzero

      ! Add long-range matrix elements 
      if(eph_polar) then
        ! Auxiliary kp set should have pset dimensions
        !eph_kpset_aux = pset
        ! Loop over k' points
        !do ik = 1, kset%nkpt
          ! Wannier function for k' set 
          !Call wfint_init(kset)
          if( allocated(umnk)) deallocate(umnk)
          allocate(umnk(wf_nwf, wf_nwf))
          umnk(1:wf_nwf, 1:wf_nwf) = zzero !wfint_transform(1:wf_nwf, 1:wf_nwf, ik)
          !do ip = 1, pset%nkpt
          !  eph_kpset_aux%vkl(:,ip) = pset%vkl(:,ip) + kset%vkl(:, ik)
          !  eph_kpset_aux%vkc(:,ip) = pset%vkc(:,ip) + kset%vkc(:, ik)
          !end do
          ! Map back from target set to path set
          allocate( ikq2iq( eph_pqset%nkpt))
          ! Generate the wannier functions for target set (qp-set)
          !Call wfint_init(eph_kpset_aux)
          if( allocated(umnkp)) deallocate(umnkp)
          allocate(umnkp(wf_nwf, wf_nwf, eph_pqset%nkpt))
          do ip = 1, eph_pqset%nkpt
            umnkp(1:wf_nwf, 1:wf_nwf, ip) =  zzero !wfint_transform(1:wf_nwf, 1:wf_nwf, ip)
            ikq2iq(ip) = ip
          end do 
          ! Generate the long range matrix elements
          Call eph_ephmat_gen_lr( qset=qset, qpset=eph_pqset, iqp2iq=ikq2iq, phfreq=phfreq_q, phevec=phevec_q, &
            epsinf=eph_epsinf, zstar=eph_zstar, umnqp=umnkp, umnp=umnk, pref=zone, ephmat=ephmat_long_wkp, firstcall=.true.)

          deallocate(umnk, umnkp, ikq2iq)
        !end do
      end if

      do ip = 1, pset%nkpt
        do iq = 1, qset%nkpt         
            ! Get the index for k-point k+q
            vpkql(:) =  pset%vkl(:,ip) + qset%vkl(:,iq)
            Call findkptinset( vpkql, eph_pqset, isym, ikq)
            ! phonon interpolation to g(k',p) (fine grids) still Wannier gauge 
            Call zgemv( 'n', wf_nwf*wf_nwf*eph_nmode, ph_wf_nrpt, zone, &
                      ephmat_wkr, wf_nwf*wf_nwf*eph_nmode, &
                      ph_wf_pqr(iq,:), 1, zzero, &
                      ephmat_wkp(:,:,:,iq), 1)
            ! Times the phonon eigenvector in the target set 
            Call zgemm( 'n', 'n' , wf_nwf*wf_nwf, eph_nmode, eph_nmode, zone, &
                      ephmat_wkp(:,:,:,iq), wf_nwf*wf_nwf, &
                      conjg(phevec_q(:,:,:, iq)), eph_nmode, zzero, &
                      ephmat_wkp_aux(:,:,:,iq), wf_nwf*wf_nwf)
            ! Add the long range part
            ephmat_wkp_aux(:,:,:,iq) = ephmat_wkp_aux(:,:,:,iq) + ephmat_long_wkp(:,:,:, iq)
            ! Move to Hamiltonian gauge, multiply times the Wannier matrices for the k'+p and
            ! k' fine grids
            Call zgemm( 'c', 'n', wf_nwf, wf_nwf*eph_nmode, wf_nwf, zone, &
                      eph_int_umnkp(:,:, ikq), wf_nwf, &
                      ephmat_wkp_aux(:,:,:,iq), wf_nwf, zzero, &
                      ephmat_hkp_aux(:,:,:,iq), wf_nwf)
            do phmode = 1, eph_nmode
               Call zgemm( 'n', 'n', wf_nwf, wf_nwf, wf_nwf, zone, &
                        ephmat_hkp_aux(:,:, phmode, iq), wf_nwf, &
                        eph_int_umnk(:, :, ip), wf_nwf, zzero, &
                        ephmat_hkp(:, :, phmode, iq), wf_nwf)
            end do
        end do
      end do

      ! Write the averaged matrix elements g(k',p) (fine grid) to file
      !if( .not. allocated( eph_pset_dpp1d)) then
      !  file_type = .True.
      !else 
      !  file_type = .False.
      !end if
      !Call eph_ephmat_print2file(el_set=pset, ph_set=qset, kqset=eph_pqset, &
      !                        fst_n=1, lst_n=wf_nwf, evalk=eph_int_evalk, &
      !                        fst_m=1, lst_m=wf_nwf, evalkq=eph_int_evalkp, &
      !                        phfreq=phfreq_q, ephmat=ephmat_hkp, file_name='EPH_MATRIX_KP.OUT', type_print=file_type)

      !  Free memory from unnecessary variable
      !Call delete_k_vectors(eph_pqset)

      ! Deallocate variables
      !deallocate(ph_wf_rvec, ph_wf_rmul, ph_wf_pkr, ph_wf_pqr)
      deallocate(ephmat_wkr, ephmat_wkp_aux, ephmat_wkp)
      deallocate(ephmat_hkp_aux) 
      deallocate(phfreq_q, phevec_q)
      deallocate(eph_int_evalk, eph_int_evalkp)
      deallocate(eph_int_umnk, eph_int_umnkp)
      deallocate(ephmat_long_wkp)

      return

    end subroutine eph_ephmat_real2fine

    subroutine eph_ephmat_generate_dw(qset, eph_h_array, eph_t_factor, ephmat_dw)
      use mod_wannier_variables, only: wf_nrpt, wf_nwf, wf_rvec, wf_fst, wf_lst, wf_nst
      type( k_set), intent( in)    :: qset
      complex(8), intent( in)      :: eph_h_array( wf_nwf, wf_nwf, 3, natmtot)
      complex(8), intent( in)      :: eph_t_factor( 3, natmtot, 3, natmtot, eph_nmode, qset%nkptnr)
      complex(8), intent( out)     :: ephmat_dw( wf_nwf, wf_nwf, eph_nmode, qset%nkptnr)

      ! Auxiliary variables
      integer :: iq, is1, ia1, ias1, is2, ia2, ias2, im, jn, imode, ip2, ip1

      do iq = 1, qset%nkptnr
        do imode = 1, eph_nmode
          do ias2 = 1, natmtot
            do ip2 = 1, 3
              do ias1 = 1, natmtot
                do ip1 = 1, 3
                  ephmat_dw(:,:, imode, iq) = ephmat_dw(:,:, imode, iq) + eph_t_factor(ip1, ias1, ip2, ias2, imode, iq)*conjg(eph_h_array(:,:, ip1, ias1))*eph_h_array(:,:, ip2, ias2)
                end do
              end do
            end do
          end do
        end do
      end do

      return
    end subroutine eph_ephmat_generate_dw

    subroutine eph_ephmat_interpolate(elset, phset, kset, pset, ephmat_hkq, ephmat_hkp)
      use modinput
      use mod_kpointset
      use m_getunit
      use mod_eph_electrons, only: eph_electrons_interpolate
      use mod_eph_phonons, only: eph_phonons_interpolate
      use mod_wannier, only: wannier_rvectors
      use mod_wannier_variables, only: wf_nrpt, wf_nwf, wf_rvec, wf_fst, wf_lst, wf_nst
      use mod_wannier_interpolate, only: wfint_ftk2r, wfint_h2wk, wfint_ftr2q, wfint_fourierphases, wfint_transform, wfint_eval, wfint_init

      type( k_set), intent( in)    :: elset, phset, kset, pset
      complex(8), intent( in)      :: ephmat_hkq( wf_fst:wf_lst, wf_fst:wf_lst, eph_nmode, elset%nkpt, phset%nkptnr)
      complex(8), intent( out)     :: ephmat_hkp( wf_nwf, wf_nwf, eph_nmode, kset%nkptnr, pset%nkptnr)

      ! Auxiliary variables
      integer  :: iq, ik, ip, ir, ikq, im, jn, phmode, isym, un
      real(8)  :: wf_vkc(3), ph_wf_vkc(3), vpkql(3), vkql(3)
      logical  :: file_type
  
      ! Interpolation variables for the e-ph matrix elements
      complex(8), allocatable :: ephmat_wkq(:,:,:,:,:)
      complex(8), allocatable :: ephmat_wrq(:,:,:,:,:)
      complex(8), allocatable :: ephmat_wrq_aux(:,:,:,:,:)
      complex(8), allocatable :: ephmat_wrr(:,:,:,:,:)
      complex(8), allocatable :: ephmat_wkr(:,:,:,:,:)
      complex(8), allocatable :: ephmat_wkp(:,:,:,:,:)
      complex(8), allocatable :: ephmat_wkp_aux(:,:,:,:,:)
      complex(8), allocatable :: ephmat_hkp_aux(:,:,:,:,:)
      
      ! Phonon variables in original set
      real(8), allocatable    :: phfreq_q(:,:)
      complex(8), allocatable :: phevec_q(:,:,:,:)

      ! Phonon variables in target set
      real(8), allocatable    :: phfreq_p(:,:)
      complex(8), allocatable :: phevec_p(:,:,:,:)

      ! Interpolation (fine grid) variables
      real(8), allocatable    :: eph_int_evalk(:,:)
      real(8), allocatable    :: eph_int_evalkp(:,:)
      complex(8), allocatable :: eph_int_umnk(:,:,:)
      complex(8), allocatable :: eph_int_umnkp(:,:,:)

      ! Auxiliary k-set
      Type( k_set) :: eph_kpset, eph_kqset

      ! Long-range variables
      complex(8), allocatable :: umnk(:,:)
      complex(8), allocatable :: umnkp(:,:,:)
      integer, allocatable :: ikp2ip(:), ikq2iq(:)
      complex(8), allocatable :: ephmat_long_wkp(:,:,:,:,:)
      complex(8), allocatable :: ephmat_long_wkq(:,:,:,:,:)

      !-----------------------------------------------------------!
      ! Wannier-Fourier interpolation of the e-ph matrix elements !
      !-----------------------------------------------------------!

      ! Allocate phonon variables in full BZ
      if( allocated(phevec_q)) deallocate(phevec_q)
      allocate(phevec_q( 3, natmtot, eph_nmode, phset%nkptnr))
      if( allocated(phfreq_q)) deallocate(phfreq_q)
      allocate(phfreq_q(eph_nmode, phset%nkptnr))

      if (eph_polar) then
        Call eph_phonons_interpolate( phset, eph_kset_ph_aux, phfreq_q, phevec_q, epsinf=eph_epsinf, zstar=eph_zstar)
      else
        Call eph_phonons_interpolate( phset, eph_kset_ph_aux, phfreq_q, phevec_q)
      end if

      ! Allocate the e-ph matrix elements in term of the Wannier functions
      if( allocated(ephmat_wkq)) deallocate(ephmat_wkq)
      allocate(ephmat_wkq( wf_nwf, wf_nwf, eph_nmode, & 
                    elset%nkpt, & 
                    phset%nkptnr))

      ! Allocate the e-ph matrix elements as g(Re,q)
      if( allocated(ephmat_wrq)) deallocate(ephmat_wrq)
      allocate(ephmat_wrq( wf_nwf, wf_nwf, eph_nmode, & 
                    wf_nrpt, & 
                    phset%nkptnr))

      ! Allocate the long-range e-ph matrix elements for the coarse grid g(k,q)
      if( allocated(ephmat_long_wkq)) deallocate(ephmat_long_wkq)
      allocate(ephmat_long_wkq(wf_nwf, wf_nwf, eph_nmode, elset%nkpt, phset%nkptnr))

      ephmat_long_wkq = zzero
      if (eph_polar) then
        !Auxiliary kqset should have qset dimension
        eph_kqset = eph_kset_ph_aux
        !Loop over k-points
        do ik = 1, elset%nkpt
          ! Generate the Wannier function for k
          !Call wfint_init(elset)
          ! Wannier function for k set 
          if( allocated(umnk)) deallocate(umnk)
          allocate(umnk(wf_nwf, wf_nwf))
          umnk(1:wf_nwf, 1:wf_nwf) = zzero !wfint_transform(1:wf_nwf, 1:wf_nwf, ik)
          do iq = 1, phset%nkptnr
            eph_kqset%vkl(:,iq) = phset%vklnr(:,iq) + elset%vkl(:, ik)
            eph_kqset%vkc(:,iq) = phset%vklnr(:,iq) + elset%vkl(:, ik) 
          end do
          ! Map back from target set to path set
          allocate( ikq2iq( eph_kqset%nkpt))
          ! Generate the Wannier function for k+q
          !Call wfint_init(eph_kqset)
          ! Generate the wannier functions for target set (qp-set)
          if( allocated(umnkp)) deallocate(umnkp)
          allocate(umnkp(wf_nwf, wf_nwf, eph_kqset%nkpt))
          do iq = 1, eph_kqset%nkpt
            umnkp(1:wf_nwf, 1:wf_nwf, iq) = zzero !wfint_transform(1:wf_nwf, 1:wf_nwf, iq)
            ikq2iq(iq) = iq
          end do
          ! Generate the long range matrix elements
          Call eph_ephmat_gen_lr( qset=eph_kset_ph_aux, qpset=eph_kqset, iqp2iq=ikq2iq, phfreq=phfreq_q, phevec=phevec_q, &
                              epsinf=eph_epsinf, zstar=eph_zstar, umnqp=umnkp, umnp=umnk, pref=zone, ephmat=ephmat_long_wkq(:,:,:,ik,:), &
                              firstcall=.true.)
          deallocate(umnk, umnkp, ikq2iq)
        end do 
      end if

      ! Transform to Re; k,q ---> Re,q
      !Call wfint_init(elset)
      do iq = 1, phset%nkptnr
        do ik = 1, elset%nkpt
          ! Get the index for k-point k+q
          vpkql(:) =  elset%vkl(:,ik) + phset%vklnr(:,iq)
          Call findkptinset( vpkql, elset, isym, ikq)
          ! Move to Wannier gauge, the Wannier functions are now the main dimensions 
          Call wfint_h2wk(ephmat_hkq(:,:,:, ik, iq), &
                          ephmat_wkq(:,:,:, ik, iq), &
                          ikq, eph_nmode, ik)
          ! Remove the long range
          ephmat_wkq(:,:,:,ik, iq) = ephmat_wkq(:,:,:,ik, iq) - ephmat_long_wkq(:,:,:,ik, iq)
        end do
        ! Fourier transform to Re vectors (real space) ---> g(Re, q)
        Call wfint_ftk2r(ephmat_wkq(:,:,:,:,iq), ephmat_wrq(:,:,:,:,iq), eph_nmode)
      end do

      ! Transform to Rp; Re, q ----> Re, Rp 
      ! Generate Rp vectors     
      Call wannier_rvectors(input%structure%crystal%basevect, eph_kset_ph_aux, &
           ph_wf_nrpt, ph_wf_rvec, ph_wf_rmul, ph_wf_pkr)

      ! Auxiliary matrix for multiplication with the phonon eigenvector
      if( allocated(ephmat_wrq_aux)) deallocate(ephmat_wrq_aux)
      allocate(ephmat_wrq_aux( wf_nwf, wf_nwf, eph_nmode, & 
                   wf_nrpt, & 
                   phset%nkptnr))

      ! E-ph matrix elements in terms of g(Re, Rp) 
      if( allocated(ephmat_wrr)) deallocate(ephmat_wrr)
      allocate(ephmat_wrr( wf_nwf, wf_nwf, eph_nmode, & 
                   wf_nrpt, & 
                   ph_wf_nrpt))

      do ir = 1, wf_nrpt
        do iq = 1, phset%nkptnr
          ! E-ph matrix elements times the phonon eigenvector in the coarse q-grid 
          Call zgemm( 'n', 'c' , wf_nwf*wf_nwf, eph_nmode, eph_nmode, zone, &
                  ephmat_wrq(:,:,:, ir, iq), wf_nwf*wf_nwf, &
                  conjg(phevec_q(:,:,:, iq)), eph_nmode, zzero, &
                  ephmat_wrq_aux(:,:,:, ir, iq), wf_nwf*wf_nwf)
        end do
        ! Times the phase factor coming from the Rp vectors ----> g(Re, Rp) in
        ! Wannier gauge
        Call zgemm( 'n', 'n', wf_nwf*wf_nwf*eph_nmode, ph_wf_nrpt, phset%nkptnr, cmplx( 1.d0/phset%nkptnr, 0.d0, 8), &
                  ephmat_wrq_aux(:,:,:, ir, :), wf_nwf*wf_nwf*eph_nmode, &
                  ph_wf_pkr, phset%nkptnr, zzero, &
                  ephmat_wrr(:,:,:, ir, :), wf_nwf*wf_nwf*eph_nmode)
      end do

      ! Write the spatial decay to file
      Call getunit( un)
      Open(un, File="EPH_SPATIAL_DECAY.OUT", Action='WRITE', Form='FORMATTED')
      Write(un,'("# !-----------------------------------!")')
      Write(un,'("# !     SPATIAL DECAY (Re, Rp)        !")')
      Write(un,'("# !-----------------------------------!")')
      Write(un,'("# Number of Wannier functions =", T40, I6)') wf_nwf
      Write(un,'("# Number of Re vectors =", T40, I6)') wf_nrpt
      Write(un,'("# Number of Rp vectors =", T40, I6)') ph_wf_nrpt
      Write(un,'("# Number of species =", T40, I6)') nspecies
      Write(un,'("# Number of atoms =", T40, I6)') natmtot
      Write(un,'("# Number of branches =", T40, I6)') eph_nmode  
      ip = 1 ! Set Rp=0
      do ir = 1, wf_nrpt
        ! Transform wf_rvec to cartesian coordinates
        Call r3mv(input%structure%crystal%basevect, dble(wf_rvec(:, ir)), wf_vkc)
        ! Write the length and max values of g(Re, q=0)
        Write(un, '(3F20.8)') norm2(wf_vkc), & 
           maxval(abs(ephmat_wrr(:,:,:, ir,ip))), &
           sqrt( sum( abs( ephmat_wrr(:,:,:,ir,ip))**2))
      end do
      Write(un, *)
      Write(un,'("-------------------------------------------------------------------------------------")')
      Write(un, *)
      ir = 1 ! Set Re=0
      do ip = 1, ph_wf_nrpt
        ! Transform wf_rvec to cartesian coordinates
        Call r3mv(input%structure%crystal%basevect, dble(ph_wf_rvec(:, ip)), ph_wf_vkc)
        ! Write the length and max values of g(Re, q=0)
        Write(un, '(3F20.8)') norm2(ph_wf_vkc), & 
          maxval(abs(ephmat_wrr(:,:,:, ir, ip))), &
          sqrt( sum( abs( ephmat_wrr(:,:,:, ir, ip))**2))
      end do
      Close(un)

      ! Transform from g(Re, Rp) to g(k',q') (fine grids), here on q' --> p
      ! Generate a k'+p set: fix k-point + target_set. Necessary to obtain the
      ! Wannier matrices at k' 
      ! When using the p-grid as the integration grid, eph_kpset is the sum of
      ! pset plus the qset
      eph_kpset = pset
      do ik = 1, kset%nkpt
        do ip = 1, pset%nkpt
          eph_kpset%vkl(:,ip) = pset%vkl(:,ip) + kset%vkl(:, ik)
          eph_kpset%vkc(:,ip) = pset%vkc(:,ip) + kset%vkc(:, ik)
        end do
      end do

      ! Allocate electronic variables for k' and k'+p grids
      if( allocated( eph_int_evalkp)) deallocate( eph_int_evalkp)
      allocate(eph_int_evalkp( wf_nwf, eph_kpset%nkpt)) 

      if( allocated( eph_int_umnkp)) deallocate( eph_int_umnkp)
      allocate(eph_int_umnkp( wf_nwf, wf_nwf, eph_kpset%nkpt)) 

      if( allocated( eph_int_evalk)) deallocate( eph_int_evalk)
      allocate(eph_int_evalk( wf_nwf, kset%nkpt)) 

      if( allocated( eph_int_umnk)) deallocate( eph_int_umnk)
      allocate(eph_int_umnk( wf_nwf, wf_nwf, kset%nkpt)) 

      ! This generates the Wannier matrices in the k'+p fine grid (eph_kpset)
      Call eph_electrons_interpolate(eph_kpset, eph_int_evalkp, evec=eph_int_umnkp) 

      ! Generates the Wannier matrix for k' 
      Call eph_electrons_interpolate(kset, eph_int_evalk, evec=eph_int_umnk) 
      
      ! E-ph matrix elements in terms of g(k', Rp) 
      if( allocated(ephmat_wkr)) deallocate(ephmat_wkr)
      allocate(ephmat_wkr( wf_nwf, wf_nwf, eph_nmode, & 
                         & kset%nkpt, & 
                         & ph_wf_nrpt))

      ! Interpolation to k' (fine grid)
      do ik = 1, kset%nkpt 
         do ip = 1, ph_wf_nrpt 
            ! E-ph matrix elements now written as g(k',Rp) in Wannier gauge
            Call wfint_ftr2q(ephmat_wrr(:,:,:,:, ip), &
                            ephmat_wkr(:,:,:, ik, ip), &
                            ik, eph_nmode) 
         end do
      end do

      ! Generate the phases for p-set 
      Call wfint_fourierphases(pset, ph_wf_nrpt, ph_wf_rvec, ph_wf_rmul, ph_wf_pqr)

      ! Allocate phonon variables for p-set
      allocate(phevec_p( 3, natmtot, eph_nmode, pset%nkpt))
      allocate(phfreq_p(eph_nmode, pset%nkpt))

      ! Interpolation of phonon frequencies and phonon eigenvectors in the p-set
      if (eph_polar) then
        Call eph_phonons_interpolate( phset, pset, phfreq_p, phevec_p, epsinf=eph_epsinf, zstar=eph_zstar)
      else
        Call eph_phonons_interpolate( phset, pset, phfreq_p, phevec_p)
      end if

      ! E-ph matrix elements in terms of g(k',p) 
      if( allocated(ephmat_wkp)) deallocate(ephmat_wkp)
      allocate(ephmat_wkp( wf_nwf, wf_nwf, eph_nmode, & 
                           kset%nkpt, & 
                           pset%nkpt))

      ! E-ph matrix elements in terms of g(k',p) auxilary variable to multiply
      ! times the Wannier matrix at k'+p 
      if( allocated(ephmat_wkp_aux)) deallocate(ephmat_wkp_aux)
      allocate(ephmat_wkp_aux( wf_nwf, wf_nwf, eph_nmode, & 
                               kset%nkpt, & 
                               pset%nkpt))

      ! E-ph matrix elements in terms of g(k',p) in H gauge 
      if( allocated(ephmat_hkp_aux)) deallocate(ephmat_hkp_aux)
      allocate(ephmat_hkp_aux( wf_nwf, wf_nwf, eph_nmode, & 
                               kset%nkpt, & 
                               pset%nkpt))

      if( allocated(ephmat_long_wkp)) deallocate(ephmat_long_wkp)
      allocate(ephmat_long_wkp(wf_nwf, wf_nwf, eph_nmode, eph_kpset%nkpt, kset%nkpt))
      ephmat_long_wkp = zzero

      ! Add long-range matrix elements 
      if(eph_polar) then
        ! Auxiliary kp set should have pset dimensions
        !eph_kpset_aux = pset
        ! Loop over k' points
        ik = 1
        !do ik = 1, kset%nkpt
          ! Wannier function for k' set 
          !Call wfint_init(kset)
          if( allocated(umnk)) deallocate(umnk)
          allocate(umnk(wf_nwf, wf_nwf))
          umnk(1:wf_nwf, 1:wf_nwf) = zzero !wfint_transform(1:wf_nwf, 1:wf_nwf, ik)
          !do ip = 1, pset%nkpt
          !  eph_kpset_aux%vkl(:,ip) = pset%vkl(:,ip) + kset%vkl(:, ik)
          !  eph_kpset_aux%vkc(:,ip) = pset%vkc(:,ip) + kset%vkc(:, ik)
          !end do
          ! Map back from target set to path set
          allocate( ikp2ip( eph_kpset%nkpt))
          ! Generate the wannier functions for target set (qp-set)
          !Call wfint_init(eph_kpset_aux)
          if( allocated(umnkp)) deallocate(umnkp)
          allocate(umnkp(wf_nwf, wf_nwf, eph_kpset%nkpt))
          do ip = 1, eph_kpset%nkpt
            umnkp(1:wf_nwf, 1:wf_nwf, ip) =  zzero !wfint_transform(1:wf_nwf, 1:wf_nwf, ip)
            ikp2ip(ip) = ip
          end do 
          ! Generate the long range matrix elements
          Call eph_ephmat_gen_lr( qset=pset, qpset=eph_kpset, iqp2iq=ikp2ip, phfreq=phfreq_p, phevec=phevec_p, &
            epsinf=eph_epsinf, zstar=eph_zstar, umnqp=umnkp, umnp=umnk, pref=zone, ephmat=ephmat_long_wkp(:,:,:,:, ik), firstcall=.true.)

          deallocate(umnk, umnkp, ikp2ip)
        !end do
      end if

      do ik = 1, kset%nkpt
         do iq = 1, pset%nkpt         
            ! Get the index for k-point k+q
            vpkql(:) =  kset%vkl(:,ik) + pset%vkl(:,iq)
            Call findkptinset( vpkql, eph_kpset, isym, ikq)
            ! phonon interpolation to g(k',p) (fine grids) still Wannier gauge 
            Call zgemv( 'n', wf_nwf*wf_nwf*eph_nmode, ph_wf_nrpt, zone, &
                      ephmat_wkr(:,:,:, ik,:), wf_nwf*wf_nwf*eph_nmode, &
                      ph_wf_pqr(iq,:), 1, zzero, &
                      ephmat_wkp(:,:,:, ik, iq), 1)
            ! Times the phonon eigenvector in the target set 
            Call zgemm( 'n', 'n' , wf_nwf*wf_nwf, eph_nmode, eph_nmode, zone, &
                      ephmat_wkp(:,:,:, ik, iq), wf_nwf*wf_nwf, &
                      conjg(phevec_p(:,:,:, iq)), eph_nmode, zzero, &
                      ephmat_wkp_aux(:,:,:, ik, iq), wf_nwf*wf_nwf)
            ! Add the long range part
            ephmat_wkp_aux(:,:,:, ik, iq) = ephmat_wkp_aux(:,:,:,ik, iq) + ephmat_long_wkp(:,:,:, iq, ik)
            ! Move to Hamiltonian gauge, multiply times the Wannier matrices for the k'+p and
            ! k' fine grids
            Call zgemm( 'c', 'n', wf_nwf, wf_nwf*eph_nmode, wf_nwf, zone, &
                      eph_int_umnkp(:,:, ikq), wf_nwf, &
                      ephmat_wkp_aux(:,:,:, ik, iq), wf_nwf, zzero, &
                      ephmat_hkp_aux(:,:,:, ik, iq), wf_nwf)
            do phmode = 1, eph_nmode
               Call zgemm( 'n', 'n', wf_nwf, wf_nwf, wf_nwf, zone, &
                        ephmat_hkp_aux(:,:, phmode, ik, iq), wf_nwf, &
                        eph_int_umnk(:, :, ik), wf_nwf, zzero, &
                        ephmat_hkp(:, :, phmode, ik, iq), wf_nwf)
            end do
        end do
      end do


      ! Write the averaged matrix elements g(k',p) (fine grid) to file
      if( .not. allocated( eph_pset_dpp1d)) then
        file_type = .True.
      else 
        file_type = .False.
      end if
      Call eph_ephmat_print2file(el_set=kset, ph_set=pset, kqset=eph_kpset, &
                              fst_n=1, lst_n=wf_nwf, evalk=eph_int_evalk, &
                              fst_m=1, lst_m=wf_nwf, evalkq=eph_int_evalkp, &
                              phfreq=phfreq_p, ephmat=ephmat_hkp, file_name='EPH_MATRIX_KP.OUT', type_print=file_type)

      !  Free memory from unnecessary variable
      Call delete_k_vectors(eph_kpset)
      Call delete_k_vectors(eph_kqset)

      ! Deallocate variables
      deallocate(ephmat_wkq, ephmat_wrq, ephmat_wrq_aux, ephmat_wrr)
      deallocate(ph_wf_rvec, ph_wf_rmul, ph_wf_pkr, ph_wf_pqr)
      deallocate(ephmat_wkr, ephmat_wkp_aux, ephmat_wkp)
      deallocate(ephmat_hkp_aux) 
      deallocate(phfreq_q, phevec_q)
      deallocate(phfreq_p, phevec_p)
      deallocate(eph_int_evalk, eph_int_evalkp)
      deallocate(eph_int_umnk, eph_int_umnkp)
      deallocate(ephmat_long_wkq)
      deallocate(ephmat_long_wkp)

      return
    end subroutine eph_ephmat_interpolate

    subroutine eph_ephmat_average(el_set, ph_set, kpset, fst_n, lst_n, fst_m, lst_m, evalk, evalkp, phfreq, ephmat, ephmat_ave)
      ! Returns the average of the (squared) matrix elements for each degenerate electronic
      ! or vibrational state. The e-ph matrix element is not gauge invariant therefore, 
      ! the matrix elements corresponding to degenerate electronic or vibrational states 
      ! do not carry any physical meaning by themselves. 
      type( k_set), intent( in)           :: el_set, ph_set, kpset
      integer, intent( in)                :: fst_n, lst_n, fst_m, lst_m
      real(8), intent( in)                :: evalk(  fst_n:lst_n, el_set%nkpt)
      real(8), intent(in)                 :: evalkp( fst_m:lst_m, kpset%nkpt)
      real(8), intent( in)                :: phfreq( eph_nmode, ph_set%nkpt)
      complex(8), intent( in)             :: ephmat( fst_m:lst_m, fst_n:lst_n, eph_nmode, el_set%nkpt, ph_set%nkpt)
      real(8), intent( out)               :: ephmat_ave( fst_m:lst_m, fst_n:lst_n, eph_nmode, el_set%nkpt, ph_set%nkpt)

      integer :: ik, iq, ikq, ph_mode, im, jn, jm, nu, i, isym
      integer :: pdeg(2,eph_nmode), ekdeg(2,fst_n:lst_n), ekqdeg(2,fst_m:lst_m)
      real(8) :: vpkql(3)
      real(8), allocatable :: ephmat_sym(:,:,:)
 
      ! Get the modulus of the e-ph matrix elements
      ephmat_ave = 0.d0 
      do iq = 1, ph_set%nkpt 
        do ph_mode = 1, eph_nmode
          do ik  = 1, el_set%nkpt    
            do jn = fst_n, lst_n
              do im = fst_m, lst_m
                ephmat_ave(im, jn, ph_mode, ik, iq) = abs( ephmat(im, jn, ph_mode, ik, iq))
              end do
            end do
          end do
        end do
      end do

      do iq = 1, ph_set%nkpt 
        ik = 1 ! Electronic state fix at Gamma
        ! Get the index for k'+p eigenvalue in kpset
        vpkql(:) = ph_set%vkl(:,iq) + el_set%vkl(:,ik)
        Call findkptinset( vpkql, kpset, isym, ikq)

        ! Get phonon degeneracies at q
        pdeg = 1
        do nu = 2, eph_nmode
          if( abs( phfreq(nu,iq) - phfreq(pdeg(1,nu-1),iq)) < 1.d-6) then
            pdeg(2,pdeg(1,nu-1):nu-1) = pdeg(2,pdeg(1,nu-1):nu-1) + 1
            pdeg(:,nu) = pdeg(:,nu-1)
          else
           pdeg(:,nu) = nu
          end if
        end do

        ! Get electron degeneracies at k
        ekdeg = fst_n
        do jn = fst_n+1, lst_n
          if( abs( evalk(jn,ik) - evalk(ekdeg(1,jn-1),ik)) < 1.d-4) then
            ekdeg(2,ekdeg(1,jn-1):jn-1) = ekdeg(2,ekdeg(1,jn-1):jn-1) + 1
            ekdeg(:,jn) = ekdeg(:,jn-1)
          else
            ekdeg(:,jn) = jn
          end if
        end do

        ! Get electron degeneracies at k+q
        ekqdeg = fst_m
        do jm = fst_m+1, lst_m
          if( abs( evalkp(jm,ikq) - evalkp(ekqdeg(1,jm-1),ikq)) < 1.d-4) then
            ekqdeg(2,ekqdeg(1,jm-1):jm-1) = ekqdeg(2,ekqdeg(1,jm-1):jm-1) + 1
            ekqdeg(:,jm) = ekqdeg(:,jm-1)
          else
            ekqdeg(:,jm) = jm
          end if
        end do
        
        ! Average matrix elements
        nu = 1
        do while( nu <= eph_nmode)
          jm = fst_m
          do while( jm <= lst_m)
            jn = fst_n
            do while( jn <= lst_n)
              i = (pdeg(2,nu)-pdeg(1,nu)+1) * (ekqdeg(2,jm)-ekqdeg(1,jm)+1) * (ekdeg(2,jn)-ekdeg(1,jn)+1)
              ephmat_ave( ekqdeg(1,jm):ekqdeg(2,jm), ekdeg(1,jn):ekdeg(2,jn), pdeg(1,nu):pdeg(2,nu), ik, iq) = &
                sqrt( sum( abs( ephmat( ekqdeg(1,jm):ekqdeg(2,jm), ekdeg(1,jn):ekdeg(2,jn), pdeg(1,nu):pdeg(2,nu), ik, iq))**2)/dble(i))
              jn = ekdeg(2,jn) + 1
            end do
            jm = ekqdeg(2,jm) + 1
          end do
          nu = pdeg(2,nu) + 1
        end do
      end do

      return
    end subroutine eph_ephmat_average

    subroutine eph_ephmat_putephmat(el_set, ph_set, fst_n, lst_n, fst_m, lst_m, ephmat)
      use m_getunit

      type( k_set), intent( in)           :: el_set, ph_set
      integer, intent( in)                :: fst_n, lst_n, fst_m, lst_m
      complex(8), intent( in)             :: ephmat(fst_m:lst_m, fst_n:lst_n, eph_nmode, el_set%nkpt, ph_set%nkpt)

      integer :: irec, iq, ik, un, recl

      inquire(iolength=recl) fst_m, lst_m, fst_n, lst_n, eph_nmode, ephmat(:,:,:, 1, 1)

      call getunit( un)
      open( un, file="EPH_MATRIX.OUT", action='write', form='unformatted', access='direct', recl=recl)
      do iq = 1, ph_set%nkpt
        do ik = 1, el_set%nkpt
          irec=(iq-1)*ph_set%nkpt + ik
          write( un, rec=irec) fst_m, lst_m, fst_n, lst_n, eph_nmode, ephmat(:,:,:, ik, iq)
        end do
      end do
      close( un)

      return
    end subroutine eph_ephmat_putephmat
 
    subroutine eph_ephmat_putephmat_real(fst_m, lst_m, fst_n, lst_n, re_nrpt, rp_nrpt, re_vec, rp_vec, ephmat_real)
      use m_getunit

      integer, intent( in)                :: fst_m, lst_m, fst_n, lst_n, re_nrpt, rp_nrpt, re_vec(3, re_nrpt), rp_vec(3, rp_nrpt)
      complex(8), intent( in)             :: ephmat_real(fst_m:lst_m, fst_n:lst_n, eph_nmode, re_nrpt, rp_nrpt)

      integer :: irec, ire, irp, un, recl

      inquire(iolength=recl) fst_m, lst_m, fst_n, lst_n, eph_nmode, re_nrpt, rp_nrpt, re_vec(:, 1), rp_vec(:, 1), ephmat_real(:,:,:, 1, 1)

      call getunit( un)
      open( un, file="EPH_MATRIX_REAL.OUT", action='write', form='unformatted', access='direct', recl=recl)
      do ire = 1, re_nrpt
        do irp = 1, rp_nrpt
          irec = (ire - 1)*rp_nrpt + irp
          write( un, rec=irec) fst_m, lst_m, fst_n, lst_n, eph_nmode, re_nrpt, rp_nrpt, re_vec(:, ire), rp_vec(:, irp), ephmat_real(:,:,:, ire, irp)
        end do
      end do
      close( un)

      return
    end subroutine eph_ephmat_putephmat_real

    subroutine eph_ephmat_getephmat(el_set, ph_set, fst_n, lst_n, fst_m, lst_m, ephmat)
      use m_getunit

      type( k_set), intent( in)           :: el_set, ph_set
      integer, intent( in)                :: fst_n, lst_n, fst_m, lst_m
      complex(8), intent( out)            :: ephmat(fst_m:lst_m, fst_n:lst_n, eph_nmode, el_set%nkpt, ph_set%nkpt)

      integer :: irec, iq, ik, un, recl, fst_n_, lst_n_, fst_m_, lst_m_, eph_nmode_
      logical :: exist

      inquire( file="EPH_MATRIX.OUT", exist=exist)
      if( exist) then
       inquire(iolength=recl) fst_m_, lst_m_, fst_n_, lst_n_, eph_nmode_
       call getunit(un)
       open( un, file="EPH_MATRIX.OUT", action='read', form='unformatted', access='direct', recl=recl)
       read( un, rec=1) fst_m_, lst_m_, fst_n_, lst_n_, eph_nmode_ 
       close( un)
       if( (fst_n_ .lt. fst_n) .or. (lst_n_ .gt. lst_n) ) then
        write(*,*)
        write(*, '("Error (ephmat_getephmat): Different number of electronic bands for k:")')
        write(*, '("Current:", i8)') fst_n, lst_n
        write(*, '("File   :", i8)') fst_n_, lst_n_
        stop
       end if
       if( (fst_m_ .lt. fst_m) .or. (lst_m_ .gt. lst_m) ) then
        write(*,*)
        write(*, '("Error (ephmat_getephmat): Different number of electronic bands for k+q:")')
        write(*, '("Current:", i8)') fst_m, lst_m
        write(*, '("File   :", i8)') fst_m_, lst_m_
        stop
       end if
       if( eph_nmode_ .ne. eph_nmode) then
        write(*,*)
        write(*, '("Error (ephmat_getephmat): Different number of phonon modes:")')
        write(*, '("Current:", i8)') eph_nmode
        write(*, '("File   :", i8)') eph_nmode_
        stop
       end if
       inquire( iolength=recl) fst_m_, lst_m_, fst_n_, lst_n_, eph_nmode_, ephmat(:,:,:,1,1)
       call getunit( un)
       open( un, file="EPH_MATRIX.OUT", action='read', form='unformatted', access='direct', recl=recl)
       do iq = 1, ph_set%nkpt
        do ik = 1, el_set%nkpt 
          irec=(iq-1)*ph_set%nkpt + ik
          read( un, rec=irec) fst_m_, lst_m_, fst_n_, lst_n_, eph_nmode_, ephmat(:,:,:, ik, iq)  
        end do
       end do
       close( un)
      else
       write(*,*)
       write(*,'("Error (ephmat_getephmat): File does not exist: EPH_MATRIX.OUT")')
       stop
      end if

      return
    end subroutine eph_ephmat_getephmat

    subroutine eph_ephmat_getephmat_real(fst_m, lst_m, fst_n, lst_n, re_nrpt, rp_nrpt, re_vec, rp_vec, ephmat_real)
      use m_getunit

      integer, intent( in)                :: fst_m, lst_m, fst_n, lst_n, re_nrpt, rp_nrpt, re_vec(3, re_nrpt), rp_vec(3, rp_nrpt)
      complex(8), intent( out)            :: ephmat_real(fst_m:lst_m, fst_n:lst_n, eph_nmode, re_nrpt, rp_nrpt)

      integer :: irec, ire, irp, un, recl, fst_n_, lst_n_, fst_m_, lst_m_, eph_nmode_, re_nrpt_, rp_nrpt_
      integer, allocatable :: re_vec_(:,:), rp_vec_(:,:)
      logical :: exist

      allocate(re_vec_(3, re_nrpt))
      allocate(rp_vec_(3, rp_nrpt))

      inquire( file="EPH_MATRIX_REAL.OUT", exist=exist)
      if( exist) then
       inquire(iolength=recl) fst_m_, lst_m_, fst_n_, lst_n_, eph_nmode_, re_nrpt_, rp_nrpt_
       call getunit(un)
       open( un, file="EPH_MATRIX_REAL.OUT", action='read', form='unformatted', access='direct', recl=recl)
       read( un, rec=1) fst_m_, lst_m_, fst_n_, lst_n_, eph_nmode_, re_nrpt_, rp_nrpt_ 
       close( un)
       if( (fst_n_ .lt. fst_n) .or. (lst_n_ .gt. lst_n) ) then
        write(*,*)
        write(*, '("Error (ephmat_getephmat_real): Different number of electronic bands for k:")')
        write(*, '("Current:", i8)') fst_n, lst_n
        write(*, '("File   :", i8)') fst_n_, lst_n_
        stop
       end if
       if( (fst_m_ .lt. fst_m) .or. (lst_m_ .gt. lst_m) ) then
        write(*,*)
        write(*, '("Error (ephmat_getephmat_real): Different number of electronic bands for k+q:")')
        write(*, '("Current:", i8)') fst_m, lst_m
        write(*, '("File   :", i8)') fst_m_, lst_m_
        stop
       end if
       if( eph_nmode_ .ne. eph_nmode) then
        write(*,*)
        write(*, '("Error (ephmat_getephmat_real): Different number of phonon modes:")')
        write(*, '("Current:", i8)') eph_nmode
        write(*, '("File   :", i8)') eph_nmode_
        stop
       end if
       if( (re_nrpt_ .ne. re_nrpt) .or. (rp_nrpt_ .ne. rp_nrpt)) then
        write(*,*)
        write(*, '("Error (ephmat_getephmat_real): Different number of lattice vectors Re, Rp:")')
        write(*, '("Current:", i8)') re_nrpt, rp_nrpt
        write(*, '("File   :", i8)') re_nrpt_, rp_nrpt_
        stop
       end if
       inquire( iolength=recl) fst_m_, lst_m_, fst_n_, lst_n_, eph_nmode_, re_nrpt_, rp_nrpt_, re_vec_(:,1), rp_vec_(:,1), ephmat_real(:,:,:,1,1)
       call getunit( un)
       open( un, file="EPH_MATRIX_REAL.OUT", action='read', form='unformatted', access='direct', recl=recl)
       do ire = 1, re_nrpt
        do irp = 1, rp_nrpt 
          irec = (ire - 1)*rp_nrpt + irp
          read( un, rec=irec) fst_m_, lst_m_, fst_n_, lst_n_, eph_nmode_, re_nrpt_, rp_nrpt_, re_vec_(:, ire), rp_vec_(:, irp), ephmat_real(:,:,:, ire, irp)  
        end do
       end do
       close( un)
      else
       write(*,*)
       write(*,'("Error (ephmat_getephmat): File does not exist: EPH_MATRIX_REAL.OUT")')
       stop
      end if

      return
    end subroutine eph_ephmat_getephmat_real

    subroutine eph_ephmat_print2file(el_set, ph_set, kqset, fst_n, lst_n, evalk, fst_m, lst_m, evalkq, phfreq, ephmat, file_name, type_print)
      use m_getunit

      type( k_set), intent( in)           :: el_set, ph_set, kqset
      integer, intent( in)                :: fst_n, lst_n, fst_m, lst_m
      real(8), intent( in)                :: evalk(  fst_n:lst_n, el_set%nkpt)
      real(8), intent(in)                 :: evalkq( fst_m:lst_m, kqset%nkpt)
      real(8), intent( in)                :: phfreq( eph_nmode, ph_set%nkpt)
      complex(8), intent( in)             :: ephmat( fst_m:lst_m, fst_n:lst_n, eph_nmode, el_set%nkpt, ph_set%nkpt)
      character(*), intent(in)            :: file_name
      logical, intent(in)                 :: type_print

      real(8)  :: hartree2meV, hartree2eV, vpkql(3)
      real(8), allocatable    :: ephmat_mod(:,:,:,:,:)
      integer :: un, iq, ik, ikq, jn, im, phmode, isym
      real(8) :: ha2ev = 27.21138624598803

      ! Hartree to eV/meV
      hartree2meV = 27211.38602
      hartree2eV = 27.21138602

      ! Allocate the modulus of the e-ph matrix elements |g|
      if( allocated(ephmat_mod)) deallocate(ephmat_mod)
      allocate(ephmat_mod( fst_m:lst_m, fst_n:lst_n, eph_nmode, & 
                    el_set%nkpt, & 
                    ph_set%nkpt)) 

      ! Average of the matrix elements
      Call eph_ephmat_average(el_set, ph_set, kqset, &
                              fst_n, lst_n, fst_m, lst_m, &
                              evalk(fst_n:lst_n, :), evalkq(fst_m:lst_m, :), phfreq, &
                              ephmat, ephmat_mod)

      Call getunit( un)
      Open(un, File=file_name, Action='WRITE', Form='FORMATTED')
      ! General information
      Write( un ,'("!-----------------------------------!")')
      Write( un ,'("!       EPH MATRIX ELEMENTS         !")')
      Write( un ,'("!-----------------------------------!")')
      Write( un ,'("Number of KS states/Wannier functions at k =", T60, I6)') lst_n-fst_n+1 
      Write( un ,'("Number of KS states/Wannier functions at k+q/p =", T60, I6)') lst_m-fst_m+1 
      Write( un ,'("Number of q/p points =", T60, I6)') ph_set%nkpt
      Write( un ,'("Number of k points =", T60, I6)') el_set%nkpt
      Write( un ,'("Number of species =", T60, I6)') nspecies
      Write( un ,'("Number of atoms =", T60, I6)') natmtot
      Write( un ,'("Number of branches =", T60, I6)') eph_nmode  

      if( type_print) then
         do iq = 1, ph_set%nkpt
            ik = 1 ! Electronic state fix at gamma
            !Do ik = 1, ephmat_kset%nkptnr
            Write( un,'("-------------------------------------------------------------------------------------")')
            Write( un,'("iq =", I4," q-vector=", 3F13.8)') iq, ph_set%vkl(:, iq)
            Write( un,'("ik =", I4," k-vector=", 3F13.8)') ik, el_set%vkl(:, ik)

            vpkql(:) = ph_set%vkl(:, iq) + el_set%vkl(:, ik) 
            Call findkptinset( vpkql, kqset, isym, ikq) 

            Write(un,'("ik+q =", I4," k+q-vector=", 3F13.8)') ikq, vpkql 
            Write(un, '("   iband", 4x, "jband", 4x, "nmode", 4x, "enk[eV]", 5x, "enk+q[eV]", &
                     &  5x, "w(meV)", 7x, "|g[meV]|", 14x, "g[meV]")')
            do jn = eph_ist, eph_jst
               do im = eph_ist, eph_jst
                  do phmode = 1, eph_nmode  
                     Write(un, '(I6, 2I9, 3F13.5, F13.6, 3F15.6)' ) jn,  im, phmode, &
                       evalk(jn, ik)*hartree2eV, evalkq(im, ikq)*hartree2eV, & 
                       phfreq(phmode, iq)*hartree2meV, &
                       ephmat_mod(im, jn, phmode, ik, iq)*hartree2meV, &
                       ephmat(im, jn, phmode, ik, iq)*hartree2meV  
                  end do ! phmode loop
               end do ! n states loop
            end do ! m states loop
            ! end do ik loop
         end do ! iq loop
      else
         do iq = 1, ph_set%nkpt 
            ik = 1 ! Electronic state fix at gamma
            Write(un, '(G20.10, 1000F20.8)' ) eph_pset_dpp1d( iq), &
                                              ephmat_mod(2, 4, :, ik, iq)*hartree2meV, & 
                                              ephmat_mod(3, 4, :, ik, iq)*hartree2meV, & 
                                              ephmat_mod(4, 4, :, ik, iq)*hartree2meV
         end do ! iq loop
      end if
      Close(un)

      deallocate(ephmat_mod)
      return
    end subroutine eph_ephmat_print2file

    subroutine eph_ephmat_ftk2r(kset, d_ini, d_out, m_k, m_r, start_, step_)
      use modinput
      use mod_wannier, only: wannier_rvectors

      type( k_set), intent( in)              :: kset
      !type( rset), intent( in)              :: rset
      integer, intent( in)                  :: d_ini, d_out
      complex(8), intent( in)               :: m_k(d_ini, *)
      complex(8), intent( out)              :: m_r(d_out, *)
      integer, optional, intent( in)        :: start_, step_

      ! Auxiliary variables
      integer :: ip, start, step
      integer                 :: aux_nrpt
      integer, allocatable    :: aux_rvec(:,:)
      integer, allocatable    :: aux_rmul(:)
      complex(8), allocatable :: aux_pkr(:,:)

      Call wannier_rvectors(input%structure%crystal%basevect, kset, &
           aux_nrpt, aux_rvec, aux_rmul, aux_pkr)
      
      start = 1
      step = 1
      if( present( start_)) start = start_
      if( present( step_)) step = step_

      do ip = start, kset%nkpt*step, step
         call zgemm( 'n', 'n', d_ini, aux_nrpt, kset%nkpt, cmplx( 1.d0/kset%nkpt, 0.d0, 8), &
             m_k(:, ip), d_ini, &
             aux_pkr, kset%nkpt, zzero, &
             m_r, d_out)
      end do

      return  
    end subroutine eph_ephmat_ftk2r

    subroutine eph_ephmat_ftr2q(qset, nrpt, rvec, rmul, d_ini, d_out, m_r, m_q, start_, step_)
      use mod_wannier_interpolate, only: wfint_fourierphases
      type( k_set), intent( in)                    :: qset
      !type( r_set), intent( in)                    :: rset
      integer, intent( in)                         :: nrpt
      integer, intent( in)                         :: rvec( 3, nrpt)
      integer, intent( in)                         :: rmul( nrpt)
      integer, intent( in)                         :: d_ini, d_out
      complex(8), intent( in)                      :: m_r(d_ini, *)
      complex(8), intent( out)                     :: m_q(d_out, *)
      integer, optional, intent( in)               :: start_, step_

      ! Auxiliary variables
      integer :: iq, ip, start, step
      complex(8), allocatable :: aux_pqr(:,:)

      Call wfint_fourierphases(qset, nrpt, rvec, rmul, aux_pqr)

      start = 1
      step = 1
      if( present( start_)) start = start_
      if( present( step_)) step = step_

      do iq = 1, qset%nkpt
        do ip = start, qset%nkpt*step, step 
          Call zgemv( 'n', d_ini, nrpt, zone, &
                 m_r, d_ini, &
                 aux_pqr( iq, :), 1, zzero, &
                 m_q(:, ip), 1)
        end do 
      end do
      
      return
    end subroutine eph_ephmat_ftr2q

    subroutine eph_ephmat_rotate_potcoeff(isym, vecq_irr, mt_pot_coeff, int_pot_coeff, mt_pot_coeff_rot, int_pot_coeff_rot)
      use modinput
      use mod_muffin_tin, only: lmmaxvr, nrmtmax
      use mod_symmetry, only: symlat, symlatc, isymlat, lsplsymc, vtlsymc, ieqatom
      use mod_Gvector, only: ngrtot, ngrid, ivg, ngvec
      use mod_constants

      integer, intent( in)           :: isym
      real(8), intent( in)           :: vecq_irr(3)
      complex(8), intent( in)        :: mt_pot_coeff(lmmaxvr, nrmtmax, natmtot)
      complex(8), intent( in)        :: int_pot_coeff(ngrtot)
      complex(8), intent( out)       :: mt_pot_coeff_rot(lmmaxvr, nrmtmax, natmtot)
      complex(8), intent( out)       :: int_pot_coeff_rot(ngrtot)

      ! Potential response variables
      complex(8), allocatable :: zfmt_aux(:,:,:)
      complex(8), allocatable :: zfir_aux(:)

      ! Local variables
      integer :: is, ia, ias, lspl, ilspl, js, ja, jas, iv(3)
      real(8) :: v1(3), v2(3), phase
      complex(8) :: zt1

      ! Allocate potential variables 
      if( allocated(zfmt_aux)) deallocate(zfmt_aux)
      allocate(zfmt_aux(lmmaxvr, nrmtmax, natmtot))

      if( allocated(zfir_aux)) deallocate(zfir_aux)
      allocate(zfir_aux(ngrtot))

      mt_pot_coeff_rot = zzero
      int_pot_coeff_rot = zzero

      zfmt_aux = zzero
      zfir_aux = zzero
      lspl = lsplsymc( isym)
      ilspl = isymlat( lspl)
      Call r3mtv( dble(symlat(:,:, ilspl)), vecq_irr, v1)
      Call r3frac ( input%structure%epslat, v1, iv)
      write(*,'(6I, 3f13.6)') isym, lspl, ilspl, iv, vtlsymc(:, isym)

      ! Fourier transform to G-space
      Call zfftifc (3, ngrid,-1, int_pot_coeff)
      ! Rotate the Int part of the potential using q-point symmetries
      Call eph_ephmat_rotzfig(isym=isym, ng=ngvec, ivg=ivg, shift=iv, qvector=vecq_irr, zfig1=int_pot_coeff, zfig2=zfir_aux)
      ! Fourier transform to real space
      Call zfftifc (3, ngrid, 1, zfir_aux)  
      int_pot_coeff_rot = zfir_aux 
      ! Rotate the MT part of the potential using q-point symmetries
      do is = 1, nspecies
        do ia = 1, natoms(is)
          ias= idxas(ia, is)
          ja = ieqatom( ia, is, isym)
          jas = idxas( ja, is)
          v1 = input%structure%speciesarray(is)%species%atomarray(ia)%atom%coord + vtlsymc(:,isym)
          call r3mv( dble(symlat(:,:, ilspl)), input%structure%speciesarray(is)%species%atomarray(ja)%atom%coord, v2)
          phase = twopi*dot_product( vecq_irr, v2-v1)
          Call rotzflm( dble(symlatc(:,:, lspl)), input%groundstate%lmaxvr, nrmtmax, lmmaxvr, mt_pot_coeff(:,:, ias), zfmt_aux(:,:, jas))
          mt_pot_coeff_rot(:,:, jas) = cmplx( cos(phase), sin(phase), 8)*zfmt_aux(:,:,jas)
        end do
      end do

      deallocate(zfmt_aux, zfir_aux)
      return
    end subroutine eph_ephmat_rotate_potcoeff

    subroutine eph_ephmat_rotzfig( isym, ng, ivg, shift, qvector, zfig1, zfig2)
      use modinput
      use mod_symmetry, only: lsplsymc, isymlat, symlat, vtlsymc
      use mod_Gvector, only: igfft, intgv, ngrid, ivgig, vgc, ngrtot
      use mod_constants, only: twopi
      integer, intent( in)       :: isym
      integer, intent( in)       :: ng
      integer, intent( in)       :: ivg(3,*)
      integer, intent( in)       :: shift(3)
      real(8), intent( in)       :: qvector(3)
      complex(8), intent( in)    :: zfig1( ngrtot)
      complex(8), intent( out)   :: zfig2( ngrtot)
    
      integer :: lspl, ilspl, ig, jg, ifg, jfg, iv(3)
      real(8) :: sym(3,3), t1
    
      ! index to lattice symmetry of spatial rotation
      lspl = lsplsymc( isym)
      ! inverse rotation required for rotation of G-vectors
      ilspl = isymlat( lspl)
      sym = symlat(:,:, ilspl)
    #ifdef USEOMP
    ! This PRIVATE/SHARED partitioning assumes ig maps uniquely onto jfg.
    
    !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(ifg,iv,ig,jg,jfg,t1)
    !$OMP DO
    #endif
      do ig = 1, ng
        ifg = igfft( ivgig( ivg(1,ig), ivg(2,ig), ivg(3,ig)))
        ! multiply the transpose of the inverse symmetry matrix with the G-vector
        iv(1) = sym(1,1)*ivg(1,ig) + sym(2,1)*ivg(2,ig) + sym(3,1)*ivg(3,ig) + shift(1)  
        iv(2) = sym(1,2)*ivg(1,ig) + sym(2,2)*ivg(2,ig) + sym(3,2)*ivg(3,ig) + shift(2) 
        iv(3) = sym(1,3)*ivg(1,ig) + sym(2,3)*ivg(2,ig) + sym(3,3)*ivg(3,ig) + shift(3)
        iv = modulo( iv-intgv(:,1), ngrid) + intgv(:,1)
        jg = ivgig( iv(1), iv(2), iv(3))
        jfg = igfft( jg)
        ! complex phase factor for translation
        t1 = -twopi*dot_product(dble(ivg(:, ig)) + qvector(:), vtlsymc(:, isym))
        zfig2(jfg) = zfig2(jfg) + cmplx( cos(t1), sin(t1), 8)*zfig1(ifg)
      end do
    #ifdef USEOMP
    !$OMP END DO
    !$OMP END PARALLEL
    #endif

      return
    end subroutine eph_ephmat_rotzfig

    subroutine eph_ephmat_gamma(isym, vpl, G)
      !use math_utils, only: all_zero
      use mod_symmetry, only: symlat, symlatc, isymlat, lsplsymc, vtlsymc, ieqatom, nsymcrys
      !use mod_lattice
      !> index of symmetry operation in global symmetry arrays
      integer, intent(in) :: isym
      !> reciprocal space point \({\bf p}\)
      real(8), intent(in) :: vpl(3)
      !> symmetry matrix
      complex(8), allocatable, intent(out) :: G(:,:)

      integer :: is, ia, ias, ja, jas, lspl, ilspl, i, j
      real(8) :: sl(3,3), sc(3,3), v1(3), v2(3), phase
      complex(8) :: aux( 3*natmtot, 3*natmtot)
      !character(256) :: errmsg

      if( allocated( G)) deallocate( G)
      allocate( G( 3*natmtot, 3*natmtot), source=zzero)

      lspl = lsplsymc( isym)
      ilspl = isymlat( lspl)
      sl = dble( symlat(:,:,ilspl))
      sc = symlatc(:,:,lspl)
      do is = 1, nspecies
        do ia = 1, natoms( is)
          ias = idxas( ia, is)
          i = (ias - 1)*3 + 1
          ja = ieqatom( ia, is, isym)
          jas = idxas( ja, is)
          j = (jas - 1)*3 + 1
          v1 = input%structure%speciesarray(is)%species%atomarray(ja)%atom%coord + vtlsymc(:,isym)
          call r3mv( sl, input%structure%speciesarray(is)%species%atomarray(ia)%atom%coord, v2)
          phase = twopi*dot_product( vpl, v2-v1)
          G( i:i+2, j:j+2) = cmplx( cos(phase), sin(phase), 8)*sc
        end do
      end do
      ! check if Gamma is unitary
      call zgemm( 'c', 'n', 3*natmtot, 3*natmtot, 3*natmtot, zone, &
             G, 3*natmtot, &
             G, 3*natmtot, zzero, &
             aux, 3*natmtot)
      do ias = 1, 3*natmtot
        aux(ias,ias) = aux(ias,ias) - zone
      end do
      !write(errmsg,'("(phonons_symmetry_G): Non-unitary Gamma for symmetry ",i2," and q = ",3f13.6)') isym, vpl
      !call terminate_if_false( all_zero( aux, tol=input%structure%epslat), trim(errmsg))
    end subroutine eph_ephmat_gamma

end module mod_eph_ephmat
