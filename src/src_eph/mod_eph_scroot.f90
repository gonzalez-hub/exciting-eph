module mod_eph_scroot
  use mod_eph_variables
  use mod_eph_sigma
  use mod_atoms, only: natmtot
  use m_getunit

  implicit none

  logical :: eph_scroot_writefile = .false.

! methods
  contains
    !
    recursive subroutine eph_scroot( xrange, yrange, space, epsm, epsf, ip, ist, nroot, roots, phfreq, etherm, ek)
      use mod_complex_root
      real(8), intent( in)                  :: xrange(2), yrange(2)     ! x- and y-range of the search region
      real(8), intent( in)                  :: space                    ! spacing of initial search mesh
      real(8), intent( in)                  :: epsm                     ! radius within which two vertices are considered equal
      real(8), intent( in)                  :: epsf                     ! value below which f is considered zero
      integer, intent( in)                  :: ip, ist                  ! index of target point p and band
      integer, intent( out)                 :: nroot                    ! number of found roots
      complex(8), allocatable, intent( out) :: roots(:)                 ! found roots

      real(8), optional, intent( in)        :: phfreq, etherm, ek

      integer :: i, j, nv, np, nr, iv(3)
      real(8) :: res, xr(2), yr(2)
      complex(8) :: z, g(3), v(3), mf1(3), mf2(3), mf3, mw
      integer, allocatable :: mroots(:), mpoles(:)
      complex(8), allocatable :: f(:), poles(:)
      character(256) :: fname

      xr = xrange
      yr = yrange
      call croot_destroy
      call croot_genmesh( xr, yr, space, pattern='honeycomb', eps=epsm)

      res = 1.d128
      i = 0
      do while( (res .gt. max( sqrt( epsm), epsm)) .and. (i .lt. 20))
        i = i + 1
        nv = croot_tri%nv
        if( allocated( croot_e)) call croot_refine
        if( eph_scroot_writefile) then
          write(*,*) i, croot_tri%nv
          write( fname, '("TRIANGULATION",i2.2)') i
          call triangulation_tofile( croot_tri, fname)
        end if
        if( croot_tri%nv .ne. nv) then
          if( allocated( f)) deallocate( f)
          allocate( f( croot_tri%nv))
          f( 1:nv) = croot_f
          !call eph_gen_sigmafm( eph_evalp( ist, ip) + croot_tri%verts( (nv+1):croot_tri%nv), croot_tri%nv-nv+1, ip, ist, ist, f( (nv+1):croot_tri%nv))
          call eph_gen_sigmaein( ek + croot_tri%verts( (nv+1):croot_tri%nv), croot_tri%nv-nv+1, phfreq, etherm, f( (nv+1):croot_tri%nv))
          ! Sigma(z) - z = 0 is what we want to solve
          f( (nv+1):croot_tri%nv) = f( (nv+1):croot_tri%nv) - croot_tri%verts( (nv+1):croot_tri%nv)
          if( allocated( croot_f)) deallocate( croot_f)
          allocate( croot_f( croot_tri%nv))
          croot_f = f
        else
          if( allocated( croot_f)) deallocate( croot_f)
          allocate( croot_f( croot_tri%nv))
          !call eph_gen_sigmafm( eph_evalp( ist, ip) + croot_tri%verts, croot_tri%nv, ip, ist, ist, croot_f)
          call eph_gen_sigmaein( ek + croot_tri%verts, croot_tri%nv, phfreq, etherm, croot_f)
          ! Sigma(z) - z = 0 is what we want to solve
          croot_f = croot_f - croot_tri%verts
        end if
        call croot_quadrants
        call croot_edges
        if( allocated( croot_e)) then
          if( eph_scroot_writefile) then
            write(*,'("CANDIDATE EDGES:",100i)') croot_e
            write( fname, '("CROOT",i2.2)') i
            call croot_tofile( fname)
          end if
          call croot_regions
          res = croot_emin
          if( eph_scroot_writefile) then
            write(*,'("CANDIDATE REGIONS:",100i)') size( croot_l, 2)
            write(*,'("EMIN:",g23.16)') res
            write( fname, '("CROOT",i2.2)') i
            call croot_tofile( fname)
          end if
        else
          ! enlarge search region if no candidate edge was found
          xr = xr*sqrt( 2.d0)
          yr = yr*sqrt( 2.d0)
          call croot_destroy
          call croot_genmesh( xr, yr, space, pattern='honeycomb')
        end if
        if( eph_scroot_writefile) then
          write(*,*) '===================================================='
        end if
      end do
      nv = i

      if( allocated( f)) deallocate( f)
      call croot_verify( roots, mroots, nroot, poles, mpoles, np, eps=epsf)
      
      ! final refinement of the solutions
      nr = 0
      do i = 1, nroot
        z = roots( i)
        iv(1) = minloc( abs( croot_tri%verts - z), 1)
        v(1) = croot_tri%verts( iv(1))
        croot_tri%verts( iv(1)) = cmplx( 1.d128, 1.d128, 8)
        iv(2) = minloc( abs( croot_tri%verts - z), 1)
        v(2) = croot_tri%verts( iv(2))
        croot_tri%verts( iv(2)) = cmplx( 1.d128, 1.d128, 8)
        iv(3) = minloc( abs( croot_tri%verts - z), 1)
        v(3) = croot_tri%verts( iv(3))
        croot_tri%verts( iv(1)) = v(1)
        croot_tri%verts( iv(2)) = v(2)

        g = croot_f( iv)
        call sortidx( 3, abs( g), iv)
        v = v( iv)
        g = g( iv)
        mf1 = g
        nv = 0
        if( eph_scroot_writefile) then
          write(*,*) '----------------------------'
        end if
        do while( nv .lt. 1000)
          nv = nv + 1
          mf2(1) = (mf1(3) - mf1(2))/(v(3) - v(2))
          mf2(2) = (mf1(1) - mf1(3))/(v(1) - v(3))
          mf2(3) = (mf1(2) - mf1(1))/(v(2) - v(1))
          mf3 = (mf2(3) - mf2(2))/(v(1) - v(3))
          mw = mf2(1) + mf2(2) - mf2(3)
          z = sqrt( mw*mw - 4.d0*mf1(3)*mf3)
          if( abs( mw + z) .gt. abs( mw - z)) then
            z = mw + z
          else
            z = mw - z
          end if
          v(1) = v(2)
          v(2) = v(3)
          v(3) = v(3) - 2.d0*mf1(3)/z
          mf1(1) = mf1(2)
          mf1(2) = mf1(3)
          !call eph_gen_sigmafm( eph_evalp( ist, ip) + v(3), 1, ip, ist, ist, mf1( 3))
          call eph_gen_sigmaein( ek + v(3), 1, phfreq, etherm, mf1(3))
          mf1(3) = mf1(3) - v(3)
          if( eph_scroot_writefile) then
            write(*,'(i,2f23.16,g13.6,3x,3g13.6)') nv, v(3), abs( mf1(3)), abs( v(1)-v(2)), abs( v(2)-v(3)), abs( v(3)-v(1))
          end if
          if( (max( abs( v(1)-v(2)), max( abs( v(2)-v(3)), abs( v(3)-v(1)))) .lt. epsm) .and. (abs( mf1(3)) .lt. epsf)) exit

          !j = maxloc( abs( g), 1)
          !z = ( g(1)*g(2)*v(3)*(v(1)-v(2)) + g(2)*g(3)*v(1)*(v(2)-v(3)) + g(3)*g(1)*v(2)*(v(3)-v(1)) )/&
          !    ( g(1)*g(2)*(v(1)-v(2))      + g(2)*g(3)*(v(2)-v(3))      + g(3)*g(1)*(v(3)-v(1)) )

          !call eph_gen_sigmafm( eph_evalp( ist, ip) + z, 1, ip, ist, ist, g( j))
          !g( j) = g( j) - z
          !v( j) = z
          !write(*,'(i,2f23.16,g13.6,3x,3g13.6)') nv, v( j), abs( g( j)), abs( v(1)-v(2)), abs( v(2)-v(3)), abs( v(3)-v(1))
          !if( (max( abs( v(1)-v(2)), max( abs( v(2)-v(3)), abs( v(3)-v(1)))) .lt. epsm) .and. (abs( g( j)) .lt. epsf)) exit
        end do
        if( abs( mf1(3)) .lt. epsf) then
        !if( abs( g( j)) .lt. epsf) then
          nr = nr + 1
          roots( nr) = v(3)
          !roots( nr) = v( j)
        end if
      end do
      nroot = nr
      allocate( f( nroot))
      !call eph_gen_sigmafm( eph_evalp( ist, ip) + roots( 1:nroot), nroot, ip, ist, ist, f)
      call eph_gen_sigmaein( ek + roots( 1:nroot), nroot, phfreq, etherm, f)
      if( eph_scroot_writefile) then
        write(*,*) "ROOTS"
        do i = 1, nroot
          write(*,'(i,2f23.16,i,g13.6)') i, roots( i), mroots( i), abs( f( i) - roots( i))
        end do
        write(*,*) "POLES"
        do i = 1, np
          write(*,'(i,2f23.16,i)') i, poles( i), mpoles( i)
        end do
      end if
      np = 0
      do i = 1, nroot
        if( abs( f( i) - roots( i)) .lt. epsf) then
          np = np + 1
          roots( np) = roots( i)
          f( np) = f( i) - roots( i)
        end if
      end do
      nroot = np
      if( np .gt. 0 .and. eph_scroot_writefile) then
        write(*,'(5i5)', advance='no') ip, ist, nv, croot_tri%nv, nroot
        do i = 1, np
          write(*,'("|   ",2f13.6,"(",g13.6,")   |")', advance='no') roots( i)*h2ev, abs( f( i))*h2ev
        end do
        write(*,*)
      end if

      deallocate( poles, mroots, mpoles, f) 
      return
    end subroutine eph_scroot

end module mod_eph_scroot
