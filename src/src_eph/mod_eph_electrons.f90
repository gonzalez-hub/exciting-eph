module mod_eph_electrons
  use mod_eph_variables

  implicit none

! methods
  contains

    ! interpolation wrapper
    subroutine eph_electrons_interpolate( qset, eval, evec, evalin, serial)
      use mod_wannier,             only: wf_fst, wf_lst, wf_kset
      use mod_wannier_interpolate, only: wfint_init, wfint_eval, wfint_transform
      type( k_set), intent( in)          :: qset
      real(8), intent( out)              :: eval( eph_fst:eph_lst, qset%nkpt)
      complex(8), optional, intent( out) :: evec( eph_fst:eph_lst, eph_fst:eph_lst, qset%nkpt)
      real(8), optional, intent( in)     :: evalin( wf_fst:wf_lst, wf_kset%nkpt)
      logical, optional, intent( in)     :: serial

      integer :: iq, ist, iv(3)
      real(8) :: v1(3), v2(3)
      logical :: ser

      ser = .false.
      if( present( serial)) ser = serial

      if( eph_einstein) then
        do iq = 1, qset%nkpt
          v1 = qset%vkl(:,iq)
          call r3ws( 1.d-3, qset%bvec, v1, iv)
          call r3mv( qset%bvec, v1, v2)
          eval(1,iq) = eph_einstein_eldisp*norm2( v2)**2 - eph_efermi
        end do
        return
      end if

      if( eph_debye) then
        eval = eph_debye_elengy
        return
      end if

      if( present( evalin)) then
        if( ser) then
          call wfint_init( qset, evalin=evalin, serial=serial)
        else
          call wfint_init( qset, evalin=evalin)
        end if
      else
        if( ser) then
          call wfint_init( qset, serial=serial)
        else
          call wfint_init( qset)
        end if
      end if

      eval = wfint_eval - eph_efermi

      if( eph_scissor .gt. 1.d-16) then
        do iq = 1, qset%nkpt
          do ist = eph_fst, eph_lst
            if( eval( ist, iq) .gt. 0.d0) exit
          end do
          if( ist .le. eph_lst) eval( ist:eph_lst, iq) = eval( ist:eph_lst, iq) + eph_scissor
        end do
      end if

      if( present( evec)) evec = wfint_transform
      return
    end subroutine eph_electrons_interpolate

    subroutine eph_electrons_efermi( ef, evalin)
      use mod_eigenvalue_occupancy, only: occmax
      use mod_charge_and_moment,    only: chgval
      use mod_optkgrid,             only: getoptkgrid
      use mod_wannier_interpolate
      use mod_wannier_util,         only: wfutil_energy_extremal
      real(8), intent( out)          :: ef
      real(8), optional, intent( in) :: evalin( wf_fst:wf_lst, wf_kset%nkpt)

      integer :: nvm, ndiv(3), iq
      real(8) :: opt, ropt, vpl(3), v(3)
      type( k_set) :: kset

      call getoptkgrid( 2.d-2, eph_kset_el%bvec, ndiv, opt, ropt)
      call generate_k_vectors( kset, eph_kset_el%bvec, ndiv, (/0.d0, 0.d0, 0.d0/), .true., uselibzint=.false.)
      if( present( evalin)) then
        call wfint_init( kset, evalin=evalin)
      else
        call wfint_init( kset)
      end if
      call wfint_interpolate_occupancy( usetetra=.true.)
      ef = wfint_efermi

      do nvm = 1, wf_nwf
        if( wfint_eval( nvm, 1) .gt. ef) exit
      end do
      nvm = nvm-1
      ! either no valence or no conduction bands wannierized
      if( (nvm .eq. 0) .or. (nvm .eq. wf_nwf)) return
      ! metal
      if( maxval( wfint_eval( nvm, :)) .gt. minval( wfint_eval( nvm+1, :))) return

      iq = maxloc( wfint_eval( nvm, :), 1)
      vpl = wfint_kset%vkl( :, iq)
      call wfutil_energy_extremal( vpl, nvm, ef, v)
      ef = dble( ceiling( ef*1.d6))*1.d-6
      
      call delete_k_vectors( kset)
      return
    end subroutine eph_electrons_efermi

    subroutine eph_electrons_output
      use m_getunit
      use modmpi
      integer :: j, iq, ist, un

      if( mpiglobal%rank .ne. 0) return

      call getunit( un)
      open( un, file='EPH_ELDISP.DAT', action='write', form='formatted')
      do ist = eph_fst, eph_lst
        do iq = 1, eph_pset%nkpt
          if( allocated( eph_pset_dpp1d)) write( un, '(g20.10e3)', advance='no') eph_pset_dpp1d( iq)
          write( un, '(g20.10e3)', advance='no') eph_evalp( ist, iq)
          if( allocated( eph_evalp_qp)) then
            do j = 1, eph_nqpmax
              if( abs( aimag( eph_evalp_qp( j, ist, iq))) .gt. 1.d-6) then
                write( un, '(g20.10e3)', advance='no') dble( eph_evalp_qp( j ,ist, iq))
              else
                write( un, '(g20.10e3)', advance='no') 0.d0
              end if
            end do
          end if
          write( un, '(3f13.6)') eph_pset%vkl( :, iq)
        end do
        write( un, *)
      end do
      close( un)

      if( allocated( eph_pset_dpp1d)) then
        call getunit( un)
        open( un, file='EPH_ELVTXLINES.DAT', action='write', form='formatted')
        do iq = 1, eph_pset_nvp1d
          write( un, '(g20.10,3f16.10,3x,a)') eph_pset_dvp1d( iq), eph_pset_verts( iq)%coord, trim( eph_pset_verts( iq)%label)
        end do
        close( un)
      end if

      return
    end subroutine eph_electrons_output

    subroutine eph_electrons_dos( nfreq, freqs, dos)
      integer, intent( in)  :: nfreq
      real(8), intent( in)  :: freqs( nfreq)
      real(8), intent( out) :: dos( nfreq)

      complex(8), allocatable :: ftdos(:,:)

      allocate( ftdos( nfreq, eph_nst))
      call my_tetra_int_delta( eph_tetra, eph_qpset%nkpt, eph_nst, eph_evalqp, nfreq, freqs, 1, ftdos)
      dos = sum( dble( ftdos), dim=2)
      deallocate( ftdos)

      return      
    end subroutine eph_electrons_dos
end module mod_eph_electrons
