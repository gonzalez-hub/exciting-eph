module mod_eph_sigma
  use mod_eph_variables
  use mod_eph_helper
  use mod_polygamma

  implicit none

! methods
  contains

    subroutine eph_sigma_get( ip, sigma)
      integer, intent( in)     :: ip
      complex(8), intent( out) :: sigma(:,:,:,:)

      logical :: success
      integer :: i, it, ist, jst, nst, next
      real(8) :: df

      real(8), allocatable :: fext(:), sigext(:,:) 
      complex(8), allocatable :: sig0(:,:,:)

      sigma = zzero
      nst = eph_nst
      if( .not. eph_diag) nst = nst*(nst+1)/2
      allocate( sig0( eph_nfreq, nst, eph_ntemp))

      ! first, try to read imaginary part from file
      call eph_sigma_fm0fromfile( ip, eph_nfreq, eph_freqs( :, ip), eph_ntemp, eph_temps, nst, sig0, success)

      ! calculate imaginary part when it could not be read
      if( .not. success) then
        call eph_sigma_fm0( eph_nfreq, eph_freqs( :, ip), eph_ntemp, eph_temps, sig0, diag=eph_diag)
        call eph_sigma_fm0tofile( ip, eph_nfreq, eph_freqs( :, ip), eph_ntemp, eph_temps, nst, sig0, success)
      end if

      ! apply Lorentzian broadening and get real part
      next = 10
      allocate( fext( eph_nfreq+2*next))
      fext( (next+1):(next+eph_nfreq)) = eph_freqs( :, ip)
      df = eph_freqs( 2, ip) - eph_freqs( 1, ip)
      do i = 0, next-1
        fext( next-i) = eph_freqs( 1, ip) - 2.d0**i*df
      end do
      df = eph_freqs( eph_nfreq, ip) - eph_freqs( eph_nfreq-1, ip)
      do i = 0, next-1
        fext( eph_nfreq+next+i+1) = eph_freqs( eph_nfreq, ip) + 2.d0**i*df
      end do
      
#ifdef USEOMP
!$omp parallel default( shared) private( i, ist, sigext)
#endif
      allocate( sigext( eph_nfreq+2*next, 4))
#ifdef USEOMP
!$omp do
#endif
      do it = 1, eph_ntemp
        i = 0
        do ist = eph_fst, eph_lst
          i = i + 1
          sigext = 0.d0
          sigext( (next+1):(next+eph_nfreq), 1) = dble( sig0( :, i, it))
          ! Lorentzian broadening
          if( eph_eta .gt. 1.d-6) then
            call convolute( eph_nfreq+2*next, fext, sigext(:,1), 1, 2.d0*eph_eta, 1, sigext(:,3))
            sigext(:,1) = sigext(:,3)
          end if
          ! apply Hilbert transform
          call eph_kramkron( eph_nfreq+2*next, fext, sigext(:,1), sigext(:,3))
          if( eph_diag) then
            sigma( :, ist, 1, it) = cmplx( sigext( (next+1):(next+eph_nfreq), 3), sigext( (next+1):(next+eph_nfreq), 1), 8)
          else
            sigma( :, ist, ist, it) = cmplx( sigext( (next+1):(next+eph_nfreq), 3), sigext( (next+1):(next+eph_nfreq), 1), 8)

            do jst = ist+1, eph_lst
              i = i + 1
              sigext = 0.d0
              sigext( (next+1):(next+eph_nfreq), 1) = dble( sig0( :, i, it))
              sigext( (next+1):(next+eph_nfreq), 2) = aimag( sig0( :, i, it))
              ! Lorentzian broadening
              if( eph_eta .gt. 1.d-6) then
                call convolute( eph_nfreq+2*next, fext, sigext(:,1), 1, 2.d0*eph_eta, 1, sigext(:,3))
                sigext(:,1) = sigext(:,3)
                call convolute( eph_nfreq+2*next, fext, sigext(:,2), 1, 2.d0*eph_eta, 1, sigext(:,3))
                sigext(:,2) = sigext(:,3)
              end if
              ! apply Hilbert transform
              call eph_kramkron( eph_nfreq+2*next, fext, sigext(:,1), sigext(:,3))
              call eph_kramkron( eph_nfreq+2*next, fext, sigext(:,2), sigext(:,4))
              sigma( :, ist, jst, it) = cmplx( sigext( (next+1):(next+eph_nfreq), 3) - sigext( (next+1):(next+eph_nfreq), 2), &
                                               sigext( (next+1):(next+eph_nfreq), 1) + sigext( (next+1):(next+eph_nfreq), 4), 8)
              sigma( :, jst, ist, it) = cmplx( sigext( (next+1):(next+eph_nfreq), 3) + sigext( (next+1):(next+eph_nfreq), 2), &
                                               sigext( (next+1):(next+eph_nfreq), 1) - sigext( (next+1):(next+eph_nfreq), 4), 8)
            end do
          end if
        end do
      end do
#ifdef USEOMP
!$omp end do
#endif
      deallocate( sigext)
#ifdef USEOMP
!$omp end parallel
#endif
      
      deallocate( sig0, fext)
      return      
    end subroutine eph_sigma_get

    subroutine eph_sigma_fm0( nfreq, freqs, ntemp, temps, sig0, diag)
      use mod_atoms, only: natmtot
      integer, intent( in)           :: nfreq                   ! number of frequencies 
      real(8), intent( in)           :: freqs( nfreq)           ! frequencies at which self-energy is calculated
      integer, intent( in)           :: ntemp                   ! number of temperatures
      real(8), intent( in)           :: temps( ntemp)           ! temperatures (in K) for which self-energy is calculated
      complex(8), intent( out)       :: sig0(:,:,:)             ! auxiliary self-energy
      logical, optional, intent( in) :: diag                    ! diagonal elements only

      integer :: imode, iqp, ist, jst, nst, it, i, j
      integer :: ns, nms, m1, m2
      real(8) :: nq, fpq, t0, t1
      logical :: diagonal
      real(8), allocatable :: evalp(:,:)
      complex(8), allocatable :: v(:), ephmat(:,:,:,:), zint(:,:,:,:)

      real(8), external :: occ

      diagonal = .false.
      if( present( diag)) diagonal = diag

      nst = eph_nst
      if( .not. diagonal) nst = nst*(nst+1)/2

      do ns = ceiling( eph_nmode/3.d0), ceiling( eph_nmode/3.d0)
        nms = eph_nmode - 3*(ns - 1)
        if( allocated( v)) deallocate( v)
        allocate( v( nst))
        if( allocated( ephmat)) deallocate( ephmat)
        allocate( ephmat( nst*ntemp, eph_fst:eph_lst, nms, eph_qpset%nkpt), stat=i)
        if( allocated( zint)) deallocate( zint)
        allocate( zint( nfreq, nst*ntemp, eph_fst:eph_lst, nms), stat=j)
        if( (i .eq. 0) .and. (j .eq. 0)) exit
      end do
      allocate( evalp( nms, eph_qpset%nkpt))

      ! get auxiliary self-energy Sigma0

      sig0 = zzero
      call timesec( t0)
      do j = 1, ns
        m1 = (j-1)*nms + 1
        m2 = j*nms
#ifdef USEOMP
!$omp parallel default( shared) private( iqp, i, imode, ist, jst, v, it, nq, fpq)
!$omp do
#endif
        do iqp = 1, eph_qpset%nkpt
          evalp(:,iqp) = -abs( eph_phfreq( m1:m2, eph_iqp2iq( iqp)))
          do imode = m1, m2
            do ist = eph_fst, eph_lst
              if( diagonal) then
                v = dble( eph_ephmat( ist, :, imode, iqp)*conjg( eph_ephmat( ist, :, imode, iqp)))
              else
                i = 1
                do jst = eph_fst, eph_lst
                  v(i:(i+eph_lst-jst)) = conjg( eph_ephmat( ist, jst, imode, iqp))*eph_ephmat( ist, jst:eph_lst, imode, iqp)
                  i = i + eph_lst-jst+1
                end do
              end if
              i = 1
              do it = 1, ntemp
                nq  = occ( -evalp( imode-m1+1, iqp)/(kboltz*temps( it)), -1)
                fpq = occ(    eph_evalqp( ist, iqp)/(kboltz*temps( it)),  1)
                ephmat( i:it*nst, ist, imode-m1+1, iqp) = (-pi*(1.d0 - fpq + nq))*v
                i = i + nst
              end do
            end do
          end do
        end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
        call my_tetra_int_deltadiff( eph_tetra, eph_qpset%nkpt, eph_nst, eph_evalqp, nms, evalp, nfreq, freqs, &
               nst*ntemp, resc=zint, matc=ephmat)
        do i = 1, nms
          do ist = eph_fst, eph_lst
            sig0 = sig0 + reshape( zint(:,:,ist,i), (/nfreq, nst, ntemp/))
          end do
        end do

#ifdef USEOMP
!$omp parallel default( shared) private( iqp, i, imode, ist, jst, v, it, nq, fpq)
!$omp do
#endif
        do iqp = 1, eph_qpset%nkpt
          evalp(:,iqp) = abs( eph_phfreq( m1:m2, eph_iqp2iq( iqp)))
          do imode = m1, m2
            do ist = eph_fst, eph_lst
              if( diagonal) then
                v = dble( eph_ephmat( ist, :, imode, iqp)*conjg( eph_ephmat( ist, :, imode, iqp)))
              else
                i = 1
                do jst = eph_fst, eph_lst
                  v(i:(i+eph_lst-jst)) = conjg( eph_ephmat( ist, jst, imode, iqp))*eph_ephmat( ist, jst:eph_lst, imode, iqp)
                  i = i + eph_lst-jst+1
                end do
              end if
              i = 1
              do it = 1, ntemp
                nq  = occ(  evalp( imode-m1+1, iqp)/(kboltz*temps( it)), -1)
                fpq = occ(    eph_evalqp( ist, iqp)/(kboltz*temps( it)),  1)
                ephmat( i:it*nst, ist, imode-m1+1, iqp) = (-pi*(fpq + nq))*v
                i = i + nst
              end do
            end do
          end do
        end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
        call my_tetra_int_deltadiff( eph_tetra, eph_qpset%nkpt, eph_nst, eph_evalqp, nms, evalp, nfreq, freqs, &
               nst*ntemp, resc=zint, matc=ephmat)
        do i = 1, nms
          do ist = eph_fst, eph_lst
            sig0 = sig0 + reshape( zint(:,:,ist,i), (/nfreq, nst, ntemp/))
          end do
        end do
      end do
      call timesec( t1)
      write(*,'("T SIGMA:",f13.6)') t1-t0

      deallocate( evalp, ephmat, zint, v)
      return
    end subroutine eph_sigma_fm0

    subroutine eph_sigma_genim( nfreq, freqs, ntemp, temps, sigim)
      use mod_atoms, only: natmtot
      integer, intent( in)  :: nfreq                                  ! number of frequencies 
      real(8), intent( in)  :: freqs( nfreq)                          ! frequencies at which self-energy is caluclated
      integer, intent( in)  :: ntemp                                  ! number of temperatures
      real(8), intent( in)  :: temps( ntemp)                          ! temperatures (in K) for which self-energy is calculated
      real(8), intent( out) :: sigim( nfreq, eph_fst:eph_lst, ntemp)  ! imaginary part of self-energy

      integer :: imode, iqp, ist, it, i, j
      integer :: ns, nms, m1, m2
      real(8) :: nq, fpq, v( eph_nst), t0, t1
      real(8), allocatable :: evalp(:,:), ephmat(:,:,:,:), rint(:,:,:,:)

      real(8), external :: occ

      do ns = natmtot, natmtot
        nms = 3*(natmtot - ns + 1)
        if( allocated( ephmat)) deallocate( ephmat)
        allocate( ephmat( eph_nst*ntemp, eph_fst:eph_lst, nms, eph_qpset%nkpt), stat=i)
        if( allocated( rint)) deallocate( rint)
        allocate( rint( nfreq, eph_nst*ntemp, eph_fst:eph_lst, nms), stat=j)
        if( (i .eq. 0) .and. (j .eq. 0)) exit
      end do
      allocate( evalp( nms, eph_qpset%nkpt))

      ! get imaginary part of Sigma
      sigim = 0.d0
      call timesec( t0)
      do j = 1, ns
        m1 = (j-1)*nms + 1
        m2 = j*nms
        write(*,*) j, ns, m1, m2
#ifdef USEOMP
!$omp parallel default( shared) private( iqp, i, imode, ist, v, it, nq, fpq)
!$omp do
#endif
        do iqp = 1, eph_qpset%nkpt
          evalp(:,iqp) = -abs( eph_phfreq( m1:m2, eph_iqp2iq( iqp)))
          do imode = m1, m2
            do ist = eph_fst, eph_lst
              v = dble( eph_ephmat( ist, :, imode, iqp)*conjg( eph_ephmat( ist, :, imode, iqp)))
              i = 1
              do it = 1, ntemp
                nq  = occ( -evalp( imode-m1+1, iqp)/(kboltz*temps( it)), -1)
                fpq = occ(    eph_evalqp( ist, iqp)/(kboltz*temps( it)),  1)
                ephmat( i:it*eph_nst, ist, imode-m1+1, iqp) = (-pi*(1.d0 - fpq + nq))*v
                i = i + eph_nst
              end do
            end do
          end do
        end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
        call my_tetra_int_deltadiff( eph_tetra, eph_qpset%nkpt, eph_nst, eph_evalqp, nms, evalp, nfreq, freqs, &
               eph_nst*ntemp, resr=rint, matr=ephmat)
        do i = 1, nms
          do ist = eph_fst, eph_lst
            sigim = sigim + reshape( rint(:,:,ist,i), (/nfreq, eph_nst, ntemp/))
          end do
        end do

#ifdef USEOMP
!$omp parallel default( shared) private( iqp, i, imode, ist, v, it, nq, fpq)
!$omp do
#endif
        do iqp = 1, eph_qpset%nkpt
          evalp(:,iqp) = abs( eph_phfreq( m1:m2, eph_iqp2iq( iqp)))
          do imode = m1, m2
            do ist = eph_fst, eph_lst
              v = dble( eph_ephmat( ist, :, imode, iqp)*conjg( eph_ephmat( ist, :, imode, iqp)))
              i = 1
              do it = 1, ntemp
                nq  = occ(  evalp( imode-m1+1, iqp)/(kboltz*temps( it)), -1)
                fpq = occ(    eph_evalqp( ist, iqp)/(kboltz*temps( it)),  1)
                ephmat( i:it*eph_nst, ist, imode-m1+1, iqp) = (-pi*(fpq + nq))*v
                i = i + eph_nst
              end do
            end do
          end do
        end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
        call my_tetra_int_deltadiff( eph_tetra, eph_qpset%nkpt, eph_nst, eph_evalqp, nms, evalp, nfreq, freqs, &
               eph_nst*ntemp, resr=rint, matr=ephmat)
        do i = 1, nms
          do ist = eph_fst, eph_lst
            sigim = sigim + reshape( rint(:,:,ist,i), (/nfreq, eph_nst, ntemp/))
          end do
        end do
      end do
      call timesec( t1)
      write(*,'("T SIGMA:",f13.6)') t1-t0

      deallocate( evalp, ephmat, rint)
      return
    end subroutine eph_sigma_genim

    subroutine eph_sigma_fm0tofile( ip, nfreq, freqs, ntemp, temps, nst, sig0, success)
      use m_getunit
      use modmpi
      integer, intent( in)    :: ip, nfreq, ntemp, nst
      real(8), intent( in)    :: freqs( nfreq), temps( ntemp)
      complex(8), intent( in) :: sig0( nfreq, nst, ntemp)
      logical, intent( out)   :: success

      character(256) :: fname
      integer :: un, recl, ios
      integer :: fst, lst, nppt
      real(8) :: vpl(3)

      success = .true.
      if( mpiglobal%rank .ne. 0) return

      ! copy variables because some of them may have different sizes (e.g. 4-byte integer instead of 8-byte integer)
      nppt  = eph_pset%nkpt
      fst   = eph_fst
      lst   = eph_lst
      vpl   = eph_pset%vkl(:,ip)
      write( fname, '("EPH_SIGMA_Q",i2.2,"_T",2(i4.4,"_"),i4.4,".OUT")'), eph_qset%ngridk(1), &
        nint( eph_temps(1)), nint( eph_temps(eph_ntemp)), nint( dble( eph_temps(eph_ntemp)-eph_temps(1))/( 1.d-10+dble(eph_ntemp-1)))
      inquire( iolength=recl) nppt, nfreq, fst, lst, nst, ntemp, vpl, freqs, temps, sig0
      call getunit( un)
      open( un, file=trim( fname), action='write', form='unformatted', access='direct', recl=recl, iostat=ios)
      if( ios .ne. 0) then
        success = .false.
        return
      end if
      write( un, rec=ip) nppt, nfreq, fst, lst, nst, ntemp, vpl, freqs, temps, sig0
      close( un)
    end subroutine eph_sigma_fm0tofile

    subroutine eph_sigma_fm0fromfile( ip, nfreq, freqs, ntemp, temps, nst, sig0, success)
      use m_getunit
      use modmpi
      integer, intent( in)     :: ip, nfreq, ntemp, nst
      real(8), intent( out)    :: freqs( nfreq), temps( ntemp)
      complex(8), intent( out) :: sig0( nfreq, nst, ntemp)
      logical, intent( out)    :: success

      character(256) :: fname
      integer :: un, recl, ios
      integer :: nppt, nf, fst, lst, ns, nt
      real(8) :: vpl(3)

      success = .true.

      write( fname, '("EPH_SIGMA_Q",i2.2,"_T",2(i4.4,"_"),i4.4,".OUT")'), eph_qset%ngridk(1), &
        nint( eph_temps(1)), nint( eph_temps(eph_ntemp)), nint( dble( eph_temps(eph_ntemp)-eph_temps(1))/( 1.d-10+dble(eph_ntemp-1)))
      inquire( file=trim( fname), exist=success)
      if( .not. success) return
      inquire( iolength=recl) nppt, nf, fst, lst, ns, nt, vpl, freqs, temps, sig0
      call getunit( un)
      open( un, file=trim( fname), action='read', form='unformatted', access='direct', recl=recl)
      read( un, rec=ip, iostat=ios) nppt, nf, fst, lst, ns, nt, vpl, freqs, temps, sig0
      if( ios .ne. 0) then
        success = .false.
        return
      end if

      if( nppt .ne. eph_pset%nkpt) then
        write(*,*)
        write(*,'("Error (eph_sigma_fm0fromfile): Current number of p-points and number in file differ.")')
        success = .false.
      end if
      if( nf .ne. nfreq) then
        write(*,*)
        write(*,'("Error (eph_sigma_fm0fromfile): Current number of frequencies and number in file differ.")')
        success = .false.
      end if
      if( (fst .ne. eph_fst) .or. (lst .ne. eph_lst)) then
        write(*,*)
        write(*,'("Error (eph_sigma_fm0fromfile): Current band range and band range in file differ.")')
        success = .false.
      end if
      if( ns .ne. nst) then
        write(*,*)
        write(*,'("Error (eph_sigma_fm0fromfile): Current matrix size and matrix size in file differ.")')
        success = .false.
      end if
      if( nt .ne. ntemp) then
        write(*,*)
        write(*,'("Error (eph_sigma_fm0fromfile): Current number of temperatures and number in file differ.")')
        success = .false.
      end if
      if( norm2( vpl - eph_pset%vkl( :, ip)) .gt. 1.d-6) then
        write(*,*)
        write(*,'("Error (eph_sigma_fm0fromfile): Current p-point and p-point in file differ.")')
        success = .false.
      end if
      close( un)
      
      return
    end subroutine eph_sigma_fm0fromfile

    subroutine eph_sigma_imtofile( ip, nfreq, freqs, ntemp, temps, sigim, success)
      use m_getunit
      use modmpi
      integer, intent( in)  :: ip, nfreq, ntemp
      real(8), intent( in)  :: freqs( nfreq), temps( ntemp)
      real(8), intent( in)  :: sigim( nfreq, eph_fst:eph_lst, ntemp)
      logical, intent( out) :: success

      character(256) :: fname
      integer :: un, recl, ios
      integer :: fst, lst, nppt
      real(8) :: vpl(3)

      success = .true.
      if( mpiglobal%rank .ne. 0) return

      ! copy variables because some of them may have different sizes (e.g. 4-byte integer instead of 8-byte integer)
      nppt  = eph_pset%nkpt
      fst   = eph_fst
      lst   = eph_lst
      vpl   = eph_pset%vkl(:,ip)
      write( fname, '("EPH_SIGMA_Q",i2.2,"_T",2(i4.4,"_"),i4.4,".OUT")'), eph_qset%ngridk(1), &
        nint( eph_temps(1)), nint( eph_temps(eph_ntemp)), nint( dble( eph_temps(eph_ntemp)-eph_temps(1))/( 1.d-10+dble(eph_ntemp-1)))
      inquire( iolength=recl) nppt, nfreq, fst, lst, ntemp, vpl, freqs, temps, sigim
      call getunit( un)
      open( un, file=trim( fname), action='write', form='unformatted', access='direct', recl=recl, iostat=ios)
      if( ios .ne. 0) then
        success = .false.
        return
      end if
      write( un, rec=ip) nppt, nfreq, fst, lst, ntemp, vpl, freqs, temps, sigim
      close( un)
    end subroutine eph_sigma_imtofile

    subroutine eph_sigma_imfromfile( ip, nfreq, freqs, ntemp, temps, sigim, success)
      use m_getunit
      use modmpi
      integer, intent( in)  :: ip, nfreq, ntemp
      real(8), intent( out) :: freqs( nfreq), temps( ntemp)
      real(8), intent( out) :: sigim( nfreq, eph_fst:eph_lst, ntemp)
      logical, intent( out) :: success

      character(256) :: fname
      integer :: un, recl, ios
      integer :: nppt, nf, fst, lst, nt
      real(8) :: vpl(3)

      success = .true.

      write( fname, '("EPH_SIGMA_Q",i2.2,"_T",2(i4.4,"_"),i4.4,".OUT")'), eph_qset%ngridk(1), &
        nint( eph_temps(1)), nint( eph_temps(eph_ntemp)), nint( dble( eph_temps(eph_ntemp)-eph_temps(1))/( 1.d-10+dble(eph_ntemp-1)))
      inquire( file=trim( fname), exist=success)
      if( .not. success) return
      inquire( iolength=recl) nppt, nf, fst, lst, nt, vpl, freqs, temps, sigim
      call getunit( un)
      open( un, file=trim( fname), action='read', form='unformatted', access='direct', recl=recl)
      read( un, rec=ip, iostat=ios) nppt, nf, fst, lst, nt, vpl, freqs, temps, sigim
      if( ios .ne. 0) then
        success = .false.
        return
      end if

      if( nppt .ne. eph_pset%nkpt) then
        write(*,*)
        write(*,'("Error (eph_sigma_imfromfile): Current number of p-points and number in file differ.")')
        success = .false.
      end if
      if( nf .ne. nfreq) then
        write(*,*)
        write(*,'("Error (eph_sigma_imfromfile): Current number of frequencies and number in file differ.")')
        success = .false.
      end if
      if( (fst .ne. eph_fst) .or. (lst .ne. eph_lst)) then
        write(*,*)
        write(*,'("Error (eph_sigma_imfromfile): Current band range and band range in file differ.")')
        success = .false.
      end if
      if( nt .ne. ntemp) then
        write(*,*)
        write(*,'("Error (eph_sigma_imfromfile): Current number of temperatures and number in file differ.")')
        success = .false.
      end if
      if( norm2( vpl - eph_pset%vkl( :, ip)) .gt. 1.d-6) then
        write(*,*)
        write(*,'("Error (eph_sigma_imfromfile): Current p-point and p-point in file differ.")')
        success = .false.
      end if
      close( un)
      
      return
    end subroutine eph_sigma_imfromfile

    subroutine eph_sigma_genim_naive( nfreq, freqs, ntemp, temps, sig)
      use mod_atoms, only: natmtot
      integer, intent( in)           :: nfreq            ! number of frequencies 
      real(8), intent( in)           :: freqs( nfreq)    ! frequencies at which self-energy is caluclated
      integer, intent( in)           :: ntemp            ! number of temperatures
      real(8), intent( in)           :: temps( ntemp)    ! temperatures (in K) for which self-energy is calculated
      complex(8), intent( out)       :: sig(:,:,:,:)     ! imaginary part of self-energy

      integer :: iqp, imode, ist, jst, it, iw, i, j
      real(8) :: t0, t1, nq, fpq
      complex(8) :: z

      complex(8), allocatable :: ephmat(:,:,:), wgt(:,:)

      real(8), external :: occ

      if( eph_diag) then
        i = 1; j = 1
      else
        i = eph_fst; j = eph_lst
      end if

      call timesec( t0)
      allocate( ephmat( eph_nst, j-i+1, eph_qpset%nkpt), wgt( nfreq, eph_qpset%nkpt))

      sig = zzero
      do it = 1, ntemp
        do imode = 1, eph_nmode
          do ist = eph_fst, eph_lst
            do iqp = 1, eph_qpset%nkpt
              fpq = occ( eph_evalqp( ist, iqp)/(kboltz*temps( it)),  1)
              nq  = occ( abs( eph_phfreq( imode, eph_iqp2iq( iqp)))/(kboltz*temps( it)), -1)
              do iw = 1, nfreq
                z = (1.d0 - fpq + nq)/(freqs(iw) - eph_evalqp( ist, iqp) - eph_phfreq( imode, eph_iqp2iq( iqp)) + eph_eta*zi)
                z = z + (fpq + nq)/(freqs(iw) - eph_evalqp( ist, iqp) + eph_phfreq( imode, eph_iqp2iq( iqp)) + eph_eta*zi)
                wgt(iw,iqp) = z*eph_qpset%wkpt( iqp)
              end do
              if( eph_diag) then
                ephmat(:,1,iqp) = eph_ephmat( ist, :, imode, iqp)*conjg( eph_ephmat( ist, :, imode, iqp))
              else
                do jst = i, j
                  ephmat(:,jst,iqp) = eph_ephmat( ist, :, imode, iqp)*conjg( eph_ephmat( ist, jst, imode, iqp))
                end do
              end if
            end do
            call zgemm( 'n', 't', nfreq, eph_nst*(j-i+1), eph_qpset%nkpt, zone, &
                   wgt, nfreq, &
                   ephmat, eph_nst*(j-i+1), zone, &
                   sig(:,:,:,it), nfreq)
          end do
        end do
      end do
            
      deallocate( ephmat, wgt)
      call timesec( t1)
      write(*,'("T SIGMA:",f13.6)') t1-t0

      return
    end subroutine eph_sigma_genim_naive

    subroutine eph_sigma_findqp( nfreq, freqs, sigma, nqpmax, nqp, qp)
      use mod_eph_sfun
      use mod_schlessinger
      use mod_polynomials
      use m_linalg
      use m_getunit
      integer, intent( in)     :: nfreq
      real(8), intent( in)     :: freqs( nfreq)
      complex(8), intent( in)  :: sigma( nfreq)
      integer, intent( in)     :: nqpmax
      integer, intent( out)    :: nqp
      complex(8), intent( out) :: qp(2,nqpmax)

      ! parameters
      integer :: no    ! interpolation order
      integer :: itmax ! maximum interations for self-consistent solution
      integer :: ni    ! #interpolation points
      real(8) :: sa    ! area unter spectral function in which QPs are searched (sa \in (0,1])
      real(8) :: mix   ! mixing parameter for self-consistent solution

      ! working variables
      real(8) :: xr(2)

      real(8), allocatable :: sfun(:), fi(:)
      complex(8), allocatable :: si(:)

      ! auxilliary variables
      integer :: i, j, k, un
      real(8) :: r1
      complex(8) :: z0(2)

      integer, allocatable :: i1d(:)
      real(8), allocatable :: r1d(:), r2d(:,:)
      complex(8), allocatable :: c1d(:)

      !******************************************
      !* SET PARAMETERS
      !******************************************
      itmax = 1000
      sa    = 0.999d0
      ni    = 400
      mix   = 0.2d0
      ! use even number for correct long-range behavior
      if( mod( ni, 2) .eq. 0) then
        no = ni
      else
        no = ni - 1
      end if

      !******************************************
      !* FIND SEARCH INTERVAL
      !******************************************
      allocate( sfun( nfreq), r1d( nfreq), r2d( 3, nfreq))
      call eph_sfun_gen( nfreq, freqs, sigma, sfun)
      call fderiv( -1, nfreq, freqs, abs( sfun), r1d, r2d)
      r1d = r1d/r1d( nfreq)

      r1 = (1.d0 - sa)*0.5d0
      i = minloc( abs( r1d-r1), 1)
      if( (r1d( i) .gt. r1) .or. (i .eq. nfreq)) i = i-1
      xr(1) = (r1 - r1d( i))*(freqs( i+1) - freqs( i))/(r1d( i+1) - r1d( i)) + freqs( i)

      r1 = (1.d0 + sa)*0.5d0
      i = minloc( abs( r1d-r1), 1)
      if( (r1d( i) .ge. r1) .or. (i .eq. nfreq)) i = i-1
      xr(2) = (r1 - r1d( i))*(freqs( i+1) - freqs( i))/(r1d( i+1) - r1d( i)) + freqs( i)
      deallocate( r2d)

      !******************************************
      !* GENERATE INTERPOLATION GRID
      !******************************************
      ! we use the spectral function to set up an interpolation frequency grid
      ! that is densly sampled where the peaks are
      r1 = 0.5d0
      sfun = r1*sfun*dble( ni-1)/r1d( nfreq) + (1.d0-r1)*dble( ni-1)/(xr(2) - xr(1))
      do i = 1, 4
        call savitzkygolay( nfreq, freqs, sfun, 1, 3, 0, r1d)
        sfun = r1d
      end do
      allocate( fi( ni), r2d( ni, 2))
      call spacing( xr(1), xr(2), ni, r2d(:,1), 1)
      call interp1d( nfreq, freqs, sfun, ni, r2d(:,1), r2d(:,2), 'linear')
      call spacingFromDensity( ni, r2d(:,1), r2d(:,2), ni, fi)
      !call spacing( xr(1), xr(2), ni, fi, 1)
      deallocate( r1d, r2d, sfun)
      
      !******************************************
      !* INTERPOLATE SELF ENERGY
      !******************************************
      allocate( si( ni), r2d( ni, 2))
      call interp1d( nfreq, freqs, dble(  sigma), ni, fi, r2d(:,1), 'spline')
      call interp1d( nfreq, freqs, aimag( sigma), ni, fi, r2d(:,2), 'spline')
      si = cmplx( r2d(:,1), r2d(:,2), 8)
      deallocate( r2d)

      !******************************************
      !* FIND PEAKS IN SPECTRAL FUNCTION
      !******************************************
      allocate( sfun( ni), r2d( ni, 2), i1d( ni))
      call eph_sfun_gen( ni, fi, si, sfun)
      call savitzkygolay( ni, fi, sfun, 1, 20, 1, r2d(:,1))
      call savitzkygolay( ni, fi, r2d(:,1), 1, 20, 1, r2d(:,2))
      k = 0
      do i = 1, ni-1
        if( (r2d(i,1)*r2d(i+1,1) .lt. 0.d0) .and. (r2d(i,2) .lt. 0.d0)) then
          k = k + 1
          i1d(k) = i
        end if
      end do
      call getunit( un)
      open( un, file='SFUN.DAT', action='write', form='formatted')
      do i = 1, ni
        write(un,'(100g20.10)') fi(i), si(i), sfun(i), r2d(i,:)
      end do
      close( un)
      deallocate( r2d)

      !******************************************
      !* INITIALIZE RATIONAL INTERPOLANT
      !******************************************
      call schlessinger_init( ni, cmplx( fi, 0.d0, 8), si)

      !******************************************
      !* FIND SELF-CONSISTENT SOLUTIONS OF S(z) = z
      !******************************************
      allocate( c1d( k))
      nqp = 0
      do j = 1, k
        z0(1) = cmplx( 0.5d0*(fi( i1d(j)) + fi( i1d(j)+1)), 0.d0, 8)
        do i = 1, itmax
          call schlessinger_interp( no, 1, z0(1), z0(2))
          if( abs( z0(1)-z0(2)) .lt. 1.d-10) exit
          z0(1) = (1.d0 - mix)*z0(1) + mix*z0(2)
        end do
        if( i .gt. itmax) cycle
        if( minval( abs( c1d(1:nqp) - z0(2))) .gt. 1.d-8) then
          nqp = nqp + 1
          c1d(nqp) = z0(2)
        end if
      end do
      qp = zzero
      if( nqp .eq. 0) return
      qp(1,1:nqp) = c1d(1:nqp)
      deallocate( i1d, c1d)

      !******************************************
      !* DETERMINE QP STRENGTH
      !******************************************
      k = 5
      allocate( r1d(-k:k), r2d(3,-k:k), c1d(-k:k))
      r1 = 1.d-7
      do i = -k, k
        r1d(i) = dble( i)*r1
      end do
      do i = 1, nqp
        ! derivative in parallel to real axis
        call schlessinger_interp( no, 2*k+1, qp(1,i)+cmplx( r1d, 0.d0, 8), c1d)
        call spline( 2*k+1, r1d, 1, dble( c1d), r2d)
        r1 = r2d(1,0)
        call spline( 2*k+1, r1d, 1, aimag( c1d), r2d)
        qp(2,i) = cmplx( r1, r2d(1,0), 8)
        ! derivative in parallel to imaginary axis
        call schlessinger_interp( no, 2*k+1, qp(1,i)+cmplx( 0.d0, r1d, 8), c1d)
        call spline( 2*k+1, r1d, 1, dble( c1d), r2d)
        r1 = r2d(1,0)
        call spline( 2*k+1, r1d, 1, aimag( c1d), r2d)
        ! average of both
        qp(2,i) = qp(2,i) + cmplx( r2d(1,0), -r1, 8)
        qp(2,i) = 0.5d0*qp(2,i)
        ! QP strength factor
        qp(2,i) = zone/(zone - qp(2,i))
      end do
      deallocate( r1d, r2d, c1d)

      !******************************************
      !* SORT RESULTS
      !******************************************
      allocate( i1d( nqp))
      call sortidx( 3, -dble( qp(2,:)), i1d)
      qp(:,1:nqp) = qp(:,i1d)
      deallocate( i1d)

      do i = 1, nqp
        write(*,'(i,4f13.6)') i, qp(:,i)
      end do
 
      call getunit( un)
      open( un, file='GAPPROX.DAT', action='write', form='formatted')
      do i = 1, ni
        !call schlessinger_interp( n, 1, cmplx( freqs(i:i), 0.d0, 8), z0(1))
        z0(1) = zzero
        do j = 1, nqp
          z0(1) = z0(1) + qp(2,j)/(fi(i) - qp(1,j))
        end do
        write(un,'(9g20.10)') fi(i), zone/(fi(i) - si(i)), si(i), z0(1)
      end do
      close( un)
      return
    end subroutine eph_sigma_findqp

    subroutine eph_sigma_output( ip, nfreq, freqs, fst, lst, ntemp, temps, sigma)
      use m_getunit
      use modmpi
      integer, intent( in)    :: ip, nfreq, fst, lst, ntemp
      real(8), intent( in)    :: freqs( nfreq), temps( ntemp)
      complex(8), intent( in) :: sigma(:,:,:,:)

      integer :: ist, jst, i, j,  iw, it, un
      character(256) :: fname

      if( mpiglobal%rank .ne. 0) return
      
      if( eph_diag) then
        i = 1; j = 1
      else
        i = fst; j = lst
      end if

      do it = 1, ntemp
        write( fname, '("EPH_SIGMA_P",i3.3,"_Q",i2.2,"_T",i4.4,"_S",i3.3,".DAT")'), ip, eph_qset%ngridk(1), nint( temps( it)), nint( eph_eta*h2ev*1.d3)
        call getunit( un)
        open( un, file=trim( fname), action='write', form='formatted')
        do jst = i, j
          do iw = 1, nfreq
            write( un, '(g20.10e3)', advance='no') freqs( iw)
            do ist = fst, lst
              write( un, '(2g20.10e3)', advance='no') sigma( iw, ist, jst, it)
            end do
            write( un, *)
          end do
          write( un, *)
          write( un, *)
        end do
        close( un)
      end do
      return
    end subroutine eph_sigma_output

    subroutine eph_gen_sigmaein( freqs, nfreq, phfreq, etherm, sigma)
      use mod_atoms, only: natmtot
      complex(8), intent( in)  :: freqs( nfreq)
      integer, intent( in)     :: nfreq
      real(8), intent( in)     :: phfreq, etherm
      complex(8), intent( out) :: sigma( nfreq)

      integer :: iw
      real(8) :: bose

      bose = phfreq/etherm
      if( bose .gt. 7.d2) then
        bose = 0.d0
      else if( abs( bose) .lt. 1.d-300) then
        bose = 1.d300
      else
        bose = 1.d0/(exp( bose) - 1.d0)  ! Bose-Einstein occupation of phonon
      end if

      do iw = 1, nfreq
        sigma( iw) = -zi*pi*(bose + 0.5d0) &
                     + 0.5d0*digamma( 0.5d0 + zi*(phfreq - ( freqs( iw)))/(twopi*etherm)) &
                     - 0.5d0*digamma( 0.5d0 - zi*(phfreq + ( freqs( iw)))/(twopi*etherm))
      end do
      return
    end subroutine eph_gen_sigmaein

    subroutine eph_sigma_dw(qset, eval, ephmat, ntemp, temps, sigma)
      use mod_atoms, only: natmtot
      use mod_wannier_variables, only: wf_nrpt, wf_nwf, wf_rvec, wf_fst, wf_lst, wf_nst
      type( k_set), intent( in)    :: qset                                                ! integration grid (q-grid)
      real(8), intent( in)         :: eval(wf_nwf)                                        ! Energy eigenvalue at specific k
      complex(8), intent( in)      :: ephmat(wf_nwf, wf_nwf, eph_nmode, qset%nkptnr)      ! DW matrix elements
      integer, intent( in)         :: ntemp                                               ! number of temperatures
      real(8), intent( in)         :: temps( ntemp)                                       ! temperatures (in K) for which self-energy is calculated
      complex(8), intent( out)        :: sigma( eph_nst, ntemp)                                  ! DW self-energy

      ! Auxiliary variables
      integer :: iq, ip, imode, im, jn, ist, it
      real (8) :: nq, t1, t2, weight
      real(8), allocatable :: evalp(:,:)

      real(8), external :: occ

      weight = 1 / qset%nkptnr

      allocate(evalp( eph_nmode, qset%nkptnr))

      do it = 1, ntemp
        do iq = 1, qset%nkptnr
          evalp(:,iq) = abs( eph_phfreq(:,iq))
          do imode = 1, eph_nmode
            nq  = occ( evalp( imode, iq)/(kboltz*temps( it)), -1)
            t2 = 2*nq + 1
            do jn = eph_fst, eph_lst
              do im = eph_fst, eph_lst
                if (eval(im) /= eval(jn)) then 
                  t1 = 1 / (eval(jn) - eval(im))
                  sigma(jn, it) = sigma(jn, it) - weight*t1*t2*ephmat(im, jn, imode, iq)  
                end if
              end do
            end do
          end do
        end do
      end do    

      return
    end subroutine eph_sigma_dw

    subroutine eph_sigma_dwtofile( ip, ntemp, temps, nst, sigma, success)
      use m_getunit
      use modmpi
      integer, intent( in)    :: ip, ntemp, nst
      real(8), intent( in)    :: temps( ntemp)
      complex(8), intent( in) :: sigma( nst, ntemp)
      logical, intent( out)   :: success

      character(256) :: fname
      integer :: un, recl, ios
      integer :: fst, lst, nppt
      real(8) :: vpl(3)

      success = .true.
      if( mpiglobal%rank .ne. 0) return

      ! copy variables because some of them may have different sizes (e.g. 4-byte integer instead of 8-byte integer)
      nppt  = eph_pset%nkpt
      fst   = eph_fst
      lst   = eph_lst
      vpl   = eph_pset%vkl(:,ip)
      write( fname, '("EPH_SIGMA_DW_Q",i2.2,"_T",2(i4.4,"_"),i4.4,".OUT")'), eph_qset%ngridk(1), &
        nint( eph_temps(1)), nint( eph_temps(eph_ntemp)), nint( dble( eph_temps(eph_ntemp)-eph_temps(1))/( 1.d-10+dble(eph_ntemp-1)))
      inquire( iolength=recl) nppt, fst, lst, nst, ntemp, vpl, temps, sigma
      call getunit( un)
      open( un, file=trim( fname), action='write', form='unformatted', access='direct', recl=recl, iostat=ios)
      if( ios .ne. 0) then
        success = .false.
        return
      end if
      write( un, rec=ip) nppt, fst, lst, nst, ntemp, vpl, temps, sigma
      close( un)
    end subroutine eph_sigma_dwtofile

    subroutine eph_sigma_dwfromfile( ip, ntemp, temps, nst, sigma, success)
      use m_getunit
      use modmpi
      integer, intent( in)     :: ip, ntemp, nst
      real(8), intent( out)    :: temps( ntemp)
      complex(8), intent( out) :: sigma( nst, ntemp)
      logical, intent( out)    :: success

      character(256) :: fname
      integer :: un, recl, ios
      integer :: nppt, nf, fst, lst, ns, nt
      real(8) :: vpl(3)

      success = .true.

      write( fname, '("EPH_SIGMA_DW_Q",i2.2,"_T",2(i4.4,"_"),i4.4,".OUT")'), eph_qset%ngridk(1), &
        nint( eph_temps(1)), nint( eph_temps(eph_ntemp)), nint( dble( eph_temps(eph_ntemp)-eph_temps(1))/( 1.d-10+dble(eph_ntemp-1)))
      inquire( file=trim( fname), exist=success)
      if( .not. success) return
      inquire( iolength=recl) nppt, fst, lst, ns, nt, vpl, temps, sigma
      call getunit( un)
      open( un, file=trim( fname), action='read', form='unformatted', access='direct', recl=recl)
      read( un, rec=ip, iostat=ios) nppt, fst, lst, ns, nt, vpl, temps, sigma
      if( ios .ne. 0) then
        success = .false.
        return
      end if

      if( nppt .ne. eph_pset%nkpt) then
        write(*,*)
        write(*,'("Error (eph_sigma_dwfromfile): Current number of p-points and number in file differ.")')
        success = .false.
      end if
      if( (fst .ne. eph_fst) .or. (lst .ne. eph_lst)) then
        write(*,*)
        write(*,'("Error (eph_sigma_dwfromfile): Current band range and band range in file differ.")')
        success = .false.
      end if
      if( ns .ne. nst) then
        write(*,*)
        write(*,'("Error (eph_sigma_dwfromfile): Current matrix size and matrix size in file differ.")')
        success = .false.
      end if
      if( nt .ne. ntemp) then
        write(*,*)
        write(*,'("Error (eph_sigma_dwfromfile): Current number of temperatures and number in file differ.")')
        success = .false.
      end if
      if( norm2( vpl - eph_pset%vkl( :, ip)) .gt. 1.d-6) then
        write(*,*)
        write(*,'("Error (eph_sigma_dwfromfile): Current p-point and p-point in file differ.")')
        success = .false.
      end if
      close( un)
      
      return
    end subroutine eph_sigma_dwfromfile
    
    !subroutine eph_findqp( nfreq, freqs, gfun, nqp, qp, irange)
    !  use mod_schlessinger
    !  use mod_complex_root
    !  use m_linalg
    !  use m_getunit 
    !  use m_plotmat
    !  integer, intent( in)                  :: nfreq         ! #frequencies
    !  real(8), intent( in)                  :: freqs( nfreq) ! frequencies
    !  complex(8), intent( in)               :: gfun( nfreq)  ! Green's function at frequencies
    !  integer, intent( out)                 :: nqp           ! number of found quasi particles
    !  complex(8), allocatable, intent( out) :: qp(:,:)       ! found quasi particles
    !  real(8), optional, intent( in)        :: irange(2)     ! inverse life-time range 

    !  ! parameters
    !  integer :: intord, itmax, ninterp
    !  real(8) :: eps, mspace, meps, searchint
    !  logical, parameter :: writefile = .true.

    !  integer :: nroots, npoles
    !  real(8) :: xrange(2), yrange(2), xnorm, ynorm
    !  integer, allocatable :: mroots(:), mpoles(:), idx(:)
    !  real(8), allocatable :: finterp(:)
    !  complex(8), allocatable :: ginterp(:), roots(:), poles(:)

    !  integer :: i, it, m, n, un, iv(3)
    !  real(8) :: r1, err
    !  complex(8) :: z1, z2, z3, zv1(3), zv2(3), zv3(3), zv4(3)
    !  real(8), allocatable :: r1d(:), r2d(:,:)
    !  complex(8), allocatable :: z1d1(:), z1d2(:), z2d1(:,:), z2d2(:,:), tmp(:)
    !  character(256) :: fname

    !  !******************************************
    !  !*           FIND SEARCH REGION           *
    !  !******************************************
    !  searchint = 0.99d0

    !  allocate( r1d( nfreq), r2d( 3, nfreq))
    !  call fderiv( -1, nfreq, freqs, aimag( gfun), r1d, r2d)
    !  r1d = r1d/r1d( nfreq)
    !  r1 = (1.d0 - searchint)*0.5d0
    !  i = minloc( abs( r1d-r1), 1)
    !  if( r1d( i) .gt. r1) i = i-1
    !  xrange(1) = (r1 - r1d( i))*(freqs( i+1) - freqs( i))/(r1d( i+1) - r1d( i)) + freqs( i)
    !  r1 = (1.d0 + searchint)*0.5d0
    !  i = minloc( abs( r1d-r1), 1)
    !  if( r1d( i) .gt. r1) i = i-1
    !  xrange(2) = (r1 - r1d( i))*(freqs( i+1) - freqs( i))/(r1d( i+1) - r1d( i)) + freqs( i)
    !  write(*,'(2f13.6)') xrange*h2ev
    !  deallocate( r1d, r2d)

    !  if( present( irange)) then
    !    yrange = irange
    !  else
    !    yrange = (/-0.1d0, 0.5d0/)*(xrange(2)-xrange(1))
    !  end if

    !  !******************************************
    !  !*     INTERPOLATE GREEN'S FUNCTION       *
    !  !******************************************
    !  ninterp = 400
    !  allocate( finterp( ninterp), ginterp( ninterp), r2d( ninterp, 2))
    !  m = maxloc( abs( aimag( gfun)), 1)
    !  call spacing( xrange(1), xrange(2), ninterp, finterp, 2, c=freqs( m), w=(xrange(2)-xrange(1))/5.d0)
    !  call interp1d( nfreq, freqs, dble(  gfun), ninterp, finterp, r2d(:,1), 'linear')
    !  call interp1d( nfreq, freqs, aimag( gfun), ninterp, finterp, r2d(:,2), 'linear')
    !  ginterp = cmplx( r2d(:,1), r2d(:,2), 8)
    !  deallocate( r2d)

    !  ! normalize data
    !  allocate( r1d( ninterp), r2d( 3, ninterp))
    !  xnorm = xrange(2) - xrange(1)
    !  finterp = finterp/xnorm
    !  xrange = xrange/xnorm
    !  yrange = yrange/xnorm
    !  call fderiv( -1, ninterp, finterp, aimag( ginterp), r1d, r2d)
    !  ynorm = r1d( ninterp)/searchint
    !  ginterp = ginterp/ynorm
    !  deallocate( r1d, r2d)
    !  call writematlab( reshape( cmplx( finterp, 0.d0, 8), (/ninterp, 1/)), 'ABSCISSA')
    !  call writematlab( reshape( ginterp, (/ninterp, 1/)), 'GFUN')

    !  !******************************************
    !  !*       FIND RATIONAL INTERPOLANT        *
    !  !******************************************
    !  m = 2
    !  allocate( z2d1( 0:m, 3), z2d2( m, 2), tmp( ninterp))
    !  call aaaratfun( ninterp, cmplx( finterp, 0.d0, 8), ginterp, m, z2d1(:,1), z2d1(:,2), z2d1(:,3), tmp, z2d2(:,1), z2d2(:,2), err)
    !  write(*,*) err
    !  deallocate( z2d1, z2d2)

    !  itmax = 10
    !  eps = 0.01d0

    !  r1  = 1.d0
    !  err = 1.d0
    !  it = 0

    !  allocate( z1d2( ninterp))
    !  do while( (r1 .gt. eps) .and. (it .lt. itmax))
    !    it = it + 1
    !    n = it
    !    m = n - 1
    !    if( allocated( z1d1)) deallocate( z1d1)
    !    if( allocated( z2d1)) deallocate( z2d1)
    !    if( allocated( z2d2)) deallocate( z2d2)
    !    allocate( z1d1( m+n+1), z2d1( ninterp, m+n+1), z2d2( m+n+1, ninterp))
    !    do i = 0, m
    !      z2d1( :, i+1) = cmplx( finterp**i, 0.d0, 8)
    !    end do
    !    do i = 1, n
    !      z2d1( :, i+1+m) = -finterp**i*ginterp
    !    end do
    !    call zpinv( z2d1, z2d2)
    !    z1d1 = matmul( z2d2, ginterp)
    !    z2d2(1,:) = matmul( z2d1, z1d1) - ginterp                                    ! p(xi) - yi*q(xi)
    !    z2d2(2,:) = matmul( -z2d1(:,(m+2):(m+n+1)), z1d1( (m+2):(m+n+1))) + ginterp  ! yi*q(xi)
    !    z2d2(2,:) = ginterp*z2d2(1,:)/z2d2(2,:)                                      ! p(xi)/q(xi) - y(i)
    !    r1 = sqrt( sum( abs( z2d2(2,:))**2)/sum( abs( ginterp)**2))
    !    if( r1 .lt. err) then
    !      err = r1
    !      z1d2 = z2d2(2,:)
    !    end if
    !    write(*,'(i,f13.6)') it, r1
    !  end do
    !  call getunit( un)
    !  open( un, file='INTERPOLANT.TMP', action='write', form='formatted')
    !  do i = 1, ninterp
    !    write( un, '(7g20.10)') finterp(i), ginterp(i), z1d2(i)+ginterp(i), tmp(i)
    !  end do
    !  close( un)
    !  stop

    !  !******************************************
    !  !*     SETUP SCHLESSINGER INTERPOLANT     *
    !  !******************************************
    !  intord = ninterp-2
    !  call schlessinger_init( ninterp, cmplx( finterp, 0.d0, 8), ginterp, c=zzero)
    !  deallocate( z1d1, z1d2, z2d1, z2d2)
    !  call eph_sigmafm2d_tofile2( xrange, yrange, 1001, 1001, 1, 20, 'CROOT.SIG', intord=intord)
    !  stop

    !  !******************************************
    !  !*      FIND POLES IN COMPLEX PLANE       *
    !  !******************************************
    !  ! (were searching for the roots of the inverse Green's function)
    !  itmax  = 20
    !  eps    = 1.d-6
    !  mspace = min( xrange(2)-xrange(1), yrange(2)-yrange(1))/50.d0
    !  meps   = eps
    !  
    !  ! extend search region a bit to find poles close to boundaries
    !  xrange(1) = xrange(1) - (xrange(2) - xrange(1))*0.1d0
    !  xrange(2) = xrange(2) + (xrange(2) - xrange(1))*0.1d0

    !  ! generate triangluation of search region
    !  call croot_destroy
    !  call croot_genmesh( xrange, yrange, mspace, pattern='honeycomb', eps=meps)
    !  
    !  ! find possible solutions
    !  r1 = 1.d128
    !  it = 0
    !  do while( (r1 .gt. max( meps, sqrt( meps))) .and. (it .lt. itmax))
    !    it = it + 1
    !    n = croot_tri%nv
    !    if( allocated( croot_e)) call croot_refine
    !    if( writefile) then
    !      write(*,*) it, croot_tri%nv
    !      write( fname, '("TRIANGULATION",i2.2)') it
    !      call triangulation_tofile( croot_tri, fname)
    !    end if
    !    if( croot_tri%nv .ne. n) then
    !      ! the mesh was refinded. only add function values at new mesh points
    !      if( allocated( z1d1)) deallocate( z1d1)
    !      allocate( z1d1( croot_tri%nv))
    !      z1d1( 1:n) = croot_f
    !      call schlessinger_interp( intord, croot_tri%nv-n+1, croot_tri%verts( (n+1):croot_tri%nv), z1d1( (n+1):croot_tri%nv))
    !      do i = n+1, croot_tri%nv
    !        if( abs( z1d1( i)) .gt. 1.d-32) then
    !          z1d1( i) = zone/z1d1( i)
    !        else if( isnan( dble( z1d1( i))) .or. isnan( aimag( z1d1( i)))) then
    !          z1d1( i) = zzero
    !        else
    !          z1d1( i) = cmplx( 1.d32, 0.d0, 8)
    !        end if
    !      end do
    !      if( allocated( croot_f)) deallocate( croot_f)
    !      allocate( croot_f( croot_tri%nv))
    !      croot_f = z1d1
    !    else
    !      ! add all function values to the mesh
    !      if( allocated( croot_f)) deallocate( croot_f)
    !      allocate( croot_f( croot_tri%nv))
    !      call schlessinger_interp( intord, croot_tri%nv, croot_tri%verts, croot_f)
    !      do i = 1, croot_tri%nv
    !        if( abs( croot_f( i)) .gt. 1.d-32) then
    !          croot_f( i) = zone/croot_f( i)
    !        else if( isnan( dble( croot_f( i))) .or. isnan( aimag( croot_f( i)))) then
    !          croot_f( i) = zzero
    !        else
    !          croot_f( i) = cmplx( 1.d32, 0.d0, 8)
    !        end if
    !      end do
    !      croot_f = zone/croot_f
    !    end if
    !    ! find candidate edges
    !    call croot_quadrants
    !    call croot_edges
    !    if( .not. allocated( croot_e)) then
    !      ! no edge was found. enlarge search region
    !      xrange = xrange*sqrt( 2.d0)
    !      yrange = yrange*sqrt( 2.d0)
    !      call croot_destroy
    !      call croot_genmesh( xrange, yrange, mspace, pattern='honeycomb', eps=meps)
    !    else
    !      if( writefile) then
    !        write(*,'("CANDIDATE EDGES:",100i)') croot_e
    !        write( fname, '("CROOT",i2.2)') it
    !        call croot_tofile( fname)
    !      end if
    !      ! construct candidate regions around candidate edges
    !      call croot_regions
    !      r1 = croot_emin
    !      if( writefile) then
    !        write(*,'("CANDIDATE REGIONS:",100i)') size( croot_l, 2)
    !        write(*,'("EMIN:",g23.16)') r1
    !        write( fname, '("CROOT",i2.2)') it
    !        call croot_tofile( fname)
    !      end if
    !    end if
    !    if( writefile) then
    !      write(*,*) '===================================================='
    !    end if
    !  end do
    !  if( allocated( z1d1)) deallocate( z1d1)
    !  !if( writefile) call eph_sigmafm2d_tofile2( xrange, yrange, 1001, 1001, ip, ist, 'CROOT.SIG', intord=intord)

    !  ! get possible roots and poles and their multiplicities
    !  call croot_verify( roots, mroots, nroots, poles, mpoles, npoles, eps=eps)

    !  ! further refine found solutions
    !  itmax = 100
    !  nqp = 0
    !  do i = 1, nroots
    !    z1 = roots( i)
    !    iv(1) = minloc( abs( croot_tri%verts - z1), 1)
    !    zv1(1) = croot_tri%verts( iv(1))
    !    croot_tri%verts( iv(1)) = cmplx( 1.d128, 1.d128, 8)
    !    iv(2) = minloc( abs( croot_tri%verts - z1), 1)
    !    zv1(2) = croot_tri%verts( iv(2))
    !    croot_tri%verts( iv(2)) = cmplx( 1.d128, 1.d128, 8)
    !    iv(3) = minloc( abs( croot_tri%verts - z1), 1)
    !    zv1(3) = croot_tri%verts( iv(3))
    !    croot_tri%verts( iv(1)) = zv1(1)
    !    croot_tri%verts( iv(2)) = zv1(2)

    !    zv2 = croot_f( iv)
    !    call sortidx( 3, abs( zv2), iv)
    !    zv1 = zv1( iv)
    !    zv2 = zv2( iv)
    !    zv3 = zv2
    !    
    !    it = 0
    !    if( writefile) then
    !      write(*,*) '----------------------------'
    !    end if
    !    do while( it .lt. itmax)
    !      it = it + 1
    !      zv4 = cmplx( 1.d32, 0.d0, 8)
    !      if( abs( zv1(3) - zv1(2)) .gt. 1.d-32) zv4(1) = (zv3(3) - zv3(2))/(zv1(3) - zv1(2))
    !      if( abs( zv1(1) - zv1(3)) .gt. 1.d-32) zv4(2) = (zv3(1) - zv3(3))/(zv1(1) - zv1(3))
    !      if( abs( zv1(2) - zv1(1)) .gt. 1.d-32) zv4(3) = (zv3(2) - zv3(1))/(zv1(2) - zv1(1))
    !      z2 = (zv4(3) - zv4(2))/(zv1(1) - zv1(3))
    !      z3 = zv4(1) + zv4(2) - zv4(3)
    !      z1 = sqrt( z3*z3 - 4.d0*zv3(3)*z2)
    !      if( abs( z3 + z1) .gt. abs( z3 - z1)) then
    !        z1 = z3 + z1
    !      else
    !        z1 = z3 - z1
    !      end if
    !      zv1(1) = zv1(2)
    !      zv1(2) = zv1(3)
    !      zv1(3) = zv1(3) - 2.d0*zv3(3)/z1
    !      zv3(1) = zv3(2)
    !      zv3(2) = zv3(3)
    !      call schlessinger_interp( intord, 1, zv1(3), zv3(3))
    !      if( abs( zv3(3)) .gt. 1.d-32) then
    !        zv3(3) = zone/zv3(3)
    !      else if( isnan( dble( zv3(3))) .or. isnan( aimag( zv3(3)))) then
    !        zv3(3) = zzero
    !      else
    !        zv3(3) = cmplx( 1.d32, 0.d0, 8)
    !      end if
    !      if( writefile) then
    !        write(*,'(i,2f23.16,g13.6,3x,3g13.6)') it, zv1(3), abs( zv3(3)), abs( zv1(1)-zv1(2)), abs( zv1(2)-zv1(3)), abs( zv1(3)-zv1(1))
    !      end if
    !      if( (max( abs( zv1(1)-zv1(2)), max( abs( zv1(2)-zv1(3)), abs( zv1(3)-zv1(1)))) .lt. meps) .and. (abs( zv3(3)) .lt. eps)) exit
    !    end do
    !    if( (abs( zv3(3)) .lt. eps) .and. (minval( abs( roots( 1:nqp) - zv1(3))) .gt. eps)) then
    !      nqp = nqp + 1
    !      roots( nqp) = zv1(3)
    !    end if
    !  end do
    !  if( nqp .eq. 0) return

    !  !******************************************
    !  !*        FIND STRENGTH OF POLES          *
    !  !******************************************
    !  allocate( z2d1( ninterp, nqp), z2d2( nqp, ninterp), z1d1( nqp), idx( nqp))
    !  do i = 1, nqp
    !    z2d1(:,i) = zone/(finterp - roots( i))
    !  end do
    !  call zpinv( z2d1, z2d2)
    !  z1d1 = matmul( z2d2, ginterp)
    !  call sortidx( nqp, -abs( z1d1), idx)
    !  write(*,*) sqrt( sum( abs( matmul( z2d1, z1d1) - ginterp)**2)/sum( abs( ginterp)**2))
    !  roots( 1:nqp) = roots( idx)
    !  mroots( 1:nqp) = mroots( idx)
    !  z1d1 = z1d1( idx)
    !  if( writefile) then
    !    write(*,*) "ROOTS"
    !    do i = 1, nqp
    !      write(*,'(i,2f23.16,i,3g13.6)') i, roots( i), mroots( i), z1d1( i)
    !    end do
    !    write(*,*) "ROOTS"
    !    do i = 1, nqp
    !      write(*,'(i,2f23.16,i,3g13.6)') i, roots( i)*xnorm*h2ev, mroots( i), z1d1( i)*ynorm
    !    end do
    !    write(*,*) "POLES"
    !    do i = 1, npoles
    !      write(*,'(i,2f23.16,i)') i, poles( i), mpoles( i)
    !    end do
    !  end if

    !  !******************************************
    !  !*            WRITE RESULTS               *
    !  !******************************************
    !  if( allocated( qp)) deallocate( qp)
    !  allocate( qp( 2, nqp))
    !  do i = 1, nqp
    !    qp( 1, i) = roots( i)
    !    qp( 2, i) = z1d1( i)
    !  end do
    !  
    !  deallocate( z2d1, z2d2, z1d1, idx)
    !  if( allocated( roots)) deallocate( roots)
    !  if( allocated( mroots)) deallocate( mroots)
    !  if( allocated( poles)) deallocate( poles)
    !  if( allocated( mpoles)) deallocate( mpoles)
    !  call croot_destroy
    !  return
    !end subroutine eph_findqp
    
    !subroutine eph_find_scroots( ip, ist, nr, r, range)
    !  use mod_schlessinger
    !  use mod_complex_root
    !  use m_linalg
    !  integer, intent( in)                  :: ip, ist
    !  integer, intent( out)                 :: nr
    !  complex(8), allocatable, intent( out) :: r(:)
    !  real(8), optional, intent( in)        :: range(2,2)

    !  ! parameters
    !  integer :: intord, itmax, ninterp
    !  real(8) :: eps, mspace, meps
    !  logical, parameter :: writefile = .true.

    !  integer :: i, it, nv, nroots, npoles, iv(3), m, n
    !  real(8) :: xrange(2), yrange(2), res
    !  complex(8) :: z(2), fz, zv1(3), zv2(3), mf1(3), mf2(3), mf3, mw
    !  character(256) :: fname

    !  integer, allocatable :: mroots(:), mpoles(:), idx(:)
    !  real(8), allocatable :: winterp(:), sigiinterp(:), sigrinterp(:), c(:,:), ci(:,:), pq(:)
    !  complex(8), allocatable :: f(:), roots(:), poles(:), p(:), q(:)

    !  if( allocated( r)) deallocate( r)
    !  nr = 0

    !  ! define the search region
    !  if( present( range)) then
    !    xrange = range(:,1)
    !    yrange = range(:,2)
    !  else
    !    xrange = (/-1.d0, 1.d0/)*1.d-2
    !    yrange = (/-1.d0, 1.d0/)*1.d-2
    !  end if

    !  ! interpolate the self energy
    !  nv = 1
    !  ninterp = 1000*nv
    !  allocate( winterp( ninterp), sigiinterp( ninterp), sigrinterp( ninterp), idx( ninterp/nv))
    !  call spacing( xrange(1)+eph_evalp( ist, ip), xrange(2)+eph_evalp( ist, ip), ninterp, winterp, 1)
    !  !call splineinterp( eph_fset%nomeg, eph_fset%freqs, aimag( eph_sigmafm( ist, :, ip)), ninterp, winterp, sigiinterp)
    !  !call splineinterp( eph_fset%nomeg, eph_fset%freqs, dble(  eph_sigmafm( ist, :, ip)), ninterp, winterp, sigrinterp)
    !  !call eph_kramkron( ninterp, winterp, sigiinterp, sigrinterp)
    !  call interp1d( eph_fset%nomeg, eph_fset%freqs, eph_specfun_band( :, ist, ip), ninterp, winterp, sigrinterp, 'spline')
    !  sigrinterp = sigrinterp/maxval( sigrinterp)

    !  m = 6
    !  n = 8
    !  allocate( c( ninterp, m+n+1), ci( m+n+1, ninterp), pq(m+n+1))
    !  do i = 0, m
    !    c( :, i+1) = (winterp-eph_evalp( ist, ip))**i
    !  end do
    !  do i = 1, n
    !    c( :, i+1+m) = -(winterp-eph_evalp( ist, ip))**i*sigrinterp
    !  end do
    !  call rpinv( c, ci)
    !  pq = matmul( ci, sigrinterp)
    !  write(*,*) 'POLYNOMIAL P'
    !  do i = 0, m
    !    write(*,'(SP,g26.16,"x^",SS,i1.1)',advance='no') pq( i+1), i
    !  end do
    !  write(*,*)
    !  write(*,*) 'POLYNOMIAL Q'
    !  do i = 1, n
    !    write(*,'(SP,g26.16,"x^",SS,i1.1)',advance='no') pq( i+1+m), i
    !  end do
    !  write(*,*) 'RELATIVE ERROR'
    !  write(*,'(f26.16)') sqrt( sum( (matmul( c, pq)-sigrinterp)**2)/dble( ninterp-1))
    !  stop

    !  ! set up Schlessinger point interpolation
    !  it = 1
    !  do i = 1, ninterp/nv
    !    idx(i) = it + (i-1)*nv
    !  end do
    !  call schlessinger_init( ninterp/nv, cmplx( winterp( idx)-eph_evalp( ist, ip), 0.d0, 8), cmplx( sigrinterp( idx), 0.d0, 8), c=zzero)
    !  deallocate( winterp, sigiinterp, sigrinterp, idx)
    !  !call schlessinger_init( eph_fset%nomeg, cmplx( eph_fset%freqs-eph_evalp( ist, ip), 0.d0, 8), eph_sigmafm( ist, :, ip), c=zzero)

    !  ! first try naive approach (can only find one solution)
    !  itmax  = 100
    !  eps    = 1.d-10
    !  !intord = 1000-1
    !  intord = 400
    !  !call eph_sigmafm2d_tofile2( xrange, yrange, 1001, 1001, ip, ist, 'SPECFUN.2D', intord=intord)
    !  !return

    !  !allocate( p( 0:intord/2), q( 0:intord/2))
    !  !call schlessinger_getpoly( intord, p, q)
    !  !mw = p( minloc( abs( p - zone), 1))
    !  !do i = 0, intord/2
    !  !  write(*,'(i,2(2g20.10,5x))') i, p(i)/mw, q(i)/mw
    !  !end do
    !  call schlessinger_interp( intord, 1, (/zzero/), z(1))
    !  it = 0
    !  z(2) = cmplx( 1.d128, 1.d128, 8)
    !  do while( (abs( z(1)-z(2)) .ge. eps) .and. (it .lt. itmax))
    !    it = it + 1
    !    z(2) = z(1)
    !    call schlessinger_interp( intord, 1, z(2), z(1))
    !  end do
    !  !if( abs( z(1)-z(2)) .lt. eps) then
    !  !  nr = 1
    !  !  allocate( r( nr))
    !  !  r(1) = z(1)
    !  !  return
    !  !end if

    !  ! if we are here the naive approach didn't work out
    !  ! use a complex root finding algorithm instead
    !  itmax  = 20
    !  mspace = min( xrange(2)-xrange(1), yrange(2)-yrange(1))/20.d0
    !  meps   = eps
    !  
    !  ! generate triangluation of search region
    !  call croot_destroy
    !  call croot_genmesh( xrange, yrange, mspace, pattern='honeycomb', eps=meps)
    !  
    !  ! find possible solutions
    !  res = 1.d128
    !  it = 0
    !  do while( (res .gt. max( meps, sqrt( meps))) .and. (it .lt. itmax))
    !    it = it + 1
    !    nv = croot_tri%nv
    !    if( allocated( croot_e)) call croot_refine
    !    if( writefile) then
    !      write(*,*) it, croot_tri%nv
    !      write( fname, '("TRIANGULATION",i2.2)') it
    !      call triangulation_tofile( croot_tri, fname)
    !    end if
    !    if( croot_tri%nv .ne. nv) then
    !      ! the mesh was refinded. only add function values at new mesh points
    !      if( allocated( f)) deallocate( f)
    !      allocate( f( croot_tri%nv))
    !      f( 1:nv) = croot_f
    !      call schlessinger_interp( intord, croot_tri%nv-nv+1, croot_tri%verts( (nv+1):croot_tri%nv), f( (nv+1):croot_tri%nv))
    !      ! Sigma(z) - z = 0 is what we want to solve
    !      !f( (nv+1):croot_tri%nv) = f( (nv+1):croot_tri%nv) - croot_tri%verts( (nv+1):croot_tri%nv)
    !      if( allocated( croot_f)) deallocate( croot_f)
    !      allocate( croot_f( croot_tri%nv))
    !      croot_f = f
    !    else
    !      ! add all function values to the mesh
    !      if( allocated( croot_f)) deallocate( croot_f)
    !      allocate( croot_f( croot_tri%nv))
    !      call schlessinger_interp( intord, croot_tri%nv, croot_tri%verts, croot_f)
    !      ! Sigma(z) - z = 0 is what we want to solve
    !      !croot_f = croot_f - croot_tri%verts
    !    end if
    !    ! find candidate edges
    !    call croot_quadrants
    !    call croot_edges
    !    if( .not. allocated( croot_e)) then
    !      ! no edge was found. enlarge search region
    !      xrange = xrange*sqrt( 2.d0)
    !      yrange = yrange*sqrt( 2.d0)
    !      call croot_destroy
    !      call croot_genmesh( xrange, yrange, mspace, pattern='honeycomb', eps=meps)
    !    else
    !      if( writefile) then
    !        write(*,'("CANDIDATE EDGES:",100i)') croot_e
    !        write( fname, '("CROOT",i2.2)') it
    !        call croot_tofile( fname)
    !      end if
    !      ! construct candidate regions around candidate edges
    !      call croot_regions
    !      res = croot_emin
    !      if( writefile) then
    !        write(*,'("CANDIDATE REGIONS:",100i)') size( croot_l, 2)
    !        write(*,'("EMIN:",g23.16)') res
    !        write( fname, '("CROOT",i2.2)') it
    !        call croot_tofile( fname)
    !      end if
    !    end if
    !    if( writefile) then
    !      write(*,*) '===================================================='
    !    end if
    !  end do
    !  if( allocated( f)) deallocate( f)
    !  if( writefile) call eph_sigmafm2d_tofile2( xrange, yrange, 1001, 1001, ip, ist, 'CROOT.SIG', intord=intord)

    !  ! get possible roots and poles and their multiplicities
    !  call croot_verify( roots, mroots, nroots, poles, mpoles, npoles, eps=eps)

    !  ! further refine and order found solutions
    !  itmax = 1000
    !  do i = 1, nroots
    !    fz = roots( i)
    !    iv(1) = minloc( abs( croot_tri%verts - fz), 1)
    !    zv1(1) = croot_tri%verts( iv(1))
    !    croot_tri%verts( iv(1)) = cmplx( 1.d128, 1.d128, 8)
    !    iv(2) = minloc( abs( croot_tri%verts - fz), 1)
    !    zv1(2) = croot_tri%verts( iv(2))
    !    croot_tri%verts( iv(2)) = cmplx( 1.d128, 1.d128, 8)
    !    iv(3) = minloc( abs( croot_tri%verts - fz), 1)
    !    zv1(3) = croot_tri%verts( iv(3))
    !    croot_tri%verts( iv(1)) = zv1(1)
    !    croot_tri%verts( iv(2)) = zv1(2)

    !    zv2 = croot_f( iv)
    !    call sortidx( 3, abs( zv2), iv)
    !    zv1 = zv1( iv)
    !    zv2 = zv2( iv)
    !    mf1 = zv2
    !    
    !    it = 0
    !    if( writefile) then
    !      write(*,*) '----------------------------'
    !    end if
    !    do while( it .lt. itmax)
    !      it = it + 1
    !      mf2(1) = (mf1(3) - mf1(2))/(zv1(3) - zv1(2))
    !      mf2(2) = (mf1(1) - mf1(3))/(zv1(1) - zv1(3))
    !      mf2(3) = (mf1(2) - mf1(1))/(zv1(2) - zv1(1))
    !      mf3 = (mf2(3) - mf2(2))/(zv1(1) - zv1(3))
    !      mw = mf2(1) + mf2(2) - mf2(3)
    !      fz = sqrt( mw*mw - 4.d0*mf1(3)*mf3)
    !      if( abs( mw + fz) .gt. abs( mw - fz)) then
    !        fz = mw + fz
    !      else
    !        fz = mw - fz
    !      end if
    !      zv1(1) = zv1(2)
    !      zv1(2) = zv1(3)
    !      zv1(3) = zv1(3) - 2.d0*mf1(3)/fz
    !      mf1(1) = mf1(2)
    !      mf1(2) = mf1(3)
    !      call schlessinger_interp( intord, 1, zv1(3), mf1(3))
    !      mf1(3) = mf1(3) - zv1(3)
    !      if( writefile) then
    !        write(*,'(i,2f23.16,g13.6,3x,3g13.6)') it, zv1(3), abs( mf1(3)), abs( zv1(1)-zv1(2)), abs( zv1(2)-zv1(3)), abs( zv1(3)-zv1(1))
    !      end if
    !      if( (max( abs( zv1(1)-zv1(2)), max( abs( zv1(2)-zv1(3)), abs( zv1(3)-zv1(1)))) .lt. meps) .and. (abs( mf1(3)) .lt. eps)) exit

    !      !j = maxloc( abs( g), 1)
    !      !fz = ( zv2(1)*zv2(2)*zv1(3)*(zv1(1)-zv1(2)) + zv2(2)*zv2(3)*zv1(1)*(zv1(2)-zv1(3)) + zv2(3)*zv2(1)*zv1(2)*(zv1(3)-zv1(1)) )/&
    !      !    ( zv2(1)*zv2(2)*(zv1(1)-zv1(2))      + zv2(2)*zv2(3)*(zv1(2)-zv1(3))      + zv2(3)*zv2(1)*(zv1(3)-zv1(1)) )

    !      !call eph_gen_sigmafm( eph_evalp( ist, ip) + fz, 1, ip, ist, ist, zv2( j))
    !      !zv2( j) = zv2( j) - fz
    !      !zv1( j) = fz
    !      !write(*,'(i,2f23.16,g13.6,3x,3g13.6)') it, zv1( j), abs( zv2( j)), abs( zv1(1)-zv1(2)), abs( zv1(2)-zv1(3)), abs( zv1(3)-zv1(1))
    !      !if( (max( abs( zv1(1)-zv1(2)), max( abs( zv1(2)-zv1(3)), abs( zv1(3)-zv1(1)))) .lt. meps) .and. (abs( zv2( j)) .lt. eps)) exit
    !    end do
    !    if( abs( mf1(3)) .lt. eps) then
    !    !if( abs( zv2( j)) .lt. eps) then
    !      nr = nr + 1
    !      roots( nr) = zv1(3)
    !      !roots( nr) = zv1( j)
    !    end if
    !  end do
    !  nroots = nr
    !  allocate( f( nroots))
    !  call schlessinger_interp( intord, nroots, roots( 1:nroots), f)
    !  allocate( idx( nroots))
    !  call sortidx( nroots, abs( f-roots)/eps, idx)
    !  roots( 1:nroots) = roots( idx)
    !  mroots( 1:nroots) = mroots( idx)
    !  f = f( idx)
    !  if( writefile) then
    !    write(*,*) "ROOTS"
    !    do i = 1, nroots
    !      write(*,'(i,2f23.16,i,g13.6)') i, roots( i)*h2ev, mroots( i), abs( f( i) - roots( i))*h2ev
    !    end do
    !    write(*,*) "POLES"
    !    do i = 1, npoles
    !      write(*,'(i,2f23.16,i)') i, poles( i)*h2ev, mpoles( i)
    !    end do
    !  end if
    !  npoles = 0
    !  do i = 1, nroots
    !    if( abs( f( i) - roots( i)) .lt. eps) then
    !      npoles = npoles + 1
    !      roots( npoles) = roots( i)
    !      f( npoles) = f( i) - roots( i)
    !    end if
    !  end do
    !  nroots = npoles
    !  if( npoles .gt. 0 .and. writefile) then
    !    write(*,'(5i5)', advance='no') ip, ist, it, croot_tri%nv, nroots
    !    do i = 1, npoles
    !      write(*,'("|   ",2f13.6,"(",g13.6,")   |")', advance='no') roots( i)*h2ev, abs( f( i))*h2ev
    !    end do
    !    write(*,*)
    !  end if
    !  nr = nroots
    !  if( nr .gt. 0) then
    !    allocate( r( nr))
    !    r = roots
    !  end if
    !  deallocate( f, idx)

    !  if( allocated( roots)) deallocate( roots)
    !  if( allocated( mroots)) deallocate( mroots)
    !  if( allocated( poles)) deallocate( poles)
    !  if( allocated( mpoles)) deallocate( mpoles)
    !  
    !end subroutine eph_find_scroots

!    subroutine eph_gen_sigmafm( freqs, nfreq, ip, fst, lst, sigma)
!      use mod_atoms, only: natmtot
!      complex(8), intent( in)  :: freqs( nfreq)
!      integer, intent( in)     :: nfreq, ip, fst, lst
!      complex(8), intent( out) :: sigma( fst:lst, nfreq)
!
!      integer :: iq, iqp, ist, jst, iw, nu, nmodes
!      real(8) :: nnuq, fnpq, sigr0, wgtr, wgti, eta
!      complex(8) :: wgt, t1, t2, f
!      complex(8), allocatable :: auxmat(:,:,:)
!
!      real(8), external :: occ
!
!      !do iw = 1, nfreq
!      !  sigma( :, iw) = (freqs( iw) + zone)*(freqs( iw) + zi)**2.d0*(freqs( iw) - zone)**(-1.d0)
!      !end do
!      !return
!
!      t1 = cmplx( 2.d0, 0.d0, 8)
!      sigr0 = 0.d0
!
!      nmodes = eph_nmode
!      !if( eph_phfreq_const .gt. 1.d-16) then
!      !  t1 = t1*nmodes
!      !  nmodes = 1
!      !end if
!
!      allocate( auxmat( eph_qpset%nkpt, nfreq, fst:lst))
!      auxmat = zzero
!#ifdef USEOMP
!!$omp parallel default( shared) private( iqp, iq, ist, jst, fnpq, nu, nnuq, sigr0, iw, wgt, wgtr, wgti, f)
!!$omp do
!#endif
!      do iqp = 1, eph_qpset%nkpt
!        iq = eph_iqp2iq( iqp)
!        do ist = fst, lst
!          jst = ist ! assuming that polar coupling is only intraband
!          do jst = eph_fst, eph_lst
!            fnpq = occ( eph_evalqp( jst, iqp)/(kboltz*eph_temp), 1)
!            do nu = 1, nmodes
!              if( abs( eph_phfreq( nu, iq)) .gt. 1.d-10) then
!                nnuq = occ( abs( eph_phfreq( nu, iq))/(kboltz*eph_temp), -1)  ! Bose-Einstein occupation of phonon
!                !sigr0 = dble( t1*((1.d0 - fnpq + nnuq)/(-evalpq( jst, iqp) - eph_phfreq( nu, iq) - eph_eta) + &
!                !                  (       fnpq + nnuq)/(-evalpq( jst, iqp) + eph_phfreq( nu, iq) - eph_eta)))
!                do iw = 1, nfreq
!                  f = freqs( iw)
!                  !call eph_nopole( f, cmplx( eph_evalqp( jst, iqp) + eph_phfreq( nu, iq), eph_eta, 8), &
!                  !                    cmplx( eph_evalqp( jst, iqp) - eph_phfreq( nu, iq), eph_eta, 8), eph_etaeps)
!
!                  eta = 0.d0
!                  if( abs( aimag( f)) .lt. eph_eta) eta = sign( abs( aimag( f)) - eph_eta, -aimag( f))
!                  wgt = t1*((1.d0 - fnpq + nnuq)/(f - eph_evalqp( jst, iqp) - eph_phfreq( nu, iq) - zi*eta) + &
!                            (       fnpq + nnuq)/(f - eph_evalqp( jst, iqp) + eph_phfreq( nu, iq) - zi*eta))
!                  auxmat( iqp, iw, ist) = auxmat( iqp, iw, ist) + &
!                                          eph_qpset%wkpt( iqp)*eph_ephmat( jst, ist, nu, iqp)*conjg( eph_ephmat( jst, ist, nu, iqp))*( wgt - sigr0)
!
!                  !wgtr = dble( wgt)*eph_qpset%wkpt( iqp)
!                  !wgti = t1*sqrt( pi)*eph_qpset%kpt( iqp)*( &
!                  !    (1.d0 - fnpq + nnuq)* exp( -((f - eph_evalqp( jst, iqp) - eph_phfreq( nu, iq))/eph_eta)**2.d0)/eph_eta + &
!                  !    (       fnpq + nnuq)* exp( -((f - eph_evalqp( jst, iqp) + eph_phfreq( nu, iq))/eph_eta)**2.d0)/eph_eta)
!                  !auxmat( iqp, iw, ist) = auxmat( iqp, iw, ist) + cmplx( wgtr, wgti, 8)*eph_ephmat( jst, ist, nu, iqp)*conjg( eph_ephmat( jst, ist, nu, iqp))
!                end do
!              end if
!            end do
!          end do
!        end do
!      end do
!#ifdef USEOMP
!!$omp end do
!!$omp end parallel
!#endif
!      do ist = fst, lst
!        do iw = 1, nfreq
!          sigma( ist, iw) = sum( auxmat( :, iw, ist))
!        end do
!      end do
!
!      deallocate( auxmat)
!
!      return
!    end subroutine eph_gen_sigmafm

!============================================================================
!============================================================================

    subroutine eph_nopole( w, c1, c2, eps, z)
      ! When c1, c2 are poles, it returns the point z closest to w such that
      ! |z-c1| >= eps and |z-c2| >= eps.
      ! When z is given the output is stored in z. Otherwise w is overwritten.
      complex(8), intent( inout)         :: w
      complex(8), intent( in)            :: c1, c2
      real(8), intent( in)               :: eps
      complex(8), optional, intent( out) :: z

      real(8) :: r, d1, d2, phi, theta
      complex(8) :: z0, u, v

      d1 = abs( w - c1)
      d2 = abs( w - c2)

      ! in none of both circles
      if( (d1 .ge. eps) .and. (d2 .ge. eps)) then
        z0 = w
      else
        ! only in circle 1
        if( (d1 .lt. eps) .and. (d2 .ge. eps)) then
          v = w - c1
          phi = atan2( aimag( v), dble( v))
          z0 = c1 + cmplx( eps*cos( phi), eps*sin( phi), 8)
        ! only in circle 2
        else if( (d1 .ge. eps) .and. (d2 .lt. eps)) then
          v = w - c2
          phi = atan2( aimag( v), dble( v))
          z0 = c2 + cmplx( eps*cos( phi), eps*sin( phi), 8)
        ! in both circles
        else
          v = c2 - c1
          r = 0.5d0*abs( v)
          theta = atan2( aimag( v), dble( v))
          phi = atan2( sqrt( eps*eps - r*r), r)
          u = c1 + cmplx( eps*cos( theta+phi), eps*sin( theta+phi), 8)
          v = c1 + cmplx( eps*cos( theta-phi), eps*sin( theta-phi), 8)
          ! find the intersection of circle 1 and 2 that is closer to w
          if( abs( u - w) .lt. abs( v - w)) then
            z0 = u
          else
            z0 = v
          end if
          ! w is closer to c1
          if( d1 .lt. d2) then
            v = w - c1
            phi = atan2( aimag( v), dble( v))
            v = c1 + cmplx( eps*cos( phi), eps*sin( phi), 8)
            ! closest point on boundary of circle 1 is not in circle 2 and closer to w than the intersection point
            if( (abs( v - c2) .ge. eps) .and. (abs( v - c1) .lt. abs( z0 - c1))) z0 = v
          ! w is closer to c2
          else
            v = w - c2
            phi = atan2( aimag( v), dble( v))
            v = c2 + cmplx( eps*cos( phi), eps*sin( phi), 8)
            ! closest point on boundary of circle 2 is not in circle 1 and closer to w than the intersection point
            if( (abs( v - c1) .ge. eps) .and. (abs( v - c2) .lt. abs( z0 - c2))) z0 = v
          end if
        end if
      end if

      if( present( z)) then
        z = z0
      else
        w = z0
      end if
      return
    end subroutine eph_nopole
    
!    subroutine eph_sigmafm2d_tofile( xrange, yrange, nx, ny, ip, ist, fname)
!      use m_getunit
!      real(8), intent( in) :: xrange(2), yrange(2)
!      integer, intent( in) :: nx, ny, ip, ist
!      character(*), intent( in) :: fname
!
!      integer :: i, j, un
!      real(8) :: dx, dy, x, y
!      complex(8), allocatable :: v(:), f(:)
!
!      dx = (xrange(2) - xrange(1))/dble( nx-1)
!      dy = (yrange(2) - yrange(1))/dble( ny-1)
!
!      allocate( v( nx), f( nx))
!      call getunit( un)
!      open( un, file=trim( fname), action='write', status='unknown', form='formatted')
!      write( un, '("#    xrange                                  yrange                                  ip   ist  eval")')
!      write( un, '("# ",4g20.10e3,2i5,g20.10e3)') xrange, yrange, ip, ist, eph_evalp( ist, ip)
!      do j = 0, ny - 1
!        y = yrange(1) + dble( j)*dy
!        do i = 0, nx - 1
!          x = xrange(1) + dble( i)*dx
!          v( i+1) = cmplx( x, y, 8)
!        end do
!        call eph_gen_sigmafm( eph_evalp( ist, ip) + v, nx, ip, ist, ist, f)
!        do i = 1, nx
!          write( un, '(6g20.10e3)') f( i), abs( f( i)), atan2( aimag( f( i)), dble( f( i))), f( i) - v( i)
!        end do
!        write( un, *)
!        write( un, *)
!      end do
!      close( un)
!
!      deallocate( v, f)
!      return
!    end subroutine eph_sigmafm2d_tofile

    subroutine eph_sigmafm2d_tofile2( xrange, yrange, nx, ny, ip, ist, fname, intord)
      use m_getunit
      use mod_schlessinger
      real(8), intent( in) :: xrange(2), yrange(2)
      integer, intent( in) :: nx, ny, ip, ist
      character(*), intent( in) :: fname
      integer, optional, intent( in) :: intord

      integer :: i, j, un, ord
      real(8) :: dx, dy, x, y
      complex(8), allocatable :: v(:), f(:)

      ord = eph_nfreq-1
      if( present( intord)) ord = intord

      dx = (xrange(2) - xrange(1))/dble( nx-1)
      dy = (yrange(2) - yrange(1))/dble( ny-1)

      allocate( v( nx), f( nx))
      call getunit( un)
      open( un, file=trim( fname), action='write', status='unknown', form='formatted')
      write( un, '("#    xrange                                  yrange                                  ip   ist  eval")')
      write( un, '("# ",4g20.10e3,2i5,g20.10e3)') xrange, yrange, ip, ist, eph_evalp( ist, ip)
      do j = 0, ny - 1
        y = yrange(1) + dble( j)*dy
        do i = 0, nx - 1
          x = xrange(1) + dble( i)*dx
          v( i+1) = cmplx( x, y, 8)
        end do
        call schlessinger_interp( ord, nx, v, f, poly=.false.)
        !f = zone/f
        do i = 1, nx
          write( un, '(6g20.10e3)') f( i), abs( f( i)), atan2( aimag( f( i)), dble( f( i))), f( i) - v( i)
        end do
        write( un, *)
        write( un, *)
      end do
      close( un)

      deallocate( v, f)
      return
    end subroutine eph_sigmafm2d_tofile2
!============================================================================
!============================================================================
end module mod_eph_sigma
