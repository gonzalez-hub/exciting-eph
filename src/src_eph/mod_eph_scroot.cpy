module mod_eph_scroot
  use mod_eph_variables
  use mod_eph_sigma
  use mod_atoms, only: natmtot
  use m_getunit

  implicit none

  logical :: eph_scroot_writefile = .false.

! methods
  contains
    !
    recursive subroutine eph_scroot( bound, eps, ip, ist, nroot, roots, uniform, guess)
      use m_linalg, only: zpinv
      ! shape of the boundary:
      ! 3..<..4
      ! :     :
      ! :     :
      ! 1..>..2
      complex(8), intent( in)               :: bound(4)
      real(8), intent( in)                  :: eps
      integer, intent( in)                  :: ip, ist                                 ! index of target point p and band
      integer, intent( inout)               :: nroot
      complex(8), allocatable, intent( out) :: roots(:)
      logical, optional, intent( in)        :: uniform
      complex(8), optional, intent( in)     :: guess

      integer :: i, j, n, nvert, ntria, un, ni(3)
      real(8) :: epsmesh, epsarg, vararg
      complex(8) :: points(50), c, nv(3), v(3), m(3,3), minv(3,3)
      complex(8), allocatable :: fv(:), fc(:), verts(:), circ(:)
      integer, allocatable :: trias(:,:), idx(:)
      logical :: isin
      character( 1024) :: fname, string

      epsmesh = min( abs( bound(1)-bound(3)), abs( bound(2)-bound(4)))*eps

      ! NON SC - ZERO ORDER
      !nroot = 1
      !if( allocated( roots)) deallocate( roots)
      !allocate( roots( nroot))
      !call eph_gen_sigmafm( (/cmplx( eph_evalp( ist, ip), 0.d0, 8)/), 1, ip, ist, ist, roots)
      !return

      ! FIND GUESS POINTS
      call eph_scroot_genmesh( bound, epsmesh, ip, ist, nvert, verts, ntria, trias, uniform, guess)
      allocate( fv( nvert))
      call eph_gen_sigmafm( eph_evalp( ist, ip) + verts, nvert, ip, ist, ist, fv)
      fv = fv - verts
      nroot = 0
      points = zzero
      do n = 1, ntria
        call eph_scroot_zerotriangle( verts( trias( :, n)), fv( trias( :, n)), c, isin) 
        if( isin) then
          if( minval( abs( points( 1:nroot) - c)) .gt. 0.5d0*epsmesh) then
            nroot = nroot + 1
            points( nroot) = c
          end if
        end if
      end do
      if( allocated( trias)) deallocate( trias)

      if( eph_scroot_writefile) then
        write( fname, '("P",i3.3,"N",i3.3,".ZRS")') ip, ist
        call getunit( un)
        open( un, file=trim( fname), action='write', status='unknown', form='formatted')
        if( present( guess)) then
          write( un, '(2g20.10e3)') guess
        else
          write( un, '(2g20.10e3)') zzero
        end if
        write( un, *)
        do n = 1, nroot
          write( un, '(2g20.10e3)') points( n)
        end do
        write( un, *)
        close( un)
      end if

      ! VALIDATE GUESS POINTS
      n = nint( 1.d0/eps)
      epsarg = twopi/dble( n)
      allocate( circ( n), fc( n))
      i = 0
      do while( i .lt. nroot)
        i = i + 1
        do j = 1, n
          circ( j) = points( i) + cmplx( epsmesh*cos( epsarg*(j-1)), epsmesh*sin( epsarg*(j-1)), 8)
        end do
        call eph_gen_sigmafm( eph_evalp( ist, ip) + circ, n, ip, ist, ist, fc)
        fc = fc - circ
        call eph_scroot_vararg( n, fc, vararg)
        !write(*,'(f13.6)') vararg/twopi
        if( nint( vararg/twopi) .ge. 0) then
          points( i) = points( nroot)
          nroot = nroot - 1
          i = i - 1
        else
        end if
      end do
      deallocate( circ, fc)

      if( eph_scroot_writefile) then
        write( fname, '("P",i3.3,"N",i3.3,".ZRS")') ip, ist
        call getunit( un)
        open( un, file=trim( fname), action='write', status='old', position='append', form='formatted')
        do n = 1, nroot
          write( un, '(2g20.10e3)') points( n)
        end do
        write( un, *)
        close( un)
      end if

      ! REFINE POINTS
      i = 0
      do while( i .lt. nroot)
        i = i + 1
        nv = cmplx( 1.d20, 1.d20, 8)
        ni = 0
        n = maxloc( abs( nv - points( i)), 1)
        vararg = maxval( abs( nv - points( i)))
        do j = 1, nvert
          if( abs( verts( j) - points( i)) .lt. vararg) then
            nv( n) = verts( j)
            ni( n) = j
            n = maxloc( abs( nv - points( i)), 1)
            vararg = maxval( abs( nv - points( i)))
          end if
        end do
        v = fv( ni)
        j = 0
        do while( max( abs( nv(2)-nv(3)), abs( nv(3)-nv(1)), abs( nv(1)-nv(2))) .gt. epsmesh**2)
          n = maxloc( abs( v), 1)
          m(:,1) = nv*v
          m(:,2) = -v
          m(:,3) = zone
          call zpinv( m, minv)
          points(i) = minv(3,1)*nv(1) + minv(3,2)*nv(2) + minv(3,3)*nv(3)
          call eph_gen_sigmafm( eph_evalp( ist, ip) + (/points( i)/), 1, ip, ist, ist, v(n))
          v(n) = v(n) - points( i)
          nv(n) = points( i)
          j = j + 1
        end do
        !write(*,*) j
        if( abs( v(n)) .gt. epsmesh**2) then
          points( i) = points( nroot)
          nroot = nroot - 1
          i = i - 1
        end if
      end do

      if( eph_scroot_writefile) then
        write( fname, '("P",i3.3,"N",i3.3,".ZRS")') ip, ist
        call getunit( un)
        open( un, file=trim( fname), action='write', status='old', position='append', form='formatted')
        do n = 1, nroot
          write( un, '(2g20.10e3)') points( n)
        end do
        write( un, *)
        close( un)
      end if

      deallocate( fv)
      if( allocated( verts)) deallocate( verts)

      ! SORT AND WRITE RESULTS
      if( allocated( roots)) deallocate( roots)
      allocate( roots( nroot))
      if( nroot .eq. 0) return
      allocate( idx( nroot))
      roots = points( 1:nroot)
      call eph_gen_sigmafm( eph_evalp( ist, ip) + roots, nroot, ip, ist, ist, points( 1:nroot))
      points( 1:nroot) = points( 1:nroot) - roots
      call sortidx( nroot, abs( points( 1:nroot)), idx)
      roots = roots( idx)
      points( 1:nroot) = points( idx)
      deallocate( idx)
      write( string, '("results: ",i,g12.4e2)') ist, eps
      do i = 1, nroot
        write( fname, '(2(f13.6," (",sp,f9.6,")")," (",g20.10e3,") | ")') dble( roots( i)*h2ev), dble( (roots( i)-guess)*h2ev), &
            aimag( roots( i)*h2ev), aimag( (roots( i)-guess)*h2ev), abs( points( i))*h2ev
        string = trim( string)//trim( fname)
      end do
      write(*,'(1024a)') trim( string)

      return
    end subroutine eph_scroot

!============================================================================
!============================================================================

    subroutine eph_scroot_vararg( n, f, vararg)
      integer, intent( in)    :: n
      complex(8), intent( in) :: f(n)
      real(8), intent( out)   :: vararg

      integer :: i, j
      complex(8) :: z

      vararg = 0.d0
      do i = 1, n
        j = modulo( i, n) + 1
        if( abs( f(j)) .gt. 1.d-20) then
          z = f(i)/f(j)
          vararg = vararg + atan2( aimag( z), dble( z))
        end if
      end do

      return
    end subroutine eph_scroot_vararg

!============================================================================
!============================================================================

    subroutine eph_gen_sigmafm_onset( zval, nz, ip, ist, sigma)
      complex(8), intent( in)  :: zval( nz)                               ! values to evaluate Sigma at
      integer, intent( in)     :: ip, ist, nz                             ! index of target point p and band
      complex(8), intent( out) :: sigma( nz)

      integer :: iq, jst, iw, nu, nnodes, i
      real(8) :: nnuq, fnpq, sigr0
      complex(8) :: t1, wgt, w
      complex(8), allocatable :: auxmat(:,:)

      t1 = cmplx( 2.d0/eph_qset%nkpt, 0.d0, 8)

      auxmat = zzero
      sigr0 = 0.d0

      nnodes = 3*natmtot
      if( eph_phfreq_const .gt. 1.d-16) then
        t1 = t1*nnodes
        nnodes = 1
      end if
      
      allocate( auxmat( eph_qset%nkpt, nz))
      auxmat = zzero
#ifdef USEOMP
!!$omp parallel default( shared) private( iq, jst, fnpq, nu, nnuq, sigr0, iw, w, wgt)
!!$omp do
#endif
      do iq = 1, eph_qset%nkpt
        jst = ist ! assuming that polar coupling is only intraband
        fnpq = 1.d0/(exp( eph_evalpq( jst, iq)/(kboltz*eph_temp)) + 1.d0)  ! Fermi-Dirac occupation of electron
        do nu = 1, nnodes
          if( abs( eph_phfreq( nu, iq)) .gt. 1.d-6) then
            nnuq = 1.d0/(exp( eph_phfreq( nu, iq)/(kboltz*eph_temp)) - 1.d0)  ! Bose-Einstein occupation of phonon
            !sigr0 = dble( t1*((1.d0 - fnpq + nnuq)/(-eph_evalpq( jst, iq) - eph_phfreq( nu, iq) - eph_eta) + &
            !                  (       fnpq + nnuq)/(-eph_evalpq( jst, iq) + eph_phfreq( nu, iq) - eph_eta)))
            do iw = 1, nz
              w = zval( iw)
              wgt = t1*((1.d0 - fnpq + nnuq)/(w - eph_evalpq( jst, iq) - eph_phfreq( nu, iq) - eph_eta) + &
                        (       fnpq + nnuq)/(w - eph_evalpq( jst, iq) + eph_phfreq( nu, iq) - eph_eta))

              auxmat( iq, iw) = auxmat( iq, iw) + eph_ephmat2( iq)*( wgt - sigr0)
            end do
          end if
        end do
      end do
#ifdef USEOMP
!!$omp end do
!!$omp end parallel
#endif
      do iw = 1, nz
        sigma( iw) = sum( auxmat( :, iw))! - zval( iw)
        !sigma( iw) = (zval( iw) - zi)**0.5*(zval( iw) + zone)*(zval( iw) + zi)**2*(zval( iw) - zone)**(-1)! + zval( iw) 
      end do

      deallocate( auxmat)

      return
    end subroutine eph_gen_sigmafm_onset

!============================================================================
!============================================================================

    function isintriangle( p, c) result( isin)
      complex(8), intent( in) :: p, c(3)
      logical                 :: isin

      real(8) :: n(3), a(4)

      call r3cross( (/1.d0, 1.d0, 1.d0/), aimag( (/c(1), c(2), c(3)/)), n)
      a(4) = abs( dot_product( dble( (/c(1), c(2), c(3)/)), n))
      call r3cross( (/1.d0, 1.d0, 1.d0/), aimag( (/p, c(2), c(3)/)), n)
      a(1) = abs( dot_product( dble( (/p, c(2), c(3)/)), n))
      call r3cross( (/1.d0, 1.d0, 1.d0/), aimag( (/p, c(3), c(1)/)), n)
      a(2) = abs( dot_product( dble( (/p, c(3), c(1)/)), n))
      call r3cross( (/1.d0, 1.d0, 1.d0/), aimag( (/p, c(1), c(2)/)), n)
      a(3) = abs( dot_product( dble( (/p, c(1), c(2)/)), n))

      isin = (abs( sum( a(1:3)) - a(4)) .lt. 1.d-15)
    end function isintriangle

    subroutine eph_scroot_zerotriangle( p, f, z, isinside)
      complex(8), intent( in)  :: p(3), f(3)
      complex(8), intent( out) :: z
      logical, intent( out)    :: isinside

      integer :: j
      real(8) :: v(3,3,2), nr(3), ni(3), r, i, t, a(4)

      do j = 1, 3
        v(1,j,:) = dble ( p(j))
        v(2,j,:) = aimag( p(j))
        v(3,j,1) = dble ( f(j))
        v(3,j,2) = aimag( f(j))
      end do

      z = zzero
      isinside = .false.

      if( (minval( v(3,:,1)) .le. 0.d0) .and. (maxval( v(3,:,1)) .ge. 0.d0) .and. &
          (minval( v(3,:,2)) .le. 0.d0) .and. (maxval( v(3,:,2)) .ge. 0.d0)) then

        call r3cross( v(:,2,1)-v(:,1,1), v(:,3,1)-v(:,1,1), nr)
        call r3cross( v(:,2,2)-v(:,1,2), v(:,3,2)-v(:,1,2), ni)
        nr = nr/norm2( nr)
        ni = ni/norm2( ni)
        r = dot_product( nr, v(:,1,1))
        i = dot_product( ni, v(:,1,2))
        
        t = nr(1)*ni(2) - nr(2)*ni(1)
        ! intersecting
        if( abs( t) .gt. 1.d-20) then
          z = cmplx( (ni(2)*r - nr(2)*i)/t, (nr(1)*i - ni(1)*r)/t, 8)

          !r = (dble( z) - v(1,2,1))*(v(2,1,1) - v(2,2,1)) - (v(1,1,1) - v(1,2,1))*(aimag( z) - v(2,2,1))
          !i = (dble( z) - v(1,3,1))*(v(2,2,1) - v(2,3,1)) - (v(1,2,1) - v(1,3,1))*(aimag( z) - v(2,3,1))
          !t = (dble( z) - v(1,1,1))*(v(2,3,1) - v(2,1,1)) - (v(1,3,1) - v(1,1,1))*(aimag( z) - v(2,1,1))
          !call r3cross( (/1.d0, 1.d0, 1.d0/), (/v(2,1,1), v(2,2,1), v(2,3,1)/), nr)
          !a(4) = abs( dot_product( (/v(1,1,1), v(1,2,1), v(1,3,1)/), nr))
          !call r3cross( (/1.d0, 1.d0, 1.d0/), (/aimag( z), v(2,2,1), v(2,3,1)/), nr)
          !a(1) = abs( dot_product( (/dble( z), v(1,2,1), v(1,3,1)/), nr))
          !call r3cross( (/1.d0, 1.d0, 1.d0/), (/aimag( z), v(2,3,1), v(2,1,1)/), nr)
          !a(2) = abs( dot_product( (/dble( z), v(1,3,1), v(1,1,1)/), nr))
          !call r3cross( (/1.d0, 1.d0, 1.d0/), (/aimag( z), v(2,1,1), v(2,2,1)/), nr)
          !a(3) = abs( dot_product( (/dble( z), v(1,1,1), v(1,2,1)/), nr))

          !if( abs( z - cmplx( 1, 0, 8)) .lt. 1.d-1) then
          !  !write(*,'(3f23.16)') r, i, t
          !  write(*,'(5f23.16)') a, abs( sum( a(1:3)) - a(4))
          !end if

          !isinside = .not. ((( r .le. 0.d0) .or. (i .le. 0.d0) .or. (t .le. 0.d0)) .and. &
          !                  (( r .ge. 0.d0) .or. (i .ge. 0.d0) .or. (t .ge. 0.d0)))
          isinside = isintriangle( z, p)
        ! identical
        else if( abs( r/nr(2) - i/ni(2) + v(1,1,1)*(ni(1)/ni(2) - nr(1)/nr(2))) .lt. 1.d-20) then
          z = cmplx( v(1,1,1), r/nr(2) - v(1,1,1)*nr(1)/nr(2), 8)
          isinside = .true.
        ! parallel
        else
          !write(*,*) 'parallel'
        end if
      end if

      return
    end subroutine eph_scroot_zerotriangle

!============================================================================
!============================================================================

    subroutine eph_scroot_addtriangle( p, f, eps, n, add)
      complex(8), intent( in)  :: p(3), f(3)
      real(8), intent( in)     :: eps
      integer, intent( out)    :: n, add(3)

      integer :: i, idx(2,3)
      real(8) :: d(3)

      n = 0
      add = 0
      
      idx(:,1) = (/2,3/)
      idx(:,2) = (/3,1/)
      idx(:,3) = (/1,2/)
      d(1) = abs( p(3)-p(2))
      d(2) = abs( p(1)-p(3))
      d(3) = abs( p(2)-p(1))

      if( maxval( d) .gt. eps) then
        do i = 1, 3
          if( (d( i) .gt. eps) .and. ((dble(  f( idx( 1, i)))*dble(  f( idx( 2, i))) .le. 0.d0) .or. &
                                      (aimag( f( idx( 1, i)))*aimag( f( idx( 2, i))) .le. 0.d0))) then
            n = n + 1
            add(n) = i
          end if
        end do
      end if

      return
    end subroutine eph_scroot_addtriangle

!============================================================================
!============================================================================

    subroutine eph_scroot_genmesh( bound, eps, ip, ist, nvert, verts, ntria, trias, uniform, guess)
      use mod_triangulation
      use mod_complex_root
      complex(8), intent( in)               :: bound(4)
      real(8), intent( in)                  :: eps
      integer, intent( in)                  :: ip, ist                                 ! index of target point p and band
      integer, intent( out)                 :: nvert, ntria
      complex(8), allocatable, intent( out) :: verts(:)
      integer, allocatable, intent( out)    :: trias(:,:)
      logical, optional, intent( in)        :: uniform
      complex(8), optional, intent( in)     :: guess

      integer :: iter, i, j, k, l, m, n, nadd, tadd(3), un, idx(2,3)
      real(8) :: r, fac, off, t1, t2
      complex(8) :: c, r1, r2
      integer, allocatable :: tmp(:,:), add(:,:)
      real(8), allocatable :: a(:)
      complex(8), allocatable :: f(:), f2(:)
      character( 256) :: fname
      logical :: u
      type( triangulation) :: tri
      integer, allocatable :: mroots(:), mpoles(:)
      complex(8), allocatable :: roots(:), poles(:)

      call croot_genmesh( (/-0.1d0, 0.1d0/), (/-0.1d0, 0.1d0/), 0.04d0, pattern='rectangular')
      r = 1.d0
      i = 1
      call timesec( t1)
      do while( r .gt. 1.d-6)
        write(*,*) i, croot_tri%nv
        n = croot_tri%nv
        if( i .gt. 1) call croot_refine
        !write( fname, '("TRIANGULATION",i2.2)') i
        !call triangulation_tofile( croot_tri, fname)
        if( croot_tri%nv .ne. n) then
          if( allocated( f2)) deallocate( f2)
          allocate( f2( croot_tri%nv))
          f2( 1:n) = f
          call eph_gen_sigmafm( eph_evalp( ist, ip) + croot_tri%verts( (n+1):croot_tri%nv), croot_tri%nv-n+1, ip, ist, ist, f2( (n+1):croot_tri%nv))
          if( allocated( f)) deallocate( f)
          allocate( f( croot_tri%nv))
          f = f2
        else
          if( allocated( f)) deallocate( f)
          allocate( f( croot_tri%nv))
          call eph_gen_sigmafm( eph_evalp( ist, ip) + croot_tri%verts, croot_tri%nv, ip, ist, ist, f)
        end if
        call croot_quadrants( f - croot_tri%verts)
        call croot_edges
        call croot_regions
        write(*,'("CANDIDATE REGIONS:",100i)') size( croot_l, 2)
        !write( fname, '("CROOT",i2.2)') i
        !call croot_tofile( fname)
        r = croot_emin
        write(*,'("EMIN:",2g23.16)') r, croot_emax
        i = i + 1
        write(*,*) '============================================================='
      end do
      call croot_verify( roots, mroots, n, poles, mpoles, m)
      call timesec( t2)
      write(*,'(f13.6)') t2 - t1
      write(*,*) "ROOTS"
      do i = 1, n
        write(*,'(i,2f13.6,i)') i, roots( i), mroots( i)
      end do
      write(*,*) "POLES"
      do i = 1, m
        write(*,'(i,2f13.6,i)') i, poles( i), mpoles( i)
      end do
      stop
      u = .false.
      if( present( uniform)) u = uniform

      if( u .and. (eps .gt. 1.d-4)) then
        fac = 1.5d0
        if( present( guess)) then
          c = guess
        else
          c = sum( bound)*0.25d0
        end if
        r = (minval( abs( bound - c)) + maxval( abs( bound - c)))*0.5d0
        k = maxloc( abs( bound - c), 1)
        n = nint( r/eps)
        r1 = bound( k) - c
        r2 = r1*exp( zi*2.d0*pi/3.d0)
        allocate( a( 0:n))
        r = 1.d0
        do i = 1, n-1
          r = 1.d0 + fac*r
        end do
        r = 1.d0/r
        a(0) = 0.d0
        do i = 1, n
          a( i) = a( i-1) + fac**(i-1)*r
        end do
        allocate( f( 3*n*(n+1)+1))
        allocate( add( 3*n*(n+1)+1, 1))
        off = 0.d0
        l = 1
        k = 1
        f( k) = zzero
        do i = 1, n
          k = k + 1
          m = l
          l = max( 0, min( nint( a( i)/(a( i) - a( i-1))), n))
          off = off + getoffset( 6*m, 6*l)
          f( k) = a( i)*r1
          do j = 1, l
            k = k + 1
            f( k) = f( k-1) + a( i)*r2/dble( l)
          end do
          do j = 1, l
            k = k + 1
            f( k) = f( k-1) - a( i)*r1/dble( l)
          end do
          do j = 1, l-1
            k = k + 1
            f( k) = f( k-1) - a( i)*(r1 + r2)/dble( l)
          end do
          f( (k-3*l+1):k) = f( (k-3*l+1):k)*exp( zi*twopi*off)
        end do
        if( 2*k-1 .gt. 3*n*(n+1)+1) then
          write(*,*) 'Ooops!'
          write(*,*) 2*k-1, 3*n*(n+1)+1
          stop
        end if
        f( (k+1):(2*k-1)) = -f( 2:k)
        f = c + f
        k = 2*k - 1
        nvert = 1
        add( 1, 1) = 1
        do i = 2, k
          !if( isintriangle( f( i), bound((/1,2,3/))) .or. isintriangle( f( i), bound((/1,3,4/)))) then
          if( minval( abs( f( add( 1:nvert, 1)) - f( i))) .gt. 1.d-10) then
            nvert = nvert + 1
            add( nvert, 1) = i
          end if
        end do
        allocate( verts( nvert))
        allocate( trias( 3, 2*nvert), tmp( 3, 2*nvert))
        do i = 1, nvert
          verts( i) = f( add( i, 1))
        end do
        call dtris2( nvert, verts, ntria, trias, tmp)
        call triangulation_gen( tri, verts)
        call triangulation_tofile( tri, 'TRIANGULATION')
        deallocate( f, tmp, add, a)
      else
        nvert = 4
        allocate( verts( nvert))
        allocate( trias( 3, 2*nvert), tmp( 3, 2*nvert))
        verts = bound
        call dtris2( nvert, verts, ntria, trias, tmp)
        allocate( f( nvert), add( 2, 3*ntria))

        iter = 1
        nadd = 1
        do while( (nadd .gt. 0) .and. (iter .lt. 15))
          iter = iter + 1
          add = 0
          call eph_gen_sigmafm( eph_evalp( ist, ip) + verts, nvert, ip, ist, ist, f)
          f = f - verts
          nadd = 0
          do i = 1, ntria
            call eph_scroot_addtriangle( verts( trias( :, i)), f( trias( :, i)), eps, n, tadd)
            do j = 1, n
              if( tadd( j) .eq. 1) then
                call eph_scroot_addvertex( trias( 2, i), trias( 3, i), nadd, add)
              else if( tadd( j) .eq. 2) then
                call eph_scroot_addvertex( trias( 3, i), trias( 1, i), nadd, add)
              else
                call eph_scroot_addvertex( trias( 1, i), trias( 2, i), nadd, add)
              end if
            end do
          end do

          f = verts
          deallocate( verts, trias, tmp)
          allocate( verts( nvert + nadd))
          verts( 1:nvert) = f
          do i = 1, nadd
            verts( nvert + i) = 0.5d0*( verts( add( 1, i)) + verts( add( 2, i)))
          end do
          nvert = nvert + nadd
          deallocate( f, add)
          allocate( trias( 3, 2*nvert), tmp( 3, 2*nvert))
          call dtris2( nvert, verts, ntria, trias, tmp)
          call triangulation_gen( tri, verts)
          call triangulation_tofile( tri, 'TRIANGULATION')
          allocate( f( nvert), add( 2, 3*ntria))
        end do
        deallocate( tmp, f, add)
      end if

      if( eph_scroot_writefile) then
        call getunit( un)
        write( fname, '("P",i3.3,"N",i3.3,".VRT")') ip, ist
        open( un, file=trim( fname), action='write', status='unknown', form='formatted')
        do i = 1, nvert
          write( un, '(2g20.10e3)') verts( i)
        end do
        close( un)
        allocate( tmp( 2, ntria + nvert - 1))
        tmp = 0
        idx(:,1) = (/2,3/)
        idx(:,2) = (/3,1/)
        idx(:,3) = (/1,2/)
        do i = 1, ntria
          do j = 1, 3
            do k = 1, n
              if( (tmp( 1, k) .eq. min( trias( idx( 1, j), i), trias( idx( 2, j), i))) .and. &
                  (tmp( 2, k) .eq. max( trias( idx( 1, j), i), trias( idx( 2, j), i)))) then
                exit
              end if
            end do
            if( k .gt. n) then
              n = n + 1
              tmp( 1, n) = min( trias( idx( 1, j), i), trias( idx( 2, j), i))
              tmp( 2, n) = max( trias( idx( 1, j), i), trias( idx( 2, j), i))
            end if
          end do
        end do
        call getunit( un)
        write( fname, '("P",i3.3,"N",i3.3,".EDG")') ip, ist
        open( un, file=trim( fname), action='write', status='unknown', form='formatted')
        do i = 1, n
          write( un, '(2i)') tmp( :, i)
        end do
        close( un)
        deallocate( tmp)
      end if

      return

      contains
        function getoffset( m, n) result( d)
          integer, intent( in) :: m, n
          real(8)              :: d

          integer :: a, b, c
          
          a = min( m, n)
          b = max( m, n)
          c = nint( dble( b)/dble( a))
          c = c*a - b
          if( c .eq. 0) then
            d = 0.5d0/b
          else
            d = 0.5d0*c/(a*b)
          end if
        end function getoffset
    end subroutine eph_scroot_genmesh

    subroutine eph_scroot_addvertex( a, b, n, add)
      integer, intent( in)    :: a, b
      integer, intent( inout) :: n, add(2,*)

      integer :: i

      do i = 1, n
        if( (add( 1, i) .eq. min( a, b)) .and. (add( 2, i) .eq. max( a, b))) return
      end do

      n = n + 1
      add( 1, n) = min( a, b)
      add( 2, n) = max( a, b)
      
      return
    end subroutine eph_scroot_addvertex

end module mod_eph_scroot
