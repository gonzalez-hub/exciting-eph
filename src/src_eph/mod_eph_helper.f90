module mod_eph_helper
  use mod_eph_variables
  use mod_spacing
  use m_linalg

  implicit none

! methods
  contains

    subroutine eph_gen_pset
      use modinput
      integer :: ip, nv
      integer, allocatable :: igp(:)
      real(8), allocatable :: vpltmp(:,:), dgp(:)
      type( point_type_array), allocatable :: vertsarray(:)

      ! points along a path
      if( associated( input%eph%target%plot1d)) then
        eph_pset_nvp1d = size( input%eph%target%plot1d%path%pointarray)
        eph_pset_npp1d = input%eph%target%plot1d%path%steps
        ! here we add a small displacement to the Gamma points according to the points
        ! they are connected with do get the right directional behaviour close to Gamma
        if( eph_polar) then
          allocate( eph_pset_verts( 2*eph_pset_nvp1d))
          nv = 0
          do ip = 1, eph_pset_nvp1d
            nv = nv + 1
            eph_pset_verts(nv) = input%eph%target%plot1d%path%pointarray(ip)%point
            if( (norm2( eph_pset_verts(nv)%coord) .lt. input%structure%epslat) .and. &
                (ip .ne. 1) .and. (ip .ne. eph_pset_nvp1d) .and. &
                .not. eph_pset_verts(nv)%breakafter) then
              nv = nv + 1
              eph_pset_verts(nv) = eph_pset_verts(nv-1)
              eph_pset_verts(nv-1)%breakafter = .true.
            end if
          end do
          eph_pset_nvp1d = nv
          do ip = 1, eph_pset_nvp1d
            if( norm2( eph_pset_verts(ip)%coord) .lt. input%structure%epslat) then
              if( (eph_pset_verts(ip)%breakafter .and. (ip .gt. 1)) .or. (ip .eq. eph_pset_nvp1d)) then
                eph_pset_verts(ip)%coord = eph_pset_verts(ip-1)%coord/norm2( eph_pset_verts(ip-1)%coord)*input%structure%epslat*2.0d0
              end if
              if( .not. eph_pset_verts(ip)%breakafter .and. ip .lt. eph_pset_nvp1d) then
                eph_pset_verts(ip)%coord = eph_pset_verts(ip+1)%coord/norm2( eph_pset_verts(ip+1)%coord)*input%structure%epslat*2.0d0
              end if
            end if
          end do

          allocate( vertsarray( eph_pset_nvp1d))
          do ip = 1, eph_pset_nvp1d
            vertsarray(ip)%point => eph_pset_verts(ip)
          end do
        else
          allocate( eph_pset_verts( eph_pset_nvp1d))
          allocate( vertsarray( eph_pset_nvp1d))
          do ip = 1, eph_pset_nvp1d
            eph_pset_verts(ip) = input%eph%target%plot1d%path%pointarray(ip)%point
            vertsarray(ip)%point => eph_pset_verts(ip)
          end do
        end if
        allocate( eph_pset_dvp1d( eph_pset_nvp1d))
        allocate( eph_pset_dpp1d( eph_pset_npp1d))
        allocate( vpltmp( 3, eph_pset_npp1d))
        call connect( eph_kset_el%bvec, eph_pset_nvp1d, eph_pset_npp1d, vertsarray, vpltmp, eph_pset_dvp1d, eph_pset_dpp1d)
        call generate_k_vectors( eph_pset, eph_kset_el%bvec, (/1, 1, eph_pset_npp1d/), (/0.d0, 0.d0, 0.d0/), .false.)
        do ip = 1, eph_pset%nkpt
          eph_pset%vkl( :, ip) = vpltmp( :, ip)
          call r3mv( eph_kset_el%bvec, eph_pset%vkl( :, ip), eph_pset%vkc( :, ip))
        end do
        allocate( igp( 1000), dgp( 1000))
        call gridptsonpath( eph_pset_nvp1d, vertsarray, eph_kset_el, input%structure%epslat*1.d1, eph_pset_ngp1d_el, igp, dgp)
        allocate( eph_pset_igp1d_el, source=igp( 1:eph_pset_ngp1d_el))
        allocate( eph_pset_dgp1d_el, source=dgp( 1:eph_pset_ngp1d_el))
        call gridptsonpath( eph_pset_nvp1d, vertsarray, eph_kset_ph, input%structure%epslat*1.d1, eph_pset_ngp1d_ph, igp, dgp)
        allocate( eph_pset_igp1d_ph, source=igp( 1:eph_pset_ngp1d_ph))
        allocate( eph_pset_dgp1d_ph, source=dgp( 1:eph_pset_ngp1d_ph))
        deallocate( vpltmp, vertsarray, igp, dgp)
      ! points on a grid
      else
        if( minval( abs( input%eph%target%ngridp)) .gt. 0) then
          call generate_k_vectors( eph_pset, eph_kset_el%bvec, input%eph%target%ngridp, input%eph%target%vploff, input%eph%target%reducep, .false.)
        else
          write(*,*)
          write(*,'("Warning (eph_gen_pset): No target point set specified. Use same grid as in the calculation of the electrons.")')
          eph_pset = eph_kset_el
        end if
      end if
    end subroutine eph_gen_pset
    
    subroutine eph_init_ppoint( ip)
      integer, intent( in) :: ip

      type( kkqmt_set) :: qqpset
      
      !********************************
      !*          q+p points          *
      !********************************
      call generate_kkqmt_vectors( qqpset, eph_Gset, eph_qset%bvec, eph_qset%ngridk, eph_qset%vkloff, eph_qset%isreduced, eph_pset%vkl( :, ip), .false.)
      
      call delete_k_vectors( eph_qpset)
      eph_qpset = qqpset%kqmtset

      if( allocated( eph_iqp2iq)) deallocate( eph_iqp2iq)
      allocate( eph_iqp2iq( eph_qpset%nkpt))
      eph_iqp2iq = qqpset%ikqmt2ik

      if( allocated( eph_iq2iqp)) deallocate( eph_iq2iqp)
      allocate( eph_iq2iqp( eph_qset%nkpt))
      eph_iq2iqp = qqpset%ik2ikqmt

      call delete_kkqmt_vectors( qqpset)

      !********************************
      !*   tetrahedron integration    *
      !********************************
      call my_tetra_init( eph_tetra, eph_qpset, 2, eph_qpset%isreduced)
      return
    end subroutine eph_init_ppoint

    subroutine eph_genfreqs( ip)
      integer, intent( in) :: ip

      integer :: ist, iw, n
      real(8) :: emin, emax, width, r, s
      
      real(8), allocatable :: w(:), d(:), id(:), cf(:,:) 

      n = 4000
      r = 0.61803399d0
      emin = minval( eph_evalqp) - maxval( abs( eph_phfreq)) - input%eph%addfreq
      emax = maxval( eph_evalqp) + maxval( abs( eph_phfreq)) + input%eph%addfreq
      width = input%eph%lwfreq

      allocate( w( n), d( n), id( n), cf( 3, n))
      call spacing( emin, emax, n, w, 1)
      d = 0.d0
      do ist = eph_fst, eph_lst
        do iw = 1, n
          d( iw) = max( d( iw), 0.5d0*width/((w( iw) - eph_evalp( ist, ip))**2 + 0.25d0*width**2))
        end do
      end do
      d = d/pi
      s = sum( d)/dble( n)
      d = r*d + (1.d0-r)*s
      call spacingFromDensity( n, w, d, eph_nfreq, eph_freqs( :, ip))

      deallocate( w, d, id, cf)
      return
    end subroutine eph_genfreqs

    subroutine eph_kramkron( n, w, im, re)
      integer, intent( in) :: n
      real(8), intent( in) :: w( n)
      real(8), intent( in) :: im( n)
      real(8), intent( out) :: re( n)

      integer :: iw, jw
      real(8) :: dw0, dw1, df, cf( 3, n), bf(3)

      re = 0.d0
      call spline( n, w, 1, im, cf)
      do iw = 1, n
        do jw = 1, n-1
          if( jw .eq. iw-1) then
            dw1 = w( iw) - w( jw)
            df = im( iw) - im( jw)
            bf(1) = (11*df - dw1*(9*cf(1,jw) + dw1*(7*cf(2,jw) + 5*cf(3,jw)*dw1)))/(2*dw1)
            bf(2) = ( 9*df - dw1*(9*cf(1,jw) + dw1*(8*cf(2,jw) + 6*cf(3,jw)*dw1)))/(dw1**2)
            bf(3) = ( 9*df - dw1*(9*cf(1,jw) + dw1*(9*cf(2,jw) + 7*cf(3,jw)*dw1)))/(2*dw1**3)
            re( iw) = re( iw) + dw1*(6*bf(1) + dw1*(-3*bf(2) + 2*bf(3)*dw1))/6.d0
          else if( jw .eq. iw) then
            dw1 = w( jw+1) - w( iw)
            re( iw) = re( iw) + dw1*(6*cf(1,jw) + dw1*(3*cf(2,jw) + 2*cf(3,jw)*dw1))/6.d0
          else
            dw0 = w( jw) - w( iw)
            dw1 = w( jw+1) - w( iw)
            df = im( jw) - im( iw)
            re( iw) = re( iw) + (dw1-dw0)*(6*cf(1,jw) + dw0*(-9*cf(2,jw) + 11*cf(3,jw)*dw0) + dw1*(3*cf(2,jw) + 2*cf(3,jw)*dw1) - 7*cf(3,jw)*dw0*dw1)/6.d0 + &
              (dw0*(cf(1,jw) - dw0*(cf(2,jw) - cf(3,jw)*dw0)) - df)*log(dw0/dw1)
          end if
        end do
        re( iw) = re( iw) + im(iw)*log( (w(n)+1.d-16-w(iw))/(w(iw)+1.d-16-w(1)))
        re( iw) = re( iw)/pi
      end do

      return
    end subroutine eph_kramkron

    subroutine eph_read_epsinf_zstar( fname, epsinf, zstar)
      use mod_atoms, only: natmtot
      use m_getunit
      character(*), intent( in) :: fname
      real(8), intent( out)     :: epsinf(3,3)
      real(8), intent( out)     :: zstar( 3, 3, natmtot)

      integer :: un, l, i, ias
      character(1024) :: buf

      call getunit( un)
      open( un, file=trim( fname), status='old', form='formatted')

      epsinf = 0.d0
      zstar = 0.d0

      ias = 0
      l = 0
      do
        read( un, '(a)', iostat=i) buf
        if( i .ne. 0) exit
        buf = trim( adjustl( buf))
        if( (buf( 1:1) .eq. '#') .or. (len( trim( buf)) .eq. 0)) cycle
        if( l .lt. 3) then
          read( buf, *) epsinf( l+1, 1), epsinf( l+1, 2), epsinf( l+1, 3)
        else
          if( modulo( l, 3) .eq. 0) ias = ias + 1
          if( ias .le. natmtot) then
            i = modulo( l, 3) + 1
            read( buf, *) zstar( i, 1, ias), zstar( i, 2, ias), zstar( i, 3, ias)
          end if
        end if
        l = l + 1
      end do
      close( un)

      if( ias .lt. natmtot) then
        write(*,*)
        write(*,'("Error (eph_read_epsinf_zstar): The file contains not enough data.")')
        stop
      end if

      !write(*,'(3f13.6)') epsinf(1,:)
      !write(*,'(3f13.6)') epsinf(2,:)
      !write(*,'(3f13.6)') epsinf(3,:)
      !write(*,*)
      !do ias = 1, natmtot
      !  write(*,'(3f13.6)') zstar(1,:,ias)
      !  write(*,'(3f13.6)') zstar(2,:,ias)
      !  write(*,'(3f13.6)') zstar(3,:,ias)
      !  write(*,*)
      !end do
      
      return
    end subroutine eph_read_epsinf_zstar
end module mod_eph_helper
