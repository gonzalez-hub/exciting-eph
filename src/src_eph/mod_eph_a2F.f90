module mod_eph_a2F
  use mod_eph_variables
  use mod_eph_helper

  implicit none

! methods
  contains

    subroutine eph_a2F_genfreqs
      integer :: i
      real(8) :: dw

      if( allocated( eph_a2F_pfreqs)) deallocate( eph_a2F_pfreqs)
      allocate( eph_a2F_pfreqs( eph_a2F_npfreq))

      dw = 1.1d0*maxval( eph_phfreq)/dble( eph_a2F_npfreq-1)
      eph_a2F_pfreqs(1) = 0.d0
      do i = 2, eph_a2F_npfreq
        eph_a2F_pfreqs(i) = eph_a2F_pfreqs(i-1) + dw
      end do
    end subroutine eph_a2F_genfreqs

    subroutine eph_a2F_gen( nefreq, efreqs, npfreq, pfreqs, a2F)
      use mod_atoms, only: natmtot
      integer, intent( in)  :: nefreq
      real(8), intent( in)  :: efreqs( nefreq)
      integer, intent( in)  :: npfreq
      real(8), intent( in)  :: pfreqs( npfreq)
      real(8), intent( out) :: a2F( nefreq, npfreq, eph_fst:eph_lst)

      integer :: i, imode, iqp, ist
      real(8) :: t1, t2

      real(8), allocatable :: peval(:,:)
      complex(8), allocatable :: ephmat(:,:,:,:), auxmat(:,:,:,:,:)

      allocate( ephmat( eph_fst:eph_lst, eph_fst:eph_lst, 3*natmtot, eph_qpset%nkpt))
      allocate( peval( 3*natmtot, eph_qpset%nkpt))
      allocate( auxmat( nefreq, npfreq, eph_fst:eph_lst, eph_fst:eph_lst, 3*natmtot))

      a2F = 0.d0
      call timesec( t1)
      do iqp = 1, eph_qpset%nkpt
        peval( :, iqp) = eph_phfreq( :, eph_iqp2iq( iqp))
      end do
      do ist = eph_fst, eph_lst
        ephmat(:,ist,:,:) = eph_ephmat(ist,:,:,:)*conjg( eph_ephmat(ist,:,:,:))
      end do
      !ephmat = zone
      call my_tetra_int_dbldelta( eph_tetra, eph_qpset%nkpt, eph_nst, eph_evalqp, 3*natmtot, peval, &
             nefreq, efreqs, npfreq, pfreqs, eph_nst, auxmat, ephmat)
      a2F = sum( sum( dble( auxmat), 5), 4)
      call timesec( t2)
      write(*,'("TIME a2F:",f13.6)') t2-t1

      deallocate( peval, ephmat, auxmat)
      return
    end subroutine eph_a2F_gen

    subroutine eph_a2F_output( nefreq, efreqs, npfreq, pfreqs, a2F)
      use m_getunit
      integer, intent( in) :: nefreq
      real(8), intent( in) :: efreqs( nefreq)
      integer, intent( in) :: npfreq
      real(8), intent( in) :: pfreqs( npfreq)
      real(8), intent( in) :: a2F( nefreq, npfreq, eph_fst:eph_lst)

      integer :: iw1, iw2, ist, un

      call getunit( un)
      open( un, file='EPH_A2F.DAT', action='write', form='formatted')

      write( un, '("# bands:",i5,3x,"nefreq:",i5,3x,"npfreq:",i5)') eph_nst, nefreq, npfreq
      do ist = eph_fst, eph_lst
        write( un, '("# band",i5)') ist
        write( un, '("#",19x,10000g20.10)') efreqs
        do iw2 = 1, npfreq
          write( un, '(100000g20.10)') pfreqs( iw2), a2F( :, iw2, ist)
        end do
        write( un, *)
        write( un, *)
      end do

      close( un)
      return
    end subroutine eph_a2F_output

end module mod_eph_a2F
