module mod_eph_phonons
  use mod_atoms, only: nspecies, natmtot, idxas, spmass, atposc
  use modmain, only: natoms
  use mod_symmetry, only: nsymcrys, symlat, lsplsymc
  use mod_lattice, only: omega
  use mod_eph_variables
  use m_plotmat

  implicit none

! methods
  contains

    ! interpolation wrapper
    subroutine eph_phonons_interpolate( kset, qset, phfreq, phevec, epsinf, zstar)
      type( k_set), intent( in)      :: kset, qset
      real(8), intent( out)          :: phfreq( eph_nmode, qset%nkpt)
      complex(8), intent( out)       :: phevec( 3, natmtot, eph_nmode, qset%nkpt)
      real(8), optional, intent( in) :: epsinf(3,3)
      real(8), optional, intent( in) :: zstar( 3, 3, natmtot)

      integer :: iq, i, imode, is, ia, ias, ip, iv(3)
      real(8) :: r, v1(3), v2(3)
      type( k_set) :: tmpqset

      complex(8), allocatable :: dynk(:,:,:), dynq(:,:,:)

      if( eph_einstein) then
        phfreq = eph_einstein_phfreq
        return
      end if

      if( eph_debye) then
        do iq = 1, qset%nkpt
          v1 = qset%vkl(:,iq)
          call r3ws( 1.d-6, qset%bvec, v1, iv)
          call r3mv( qset%bvec, v1, v2)
          phfreq(1,iq) = eph_debye_phdisp*norm2( v2)
          if( phfreq(1,iq) .gt. eph_debye_phfreq) phfreq(1,iq) = 0.d0
        end do
        return
      end if

      allocate( dynk( eph_nmode, eph_nmode, kset%nkpt))
      allocate( dynq( eph_nmode, eph_nmode, qset%nkpt))

      call eph_phonons_read_dynmat( kset, dynk, tsym=.true.)
      !call eph_phonons_sumrule( kset, dynk)
      if( present( epsinf) .and. present( zstar)) then
        if( kset%nkpt .eq. 1) then
          if( norm2( kset%vkl(:,1)) .lt. 1.d-6) then
            tmpqset = qset
            do iq = 1, qset%nkpt
              r = norm2( tmpqset%vkc(:,iq))
              if( r .gt. 1.d-16) then
                tmpqset%vkl(:,iq) = tmpqset%vkl(:,iq)*1.d-16/r
                tmpqset%vkc(:,iq) = tmpqset%vkc(:,iq)*1.d-16/r
              end if
            end do
            call eph_phonons_interpolate_dynmat( kset, dynk, tmpqset, dynq, epsinf=epsinf, zstar=zstar)
          else
            do iq = 1, qset%nkpt
              dynq(:,:,iq) = dynk(:,:,1)
            end do
          end if
        else
          call eph_phonons_interpolate_dynmat( kset, dynk, qset, dynq, epsinf=epsinf, zstar=zstar)
        end if
      else
        if( kset%nkpt .eq. 1) then
          do iq = 1, qset%nkpt
            dynq(:,:,iq) = dynk(:,:,1)
          end do
        else
          call eph_phonons_interpolate_dynmat( kset, dynk, qset, dynq)
        end if
      end if
      deallocate( dynk)

      do iq = 1, qset%nkpt
        call eph_phonons_diag_dynmat( dynq(:,:,iq), phfreq(:,iq), phevec(:,:,:,iq))
      end do
      deallocate( dynq)

      return
    end subroutine eph_phonons_interpolate
      
    ! read dynamical matrices from file
    subroutine eph_phonons_read_dynmat( kset, dynk, tsym)
      use mod_dynmat
      use m_getunit
      type( k_set), intent( in)     :: kset
      complex(8), intent( out)      :: dynk( eph_nmode, eph_nmode, kset%nkpt)
      logical, optional, intent(in) :: tsym

      logical :: exist, tsym_
      integer :: ik, ik0, is, js, ia, ja, ip, jp, i, j, un
      real(8) :: a, b, vc(3), vl(3), m(3,3),  w( eph_nmode)
      character(256) :: fext
      character(256) :: chdummy

      tsym_ = .true.
      if( present( tsym)) tsym_ = tsym

      !call readdyn_espresso( .true., dynk)
      !return

      if( eph_dfpt) then
        call read_dyn( 'dyn_mat_bGa2O3/beta')
        call r3minv( eph_kset_ph%bvec, m)
        do ik = 1, nqredtot
          call r3mv( m, xqcred(:,ik), vl)
          call findkptinset( vl, kset, is, ik0)
          !write(*,'(2(i,3f13.6))') ik, vl, ik0, kset%vkl(:,ik0)
          dynk(:,:,ik0) = cmplx( 0.5d0, 0.d0, 8)*dynmat(:,:,ik)
          if( tsym_) call dynsym( kset%vkl(:,ik0), dynk(:,:,ik0))
        end do
        call dynmat_destroy
        return
      end if

      do ik = 1, kset%nkpt
        i = 0
        do is = 1, nspecies
          do ia = 1, natoms (is)
            do ip = 1, 3
              i = i + 1
              call phfext( ik, is, ia, ip, 0, 1, chdummy, fext, chdummy)
              inquire( file='DYN'//trim(fext), exist=exist)
              if( .not. exist) then
                write(*,*)
                write(*, '("Error (eph_read_dynmat): file not found :")')
                write(*, '(A)') ' DYN' // trim (fext)
                write(*,*)
                stop
              end if
              call getunit( un)
              open( un, file='DYN'//trim(fext), action='read', status='old', form='formatted')
              j = 0
              do js = 1, nspecies
                do ja = 1, natoms (js)
                  do jp = 1, 3
                    j = j + 1
                    read( un, *) a, b
                    ! NOTE: changed indices compared to subroutine `readdyn`
                    dynk( i, j, ik) = cmplx( a, b, 8)
                  end do
                end do
              end do
              close( un)
            end do
          end do
        end do

        ! symmetrise the dynamical matrix
        if( tsym_) call dynsym( kset%vkl( :, ik), dynk( :, :, ik))
      end do

      return
    end subroutine eph_phonons_read_dynmat

    ! apply acoustic sum rule
    subroutine eph_phonons_sumrule( kset, dynk)
      use m_linalg, only: zhediag
      type( k_set), intent( in)  :: kset
      complex(8), intent( inout) :: dynk( eph_nmode, eph_nmode, kset%nkpt)

      integer :: ik, ik0, i, j, k
      real(8) :: eval( eph_nmode)
      complex(8) :: dyn0( eph_nmode, eph_nmode), evec( eph_nmode, eph_nmode)

      call findkptinset( (/0.d0, 0.d0, 0.d0/), kset, i, ik0)
      dyn0 = 0.5d0*( dynk(:,:,ik0) + conjg( transpose( dynk(:,:,ik0))))
      call zhediag( dyn0, eval, evec=evec)

      do ik = 1, kset%nkpt
        do i = 1, eph_nmode
          do j = 1, eph_nmode
            do k = 1, 3
              dynk( i, j, ik) = dynk( i, j, ik) - eval(k)*evec(i,k)*conjg( evec(j,k))
            end do
          end do
        end do
      end do

      return        
    end subroutine eph_phonons_sumrule

    ! interpolate dynamical matrices from original grid to interpolation grid
    subroutine eph_phonons_interpolate_dynmat( kset, dynk, qset, dynq, epsinf, zstar)
      use m_wsweight
      use m_plotmat
      use mod_lattice,   only: omega
      use m_getunit

      type( k_set), intent( in)      :: kset, qset                              ! coarse (original) and fine (interpolation) grid
      complex(8), intent( inout)     :: dynk( eph_nmode, eph_nmode, kset%nkpt)  ! dynamical matrices on original grid
      complex(8), intent( out)       :: dynq( eph_nmode, eph_nmode, qset%nkpt)  ! dynamical matrices on interpolation grid
      real(8), optional, intent( in) :: epsinf(3,3)                             ! clamped nuclei dielectric tensor
      real(8), optional, intent( in) :: zstar( 3, 3, natmtot)                   ! Born effective charge tensors

      logical :: nan
      integer :: nrpt
      real(8) :: atpos( 3, natmtot)
      complex(8), allocatable :: dynrc(:,:,:)
      real(8), allocatable :: dynr(:,:,:)

      integer :: ngridkph(3), i1, i2, i3, r1, r2, r3, ir, ik, iq, n, isym, lspl, iv(3), is, ia, js, ja, ias, jas, un
      real(8) :: v1(3), v2(3), v3(3), s(3,3), rdotk, avec(3,3)
      complex(8) :: dyns( eph_nmode, eph_nmode), zwgt

      nan = .false.
      if( .true. .and. present( epsinf) .and. present( zstar)) nan = .true.
      ngridkph = kset%ngridk

      ! substract non-analytic part
      if( nan) then
        call eph_phonons_gendynmat_lr( kset, epsinf, zstar, -zone, dynk)
      end if

      ! get analytic dynamical matrix in real-space representation (short range force constants)
      nrpt = product( ngridkph)
      allocate( dynrc( eph_nmode, eph_nmode, nrpt))
      allocate( dynr( eph_nmode, eph_nmode, nrpt))
      dynrc = zzero
      do i1 = 0, ngridkph(1)-1
        v1(1) = dble( i1)/dble( ngridkph(1))
        do i2 = 0, ngridkph(2)-1
          v1(2) = dble( i2)/dble( ngridkph(2))
          do i3 = 0, ngridkph(3)-1
            v1(3) = dble( i3)/dble( ngridkph(3))
            ik = kset%ikmap( i1, i2, i3)
            v2 = v1
            call vecfbz( input%structure%epslat, kset%bvec, v2, iv)
            n = 0
            dyns = zzero
            do isym = 1, nsymcrys
              lspl = lsplsymc (isym)
              s = dble( symlat( :, :, lspl))
              call r3mtv (s, kset%vkl(:, ik), v3)
              call vecfbz (input%structure%epslat, kset%bvec, v3, iv)
              if (norm2(v2 - v3) .lt. input%structure%epslat) then
                call dynsymapp( isym, kset%vkl(:, ik), dynk(:, :, ik), dyns)
                n = n + 1
              end if
            end do
            if( n .eq. 0) Then
               write(*,*)
               write(*,'("Error( eph_phonons_interpolate_dynmat): vector ", 3G18.10)') v1
               write(*,'(" cannot be mapped to reduced q-point set&
              &")')
               write (*,*)
               stop
            end if
            dyns = dyns/dble( n)
            ir = 0
            do r3 = 0, ngridkph(3)-1
              do r2 = 0, ngridkph(2)-1
                do r1 = 0, ngridkph(1)-1
                  ir = ir + 1
                  rdotk = twopi*dot_product( v1, dble( (/r1, r2, r3/)))
                  zwgt = cmplx( cos( rdotk), sin( rdotk), 8)
                  dynrc(:,:,ir) = dynrc(:,:,ir) + zwgt*dyns
                end do
              end do
            end do

          end do
        end do
      end do
                        
      dynrc = dynrc/product( ngridkph)
      dynr = dble( dynrc)
      call eph_phonons_symmetrize_dynr( eph_kset_ph%ngridk, dynr)

      !call r3minv( kset%bvec, avec)
      !avec = twopi*avec
      !ir = 0
      !do r3 = 0, ngridkph(3)-1
      !  do r2 = 0, ngridkph(2)-1
      !    do r1 = 0, ngridkph(1)-1
      !      ir = ir + 1
      !      v1 = dble( (/r1, r2, r3/))/ngridkph
      !      call r3ws( 1.d-6, avec, v1, iv)
      !      call r3mv( avec, v1, v2)
      !      write(*,'(3i4,4g20.10)') r1, r2, r3, norm2( v2), &
      !                               maxval( abs( dble( dynrc(:,:,ir)))), &
      !                               maxval( abs( aimag( dynrc(:,:,ir)))), &
      !                               maxval( abs( dynrc(:,:,ir)))
      !    end do
      !  end do
      !end do

      ! do interpolation
      dynq = zzero
      do iq = 1, qset%nkpt
        i1 = 1
        do is = 1, nspecies
          do ia = 1, natoms( is)
            i2 = 1
            do js = 1, nspecies
              do ja = 1, natoms( js)
                ir = 0
                do r3 = 0, ngridkph(3)-1
                  do r2 = 0, ngridkph(2)-1
                    do r1 = 0, ngridkph(1)-1
                      ir = ir + 1
                      v1 = dble( (/r1, r2, r3/))
                      if( eph_dfpt) then
                        v2 = v1 + input%structure%speciesarray(is)%species%atomarray(ia)%atom%coord(:) &
                                - input%structure%speciesarray(js)%species%atomarray(ja)%atom%coord(:)
                      else
                        v2 = v1 + input%structure%speciesarray(js)%species%atomarray(ja)%atom%coord(:) &
                                - input%structure%speciesarray(is)%species%atomarray(ia)%atom%coord(:)
                      end if
                      call ws_weight( v1, v2, qset%vkl(:,iq), kset%ngridk, zwgt)
                      dynq( i1:i1+2, i2:i2+2, iq) = dynq( i1:i1+2, i2:i2+2, iq) + zwgt*dynr( i1:i1+2, i2:i2+2, ir)
                    end do
                  end do
                end do
                call dynsym( qset%vkl( :, iq), dynq( :, :, iq))
                i2 = i2 + 3
              end do
            end do
            i1 = i1 + 3
          end do
        end do
      end do

      ! add non-analytic part
      if( nan) then
        call eph_phonons_gendynmat_lr( qset, epsinf, zstar, zone, dynq)
        ! restore input dynamical matrices
        call eph_phonons_gendynmat_lr( kset, epsinf, zstar, zone, dynk)
      end if

      deallocate( dynr, dynrc)

      return        
    end subroutine eph_phonons_interpolate_dynmat

    subroutine eph_phonons_symmetrize_dynr( ngrid, dynr)
      ! apply symmetry constraints and impose acoustic sum rules on
      ! interatomic force constants according to Nicolas Mounet
      integer, intent( in)    :: ngrid(3)
      real(8), intent( inout) :: dynr( eph_nmode, eph_nmode, product( ngrid))

      integer :: i, j, l, n1, n2, n3, ias, jas, ia, ja, ir1, ir2, nw, nx, ndim, nvmax
      real(8) :: r1
      real(8) :: t1, t2
      logical :: next

      integer, allocatable :: wpos(:,:,:)
      real(8), allocatable :: wval(:,:), xvec(:,:,:,:), lvec1(:,:,:), lvec2(:,:,:)

      real(8), external :: ddot

      ndim = 9*natmtot*natmtot*product( ngrid)
      nvmax = eph_nmode*((natmtot+1)*product( ngrid) + 2)

      nw = ndim
      allocate( wpos( 2, 3, nw), wval( 2, nw))
      wpos = 0
      wval = 0.d0

      !do ias = 1, natmtot
      !  do i = 1, 3
      !    ia = (ias-1)*3+i
      !    do jas = ias, natmtot
      !      do j = 1, 3
      !        ja = (jas-1)*3+j
      !        write(*,*) ia, ja
      !      end do
      !    end do
      !    write(*,*)
      !  end do
      !end do
      call timesec(t1)
      ! set up orthonormal vectors w
      nw = 0
      do n3 = 0, ngrid(3) - 1
        do n2 = 0, ngrid(2) - 1
          do n1 = 0, ngrid(1) - 1
            ir1 = n3*ngrid(2)*ngrid(1) + n2*ngrid(1) + n1 + 1
            ir2 = mod( ngrid(3)-n3, ngrid(3))*ngrid(2)*ngrid(1) + &
                  mod( ngrid(2)-n2, ngrid(2))*ngrid(1) + &
                  mod( ngrid(1)-n1, ngrid(1)) + 1
            do ias = 1, natmtot
              do i = 1, 3
                ia = (ias-1)*3+i
                do jas = ias, natmtot
                  do j = 1, 3
                    ja = (jas-1)*3+j
                    if( ir1 .eq. ir2 .and. ias .eq. jas .and. i .eq. j) cycle
                    next = .false.
                    do l = 1, nw
                      if( wpos(2,1,l) .eq. ia .and. &
                          wpos(2,2,l) .eq. ja .and. &
                          wpos(2,3,l) .eq. ir1) then
                        next = .true.
                        exit
                      end if
                    end do
                    if( next) cycle

                    nw = nw + 1
                    wpos(1,1,nw) = ia
                    wpos(1,2,nw) = ja
                    wpos(1,3,nw) = ir1
                    wval(1,nw)   = 1.d0/dsqrt(2.d0)
                    wpos(2,1,nw) = ja
                    wpos(2,2,nw) = ia
                    wpos(2,3,nw) = ir2
                    wval(2,nw)   = -1.d0/dsqrt( 2.d0)
                  end do
                end do
              end do
            end do
          end do
        end do
      end do
      ! find additional linearly independet vectors u
      ! and orthonormalize them
      allocate( lvec1( eph_nmode, eph_nmode, product( ngrid)))
      allocate( lvec2( eph_nmode, eph_nmode, product( ngrid)))
      allocate( xvec( eph_nmode, eph_nmode, product( ngrid), 9*natmtot))
      xvec = 0.d0
      nx = 0
      do ias = 1, natmtot
        do i = 1, 3
          do j = 1, 3
            lvec1 = 0.d0
            do jas = 1, natmtot
              lvec1( (ias-1)*3+i, (jas-1)*3+j, :) = 1.d0
            end do
            lvec2 = lvec1
            do l = 1, nw
              r1 = wdotg( wpos(:,:,l), wval(:,l), lvec1)
              lvec2( wpos(1,1,l), wpos(1,2,l), wpos(1,3,l)) = &
                lvec2( wpos(1,1,l), wpos(1,2,l), wpos(1,3,l)) - r1*wval(1,l)
              lvec2( wpos(2,1,l), wpos(2,2,l), wpos(2,3,l)) = &
                lvec2( wpos(2,1,l), wpos(2,2,l), wpos(2,3,l)) - r1*wval(2,l)
            end do
            do l = 1, nx
              r1 = ddot( ndim, xvec(:,:,:,l), 1, lvec1, 1)
              lvec2 = lvec2 - r1*xvec(:,:,:,l)
            end do
            r1 = dsqrt( ddot( ndim, lvec2, 1, lvec2, 1))
            if( r1 .gt. 1.d-10) then
              nx = nx + 1
              xvec(:,:,:,nx) = lvec2/r1
            end if
          end do
        end do
      end do
      call timesec(t2)
      !write(*,*) "ASR"
      !write(*,*) nw, nx, nvmax
      !write(*,'("T BASIS",f13.6)') t2-t1
      ! calculate projection on subspace
      lvec1 = 0.d0
      do l = 1, nw
        r1 = wdotg( wpos(:,:,l), wval(:,l), dynr)
        lvec1( wpos(1,1,l), wpos(1,2,l), wpos(1,3,l)) = &
          lvec1( wpos(1,1,l), wpos(1,2,l), wpos(1,3,l)) + r1*wval(1,l)
        lvec1( wpos(2,1,l), wpos(2,2,l), wpos(2,3,l)) = &
          lvec1( wpos(2,1,l), wpos(2,2,l), wpos(2,3,l)) + r1*wval(2,l)
      end do
      do l = 1, nx
        r1 = ddot( ndim, xvec(:,:,:,l), 1, dynr, 1)
        lvec1 = lvec1 + r1*xvec(:,:,:,l)
      end do
      dynr = dynr - lvec1
      call timesec( t1)
      !write(*,'("T PROJECTION",f13.6)') t1-t2
      !write(*,'(2f13.6)') maxval( abs( lvec1)), dsqrt( ddot( ndim, lvec1, 1, lvec1, 1)/dble( ndim))
      deallocate( wpos, wval, lvec1, lvec2, xvec)
      return

      contains
        function wdotg( pos, val, vec) result( r)
          integer, intent( in) :: pos(2,3)
          real(8), intent( in) :: val(2), vec( eph_nmode, eph_nmode, product( ngrid))
          real(8) :: r

          r = vec( pos(1,1), pos(1,2), pos(1,3))*val(1) + vec( pos(2,1), pos(2,2), pos(2,3))*val(2)
          return
        end function wdotg
    end subroutine eph_phonons_symmetrize_dynr

    ! diagonalize dynamical matrix to obtain phonon frequencies and eigenvectors
    subroutine eph_phonons_diag_dynmat( dyn, w, evec)
      use m_linalg, only: zhediag
      complex(8), intent( in)  :: dyn( eph_nmode, eph_nmode)
      real(8), intent( out)    :: w( eph_nmode)
      complex(8), intent( out) :: evec( 3, natmtot, eph_nmode)

      integer :: i, is, ia, ias, ip, j, js, ja, jp
      real(8) :: t1
      complex(8) :: tmp( eph_nmode, eph_nmode), ev( eph_nmode, eph_nmode)

      tmp = zzero
      i = 0
      do is = 1, nspecies
        do ia = 1, natoms( is)
          do ip = 1, 3
            i = i + 1
            j = 0
            do js = 1, nspecies
              if( (spmass( is) .le. 0.d0) .or. (spmass( js) .le. 0.d0)) then
                t1 = 0.d0
              else
                t1 = 1.d0/dsqrt( spmass( is)*spmass( js))
              end if
              do ja = 1, natoms( js)
                do jp = 1, 3
                  j = j + 1
                  tmp( i, j) = 0.5d0*t1*(dyn( i, j) + conjg( dyn( j, i)))
                end do
              end do
            end do
          end do
        end do
      end do
      call zhediag( tmp, w, ev)
      do i = 1, eph_nmode
        if( w(i) .ge. 0.d0) then
          w(i) = dsqrt( w(i))
        else
          w(i) = -dsqrt( abs( w(i)))
        end if
        j = 0
        do is = 1, nspecies
          do ia = 1, natoms( is)
            ias = idxas( ia, is)
            do ip = 1, 3
              j = j + 1
              evec( ip, ias, i) = ev( j, i)
            end do
          end do
        end do
      end do

      return
    end subroutine eph_phonons_diag_dynmat

    subroutine eph_phonons_gendynmat_lr( kset, epsinf, zstar, pref, dynk)
      ! See Gonze and Lee, PRB 55 (1997)
      type( k_set), intent( in)  :: kset
      real(8), intent( in)       :: epsinf(3,3)
      real(8), intent( in)       :: zstar( 3, 3, natmtot)
      complex(8), intent( in)    :: pref
      complex(8), intent( inout) :: dynk( eph_nmode, eph_nmode, kset%nkpt)

      real(8), parameter :: gmax = 37.d0 ! exp(-37) ~ 1e-16
      real(8), parameter :: rmax = 6.d0  ! exp(-6^2) ~ erfc(6) ~ 1e-16
      real(8), parameter :: alpha = 2.d0

      integer :: ik, g1, g2, g3, r1, r2, r3, is, ia, ias, js, ja, jas, itg, itr
      integer :: ng(3), nr(3)
      real(8) :: avec(3,3), bvec(3,3), epsi(3,3), lgqc, vgqc(3), nvgqc(3), r(3), d(3), dd(3), m(3,3), nd, gqegq, epsid, dotp, c1, c2
      complex(8) :: z1, z2

      real(8), allocatable :: tr(:)
      complex(8), allocatable :: tz(:), dyn0(:,:,:), tmp(:,:)

      real(8), external :: r3mdet

      bvec = kset%bvec
      call r3minv( bvec, avec)
      avec = twopi*avec
      call r3minv( epsinf, epsi)
      epsid = r3mdet( epsi)

      m = matmul( transpose( bvec), matmul( epsinf, bvec))
      ng(1) = ceiling( sqrt( gmax*4.d0*alpha**2/m(1,1)))
      ng(2) = ceiling( sqrt( gmax*4.d0*alpha**2/m(2,2)))
      ng(3) = ceiling( sqrt( gmax*4.d0*alpha**2/m(3,3)))
      m = matmul( transpose( avec), matmul( epsi, avec))
      nr(1) = ceiling( rmax/sqrt( alpha**2*m(1,1)))
      nr(2) = ceiling( rmax/sqrt( alpha**2*m(2,2)))
      nr(3) = ceiling( rmax/sqrt( alpha**2*m(3,3)))

      !write(*,*) ng
      !write(*,'(3f13.6)') bvec(:,1)
      !write(*,'(3f13.6)') bvec(:,2)
      !write(*,'(3f13.6)') bvec(:,3)
      !write(*,*) nr
      !write(*,'(3f13.6)') avec(:,1)
      !write(*,'(3f13.6)') avec(:,2)
      !write(*,'(3f13.6)') avec(:,3)

      ! k=0 part
      allocate( dyn0(3,3,natmtot), tmp(eph_nmode,eph_nmode))
      allocate( tr(eph_nmode), tz(eph_nmode))
      dyn0 = zzero; tmp = zzero
      ! reciprocal space sum
      z1 = pref*fourpi/omega
      do g1 = -ng(1), ng(1)
        do g2 = -ng(2), ng(2)
          do g3 = -ng(3), ng(3)
            call r3mv( bvec, dble((/g1,g2,g3/)), vgqc)
            lgqc = norm2( vgqc);
            if( lgqc < 1.d-16) cycle
            nvgqc = vgqc/lgqc
            gqegq = nvgqc(1)*(epsinf(1,1)*nvgqc(1) + epsinf(1,2)*nvgqc(2) + epsinf(1,3)*nvgqc(3)) + &
                    nvgqc(2)*(epsinf(2,1)*nvgqc(1) + epsinf(2,2)*nvgqc(2) + epsinf(2,3)*nvgqc(3)) + &
                    nvgqc(3)*(epsinf(3,1)*nvgqc(1) + epsinf(3,2)*nvgqc(2) + epsinf(3,3)*nvgqc(3))
            if( lgqc**2*abs(gqegq) < 1.d-16 .or. lgqc**2*abs(gqegq)/(4.d0*alpha**2) > gmax) cycle
            z2 = z1*exp( -lgqc**2*abs(gqegq)/(4.d0*alpha**2))/gqegq
            call dgemv( 't', 3, eph_nmode, 1.d0, zstar, 3, nvgqc, 1, 0.d0, tr, 1)
            do is = 1, nspecies
              do ia = 1, natoms( is)
                ias = (idxas( ia, is) - 1)*3 + 1
                dotp = dot_product( vgqc, atposc(:,ia,is))
                tz(ias:(ias+2)) = tr(ias:(ias+2))*cmplx( cos( dotp), sin( dotp), 8)
              end do
            end do
            call zgemm( 'c', 'n', eph_nmode, eph_nmode, 1, z2, tz, 1, tz, 1, zone, tmp, eph_nmode)
          end do
        end do
      end do
      ! real space sum
      z1 = -pref*sqrt( epsid)
      do r1 = -nr(1), nr(1)
        do r2 = -nr(2), nr(2)
          do r3 = -nr(3), nr(3)
            call r3mv( avec, dble((/r1,r2,r3/)), r)
            z2 = z1
            do is = 1, nspecies
              do ia = 1, natoms( is)
                ias = (idxas( ia, is) - 1)*3 + 1
                do js = 1, nspecies
                  do ja = 1, natoms( js)
                    jas = (idxas( ja, js) - 1)*3 + 1
                    d = r + atposc(:,ja,js) - atposc(:,ia,is)
                    call r3mv( epsi, d, dd)
                    nd = sqrt( dot_product( dd, d))
                    if( nd < 1.d-16 .or. alpha*nd > rmax) cycle ! exp(-6^2) ~ erfc(6) ~ 1e-16
                    c1 = (3.d0/nd**3*erfc(alpha*nd) + 2.d0*alpha/sqrt(pi)*(2.d0*alpha**2+3.d0/nd**2)*exp(-(alpha*nd)**2))/dot_product( dd, d)
                    c2 = -(erfc(alpha*nd)/nd**3 + 2.d0*alpha/(sqrt(pi)*nd**2)*exp(-(alpha*nd)**2))
                    m = c2*epsi
                    call dgemm( 't', 'n', 3, 3, 1, c1, dd, 1, dd, 1, 1.d0, m, 3)
                    m = matmul( m, zstar(:,:,idxas(ja,js)))
                    m = matmul( transpose( zstar(:,:,idxas(ia,is))), m)
                    tmp(ias:(ias+2),jas:(jas+2)) = tmp(ias:(ias+2),jas:(jas+2)) + z2*m
                  end do
                end do
              end do
            end do
          end do
        end do
      end do
      ! limiting contribution and sum over atoms
      ! Note: We can omit the limiting contribution. It gets cancled anyway.
      do is = 1, nspecies
        do ia = 1, natoms( is)
          ias = (idxas( ia, is) - 1)*3 + 1
          do js = 1, nspecies
            do ja = 1, natoms( js)
              jas = (idxas( ja, js) - 1)*3 + 1
              dyn0(:,:,idxas(ia,is)) = dyn0(:,:,idxas(ia,is)) + tmp(ias:(ias+2),jas:(jas+2))
            end do
          end do
        end do
      end do

      ! k-dependent part
      do ik = 1, kset%nkpt
        ! reciprocal space sum
        z1 = pref*fourpi/omega
        itg = 0
        do g1 = -ng(1), ng(1)
          do g2 = -ng(2), ng(2)
            do g3 = -ng(3), ng(3)
              call r3mv( bvec, kset%vkl(:,ik)+dble((/g1,g2,g3/)), vgqc)
              lgqc = norm2( vgqc)
              if( lgqc < 1.d-16) cycle
              nvgqc = vgqc/lgqc
              gqegq = nvgqc(1)*(epsinf(1,1)*nvgqc(1) + epsinf(1,2)*nvgqc(2) + epsinf(1,3)*nvgqc(3)) + &
                      nvgqc(2)*(epsinf(2,1)*nvgqc(1) + epsinf(2,2)*nvgqc(2) + epsinf(2,3)*nvgqc(3)) + &
                      nvgqc(3)*(epsinf(3,1)*nvgqc(1) + epsinf(3,2)*nvgqc(2) + epsinf(3,3)*nvgqc(3))
              if( lgqc**2*abs(gqegq) < 1.d-16 .or. lgqc**2*abs(gqegq)/(4.d0*alpha**2) > gmax) cycle
              itg = itg + 1
              z2 = z1*exp( -lgqc**2*abs(gqegq)/(4.d0*alpha**2))/gqegq
              call dgemv( 't', 3, eph_nmode, 1.d0, zstar, 3, nvgqc, 1, 0.d0, tr, 1)
              do is = 1, nspecies
                do ia = 1, natoms( is)
                  ias = (idxas( ia, is) - 1)*3 + 1
                  dotp = dot_product( vgqc, atposc(:,ia,is))
                  tz(ias:(ias+2)) = tr(ias:(ias+2))*cmplx( cos( dotp), sin( dotp), 8)
                end do
              end do
              call zgemm( 'c', 'n', eph_nmode, eph_nmode, 1, z2, tz, 1, tz, 1, zone, dynk(:,:,ik), eph_nmode)
            end do
          end do
        end do
        ! real space sum
        z1 = -pref*sqrt( epsid)
        itr = 0
        do r1 = -nr(1), nr(1)
          do r2 = -nr(2), nr(2)
            do r3 = -nr(3), nr(3)
              call r3mv( avec, dble((/r1,r2,r3/)), r)
              dotp = dot_product( r, kset%vkc(:,ik))
              z2 = z1*cmplx( cos( dotp), sin( dotp), 8)
              do is = 1, nspecies
                do ia = 1, natoms( is)
                  ias = (idxas( ia, is) - 1)*3 + 1
                  do js = 1, nspecies
                    do ja = 1, natoms( js)
                      jas = (idxas( ja, js) - 1)*3 + 1
                      d = r + atposc(:,ja,js) - atposc(:,ia,is)
                      call r3mv( epsi, d, dd)
                      nd = sqrt( dot_product( dd, d))
                      if( nd < 1.d-16 .or. alpha*nd > rmax) cycle
                      itr = itr + 1
                      c1 = (3.d0/nd**3*erfc(alpha*nd) + 2.d0*alpha/sqrt(pi)*(2.d0*alpha**2+3.d0/nd**2)*exp(-(alpha*nd)**2))/dot_product( dd, d)
                      c2 = -(erfc(alpha*nd)/nd**3 + 2.d0*alpha/(sqrt(pi)*nd**2)*exp(-(alpha*nd)**2))
                      m = c2*epsi
                      call dgemm( 't', 'n', 3, 3, 1, c1, dd, 1, dd, 1, 1.d0, m, 3)
                      m = matmul( m, zstar(:,:,idxas(ja,js)))
                      m = matmul( transpose( zstar(:,:,idxas(ia,is))), m)
                      dynk(ias:(ias+2),jas:(jas+2),ik) = dynk(ias:(ias+2),jas:(jas+2),ik) + z2*m
                    end do
                  end do
                end do
              end do
            end do
          end do
        end do
        ! limiting contribution and q=0 term
        ! Note: We can omit the limiting contribution. It gets cancled anyway.
        do is = 1, nspecies
          do ia = 1, natoms( is)
            ias = (idxas( ia, is) - 1)*3 + 1
            dynk(ias:(ias+2),ias:(ias+2),ik) = dynk(ias:(ias+2),ias:(ias+2),ik) - dyn0(:,:,idxas(ia,is))
          end do
        end do
      end do

      deallocate( tmp, tr, tz, dyn0)

    end subroutine eph_phonons_gendynmat_lr

    subroutine eph_phonons_output
      use m_getunit
      use modmpi
      integer :: iq, imode, un

      real(8), allocatable :: phfreq(:,:)
      complex(8), allocatable :: phevec(:,:,:,:), dynk(:,:,:)

      if( mpiglobal%rank .ne. 0) return

      allocate( phfreq( eph_nmode, eph_pset%nkpt))
      allocate( phevec( 3, natmtot, eph_nmode, eph_pset%nkpt))

      if( eph_polar) then
        call eph_phonons_interpolate( eph_kset_ph, eph_pset, phfreq, phevec, epsinf=eph_epsinf, zstar=eph_zstar)
      else
        call eph_phonons_interpolate( eph_kset_ph, eph_pset, phfreq, phevec)
      end if

      call getunit( un)
      open( un, file='EPH_PHDISP.DAT', action='write', form='formatted')
      do imode = 1, eph_nmode
        do iq = 1, eph_pset%nkpt
          if( allocated( eph_pset_dpp1d)) write( un, '(g20.10e3)', advance='no') eph_pset_dpp1d( iq)
          write( un, '(g20.10e3,3f16.6)') phfreq( imode, iq), eph_pset%vkl( :, iq)
        end do
        write( un, *)
      end do
      close( un)
      deallocate( phfreq, phevec)

      if( allocated( eph_pset_dpp1d)) then
        call getunit( un)
        open( un, file='EPH_PHVTXLINES.DAT', action='write', form='formatted')
        do iq = 1, eph_pset_nvp1d
          write( un, '(g20.10,3f16.10,3x,a)') eph_pset_dvp1d( iq), eph_pset_verts( iq)%coord, trim( eph_pset_verts( iq)%label)
        end do
        close( un)

        ! print also the points the original grid points along the path
        if( eph_pset_ngp1d_ph > 0) then
          allocate( dynk( eph_nmode, eph_nmode, eph_kset_ph%nkpt))
          allocate( phfreq( eph_nmode, eph_pset_ngp1d_ph))
          allocate( phevec( 3, natmtot, eph_nmode, eph_pset_ngp1d_ph))
          call eph_phonons_read_dynmat( eph_kset_ph, dynk, tsym=.true.)
          do iq = 1, eph_pset_ngp1d_ph
            call eph_phonons_diag_dynmat( dynk( :, :, eph_pset_igp1d_ph( iq)), phfreq(:,iq), phevec(:,:,:,iq))
          end do
          call getunit( un)
          open( un, file='EPH_PHDISP_GRID.DAT', action='write', form='formatted')
          do imode = 1, eph_nmode
            do iq = 1, eph_pset_ngp1d_ph
              write( un, '(2g20.10e3,3f16.6)') eph_pset_dgp1d_ph( iq), phfreq( imode, iq), &
                eph_kset_ph%vkl( :, eph_pset_igp1d_ph( iq))
            end do
            write( un, *)
            write( un, *)
          end do
          close( un)
        end if
      end if

      return
    end subroutine eph_phonons_output
end module mod_eph_phonons
