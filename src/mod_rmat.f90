module mod_rmat
  use modinput
  use modmpi
  use mod_atoms
  use mod_APW_LO
  use mod_muffin_tin
  use mod_constants
  use mod_Gvector
  use mod_eigensystem, only : idxlo, nmatmax_ptr, oalo, ololo
  use mod_lattice, only : ainv, omega
  use mod_Gkvector, only : ngkmax_ptr, gkmax
  use mod_eigenvalue_occupancy, only : nstfv
  use mod_misc, only : filext
  use mod_kpointset
  use modxs, only : fftmap_type
  use m_plotmat

  implicit none
  private

  integer              :: rmat_fst1, rmat_lst1, rmat_fst2, rmat_lst2, rmat_nst1, rmat_nst2
  integer              :: ng, nlammax, nlmlammax, rmat_lmaxapw
  real(8)              :: rmat_gmax
  real(8)              :: vecql(3)
  character(256)       :: rmat_fname
  type( k_set)         :: rmat_kset
  type( fftmap_type)   :: rmat_fftmap

  integer, allocatable    :: nlam(:,:), nlmlam(:), lam2apwlo(:,:,:), idxlmlam(:,:,:), lm2l(:), fftigidx(:,:,:)
  complex(8), allocatable :: rignt(:,:,:,:), rmat_cfunir(:,:)

  real(8) :: ts, te, t0, t1
  real(8) :: ttot, tinit, tri, trignt, tgk, tprep, tmt, tgg, tir, tio

  public :: rmat_init, rmat_prepare, rmat_genrmat, rmat_destroy

! variable

! methods
  contains
      subroutine rmat_init( apwlmax, kset, fst1, lst1, fst2, lst2, fname)
          integer, intent( in) :: apwlmax, fst1, lst1, fst2, lst2
          type( k_set), intent( in) :: kset
          character(*), optional, intent( in) :: fname

          integer :: l1, l2, l3, m1, m2, m3, o1, lm1, lm2, lm3, i, is, ilo, lam, ig1, ig2, igg, vi(3), igv(3,2)
          real(8) :: gnt, gaunt, vl(3), vc(3)
          complex(8) :: vz(3)

          complex(8), allocatable :: zfft(:,:), rfun(:,:)

          external :: gaunt

          ttot   = 0.d0
          tinit  = 0.d0
          tri    = 0.d0
          trignt = 0.d0
          tgk    = 0.d0
          tprep  = 0.d0
          tmt    = 0.d0
          tgg    = 0.d0
          tir    = 0.d0
          tio    = 0.d0

          call timesec( ts)

          rmat_fname = 'EVEC'
          if( present( fname)) rmat_fname = trim( fname)

          rmat_lmaxapw = min( apwlmax, input%groundstate%lmaxapw)
          rmat_kset = kset
          rmat_fst1 = fst1
          rmat_lst1 = lst1
          rmat_fst2 = fst2
          rmat_lst2 = lst2
          rmat_nst1 = rmat_lst1-rmat_fst1+1
          rmat_nst2 = rmat_lst2-rmat_fst2+1

          ! count combined (l,m,o) indices and build index maps
          allocate( nlam( 0:rmat_lmaxapw, nspecies))
          allocate( lam2apwlo( apwordmax+nlomax, 0:rmat_lmaxapw, nspecies))
          nlammax = 0
          do is = 1, nspecies
            do l1 = 0, rmat_lmaxapw
              nlam( l1, is) = 0
              lam2apwlo( :, l1, is) = 0
              do o1 = 1, apword( l1, is)
                nlam( l1, is) = nlam( l1, is)+1
                lam2apwlo( nlam( l1, is), l1, is) = o1
              end do
              do ilo = 1, nlorb( is)
                if( lorbl( ilo, is) .eq. l1) then
                  nlam( l1, is) = nlam( l1, is) + 1
                  lam2apwlo( nlam( l1, is), l1, is) = -ilo
                end if
              end do
              nlammax = max( nlammax, nlam( l1, is))
            end do
          end do

          allocate( lm2l( (rmat_lmaxapw + 1)**2))
          allocate( nlmlam( nspecies))
          allocate( idxlmlam( (rmat_lmaxapw + 1)**2, nlammax, nspecies)) 
          idxlmlam(:,:,:) = 0
          nlmlammax = 0
          nlmlam(:) = 0
          do l1 = 0, rmat_lmaxapw
            do m1 = -l1, l1
              lm1 = idxlm( l1, m1)
              lm2l( lm1) = l1
              do is = 1, nspecies
                do lam = 1, nlam( l1, is)
                  nlmlam( is) = nlmlam( is) + 1
                  idxlmlam( lm1, lam, is) = nlmlam( is)
                end do
              end do
            end do
          end do
          nlmlammax = maxval( nlmlam, 1)
          !write(*,'("pwmat init: nlmlammax = :",I4)') nlmlammax

          ! build characteristic function times r
          call genfftmap( rmat_fftmap, 2*input%groundstate%gmaxvr)
          allocate( rmat_cfunir( rmat_fftmap%ngrtot+1, 3))
          rmat_cfunir = zzero
          do ig1 = 1, ngrtot
            rmat_cfunir( rmat_fftmap%igfft( ig1), 1) = cfunig( ig1)
          end do
          ! FT characteristic function to real space
          call zfftifc( 3, rmat_fftmap%ngrid, 1, rmat_cfunir( :, 1))
          rmat_cfunir( :, 2) = rmat_cfunir( :, 1)
          rmat_cfunir( :, 3) = rmat_cfunir( :, 1)
          ! multiply with r
          igv(:,1) = rmat_fftmap%ngrid/2 - rmat_fftmap%ngrid + 1
          igv(:,2) = rmat_fftmap%ngrid/2
          allocate( fftigidx( igv(1,1):igv(1,2), igv(2,1):igv(2,2), igv(3,1):igv(3,2)))
          do l3 = igv(3,1), igv(3,2)
            m3 = modulo( l3, rmat_fftmap%ngrid(3))
            do l2 = igv(2,1), igv(2,2)
              m2 = modulo( l2, rmat_fftmap%ngrid(2))
              do l1 = igv(1,1), igv(1,2)
                m1 = modulo( l1, rmat_fftmap%ngrid(1))
                vl = dble( 2*(/l1, l2, l3/) - (/1, 1, 1/))/dble( 2*rmat_fftmap%ngrid)
                !call r3frac( 1.d-6, vl, vi)
                call r3mv( input%structure%crystal%basevect, vl, vc)
                ig1 = m3*rmat_fftmap%ngrid(2)*rmat_fftmap%ngrid(1) + m2*rmat_fftmap%ngrid(1) + m1 + 1
                rmat_cfunir( ig1, 1) = cmplx( dble( rmat_cfunir( ig1, 1))*vc(1), 0.d0, 8)
                rmat_cfunir( ig1, 2) = cmplx( dble( rmat_cfunir( ig1, 2))*vc(2), 0.d0, 8)
                rmat_cfunir( ig1, 3) = cmplx( dble( rmat_cfunir( ig1, 3))*vc(3), 0.d0, 8)
                fftigidx( l1, l2, l3) = ig1
              end do
            end do
          end do
          ! FT back to reciprocal space
          call zfftifc( 3, rmat_fftmap%ngrid, -1, rmat_cfunir( :, 1))
          call zfftifc( 3, rmat_fftmap%ngrid, -1, rmat_cfunir( :, 2))
          call zfftifc( 3, rmat_fftmap%ngrid, -1, rmat_cfunir( :, 3))
          ! asign values to G-vectors
          tinit = tinit + t1 - ts

          call rmat_init_rignt
          return
      end subroutine rmat_init

      subroutine rmat_prepare( ik, evec)
          integer, intent( in)      :: ik
          complex(8), intent( in)   :: evec( nmatmax_ptr, nstfv)

          integer :: ngp, is, ia, ias, lm, l, lam, o, ilo, fst, lst, nst
          integer :: ig, igp

          integer, allocatable :: igpig(:) 
          real(8), allocatable :: vgpl(:,:), vgpc(:,:), gpc(:), tpgpc(:,:)
          complex(8), allocatable :: evecmt(:,:,:), evecir(:,:)
          complex(8), allocatable :: sfacgp(:,:), apwalm(:,:,:,:), auxvec(:)

          fst = min( rmat_fst1, rmat_fst2)
          lst = max( rmat_lst1, rmat_lst2)
          nst = lst-fst+1

          call timesec( t0)
          allocate( evecmt( nlmlammax, fst:lst, natmtot))
          allocate( igpig( ngkmax_ptr), vgpl( 3, ngkmax_ptr), vgpc( 3, ngkmax_ptr), gpc( ngkmax_ptr), tpgpc( 2, ngkmax_ptr))
          allocate( sfacgp( ngkmax_ptr, natmtot))
          allocate( apwalm( ngkmax_ptr, apwordmax, lmmaxapw, natmtot))
          allocate( auxvec( fst:lst))

          ! generate the G+k+q-vectors
          call gengpvec( rmat_kset%vkl( :, ik), rmat_kset%vkc( :, ik), ngp, igpig, vgpl, vgpc, gpc, tpgpc)
          ! generate the structure factors
          call gensfacgp( ngp, vgpc, ngkmax_ptr, sfacgp)
          ! find matching coefficients for k-point k+q
          call match( ngp, gpc, tpgpc, sfacgp, apwalm)

          evecmt = zzero
          do is = 1, nspecies
            do ia = 1, natoms( is)
              ias = idxas( ia, is)
              do lm = 1, (rmat_lmaxapw + 1)**2
                l = lm2l( lm)
                do lam = 1, nlam( l, is)
                  ! lam belongs to apw
                  if( lam2apwlo( lam, l, is) .gt. 0) then
                    o = lam2apwlo( lam, l, is)
                    call zgemv( 't', ngp, nst, zone, &
                           evec( :, fst:lst), nmatmax_ptr, &
                           apwalm( :, o, lm, ias), 1, zzero, &
                           auxvec, 1)
                    evecmt( idxlmlam( lm, lam, is), :, ias) = auxvec
                  end if
                  ! lam belongs to lo
                  if( lam2apwlo( lam, l, is) .lt. 0) then
                    ilo = -lam2apwlo( lam, l, is)
                    evecmt( idxlmlam( lm, lam, is), :, ias) = evec( ngp+idxlo( lm, ilo, ias), fst:lst)
                  end if
                end do
              end do
            end do
          end do

          call timesec( t1)
          tprep = tprep + t1 - t0
          call rmat_putevecmt( rmat_kset%vkl( :, ik), fst, lst, evecmt)
          call timesec( t0)
          tio = tio + t0 - t1
          deallocate( evecmt, vgpl, vgpc, gpc, tpgpc, sfacgp, apwalm, auxvec)
          call timesec( t1)
          tprep = tprep + t1 - t0
          return
      end subroutine rmat_prepare
      
      subroutine rmat_init_rignt
          integer :: l1, l2, l3, m1, m2, m3, o1, o2, lm1, lm2, lm3, is, ia, ias
          integer :: lam1, lam2, ilo1, ilo2, idxlo1, idxlo2, idx1, idx2
          real(8) :: wgt, atl(3), atc(3)

          real(8), allocatable :: radint(:,:)

          complex(8), external :: gauntyry

          ! compute radial integrals times Gaunt and expansion prefactor
          call timesec( t0)
          if( allocated( rignt)) deallocate( rignt)
          allocate( rignt( nlmlammax, nlmlammax, 3, natmtot))
          rignt = zzero
          allocate( radint( nlammax, nlammax))
          ! generate radial functions
          call readfermi

          wgt = sqrt( fourpi/3.d0)
          do is = 1, nspecies
            do ia = 1, natoms( is)
              ias = idxas( ia, is)
          
              call r3mv( ainv, atposc( :, ia, is), atl)
              !atl = atl - (/0.5d0, 0.5d0, 0.5d0/)
              if( atl(1) .gt. 0.5d0) atl(1) = atl(1) - 1.d0
              if( atl(2) .gt. 0.5d0) atl(2) = atl(2) - 1.d0
              if( atl(3) .gt. 0.5d0) atl(3) = atl(3) - 1.d0
              call r3mv( input%structure%crystal%basevect, atl, atc)
              !write(*,'(2i,3f13.6)') ia, is, atc

              ! generate radial integral
              do l1 = 0, rmat_lmaxapw
                do l2 = 0, rmat_lmaxapw
                  call timesec( t1)
                  trignt = trignt + t1 - t0
                  call rmat_genri( is, ia, l1, l2, radint)
                  call timesec( t0)
                  tri = tri + t0 - t1
                  !write(*,'("init_qg: ",3I)') ias, l1, l2
                  do m1 = -l1, l1
                    lm1 = idxlm( l1, m1)
                    do m2 = -l2, l2
                      lm2 = idxlm( l2, m2)
                      
                      do lam1 = 1, nlam( l1, is)
                        do lam2 = 1, nlam( l2, is)
                        
                          idx1 = idxlmlam( lm1, lam1, is)
                          idx2 = idxlmlam( lm2, lam2, is)

                          rignt( idx1, idx2, 1, ias) = -cmplx( wgt*radint( lam1, lam2), 0.d0, 8)*gauntyry( l1, 1, l2, m1,  1, m2) 
                          rignt( idx1, idx2, 2, ias) = -cmplx( wgt*radint( lam1, lam2), 0.d0, 8)*gauntyry( l1, 1, l2, m1, -1, m2) 
                          rignt( idx1, idx2, 3, ias) =  cmplx( wgt*radint( lam1, lam2), 0.d0, 8)*gauntyry( l1, 1, l2, m1,  0, m2) 
                          
                        end do
                      end do

                    end do
                  end do
                end do
              end do

              ! add atomic contribution
              do l1 = 0, rmat_lmaxapw
                do m1 = -l1, l1
                  lm1 = idxlm( l1, m1)
                  do lam1 = 1, nlam( l1, is)
                    idx1 = idxlmlam( lm1, lam1, is)
                    do lam2 = 1, nlam( l1, is)
                      idx2 = idxlmlam( lm1, lam2, is)

                      if( lam2apwlo( lam1, l1, is) .gt. 0) then
                        o1 = lam2apwlo( lam1, l1, is)
                        ! APW-APW
                        if( lam2apwlo( lam2, l1, is) .gt. 0) then
                          o2 = lam2apwlo( lam2, l1, is)
                          if( o1 .eq. o2) rignt( idx1, idx2, :, ias) = rignt( idx1, idx2, :, ias) + cmplx( atc, 0.d0, 8)
                        ! APW-LO
                        else if( lam2apwlo( lam2, l1, is) .lt. 0) then
                          ilo2 = -lam2apwlo( lam2, l1, is)
                          rignt( idx1, idx2, :, ias) = rignt( idx1, idx2, :, ias) + cmplx( atc*oalo( o1, ilo2, ias), 0.d0, 8)
                        end if
                      else if( lam2apwlo( lam1, l1, is) .lt. 0) then
                        ilo1 = -lam2apwlo( lam1, l1, is)
                        ! LO-APW
                        if( lam2apwlo( lam2, l1, is) .gt. 0) then
                          o2 = lam2apwlo( lam2, l1, is)
                          rignt( idx1, idx2, :, ias) = rignt( idx1, idx2, :, ias) + cmplx( atc*oalo( o2, ilo1, ias), 0.d0, 8)
                        ! LO-LO
                        else if( lam2apwlo( lam2, l1, is) .lt. 0) then
                          ilo2 = -lam2apwlo( lam2, l1, is)
                          rignt( idx1, idx2, :, ias) = rignt( idx1, idx2, :, ias) + cmplx( atc*ololo( ilo1, ilo2, ias), 0.d0, 8)
                        end if
                      end if

                    end do
                  end do
                end do
              end do

            end do ! atoms
          end do !species


          deallocate( radint)
          call timesec( t1)
          trignt = trignt + t1 - t0
          return
      end subroutine rmat_init_rignt
      
      subroutine rmat_genri( is, ia, l1, l2, radint)
          integer, intent( in) :: is, ia, l1, l2 
          real(8), intent( out) :: radint( nlammax, nlammax)

          integer :: ias, ir, nr, o1, o2, ilo1, ilo2, lam1, lam2

          real(8), allocatable :: fr(:), gf(:), cf(:,:)
          
          radint(:,:) = 0.d0
          
          ias = idxas( ia, is)
          nr = nrmt( is)

#ifdef USEOMP
!!$omp parallel default( shared) private( lam1, lam2, o1, o2, ilo1, ilo2, l3, ir, fr, gf, cf)
#endif
          allocate( fr( nr), gf( nr), cf( 3, nr))
#ifdef USEOMP
!!$omp do collapse( 2)
#endif
          do lam1 = 1, nlam( l1, is)
            do lam2 = 1, nlam( l2, is)
              
              ! lam1 belongs to apw
              if( lam2apwlo( lam1, l1, is) .gt. 0) then
                o1 = lam2apwlo( lam1, l1, is)
                ! lam2 belongs to apw
                if( lam2apwlo( lam2, l2, is) .gt. 0) then
                  o2 = lam2apwlo( lam2, l2, is)
                  do ir = 1, nr
                    fr( ir) = apwfr( ir, 1, o1, l1, ias)*apwfr( ir, 1, o2, l2, ias)*spr( ir, is)**3
                  end do
                  call fderiv( -1, nr, spr( :, is), fr, gf, cf)
                  radint( lam1, lam2) = gf( nr)
                end if
                ! lam2 belongs to lo
                if( lam2apwlo( lam2, l2, is) .lt. 0) then
                  ilo2 = -lam2apwlo( lam2, l2, is)
                  do ir = 1, nr
                    fr( ir) = apwfr( ir, 1, o1, l1, ias)*lofr( ir, 1, ilo2, ias)*spr( ir, is)**3
                  end do
                  call fderiv( -1, nr, spr( :, is), fr, gf, cf)
                  radint( lam1, lam2) = gf( nr)
                end if
              end if
              ! lam1 belongs to lo
              if( lam2apwlo( lam1, l1, is) .lt. 0) then
                ilo1 = -lam2apwlo( lam1, l1, is)
                ! lam2 belongs to apw
                if( lam2apwlo( lam2, l2, is) .gt. 0) then
                  o2 = lam2apwlo( lam2, l2, is)
                  do ir = 1, nr
                    fr( ir) = lofr( ir, 1, ilo1, ias)*apwfr( ir, 1, o2, l2, ias)*spr( ir, is)**3
                  end do
                  call fderiv( -1, nr, spr( :, is), fr, gf, cf)
                  radint( lam1, lam2) = gf( nr)
                end if
                ! lam2 belongs to lo
                if( lam2apwlo( lam2, l2, is) .lt. 0) then
                  ilo2 = -lam2apwlo( lam2, l2, is)
                  do ir = 1, nr
                    fr( ir) = lofr( ir, 1, ilo1, ias)*lofr( ir, 1, ilo2, ias)*spr( ir, is)**3
                  end do
                  call fderiv( -1, nr, spr( :, is), fr, gf, cf)
                  radint( lam1, lam2) = gf( nr)
                end if
              end if

            end do
          end do
#ifdef USEOMP
!!$omp end do
#endif
          deallocate( fr, gf, cf)
#ifdef USEOMP
!!$omp end parallel
#endif
          !write(*,*) ias, l1, l2
          !call plotmat( cmplx( radint, 0.d0, 8))
          !write(*,*)

          return
      end subroutine rmat_genri
      
      subroutine rmat_genrmat( ik, evec, rmat)
          integer, intent( in) :: ik
          complex(8), intent( in) :: evec( nmatmax_ptr, rmat_fst1:rmat_lst1)
          complex(8), intent( out) :: rmat( rmat_fst1:rmat_lst1, rmat_fst2:rmat_lst2, 3)

          integer :: ngp, i, is, ia, ias
          real(8) :: veckl(3), veckc(3), vr(3), vl(3)
          integer :: g(3), gs(3), igk, igq, ig, ivg_(3,ngrtot)
          integer :: l1, l2, l3, m1, m2, m3, igv(3,2)

          integer, allocatable :: igpig(:) 
          real(8), allocatable :: vgpql(:,:), vgpqc(:,:), gkqc(:), tpgkqc(:,:)
          complex(8), allocatable :: sfacgkq(:,:), apwalm(:,:,:,:)
          complex(8), allocatable :: auxmat(:,:), auxvec(:), evecmt1(:,:,:), evecmt2(:,:,:), cfunmat(:,:,:)

          complex(8) :: zdotc

          rmat = zzero

          veckl = rmat_kset%vkl( :, ik)
          veckc = rmat_kset%vkc( :, ik)

          !--------------------------------------!
          !      muffin-tin matrix elements      !
          !--------------------------------------!

          allocate( igpig( ngkmax_ptr), vgpql( 3, ngkmax_ptr), vgpqc( 3, ngkmax_ptr), gkqc( ngkmax_ptr), tpgkqc( 2, ngkmax_ptr))

          call timesec( t0)
          ! generate the G+k-vectors
          call gengpvec( veckl, veckc, ngp, igpig, vgpql, vgpqc, gkqc, tpgkqc)
          call timesec( t1)
          tgk = tgk + t1 - t0
          allocate( evecmt1( nlmlammax, rmat_fst1:rmat_lst1, natmtot))
          allocate( evecmt2( nlmlammax, rmat_fst2:rmat_lst2, natmtot))
          allocate( auxmat( rmat_fst1:rmat_lst1, nlmlammax))
          call timesec( t0)
          call rmat_getevecmt( veckl, rmat_fst1, rmat_lst1, evecmt1)
          call rmat_getevecmt( veckl, rmat_fst2, rmat_lst2, evecmt2)
          call timesec( t1)
          tio = tio + t1 - t0

          do is = 1, nspecies
            do ia = 1, natoms( is)
              ias = idxas( ia, is)
              do ig = 1, 3
                call zgemm( 'c', 'n', rmat_nst1, nlmlam( is), nlmlam( is), zone, &
                       evecmt1( :, :, ias), nlmlammax, &
                       rignt( :, :, ig, ias), nlmlammax, zzero, &
                       auxmat, rmat_nst1)
                call zgemm( 'n', 'n', rmat_nst1, rmat_nst2, nlmlam( is), zone, &
                       auxmat, rmat_nst1, &
                       evecmt2( :, :, ias), nlmlammax, zone, &
                       rmat( :, :, ig), rmat_nst1)
              end do
            end do
          end do
          call timesec( t0)
          tmt = tmt + t0 - t1
          deallocate( evecmt1, evecmt2, auxmat)
          
          return
          rmat = zzero
          !--------------------------------------!
          !     interstitial matrix elements     !
          !--------------------------------------!

          igv(:,1) = rmat_fftmap%ngrid/2 - rmat_fftmap%ngrid + 1
          igv(:,2) = rmat_fftmap%ngrid/2
          allocate( auxvec( rmat_fftmap%ngrtot+1))
          allocate( auxmat( rmat_fst1:rmat_lst1, ngp))
          allocate( cfunmat( ngp, ngp, 3))
          cfunmat = zzero
          do is = 1, 3
            do igk = 1, ngp
              do igq = 1, ngp
                g = ivg( :, igpig( igk)) - ivg( :, igpig( igq))
                if( (g(1) .ge. igv(1,1)) .and. (g(1) .le. igv(1,2)) .and. &
                    (g(2) .ge. igv(2,1)) .and. (g(2) .le. igv(2,2)) .and. &
                    (g(3) .ge. igv(3,1)) .and. (g(3) .le. igv(3,2))) then
                  cfunmat( igk, igq, is) = rmat_cfunir( fftigidx( g(1), g(2), g(3)), is)
                end if
              end do
            end do
            call zgemm( 'c', 'n', rmat_nst1, ngp, ngp, zone, &
                   evec, nmatmax_ptr, &
                   cfunmat( :, :, is), ngp, zzero, &
                   auxmat, rmat_nst1)
            call zgemm( 'n', 'n', rmat_nst1, rmat_nst2, ngp, zone, &
                   auxmat, rmat_nst1, &
                   evec, nmatmax_ptr, zone, &
                   rmat( :, :, is), rmat_nst1)
          end do
 
!          allocate( cfunmat( ngp, ngp, 3))
!          allocate( auxmat( rmat_fst1:rmat_lst1, ngp))
!          ivg_ = ivg
!          cfunmat = zzero
!          call timesec( t0)
!#ifdef USEOMP
!!$omp parallel default( shared) private( igk, igq, g)
!!$omp do collapse( 2)
!#endif
!          do igk = 1, ngp
!            do igq = 1, ngp
!              g = ivg_( :, igpig( igk)) - ivg_( :, igpig( igq))
!              if( (g(1) .ge. intgv(1,1)) .and. (g(1) .le. intgv(1,2)) .and. &
!                  (g(2) .ge. intgv(2,1)) .and. (g(2) .le. intgv(2,2)) .and. &
!                  (g(3) .ge. intgv(3,1)) .and. (g(3) .le. intgv(3,2))) then
!                cfunmat( igk, igq, :) = rmat_cfunir( ivgig( g(1), g(2), g(3)), :)
!              end if
!            end do
!          end do
!#ifdef USEOMP
!!$omp end do
!!$omp end parallel
!#endif
!          do ig = 1, 3
!            call timesec( t1)
!            tgg = tgg + t1 - t0
!
!            call zgemm( 'c', 'n', rmat_nst1, ngp, ngp, zone, &
!                   evec, nmatmax_ptr, &
!                   cfunmat, ngp, zzero, &
!                   auxmat, rmat_nst1)
!            call zgemm( 'n', 'n', rmat_nst1, rmat_nst2, ngp, zone, &
!                   auxmat, rmat_nst1, &
!                   evec, nmatmax_ptr, zone, &
!                   rmat( :, :, ig), rmat_nst1)
!            call timesec( t0)
!            tir = tir + t0 - t1
!          end do
!
!          deallocate( auxmat, cfunmat, igpig)

          return
      end subroutine rmat_genrmat

      subroutine rmat_putevecmt( vpl, fst, lst, evecmt)
          use m_getunit
          real(8), intent( in)    :: vpl(3)
          integer, intent( in)    :: fst, lst
          complex(8), intent( in) :: evecmt( nlmlammax, fst:lst, natmtot)

          integer :: ik, un, recl

          call findkptinset( vpl, rmat_kset, un, ik)

          inquire( iolength=recl) rmat_kset%vkl( :, ik), nlmlammax, fst, lst, evecmt
          call getunit( un)
          open( un, file=trim( rmat_fname)//'MT'//trim( filext), action='write', form='unformatted', access='direct', recl=recl)
          write( un, rec=ik) rmat_kset%vkl( :, ik), nlmlammax, fst, lst, evecmt
          close( un)
          !if( ik .eq. 12) then
          !  write(*,'(3f13.6,3i)') rmat_kset%vkl( :, ik), nlmlammax, fst, lst
          !  call plotmat( evecmt( :, :, 3))
          !  write(*,*)
          !end if

          return
      end subroutine rmat_putevecmt

      subroutine rmat_getevecmt( vpl, fst, lst, evecmt)
          use m_getunit
          real(8), intent( in)     :: vpl(3)
          integer, intent( in)     :: fst, lst
          complex(8), intent( out) :: evecmt( nlmlammax, fst:lst, natmtot)

          integer :: ik, un, recl, nlmlammax_, fst_, lst_
          real(8) :: vpl_(3)
          logical :: exist

          complex(8), allocatable :: tmp(:,:,:)

          call findkptinset( vpl, rmat_kset, un, ik)

          inquire( file=trim( rmat_fname)//'MT'//trim( filext), exist=exist)
          if( exist) then
            inquire( iolength=recl) vpl_, nlmlammax_, fst_, lst_
            call getunit( un)
            open( un, file=trim( rmat_fname)//'MT'//trim( filext), action='read', form='unformatted', access='direct', recl=recl)
            read( un, rec=1) vpl_, nlmlammax_, fst_, lst_
            close( un)
            if( nlmlammax_ .ne. nlmlammax) then
              write(*,*)
              write(*,'("Error (rmat_geteveshort): Different number of basis functions:")')
              write(*,'(" current:",i8)') nlmlammax
              write(*,'(" file   :",i8)') nlmlammax_
              stop
            end if
            if( (fst .lt. fst_) .or. (lst .gt. lst_)) then
              write(*,*)
              write(*,'("Error (rmat_geteveshort): Band indices out of range:")')
              write(*,'(" current:",2i8)') fst, lst
              write(*,'(" file   :",2i8)') fst_, lst_
              stop
            end if
            allocate( tmp( nlmlammax, fst_:lst_, natmtot))
            inquire( iolength=recl) vpl_, nlmlammax_, fst_, lst_, tmp
            call getunit( un)
            open( un, file=trim( rmat_fname)//'MT'//trim( filext), action='read', form='unformatted', access='direct', recl=recl)
            read( un, rec=ik) vpl_, nlmlammax_, fst_, lst_, tmp
            close( un)
            evecmt = tmp( :, fst:lst, :)
            deallocate( tmp)
            !if( ik .eq. 12) then
            !  write(*,'(3f13.6,3i)') vpl_, nlmlammax_, fst_, lst_
            !  call plotmat( evecmt( :, :, 3))
            !  write(*,*)
            !end if
          else
            write(*,*)
            write(*,'("Error (rmat_getevecmt): File does not exist:",a)') trim( rmat_fname)//'MT'//trim( filext)
            stop
          end if

          return
      end subroutine rmat_getevecmt

      subroutine rmat_destroy
          call timesec( te)
          ttot = te - ts
          if( mpiglobal%rank .eq. 0) then
            write(*,'("          PWMAT TIMING          ")')
            write(*,'("================================")')
            write(*,'("initialization   : ",f13.6)') tinit
            write(*,'("preparation      : ",f13.6)') tprep
            write(*,'("radial integrals : ",f13.6)') tri
            write(*,'("gaunt integrals  : ",f13.6)') trignt
            write(*,'("G+k vectors      : ",f13.6)') tgk
            write(*,'("muffin-tin part  : ",f13.6)') tmt
            write(*,'("char. fun. GG    : ",f13.6)') tgg
            write(*,'("interstitial part: ",f13.6)') tir
            write(*,'("I/O              : ",f13.6)') tio
            write(*,'("--------------------------------")')
            write(*,'("sum              : ",f13.6)') tinit + tri + trignt + tmt + tgk + tprep + tgg + tir + tio
            write(*,'("rest             : ",f13.6)') ttot - (tinit + tri + trignt + tmt + tgk + tprep + tgg + tir + tio)
            write(*,'("--------------------------------")')
            write(*,'("total            : ",f13.6)') ttot
            write(*,'("================================")')
          end if

          if( allocated( nlam)) deallocate( nlam)
          if( allocated( nlmlam)) deallocate( nlmlam)
          if( allocated( lam2apwlo)) deallocate( lam2apwlo)
          if( allocated( idxlmlam)) deallocate( idxlmlam)
          if( allocated( lm2l)) deallocate( lm2l)
          if( allocated( rignt)) deallocate( rignt)
          
          return
      end subroutine rmat_destroy
end module mod_rmat
