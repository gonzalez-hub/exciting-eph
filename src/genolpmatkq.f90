subroutine genolpmatkq( kset, qset, Gkset, Gqset, Gset, ik, iq, olp)
  use modinput
  use mod_kpointset
  use mod_Gkvector,    only: ngkmax, ngkmax_ptr
  use mod_Gvector,     only: cfunig, gc, ngvec, ngrid
  use mod_constants,   only: zzero, zone, twopi
  use mod_atoms,       only: nspecies, natoms, natmtot, idxas
  use mod_muffin_tin,  only: idxlm
  use mod_APW_LO
  use mod_eigensystem, only: idxlo, oalo, ololo
  use modxs,           only: fftmap_type
  implicit none

  type( k_set), intent( in)  :: kset, qset
  type( Gk_set), intent( in) :: Gkset, Gqset
  type( G_set), intent( in)  :: Gset
  integer, intent( in)       :: ik, iq
  complex(8), intent( out)   :: olp( Gkset%ngkmax+nlotot, Gqset%ngkmax+nlotot)

  integer :: l, m, lm1, lm2, o, ia, is, ias, ilo1, ilo2, ig, jg, igk, igq, vg(3), iv(3)
  integer :: ngkmax_, lmax, lmmax, ngpk, ngpq
  real(8) :: t1, vkql(3)
  type( fftmap_type) :: fftmap
  
  complex(8), allocatable :: sfac(:,:), apwalmk(:,:,:,:), apwalmq(:,:,:,:), apwik(:,:), apwiq(:,:), cfunkq(:)

  lmax = input%groundstate%lmaxapw
  lmmax = (lmax + 1)**2
  ngkmax_ = ngkmax_ptr
  ngkmax = max( Gkset%ngkmax, Gqset%ngkmax)
  ngpk = Gkset%ngk( 1, ik)
  ngpq = Gqset%ngk( 1, iq)

  allocate( sfac( ngkmax_ptr, natmtot))
  allocate( apwalmk( ngkmax_ptr, apwordmax, lmmax, natmtot))
  allocate( apwalmq( ngkmax_ptr, apwordmax, lmmax, natmtot))
  
  sfac = zzero
  call gensfacgp( ngpk, Gkset%vgkc( :, :, 1, ik), Gkset%ngkmax, sfac( 1:Gkset%ngkmax, :))
  call match( ngpk, Gkset%gkc( :, 1, ik), Gkset%tpgkc( :, :, 1, ik), sfac, apwalmk)
  sfac = zzero
  call gensfacgp( ngpq, Gqset%vgkc( :, :, 1, iq), Gqset%ngkmax, sfac( 1:Gqset%ngkmax, :))
  call match( ngpq, Gqset%gkc( :, 1, iq), Gqset%tpgkc( :, :, 1, iq), sfac, apwalmq)

  deallocate( sfac)
  allocate( apwik( apwordmax*lmmax, ngpk))
  allocate( apwiq( apwordmax*lmmax, ngpq))

  olp = zzero

  ! MUFFIN-TIN PART
  do is = 1, nspecies
    do ia = 1, natoms( is)
      ias = idxas( ia, is)
      ! APW-APW
      ig = 0
      apwik = zzero
      apwiq = zzero
      do l = 0, lmax
        do m = -l, l
          lm1 = idxlm( l, m)
          do o = 1, apword( l, is)
            ig = ig + 1
            apwik( ig, :) = apwalmk( 1:ngpk, o, lm1, ias)
            apwiq( ig, :) = apwalmq( 1:ngpq, o, lm1, ias)
          end do
        end do
      end do
      call zgemm( 'c', 'n', ngpk, ngpq, ig, zone, &
             apwik, apwordmax*lmmax, &
             apwiq, apwordmax*lmmax, zone, &
             olp, Gkset%ngkmax+nlotot)
      do ilo1 = 1, nlorb( is)
        ! APW-LO
        l = lorbl( ilo1, is)
        lm1 = idxlm( l, -l)
        lm2 = idxlm( l,  l)
        ig = ngpq + idxlo( lm1, ilo1, ias)
        jg = ngpq + idxlo( lm2, ilo1, ias)
        do o = 1, apword( l, is)
          olp( 1:ngpk, ig:jg) = olp( 1:ngpk, ig:jg) + conjg( apwalmk( 1:ngpk, o, lm1:lm2, ias))*oalo( o, ilo1, ias)
        end do
        ig = ngpk + idxlo( lm1, ilo1, ias)
        jg = ngpk + idxlo( lm2, ilo1, ias)
        do o = 1, apword( l, is)
          olp( ig:jg, 1:ngpq) = olp( ig:jg, 1:ngpq) + transpose( apwalmq( 1:ngpq, o, lm1:lm2, ias))*oalo( o, ilo1, ias)
        end do
        ! LO-LO
        do ilo2 = 1, nlorb( is)
          if( lorbl( ilo2, is) .eq. l) then
            ig = ngpk + idxlo( lm1, ilo1, ias)
            jg = ngpq + idxlo( lm1, ilo2, ias)
            do m = 0, 2*l
              olp( ig+m, jg+m) = olp( ig+m, jg+m) + cmplx( ololo( ilo1, ilo2, ias), 0.d0, 8)
            end do
          end if
        end do
      end do
    end do
  end do
  deallocate( apwik, apwiq)

  ! INTERSTITIAL PART
  call genfftmap( fftmap, Gset%gmaxvr)
  allocate( cfunkq( fftmap%ngrtot))
  cfunkq = zzero
  do ig = 1, ngvec
    if( gc( ig) .lt. Gset%gmaxvr) then
      cfunkq( fftmap%igfft( ig)) = cfunig( ig)
    end if
  end do
  call zfftifc( 3, fftmap%ngrid, 1, cfunkq)
  
  vkql = qset%vkl( :, iq) - kset%vkl( :, ik)
  !call r3frac( input%structure%epslat, vkql, iv)
  do ig = 1, fftmap%ngrtot
    vg = Gset%ivg( :, ig)
    t1 = twopi*dot_product( vkql, dble( vg)/dble( fftmap%ngrid)) 
    cfunkq( fftmap%igfft( ig)) = cfunkq( fftmap%igfft( ig))*cmplx( cos( t1), sin( t1), 8)
  end do
  call zfftifc( 3, fftmap%ngrid, -1, cfunkq)
    
  do igk = 1, ngpk
    ig = Gkset%igkig( igk, 1, ik)
    do igq = 1, ngpq
      jg = Gqset%igkig( igq, 1, iq)
      vg = Gset%ivg( :, ig) - Gset%ivg( :, jg)! - iv
      if( vg(1) .ge. Gset%intgv( 1, 1) .and. vg(1) .le. Gset%intgv( 1, 2) .and. &
          vg(2) .ge. Gset%intgv( 2, 1) .and. vg(2) .le. Gset%intgv( 2, 2) .and. &
          vg(3) .ge. Gset%intgv( 3, 1) .and. vg(3) .le. Gset%intgv( 3, 2)) then
        jg = Gset%ivgig( vg(1), vg(2), vg(3))
        olp( igk, igq) = olp( igk, igq) + cfunkq( fftmap%igfft( jg))
      end if
    end do
  end do
  deallocate( cfunkq)
  
  ngkmax = ngkmax_

  return
end subroutine genolpmatkq
